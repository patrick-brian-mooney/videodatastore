#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A simple plugin demonstrating how VideoDataStore plugins must be written. It
goes through all the filenames in the database and makes sure they can be
encoded as UTF-8. This is pointless on some systems, such as Windows, where
file names are already always Unicode-encoded strings. But on some OSes, e.g.
Linux, filenames are just a sequence of bytes, and need not be decipherable as
text under any Unicode encoding. This is inconvenient, because it makes it
difficult to process data about those files in ways that keep compatibility
with the kinds of assumptions that get made about text. This plug-in searches
for filenames in the database that cannot be represented as UTF-8 text.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-23 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import json

from pathlib import Path

import tkinter as tk

from typing import Iterable, Optional, Tuple, Type

import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins


class ValidateFileNamesPlugin(plugins.BasePlugin):
    plugin_name = "Validate file names"
    needs_write_access = True

    def __init__(self) -> None:
        """No special initialization needed here.
        """
        pass

    def _ask_if_rename(self, database: mh.MetadataStore,
                       name: Path) -> bool:
        """Let the user know that  NAME is not a valid name, and give them a chance to
        rename it. Returns True if the user renamed to a valid name, or False if the uer
        cancels.
        """
        new_name = gu.rename_video(old_name=name, which_database=database, parent_window=None)
        while new_name and not self._check_if_name_valid(new_name):
            gu.error_message_box(f"{new_name} is not a valid filename")
            new_name = gu.rename_video(old_name=name, which_database=database, parent_window=None)

        return bool(new_name)

    @staticmethod
    def _check_if_name_valid(name: Path) -> bool:
        """Check to see if the name is valid by trying to serialize it to JSON.
        """
        try:
            _ = json.dumps(str(name))
            return True
        except (json.JSONDecodeError,):
            return False
        except BaseException as errrr:      # really just a place for a breakpost so I can see what's going on here.
            return False

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, tk.Toplevel]]:
        """Go through each file that has information stored in the database, checking
        to see if its metadata is valid.
        """
        for i, f in enumerate(database):
            if not self._check_if_name_valid(Path(f)):
                if not self._ask_if_rename(database, Path(f)):
                    break
        return list()


__PLUGIN_CLASS__ = ValidateFileNamesPlugin


if __name__ == "__main__":
    print("Sorry, no self-test code here!")
