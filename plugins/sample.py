#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A sample plugin demonstrating how VideoDataStore plugins must be written.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-23 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import tkinter as tk

from typing import Iterable, Optional, Tuple, Type

import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins


class SamplePlugin(plugins.BasePlugin):
    plugin_name = "Sample plug-in"
    hide = True                         # must make this False to make plug-in visible in menu

    def __init__(self) -> None:
        """No special initialization needed here.
        """
        pass

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, Type[tk.Toplevel]]]:
        """Do whatever actual processing is needed here. This sample plugin just pops up
        a stupid-looking dialog.
        """
        gu.redirect_prints_to_text_box(print, f"There are {len(database)} entries in the database!")
        gu.confirm_prompt("Just letting you know: a plugin is displaying this information!")
        return list()


__PLUGIN_CLASS__ = SamplePlugin     # This variable must be defined and must refer to the plugin class.


if __name__ == "__main__":
    print("Sorry, no self-test code here!")
