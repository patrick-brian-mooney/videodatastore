#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A plug-in check URLs attached to file metadata as tags.

VideoDataStore does nto mandate any particular data ontology, so there is no
definitive answer to the question "what tags store URL information?" However,
this plug-in assumes that URLs are stored in tags beginning with a known
prefix string; I use url:SOMETHING (such as url:reddit.com) to store url info.

This plug-in prompts the user for options, including the string prefix for URL
tags, then scans through all entries in the database, collects URLs, and checks
their validity before displaying a list of valid and invalid URLs, along with
summary data.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import abc
import bisect
import collections
import csv
import datetime
import functools
import http.client
import json
import threading
import time
import urllib.parse
import urllib3
import webbrowser

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.messagebox as tk_msgbox
import tkinter.filedialog as tk_filedialog

from pathlib import Path
from typing import Iterable, List, Optional, Tuple, Type, Union


import requests                     # https://pypi.org/project/requests/


import vds.get_data as gd
import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins

import vds.utils as utils
from vds.utils import pref


@functools.lru_cache(maxsize=None)
def domain_of(url: str) -> str:
    """Makes a good-faith effort to extract just the domain name (and, possibly,
    scheme) from a string representing what may be a full URL to a specific
    document.

    This does not necessarily simply reconstruct the early parts of the namedtuple
    returned by urllib.parse.urlparse, because urlparse returns a blank domain
    (i.e., netloc) if the URL it's parsing isn't fully qualified, i.e, if it doesn't
    specify a scheme. This may be technically correct in some ways, but it doesn't
    match up well with how users do data entry in real life, where http:// is
    omitted quite frequently, which would mean that an awful lot of entered URLs
    are actually relative URLs beginning with a path component of, say,
    www.example.com, whereas a competent human reader is likely to interpret that as
    the netloc, regardless of what the specs say.

    So we use a hybrid approach here.

    Domain names are, by definition, case-insensitive, so we return all results in
    casefold case. If there is no scheme in the supplied URL, there will be no
    scheme in the returned value.
    """
    splits = urllib.parse.urlparse(url)
    if splits.scheme and splits.netloc:
        return urllib.parse.urlunparse((splits.scheme, splits.netloc, '', '', '', '')).casefold()

    # If we can't do that, carefully try to do some hand-parsing. Assume http:// is meant.
    if '://' in url:
        url = url[url.index('://')+len('://'):]

    if '/' in url:
        url = url[:url.index('/')]

    return urllib.parse.urlparse(urllib.parse.urlunparse(('http', url.strip(), '', '', '', ''))).netloc.casefold()


def domains_only(urls: Iterable[str]) -> List[str]:
    """Return just the domain names present in the URLs provided in the URLS list.
    """
    return list({domain_of(url) for url in urls})


class URLResultsWindow(tk.Toplevel):
    """A window that displays the results of a URL validation process.
    """
    def __init__(self, parent: Optional[Type[tk.Widget]],
                 database: 'MetadataStore',
                 plugin: 'URLValidatorPlugin',
                 exact_rename: bool,
                 *pargs, **kwargs) -> None:
        self.db = database
        self.parent = parent
        self.plugin = plugin
        self.exact_rename = exact_rename
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.title("URL Validation Results")

        # create subordinate widgets
        self.status_bar = gu.StatusBar(parent=self, initial_messages=['Validating ...',])
        self.status_bar.pack(side=tk.BOTTOM, fill=tk.X)

        self.results_pane = URLResultsWindow.ResultsDataDisplay(self)
        self.results_pane.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.BOTH)

    @staticmethod
    def informative_http_status(result: Union[int, Type[BaseException]]) -> str:
        """Given either an integral HTTP status code or an error received while trying to
        get one, produce a suitable-for-display string version to put into a status
        display.
        """
        if isinstance(result, int):
            try:
                return f'{result} ({http.client.responses[result]})'
            except KeyError:
                return f'{result} (unknown response code)'
        else:
            return str(result)

    def add_result_to_pane(self, which_pane: Type['ResultsPane'],
                           url: str,
                           result: Union[int, Type[BaseException]]) -> None:
        """Add a URL to the list of URLs that did (or did not) test successfully. Also
        add its corresponding HTTP status code to the pane or, if we were unable to
        successfully download HTTP headers, a textual description of the problem we
        encountered when trying to do so.

        # FIXME! We should be inserting in sorted order.
        """
        which_pane.url_list.insert('', tk.END, iid=url, text=url, values=(url, self.informative_http_status(result)))
        which_pane.update_summary()
        self.update_idletasks()

    class NoColumnHeaderPopupsTreeView(gu.FlexibleTreeview):
        def __init__(self, master=None, *pargs, **kwargs):
            gu.FlexibleTreeview.__init__(self, master=master, *pargs, **kwargs)
            self.bind("<Double-1>", self.do_double_click)

        def do_double_click(self, event) -> None:
            """Handle a double click on the content of the list by opening the clicked URL(s)
            in a web browser.
            """
            if self.identify("region", event.x, event.y) in ('tree', 'cell'):
                for url in self.selection():
                    webbrowser.open(url)

        def do_select_items_with_tag(self) -> None:
            """Find all items in the database that have a specified tag and select them in the
            main window's items list, unselecting any items that are already selected.

            #FIXME: This is robbed more or less verbatim from a similarly named method in
            gui.TagsTreeView. Maybe we should refactor NoColumnHeaderPopupsTreeView to be a
            subclass of TagsTreeView to avoid duplicating code?
            """
            tags, new_sel_files = {self.master.master.master.plugin.prefix + tag for tag in self.selection()}, set()
            for t in tags:
                tagged_files = set(str(p) for p in self._root().displayed_database.get_files_with_tag(t))
                vis_files = set(self._root().video_list.scroller.the_list.get_children())
                vis_tagged_files = tagged_files & vis_files
                new_sel_files |= vis_tagged_files
            if new_sel_files:
                self._root().video_list.scroller.the_list.selection_set(*new_sel_files)
                self._root().video_list.scroller.the_list.see(list(new_sel_files)[0])

        def do_rename_tag(self) -> None:
            if self.master.master.master.exact_rename:
                self.master.do_exact_rename_tag()
            else:
                self.master.do_rename_tags_from_stem()

        def make_popups(self) -> None:
            """Make the popup menu that's held in reserve for right-clicks.
            """
            self.column_body_popup = tk.Menu(self, tearoff=False)
            self.column_body_popup.add_command(label="Select files with this tag in main window",
                                               command=self.do_select_items_with_tag)
            self.column_body_popup.add_separator()
            self.column_body_popup.add_command(label="Rename tag ...", command=self.do_rename_tag)
            self.column_body_popup.add_command(label="Delete tag ...", command=self.master.do_delete_tag)
            self.column_body_popup.add_separator()
            self.column_body_popup.add_command(label="Export list ...", command=self.master.do_export_list)
            gu.right_click_bind(self, self.popup)
            # No need to inherit superclass method: it only handles column heading clicks, and we don't have any!

        def popup(self, event: tk.Event):
            """Pop up the popup menu.
            """
            region = self.identify("region", event.x, event.y)
            if region == "heading":
                gu.FlexibleTreeview.popup(self, event)  # headings are handled by the superclass!
            else:
                self.column_body_popup.post(event.x_root, event.y_root)
                self.column_body_popup.focus_set()

        def rebuild_column_heading_popup_menu(self) -> None:
            """Again, do nothing here.
            """
            pass

    class ResultsPane(ttk.LabelFrame, abc.ABC):
        """A pane for displaying, and dealing with, URLs that are found not to be valid.
        """
        allowed_export_types = [('JSON', '*.json'), ('plain text', '*.txt'), ('HTML', '*.html'), ('CSV', '*.csv')]

        def __init__(self, parent: Type[tk.Misc],
                     *pargs, **kwargs):
            heading = self.validity_text + " URLs:"
            self.parent = parent
            ttk.LabelFrame.__init__(self, parent, *pargs, text=heading, **kwargs)

            # create subordinate widgets
            self.vscrollbar = ttk.Scrollbar(self)
            self.vscrollbar.grid(column=1, row=0, rowspan=4, sticky=(tk.W + tk.S + tk.E + tk.N))

            self.url_list = URLResultsWindow.NoColumnHeaderPopupsTreeView(self, yscrollcommand=self.vscrollbar.set)
            self.vscrollbar.config(command=self.url_list.yview)

            columns = ('URL', 'status')
            self.url_list['show'] = 'headings'
            self.url_list['columns'] = columns
            self.url_list['displaycolumns'] = columns
            self.url_list.configure(columns=columns)
            self.url_list.column(columns[0], anchor='w', stretch=True)
            self.url_list.heading(columns[0], text=columns[0], command=lambda: self.url_list.sort_by_column(columns[0]))

            if len(columns) > 1:
                self.url_list.column(columns[1], anchor='e', stretch=True)
                self.url_list.heading(columns[1], text=columns[1], command=lambda: self.url_list.sort_by_column(columns[1]))

            self.url_list.grid(column=0, row=0, rowspan=4, sticky=(tk.W + tk.S + tk.E + tk.N))

            ren = self.do_exact_rename_tag if self.parent.parent.exact_rename else self.do_rename_tags_from_stem
            ttk.Button(self, text='Change tagged URL', command=ren).grid(row=1, column=2)
            ttk.Button(self, text='Delete tagged URL', command=self.do_delete_tag).grid(row=2, column=2)

            self.summary_label = ttk.Label(self, text="working ...")
            self.summary_label.config(font=pref('statusbar-font'))
            self.summary_label.grid(row=4, column=0, columnspan=2, sticky=(tk.W + tk.E))

            self.rowconfigure(0, weight=1)
            self.rowconfigure(3, weight=1)
            self.columnconfigure(0, weight=1)

        @abc.abstractmethod
        def update_summary(self) -> None:
            """Each subclass is responsible for implementing this routine to update the
            summary label after changes are made to the list.
            """

        def do_rename_tags_from_stem(self) -> None:
            """Perform a (non-exact) rename of a tag. This means that any tags anywhere in the
            database that have tags matching the text of the old tag, either in whole or in
            part, will have those tags changed such that the old tag is replaced with user-
            specified new text.

            Practically speaking, this is currently (19 May 2024) only used when the user
            has chosen to test only domain names, not full URLs; so the upshots is that only
            domain names are being replaced, but the later bits of the URL should remain the
            same.

            When full URLs are being tested, do_exact_rename_tag is bound to the relevant
            UI button instead of this function.
            """
            num_sel_tags = len(self.url_list.selection())
            changed_files = set()
            database = self.parent.parent.db

            for url in self.url_list.selection():
                old_tag = f"{self.parent.parent.plugin.prefix.strip(':')}:{url}"

                resp = gd.get_data_from_user(data_needed={
                    f'Change tags with': {'initial': old_tag, 'kind': 'text',
                                          'tooltip':
                                              'The tag containing the URL you would like to replace.'},
                    'new tag': {'initial': old_tag, 'kind': str,
                                'tooltip':
                                    'A new tag containing the new URL you would like to replace the old URL with.'},
                    '': {'initial': 'existing tags are always matched case-insensitively', 'kind': 'text'},
                }, window_title="Change URL in tags")

                if not resp:
                    if num_sel_tags == 1:   # User canceled the single rename operation?
                        return                  # We're done!

                    if "yes" == tk_msgbox.askquestion(message="Cancel the entire rest of the renaming operation? "
                                                              "('No' just skips this single URL.)"):
                        return
                    else:
                        continue

                # FIXME! We should really hoist this into the MetadataStore class; gu.rename_tag is very similar.
                rename_to = resp['new tag'].strip()

                for to_rename in (t.strip().casefold() for t in database.get_tags_starting_with(old_tag)):
                    if to_rename == rename_to.strip().casefold():   # don't loop endlessly!
                        continue                                    # just skip non-change changes.
                    for f in database.get_files_with_tag(to_rename):
                        while to_rename in {s.strip().casefold() for s in database.get_tags(f)}:
                            f_tags = database.get_tags(f)
                            comp_tags = [t.strip().casefold() for t in f_tags]
                            where = comp_tags.index(to_rename)
                            cur_tag = f_tags[where]
                            loc_in_tag = cur_tag.casefold().index(to_rename.strip().casefold())
                            new_tag = cur_tag[:loc_in_tag] + rename_to + cur_tag[loc_in_tag + len(to_rename):]
                            database[f]['tags'][where] = new_tag.strip()
                            database.set_tags(f, collections.OrderedDict.fromkeys(database[f]['tags']).keys())
                            changed_files.add(f)
                self.url_list.delete(url)

            for f in changed_files:
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_tags_change', which_file=Path(f))

            self.parent.parent.status_bar.set_default_text()
            self.update_summary()
            database.write_data()

        def do_exact_rename_tag(self) -> None:
            """For each item in the current selection, change it (exactly, and not fuzzily, it)
            to another tag, for which the function prompts the user. Unlike
            do_rename_tags_from_stem(), above, this does exact matching for the tag(s) being
            replaced, not fuzzy matching based on the beginning of the tag string. (However,
            this "exact" matching still strips whitespace for a comparison that it does in
            casefold case).
            """
            num_sel_tags = len(self.url_list.selection())
            changed_files = set()
            database = self.parent.parent.db

            for url in self.url_list.selection():
                old_tag = f"{self.parent.parent.plugin.prefix.strip(':')}:{url}"

                resp = gd.get_data_from_user(data_needed={
                    'Change tag': {'initial': old_tag, 'kind': 'text',
                                   'tooltip':
                                       'The tag containing the URL you would like to change.'},
                    'to': {'initial': old_tag, 'kind': str,
                           'tooltip':
                               'A new tag containing the new URL you would like associated with the files currently '
                               'tagged with the URL you are changing.'},
                    '': {'initial': 'existing tags are always matched case-insensitively', 'kind': 'text'},
                }, window_title="Change URL in tags")

                if not resp:
                    if num_sel_tags == 1:  # User canceled the single rename operation?
                        return  # We're done!

                    if "yes" == tk_msgbox.askquestion(message="Cancel the entire rest of the renaming operation? "
                                                              "('No' just skips this single URL.)"):
                        return
                    else:
                        continue

                to_rename = resp['Change tag'].strip().casefold()
                rename_to = resp['to'].strip()

                for f in database.get_files_with_tag(to_rename):
                    while to_rename in {s.strip().casefold() for s in database.get_tags(f)}:
                        f_tags = database.get_tags(f)
                        comp_tags = [t.strip().casefold() for t in f_tags]
                        where = comp_tags.index(to_rename)
                        database[f]['tags'][where] = rename_to
                        database.set_tags(f, collections.OrderedDict.fromkeys(database[f]['tags']).keys())
                        changed_files.add(f)
                self.url_list.delete(url)

            for f in changed_files:
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_tags_change', which_file=Path(f))

            self.parent.parent.status_bar.set_default_text()
            self.update_summary()
            database.write_data()

        def do_delete_tag(self) -> None:
            """For each tag selected in the URL list, confirm that the user really wants to
            delete it from all files in the database, then do so.
            """
            num_sel_tags = len(self.url_list.selection())

            for url in self.url_list.selection():
                tag_to_del = self.parent.parent.plugin.prefix + url
                if "no" == tk_msgbox.askquestion(message=f"Really delete the tag {tag_to_del} from all files in the database?"):
                    if num_sel_tags == 1:   # only one URL was selected, and user chose not to delete it
                        return

                    if "yes" == tk_msgbox.askquestion(message=f"Stop deleting all tags? ('No' just skips this single tag.)"):
                        return
                    else:
                        continue

                self.parent.parent.db.delete_tag_from_all(tag_to_del, suppress_write=True)
                self.parent.parent.status_bar.set_text(f"Deleted tag {tag_to_del} from all files.", immediate=True)
            self.update_summary()
            self.parent.parent.db.write_data()

        def do_export_list(self) -> None:
            out_file = tk_filedialog.asksaveasfilename(filetypes=self.allowed_export_types, title="Export to ...",
                                                       initialfile=f'{self.validity_text} results',
                                                       parent=self.parent.parent)
            if not out_file:
                self.parent.parent.status_bar.set_text("Export canceled.", immediate=True)
                return
            else:
                out_file = Path(out_file)

            if out_file.suffix.strip().casefold() == '.json':
                self.do_export_json(out_file)
            elif out_file.suffix.strip().casefold() == '.txt':
                self.do_export_text(out_file)
            elif out_file.suffix.strip().casefold() == '.html':
                self.do_export_html(out_file)
            elif out_file.suffix.strip().casefold() == '.csv':
                self.do_export_csv(out_file)
            else:
                gu.error_message_box(f"Could not determine file type from extension! Extension must be one of: {self.allowed_export_types}.")

        def do_export_json(self, which_file: Path):
            data = dict()
            for url in self.url_list.get_children():
                item = self.url_list.set(url)
                data[url] = item['status']
            with open(which_file, mode='w', encoding='utf-8') as json_file:
                json.dump(data, json_file, indent=2, default=str)

        def do_export_text(self, which_file: Path):
            with open(which_file, mode='w', encoding='utf-8') as text_file:
                text_file.write(f"{self.validity_text} URLs\n(Report generated on {datetime.datetime.now().isoformat(' ')}\n\n")
                for url in self.url_list.get_children():
                    data = self.url_list.set(url)
                    text_file.write(f"{url}:\t\t{data['status']}\n")

        def do_export_html(self, which_file: Path):
            with open(which_file, mode='w', encoding='utf-8') as html_file:
                html_file.write(f'<!doctype html>\n<head>\n<title>{self.validity_text} URLs</title>\n')
                html_file.write(f'  <meta name="generator" content="{utils.__progname__} ({utils.__version__})" />\n')
                html_file.write(f"""  <meta name="date" content="{datetime.datetime.now().isoformat('T')}" />\n""")
                html_file.write("</head>\n<body>\n<ul>\n")
                for url in self.url_list.get_children():
                    data = self.url_list.set(url)
                    html_file.write(f"""  <li><a href="{url}">{url}</a>: {data['status']}</li>\n""")
                html_file.write('</ul>\n</body>\n')

        def do_export_csv(self, which_file: Path):
            with open(which_file, 'w', newline='') as csvfile:
                writer = csv.DictWriter(csvfile, fieldnames=['url', 'status'])
                writer.writeheader()
                for url in self.url_list.get_children():
                    data = self.url_list.set(url)
                    writer.writerow({'url': url, 'status': data['status']})

    class InvalidResultsPane(ResultsPane):
        validity_text = "Invalid"

        def __init__(self, parent, *pargs, **kwargs):
            URLResultsWindow.ResultsPane.__init__(self, parent, *pargs, **kwargs)

        def update_summary(self) -> None:
            """Update the summary information displayed in the summary label.

            #FIXME: also display most common error!
            """
            num_urls = len(self.url_list.get_children())
            self.summary_label['text'] = f"{num_urls} invalid URL{'' if num_urls == 1 else 's'}"
            self.update_idletasks()

    class ValidResultsPane(ResultsPane):
        validity_text = "Valid"

        def __init__(self, parent, *pargs, **kwargs):
            URLResultsWindow.ResultsPane.__init__(self, parent, *pargs, **kwargs)

        def update_summary(self) -> None:
            """Update the summary information displayed in the summary label.
            """
            num_urls = len(self.url_list.get_children())
            self.summary_label['text'] = f"{num_urls} valid URL{'' if num_urls == 1 else 's'}"
            self.update_idletasks()

    class ResultsDataDisplay(ttk.Frame):
        def __init__(self, parent: Union[Type[tk.Widget], Type[tk.Toplevel], Type[tk.Tk]], *pargs, **kwargs):
            ttk.Frame.__init__(self, parent, *pargs, **kwargs)
            self.parent = parent

            # create subordinate widgets
            self.valid_results = URLResultsWindow.ValidResultsPane(self)
            self.valid_results.grid(row=0, column=0, sticky=(tk.W + tk.S + tk.E + tk.N))

            self.invalid_results = URLResultsWindow.InvalidResultsPane(self)
            self.invalid_results.grid(row=1, column=0, sticky=(tk.W + tk.S + tk.E + tk.N))

            self.rowconfigure(0, weight=1)
            self.rowconfigure(1, weight=1)
            self.columnconfigure(0, weight=1)


class URLValidatorPlugin(plugins.BasePlugin):
    plugin_name = "URL Validator"
    hide = False

    # This plugin intentionally makes changes to the database! & needs resources provided by the main window
    needs_write_access = True

    # default values that the user can override in the dialog
    max_threads = 1
    same_domain_delay = 5       # in seconds
    request_timeout = 3         # in seconds
    allow_redirects = True
    prefix = 'url:'

    urls_to_check, valid_urls, invalid_urls = None, None, None
    last_check_time, workers, root_window = None, None, None

    url_check_lock, valid_urls_lock, invalid_urls_lock = threading.Lock(), threading.Lock(), threading.Lock()
    last_check_lock, print_lock = threading.Lock(), threading.Lock()

    def __init__(self) -> None:
        """Set up basic parameters for the plugin.

        REMINDER: the plugin is created (and __init__ is called) at PROGRAM LAUNCH TIME,
        not right before the plugin runs!
        """
        plugins.BasePlugin.__init__(self)

    def display_message(self, message: str, *pargs, **kwargs) -> None:
        """Convenience function. Queues this on the TaskScheduler task list so it can
        be safely called from a thread other than the main GUI thread.
        """
        if self.root_window:
            self.root_window.scheduler.queue_task(self.root_window.set_window_status, message, *pargs, **kwargs)
        else:
            with self.print_lock:
                print(message)

    class Worker(threading.Thread):
        """An individual worker thread that pops URLs off of self.urls_to_check when
        it's time to check them, then checks them and stashes the results.
        """
        def __init__(self, plugin: 'URLValidatorPlugin',
                     results_window: URLResultsWindow,
                     *pargs, **kwargs):
            """Just dispatch to the superclass.
            """
            self.plugin = plugin
            self.results_window = results_window
            self.scheduler = self.plugin.root_window.scheduler

            threading.Thread.__init__(self, *pargs, **kwargs)

        def check_url(self, url: str) -> None:
            """Check to see whether the address given by URL results in an HTTP 200 status
            (or equivalent), and stuff data about the results into the relevant attributes
            of SELF. Makes no effort to check whether it's "appropriate" (based on time) to
            check this URL right now; that needs to be verified before this method is
            called.

            This plugin both accumulates results (largely for ease of debugging, though we
            may later choose to pop up details after all results are in; also, so the status
            bar summary can be accurately updated) and sends updates to the ResultsWindow.
            Because the results being sent to the ResultsWindow come from threads other than
            the main GUI thread, the call is indirectly made by scheduling it on the
            MainWindow's TaskScheduler object, to run when the main GUI thread is idle.
            """
            try:
                try:
                    try:
                        header = requests.head(url, timeout=self.plugin.request_timeout,
                                               allow_redirects=self.plugin.allow_redirects)
                    except requests.exceptions.MissingSchema:
                        header = requests.head('http://' + url, timeout=self.plugin.request_timeout,
                                               allow_redirects=self.plugin.allow_redirects)
                except (requests.exceptions.RequestException, urllib3.exceptions.LocationParseError) as errrr:
                    with self.plugin.invalid_urls_lock:
                        self.plugin.invalid_urls[url] = errrr
                    self.scheduler.queue_task(self.results_window.add_result_to_pane,
                                              self.results_window.results_pane.invalid_results, url, errrr)
                    return

                when = time.monotonic()

                if header.status_code == 200:
                    self.plugin.valid_urls.append(url)  # No Lock needed with a deque.
                    self.scheduler.queue_task(self.results_window.add_result_to_pane,
                                              self.results_window.results_pane.valid_results, url, header.status_code)
                else:
                    with self.plugin.invalid_urls_lock:
                        self.plugin.invalid_urls[url] = header
                    self.scheduler.queue_task(self.results_window.add_result_to_pane,
                                              self.results_window.results_pane.invalid_results, url, header.status_code)

                domain = domain_of(url)
                with self.plugin.last_check_lock:
                    self.plugin.last_check_time[domain] = when

            finally:
                # No need to gatekeep with Locks here -- len(collections.deque) is thread-safe:
                # https://stackoverflow.com/a/34239481/5562328
                num_valid = len(self.plugin.valid_urls)
                num_invalid = len(self.plugin.invalid_urls)
                num_to_check = len(self.plugin.urls_to_check)

                total_urls = num_valid + num_to_check + num_invalid
                half_percent = round(total_urls * 0.005)

                if ((num_valid + num_invalid) % half_percent == 0) or ((total_urls - (num_valid + num_invalid)) <= 10):
                    self.plugin.root_window.scheduler.queue_task(self.results_window.status_bar.set_text,
                                                                 f"{num_valid} valid URLs, {num_invalid} invalid; {num_to_check} remaining.",
                                                                 immediate=True)

        def run(self, verbose: bool = True) -> None:
            """Each thread runs until there's nothing left in self.urls_to_check that
            needs checking.
            """
            while True:
                try:
                    next_url, sleep_time = None, 0

                    with self.plugin.url_check_lock:
                        if not self.plugin.urls_to_check:
                            self.plugin.root_window.scheduler.queue_task(self.results_window.status_bar.set_text,
                                                                         "All URLs checked!", immediate=True)
                            return  # all done!

                    # since we maintain this list in sorted order, we need only check the first item.
                    cur_time = time.monotonic()

                    with self.plugin.url_check_lock:
                        if self.plugin.urls_to_check[0][0] <= cur_time:
                            next_url = self.plugin.urls_to_check.pop()[1]
                        else:
                            sleep_time = self.plugin.urls_to_check[0][0] - cur_time

                    if sleep_time:
                        time.sleep(sleep_time)
                        continue

                    if verbose:
                        with self.plugin.print_lock:
                            print(f"Checking {next_url} ...")

                    domain = domain_of(next_url)
                    cur_time, earliest_time = time.monotonic(), None

                    with self.plugin.last_check_lock:
                        if domain in self.plugin.last_check_time:
                            earliest_time = self.plugin.last_check_time[domain] + self.plugin.same_domain_delay

                    if earliest_time and (cur_time <= earliest_time):
                        with self.plugin.url_check_lock:
                            bisect.insort(self.plugin.urls_to_check, (earliest_time, next_url))
                        continue

                    self.check_url(next_url)

                finally:
                    time.sleep(0.01)       # Sleep once each iteration through the loop so other threads can work.

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type['MainWindow']] = None
                      ) -> Iterable[Tuple[str, Type[tk.Toplevel]]]:
        """Do the actual work of finding and validating URLs.
        """
        if root_window is None:
            gu.error_message_box(f"You elected not to give write access to the {self.plugin_name} plug-in, but {self.plugin_name} cannot work without write access! Aborting ...")
            return list()

        self.root_window = root_window

        self.urls_to_check = collections.deque()    # of tuples: (min monotonic time when check is OK, URL to check)
        self.valid_urls = collections.deque()
        self.invalid_urls = dict()      # mapping URL -> requests.models.Response headers
        self.last_check_time = dict()   # mapping domain -> monotonic time() of last response
        self.workers = list()           # of threads

        # FIXME: should we allow configuring the UserAgent string here? What is the Requests default, anyway?
        # See: https://requests.readthedocs.io/en/latest/user/quickstart/#custom-headers
        opts = gd.get_data_from_user(data_needed={
            'tag prefix': {'kind': str, 'initial': self.prefix,
                           'tooltip':
                               'The tag prefix with which URL-containing tags begin. For instance, if URLs in your '
                               'database have the form "url:http://example.com", then your tag prefix is "url:".'},
            'only domain names': {'kind': bool, 'initial': True,
                                  'tooltip':
                                      'If checked, the plug-in will only validate that the domain names from URLs '
                                      'exist, ignoring the later parts of the URL; if unchecked, each exact URL will '
                                      'be verified.'},
            'seconds between same-domain checks': {'kind': float, 'initial': self.same_domain_delay,
                                                   'validator': gd.nonnegative_number_validator_func,
                                                   'tooltip':
                                                       'The plug-in will wait at least this long between checks made '
                                                       'to the same domain name. Leaving at least a few seconds '
                                                       'between same-domain checks is polite to server administrators '
                                                       'and helps to ensure that you are not inadvertently burdening '
                                                       'the server with too many requests at once (and that the server '
                                                       'does not think you are launching a denial-of-service attack).'
                                                       '\n\nIn between checks on a single domain, the plug-in will '
                                                       'still attempt to check any remaining URLs on different domain '
                                                       'names that need checking.\n\nThis setting is not practically '
                                                       'useful unless "only domain names" is unchecked.'},
            'timeout (seconds)': {'kind': float, 'initial': self.request_timeout,
                                  'validator': gd.nonnegative_number_validator_func,
                                  'tooltip':
                                      'The number of seconds to wait for a check to complete before giving up and '
                                      'assuming the URL is invalid.'},
            'treat redirects as valid': {'kind': bool, 'initial': self.allow_redirects,
                                         'tooltip':
                                             'If checked, URLs that redirect to another URL will be treated as valid '
                                             'as long as that other URL can be validated.'},
            'max threads': {'kind': int, 'initial': self.max_threads, 'validator': gd.nonnegative_number_validator_func,
                            'tooltip':
                                'The maximum number of simulataneous threads to use to check URLs, which specifies '
                                'the maximum number of simulataneous URL checks that can be made. Up to a point, '
                                'increasing this number will make the process of URL checking faster, but increasing '
                                'this number too far may slow down the process or leave your computer unresponsive. 4 '
                                'is probably a good starting point for most contemporary computers.'}
        }, window_title="URL validation options")

        if not opts:
            if root_window:
                root_window.set_window_status_to_canceled(True, True)
            return list()

        self.max_threads = opts['max threads']
        self.same_domain_delay = opts['seconds between same-domain checks']
        self.request_timeout = opts['timeout (seconds)']
        self.allow_redirects = opts['treat redirects as valid']

        self.prefix = opts['tag prefix']
        urls = database.get_tags_starting_with(self.prefix)
        if not urls:
            self.display_message('No tags found with specified tag prefix!', True, True)
            return list()
        else:
            urls = [u[len(self.prefix):] for u in urls]

        if opts['only domain names']:
            urls = domains_only(urls)

        cur_time = time.monotonic()
        self.urls_to_check = [(cur_time, url) for url in urls]       # at the beginning, all URLs are OK to check.

        win = URLResultsWindow(None, database=database, plugin=self,
                               exact_rename=not opts['only domain names'])

        for i in range(opts['max threads']):
            worker = self.Worker(plugin=self, results_window=win)
            self.workers.append(worker)

        for w in self.workers:
            w.start()

        # we don't need to .join() the Worker threads: that would block the main event loop. Instead, the first
        # thread that discovers there are no more URLs to check announces (on the status bar) that we're done scanning.
        # We just return the relevant data to the calling function so the main event loop can keep going, and let
        # the threads run as they get data.

        return [("URL Validator", win)]


__PLUGIN_CLASS__ = URLValidatorPlugin     # This variable must be defined and must refer to the plugin class.


if __name__ == "__main__":
    # test harness: first, do setup

    from pathlib import Path
    import vds.metadata_handling as mh

    root = tk.Tk()
    root.withdraw()

    print('\nOpening MetadataStore ...')
    db = mh.MetadataStore(Path("/media/patrick/b42ae87d-c564-4aaf-957d-23f0072ac43d/.oubliette/.metadata.dat"))
    print("   ... opened!\n\n")

    p_in = URLValidatorPlugin()

    # option 1: open a results window.
    if True:
        import sys
        win = URLResultsWindow(root, db, p_in, False)
        win.protocol("WM_DELETE_WINDOW", sys.exit)
        win.mainloop()

    # option 2: run checking code in non-threaded mode.
    else:
        p_in.root_window = None

        p_in.valid_urls = collections.deque()
        p_in.invalid_urls = dict()
        p_in.last_check_time = dict()
        p_in.workers = list()

        p_in.max_threads = 1
        p_in.same_domain_delay = 5

        prefix = "url:"
        to_check = db.get_tags_starting_with(prefix)
        to_check = [u[len(prefix):] for u in to_check]
        to_check = domains_only(to_check)

        now = time.monotonic()
        p_in.urls_to_check = collections.deque([(now, url) for url in to_check])   # at the beginning, all URLs are OK to check.

        single_worker = p_in.Worker(plugin=p_in)
        p_in.workers = [single_worker, ]

        single_worker.run()

        print('\n\nRESULTS')
        print(f'\nValid domains: {len(p_in.valid_urls)}')
        print(f'Invalid domains: {len(p_in.invalid_urls)}\n\n')
