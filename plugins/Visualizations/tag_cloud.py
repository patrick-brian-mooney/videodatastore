#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A sample plugin demonstrating how VideoDataStore plugins must be written.
This plug-in creates a tag cloud representing the used tags in a database.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-23 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import shutil
import tempfile
import uuid

import tkinter as tk
from tkinter import filedialog as filedialog

from pathlib import Path
from typing import Iterable, Optional, Tuple, Type


import vds.get_data
import vds.subwindows
import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins

from vds.utils import pref


class WordCloudVisualizer(plugins.BasePlugin):
    plugin_name = "Tag Cloud"

    def __init__(self) -> None:
        """No special initialization needed here.
        """
        for mod in ['wordcloud', 'matplotlib', 'matplotlib.pyplot', 'PIL', 'numpy']:
            if not gu.redirect_prints_to_text_box(plugins.ensure_installed, mod,
                                                  window_title='Checking requirements ...'):
                self.hide = True

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, tk.Toplevel]]:
        """Get processing parameters,
        """
        import wordcloud    # Don't import at top level of script: let rest of program run (just not this plugin) for people who don't have wordcloud installed.

        params = vds.get_data.get_data_from_user({
            'width': {'kind': int, 'validator': vds.get_data.positive_integer_validator_func,
                      'initial': pref('default-vis-width'),
                      'tooltip':
                          'Width (in pixels) of the visualization to be created.',},
            'height': {'kind': int, 'validator': vds.get_data.positive_integer_validator_func,
                       'initial': pref('default-vis-height'),
                       'tooltip':
                           'Height (in pixels) of the visualization to be created.'},
            'scale': {'kind': float, 'validator': vds.get_data.positive_number_validator_func, 'initial': 1.0,
                      'tooltip':
                          'This parameter controls how finely the algorithm tries to fit groups of words together. '
                          'Adjusting this number up and down slightly will alter the layout of the generated clouds.'},
            'save to disk': {'kind': bool, 'validator': None, 'initial': False,
                             'tooltip':
                                 'If this option is checked, VideoDataStore will prompt you for a location to save the '
                                 'generated word cloud to (and will save the image to that location). You must choose '
                                 'this option or "display in window" (or both), or else VDS will not have anything to '
                                 'do with the word cloud it generates!'},
            'display in window': {'kind': bool, 'validator': None, 'initial': True,
                                  'tooltip':
                                      'If this option is checked, VideoDataStore will create a new windows in which it '
                                      'displays the generated word cloud. You must select this option or "save to '
                                      'disk" (or both), or else VDS will not have anything to do with the generated '
                                      'word cloud!'},
            'maximum tags in cloud': {'kind': int, 'validator': vds.get_data.positive_integer_validator_func,
                                      'initial': 200,
                                      'tooltip':
                                          'The maximum number of tags to display in the generated image.'}
        })

        if not params:
            return list()

        if (not params['save to disk']) and (not params['display in window']):
            gu.error_message_box("If you do not want to display the word cloud or save it to disk, there's nothing to do!")
            return list()

        counts = database.all_tags_with_counts()
        wc = wordcloud.WordCloud(width=params['width'], height=params['height'], scale=params['scale'],
                                 max_words=params['maximum tags in cloud'])
        wc.generate_from_frequencies({phrase: float(frequency) for phrase, frequency in counts.items()})        # not sure if we NEED to convert int to float here, but the documentation says "float," so it gets floats

        if params['display in window']:
            tempdir = Path(tempfile.gettempdir())
            rendered_image_path = None
            while not rendered_image_path:  # Not totally immune to race conditions, but should make them very unlikely
                new_path = tempdir / str(Path(str(uuid.uuid4())).with_suffix('.png'))
                if not new_path.exists():
                    rendered_image_path = new_path
            wc.to_file(str(rendered_image_path))
            new_window = vds.subwindows.GraphicDisplayWindow(None, rendered_image_path, 'Tag Cloud Visualization')
        else:
            rendered_image_path, new_window = None, None

        if params['save to disk']:
            save_name = filedialog.asksaveasfilename(title=f"Save tag cloud as ...")
            if save_name:
                save_name = Path(save_name)
                if rendered_image_path:
                    shutil.copy2(rendered_image_path, save_name)
                else:
                    wc.to_file(str(save_name))

        if new_window:
            return [(str(rendered_image_path), new_window)]
        else:
            return list()


__PLUGIN_CLASS__ = WordCloudVisualizer     # This variable must be defined and must point to the plugin class.


if __name__ == "__main__":
    print("Sorry, no self-test code here!")
