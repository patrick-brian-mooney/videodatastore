#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A simple plugin demonstrating how VideoDataStore plugins must be written. It
goes through all the files in the database and makes sure their metadata can
be encoded as UTF-8. If any data that cannot be encoded as UTF-8 is found, it
pops up a metadata-editing dialog box for those files.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-23 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import json

from pathlib import Path

import tkinter as tk

from typing import Iterable, Optional, Tuple, Type

import vds.subwindows
import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins

import vds.utils as utils
from vds.utils import pref



class ValidateFileNamesPlugin(plugins.BasePlugin):
    plugin_name = "Validate file metadata"
    needs_write_access = True

    def __init__(self) -> None:
        """No special initialization needed here.
        """
        pass

    @staticmethod
    def _check_if_metadata_valid(database: mh.MetadataStore,
                                 name: Path) -> bool:
        """Check to see if the name is valid by trying to serialize it to JSON.
        """
        try:
            _ = utils.jsonify(database[name])
            return True
        except (json.JSONDecodeError,):
            return False
        except Exception as errrr:
            return False

    @staticmethod
    def _print_invalid_filenames(which_files: Iterable[Path]) -> None:
        print("The following files have metadata that cannot be serialized as Unicode:\n\n")
        for f in which_files:
            print(f"  {f}")
        print("\n\n\n")

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, Type[tk.Toplevel]]]:
        """Try encoding file metadata as JSON. Keep a list of files where this can't
        be done. If there is a ROOT_WINDOW supplied, and if there are few of these or
        if the user approves opening many, open a file-info-editing window for each.
        Otherwise, print a list of non-conforming files in a new text output window.
        """
        invalid_data_files = list()

        for i, f in enumerate(database, start=1):
            if not self._check_if_metadata_valid(database, Path(f)):
                invalid_data_files.append(Path(f))

        if (not root_window) or (len(invalid_data_files) > pref('max-windows-opened-unwarned')):
            if not gu.confirm_prompt(f"Really open {len(invalid_data_files)} windows?"):
                gu.redirect_prints_to_text_box(self._print_invalid_filenames, *invalid_data_files)
                return list()

        return [(str(p), vds.subwindows.FileInfoWindow(p, database, root_window)) for p in invalid_data_files]


__PLUGIN_CLASS__ = ValidateFileNamesPlugin


if __name__ == "__main__":
    print("Sorry, no self-test code here!")
