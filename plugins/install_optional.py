#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""A plug-in for Patrick Mooney's VideoDataStore program that attempts to
install any optional modules that other plug-ins require.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-23 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import collections
import os
import re
import tkinter as tk

from pathlib import Path
from typing import Iterable, Optional, Tuple, Type

import vds.metadata_handling as mh
import vds.plugins as plugins
import vds.utils as utils


class InstallOptionalModules(plugins.BasePlugin):
    plugin_name = "Install optional Python modules"

    exclude_from_search = ('.git', 'lib', 'lib64', '.idea', )

    STATEMENT_imp_from_as = re.compile(r'from (.*) \bimport\b (.*) as (.*)')
    STATEMENT_imp_from = re.compile(r'from (.*) \bimport\b(.*)')
    STATEMENT_imp_as = re.compile(r'\bimport\b (.*) as (.*)')
    STATEMENT_imp_simple = re.compile(r'\bimport\b (.*)')

    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, tk.Toplevel]]:
        """Go through each Python file that makes up the program, line by line, and
        find all the `import` statements. Parse them to see which modules are
        being imported, and try to load those modules. If that's not possible, try
        to install those modules.
        """
        python_files, import_lines, module_names = set(), set(), set()

        # First, find all the Python files in the project, aside from those in excluded directories.
        for dirpath, dirnames, filenames in os.walk(utils.__program_loc__):
            excluded = False
            for d in self.exclude_from_search:
                if d in dirpath.lower():
                    excluded = True
                    break
            if not excluded:
                meaningful_files = [(Path(dirpath) / f) for f in filenames if Path(f).suffix == '.py']
                if meaningful_files:
                    python_files.update(meaningful_files)

        # Now, find all lines in those files with the word "import" in them.
        for f in python_files:
            with open(f, mode='r', encoding='utf-8') as p_file:
                import_lines.update([l.strip() for l in p_file if 'import' in l])

        # Now, go through the list exactly once, splitting any lines containing semicolons, in case they contain
        # multiple imports.
        signal = object()
        import_lines = collections.deque(import_lines)
        import_lines.append(signal)
        while import_lines[0] is not signal:
            curr_line = import_lines.popleft()
            if ';' in curr_line:
                for l in [clause.strip() for clause in curr_line.split(';')]:
                    import_lines.append(l)
            else:
                import_lines.append(curr_line)

        # now we've been through once, and import_lines[0] is the signal object. Remove it, and convert back to set.
        import_lines.popleft()
        import_lines = set(import_lines)

        # now iterate over import_lines, checking for syntactical validity & finding the imported module names.
        for l in import_lines:
            for regex in (self.STATEMENT_imp_from_as, self.STATEMENT_imp_from,
                          self.STATEMENT_imp_as, self.STATEMENT_imp_simple):
                match = regex.findall(l)
                if match:
                    for data in match:              # Conveniently, module name is always the 1st element in the tuple
                        module_names.add(data[0] if isinstance(data, tuple) else data)
                    break

        for m in module_names:
            _ = plugins.ensure_installed(m)

        return list()


__PLUGIN_CLASS__ = InstallOptionalModules


if __name__ == "__main__":
    print("Sorry, no self-test code here!")
