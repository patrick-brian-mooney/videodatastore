#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""""Provides basic GUI functionality for the VideoDataStore project.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


try:
    import pyximport
    pyximport.install()           # FIXME! decide whether we actually want to do this
except ImportError:
    pass

import argparse
import bisect
import datetime
import io
import os
from pathlib import Path
import pprint
import random
import re
import shlex
import shutil
import subprocess
import sys
import textwrap
import time

from typing import Callable, Generator, Iterable, List, Literal, Optional, Sequence, Tuple, Type, Union

import tkinter as tk
from tkinter import ttk
import tkinter.filedialog as filedialog
import tkinter.simpledialog as simpledialog
import tkinter.messagebox as tk_msgbox
import tkinter.dialog as tk_dialog


import vds.ffmpeg_utils as fu
import vds.get_data as gd
import vds.gui_utils as gu
import vds.metadata_handling as mh
import vds.plugins as plugins
import vds.prefswindow as pwin
import vds.subwindows as subwindows

import vds.utils as utils
from vds.utils import pref              # Used frequently enough that the short alias is worthwhile


# Definitions of Tkinter-based classes used to construct the main window.
class MainTreeview(gu.FlexibleTreeview):
    """A FlexibleTreeview customized to be the main display of the video list.
    """
    columns = ['Name', 'Duration', 'Tags', 'Last Modified', 'File Size', 'Location']  # FIXME: mo' columns, mo' info

    @gu.status_bar_announce_bracket('Sorting data ...')
    def re_sort_list(self, new_sort: Optional[Tuple[str, bool]] = None) -> None:
        """Override this method to wrap it in statusbar update calls, since sorting a
        large database can take a while.
        """
        gu.FlexibleTreeview.re_sort_list(self, new_sort)

    @gu.status_bar_announce_bracket('Sorting data ...')
    def sort_by_column(self, col: str, reverse: bool = False) -> None:
        gu.FlexibleTreeview.sort_by_column(self, col=col, reverse=reverse)

    def delete_video(self, which_file: Path,
                     suppress_write: bool = True) -> None:
        """Delete the entry with NODE_ID as its ID from the database, permanently
        (after confirming, if we are allowed to confirm).
        """
        gu.delete_video(which_database=self.master.master.master.displayed_database, suppress_write=suppress_write,
                        parent_window=self._root(), which_file=which_file)

    @gu.trap_and_report_errors
    def handle_delete(self, suppress_write: bool = False) -> None:
        """Handle the 'delete video' command by getting the ID of the currently
        selected video and passing that off to self.delete_video().
        """
        for file_id in self._root().yield_current_selection():
            self.delete_video(file_id, suppress_write=True)
            self.master.delete_list_item(file_id)
        if not suppress_write:
            self.master.master.master.displayed_database.write_data()

    def do_view_media(self) -> None:
        """Just route the event to the parent's method.
        """
        gu.view_media(self.master.master.master.entire_current_selection())

    def do_video_info(self) -> None:
        """Pop up a "media info" window for each selected file. If there are too many files
        selected, confirm that that's what the user really wants to pop up (however
        many) windows. ("Too many" is configurable in the preferences.) If any of the
        windows are already open, bring it to the front instead of popping up a new
        copy of the same window.
        """
        if len(self._root().entire_current_selection()) > pref('max-windows-opened-unwarned'):
            if not gu.confirm_prompt(f"Really open {len(self._root().entire_current_selection())} windows?",
                                     parent=self):
                self._root().set_window_status_to_canceled()
                return
        for f in self._root().yield_current_selection():
            self._root().open_file_info_window(f)

    @gu.trap_and_report_errors
    def handle_rename(self) -> None:
        """Ask for a new name for the selected file, then rename it, keeping track of
        the file's old information under the new name.
        """
        db = self._root().displayed_database
        for old_iid in self.master.master.master.entire_current_selection():
            new_iid = gu.rename_video(old_iid, db, parent_window=self._root())
            if new_iid:     # if we didn't cancel, change the iid for the row in the list to the new filename.
                new_iid = utils.relative_to_with_name(self._root().displayed_database.file_location, new_iid)
                self.master.change_item_iid(str(old_iid), str(new_iid))

    @gu.trap_and_report_errors
    def handle_create_playlist(self) -> None:
        """Create a playlist including the selected media files. Optionally, open the
        playlist immediately in a specified application.
        """
        which_files = self._root().video_list.scroller.the_list.selection()
        if not which_files:
            self._root().set_window_status("No files selected!", True, True)
            return
        data_requested = {
            'Playlist type': {'kind': list,
                              'validator': None,
                              'initial': ['.m3u, relative', '.m3u, absolute', '.m3u8, relative', '.m3u8, absolute'],
                              'tooltip':
                                  'Playlists in .m3u8 format are encoded using Unicode\'s UTF-8 encoding scheme; '
                                  'playlists in .m3u format are encoded in your operating system\'s native encoding'
                                  '(which may be UTF-8 anyway, and often is on Linux). "Relative" playlists contain '
                                  'file paths relative to the location where the playlist is saved; "absolute" '
                                  'playlists contain file paths starting at the filesystem root (or drive letter, on'
                                  'Windows).\n\nYou may need to consult the documentation for the media player you '
                                  'want to open the created file list with, or experiment, to discover what works best '
                                  'for you.',
                              },
            'Open playlist in default player': {
                'kind': bool, 'validator': None, 'initial': False,
                'tooltip': 'If checked, the default media player will be used to open the created playlist'},
        }
        if pref('helper-apps'):
            data_requested.update({'Open playlist in helper app': {
                'kind': bool, 'validator': None, 'initial': False,
                'tooltip':
                    'If checked, open the created playlist in the helper application selected below.',
            },
                'Which helper application': {
                    'kind': list, 'validator': None, 'initial': list(pref('helper-apps').keys()),
                    'tooltip':
                        'If "Open palylist in helper app" is checked above, this program will be used to open the '
                        'playlist after it is created.',
                }}
            )

        data = gd.get_data_from_user(data_needed=data_requested,
                                     window_title=f'Create playlist for {len(which_files)} files...')
        if not data:
            self._root().set_window_status_to_canceled(True, True)
            return
        if data['Playlist type'] == '.m3u, relative':
            fmt, relative = 'm3u', True
        elif data['Playlist type'] == '.m3u, absolute':
            fmt, relative = 'm3u', False
        elif data['Playlist type'] == '.m3u8, relative':
            fmt, relative = 'm3u8', True
        elif data['Playlist type'] == '.m3u8, absolute':
            fmt, relative = 'm3u8', False
        else:
            raise RuntimeError("Impossible playlist format somehow selected!")
        if data['Open playlist in default player']:
            apps_to_open = [pref('media-viewer')]
        else:
            apps_to_open = list()
        if 'Open playlist in helper app' in data:
            if data['Open playlist in helper app']:
                helper_apps = pref('helper-apps')
                app_name = data['Which helper application']
                apps_to_open.append(str(helper_apps[app_name]))
        save_location = filedialog.asksaveasfilename(parent=self)
        if not save_location:
            self._root().set_window_status_to_canceled(True, True)
            return
        save_location = Path(save_location)
        utils.produce_playlist([Path(p) for p in which_files], save_location, fmt, not relative)
        for a in apps_to_open:
            subprocess.Popen([a, str(save_location)])

    @gu.trap_and_report_errors
    def handle_tag(self) -> None:
        """Handle the 'Tag files ...' right-click command.

        We should maybe scan all selected videos and pre-populate the entry field with
        tags common to all, but we don't currently do this. (#FIXME?)
        """
        sel = self._root().entire_current_selection()
        db = self._root().displayed_database
        tags = gd.do_tag_files(sel)
        if tags:
            for f in sel:
                db.add_tags(f, tags, suppress_write=True)
                self.master.rebuild_single_list_item(f, db)
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_tags_change', which_file=f,
                                                      exception_windows=(self.master,))
            self.master.master.master.displayed_database.write_data()

    @gu.trap_and_report_errors
    def clear_tags(self) -> None:
        """Handle the 'Remove tags ...' right-click command.
        """
        sel = self.master.master.master.entire_current_selection()
        db = self.master.master.master.displayed_database
        if gu.confirm_prompt(f"Really clear tags from {len(sel)} file(s)?", title="Clear tags?", parent=self):
            for p in sel:
                db.clear_tags(p)
                self.master.rebuild_single_list_item(p, db)
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_tags_change', which_file=p,
                                                      exception_windows=[self.master])

    @gu.trap_and_report_errors
    def tag_this_now(self) -> None:
        """Handle the 'move to front of incremental tagging queue' command by passing it on
        to the incremental tagging window, if one exists; if one doesn't exist, simply
        starting the process will put selected files at the front of the window anyway.
        """
        if self._root().subwindows['incremental tag']:
            self._root().subwindows['incremental tag'].move_files_to_front(self._root().entire_current_selection())
        else:
            self._root().do_incremental_tag()

    @gu.trap_and_report_errors
    def schedule_audio_removal(self) -> None:
        """Schedule removal of audio from this video on the next processing run.
        """
        for f in self._root().yield_current_selection():
            self._root().displayed_database.schedule_audio_removal(f, suppress_write=True)
        self._root().displayed_database.write_data()

    @gu.trap_and_report_errors
    def schedule_audio_downsampling(self) -> None:
        """Schedule the downsampling of audio to prefs-specified maximums, to happen on the
        next processing run.
        """
        for f in self.master.master.master.yield_current_selection():
            gd.schedule_audio_downsampling(self._root().displayed_database, f, suppress_write=True)
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', f,
                                                  exception_windows=(self.master,))
        self._root().displayed_database.write_data()

    @gu.trap_and_report_errors
    def schedule_video_resize(self) -> None:
        """Schedule the resizing of the video for the next processing run, based on
        settings configured in user preferences.
        """
        changed = False
        database = self._root().displayed_database

        try:
            for f in self._root().yield_current_selection():
                movie_metadata = database[f]['movie_metadata']
                ini_width, ini_height = movie_metadata['frame width'], movie_metadata['frame height']
                data = gd.get_data_from_user({
                    'new width':  {'kind': int, 'validator': gd.positive_integer_validator_func, 'initial': ini_width},
                    'new height': {'kind': int, 'validator': gd.positive_integer_validator_func, 'initial': ini_height},
                }, window_title=f"Enter new dimensions for {f.name}")
                if data:
                    changed = True
                    mh.schedule_video_resize(which_file=f, new_width=data['new width'],
                                             new_height=data['new height'], suppress_write=True)
                    gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', f,
                                                          exception_windows=(self._root(),))
        finally:
            if changed:
                self._root().displayed_database.write_data()

    def schedule_aspect_ratio_change(self) -> None:
        """A convenience wrapper for the friendly version near the top of this module.
        """
        gd.schedule_aspect_ratio_change(self._root().displayed_database,
                                        self.master.master.master.yield_current_selection())

    @gu.trap_and_report_errors
    def do_adjust_volume(self) -> None:
        """Convenience function to invoke the volume-adjusting code sequentially on each
        selected file.
        """
        for item in self._root().yield_current_selection():
            subwindows.do_volume_change(media_loc=Path(item), which_db=self._root().displayed_database,
                                        parent=self._root())
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                                  which_file=Path(item))

    @gu.trap_and_report_errors
    def do_normalize_volume(self) -> None:
        """Prompt the user for parameters, then schedule a volume normalization operation
        for the next processing run.
        """
        cur_sel = self._root().entire_current_selection()

        opts = gd.get_loudness_normalization_params(len(cur_sel))
        if not opts:
            self._root().set_window_status_to_canceled(True, True)
            return

        which_db = self._root().displayed_database
        for f in cur_sel:
            which_db.schedule_audio_normalization(which_file=Path(f), target=opts['Loudness target'],
                                                  range=opts['Loudness range'], true_peak=opts['True peak'],
                                                  dual_mono=opts['Dual mono'], suppress_write=True)
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                                  which_file=Path(f))
        which_db.write_data()
        self._root().set_window_status(f"Scheduled audio normalization for {len(cur_sel)} "
                                       f"file{'s' if (len(cur_sel) > 1) else ''}", True, True)

    def schedule_beginning_cut(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        gd.schedule_beginning_cut(self._root().displayed_database, self.master.master.master.yield_current_selection())

    def schedule_end_cut(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        gd.schedule_end_cut(self._root().displayed_database, self.master.master.master.yield_current_selection())

    def schedule_frame_trim(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        subwindows.schedule_frame_trim(self._root(), self._root().displayed_database,
                                       self.master.master.master.yield_current_selection())

    @gu.trap_and_report_errors
    def schedule_video_rotation(self, how_much: int) -> None:
        """Schedule a video rotation by HOW_MUCH degrees, with the same meaning and
        constraints as in the underlying function it's passed to.
        """
        for f in self._root().yield_current_selection():
            self._root().displayed_database.schedule_video_rotation(f, how_much=how_much, suppress_write=True)
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                                  exception_windows=(self,))
        self._root().displayed_database.write_data()

    @gu.trap_and_report_errors
    def schedule_video_flip(self, horizontal: bool = False,
                            vertical: bool = False) -> None:
        """Schedule a horizontal and/or vertical flip of the video(s) selected for the
        next processing run.
        """
        for f in self._root().yield_current_selection():
            self._root().displayed_database.schedule_video_flip(f, horizontal=horizontal, vertical=vertical,
                                                                suppress_write=True)
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', f,
                                                  exception_windows=(self.parent,))
        self.master.master.master.displayed_database.write_data()

    @gu.trap_and_report_errors
    def clear_scheduled_processing(self) -> None:
        """Clear the scheduled processing plans for all selected files.
        """
        for f in self.master.master.master.yield_current_selection():
            self.master.master.master.displayed_database[f]['next_processing'] = list()
        self.master.master.master.displayed_database.write_data()

    @gu.trap_and_report_errors
    def schedule_raw_ffmpeg_params(self) -> None:
        """Wrapper for routine that adds free-entry parameters to the FFmpeg invocation on
        the next processing run.
        """
        for f in self._root().yield_current_selection():
            gd.generic_change_ffmpeg_parameters(self._root().displayed_database, f)
        self._root().displayed_database.write_data()

    @gu.trap_and_report_errors
    def do_execute_with(self, prog: Path) -> None:
        """Open the currently selected files using PROG, which is the full path to
        a program.
        """
        assert isinstance(prog, Path)
        assert prog.is_file(), f"Helper app {prog} is a not a regular file!"
        files = [str(p) for p in self._root().entire_current_selection()]
        subprocess.Popen([str(prog.resolve())] + files)

    @gu.trap_and_report_errors
    def do_add_new_helper_app(self) -> None:
        """Add a new helper app to the Open With ... menu.
        """
        data = gd.get_data_from_user(data_needed={
            'displayed name':
                {'kind': str, 'validator': None, 'initial': '',
                 'tooltip':
                     'The name of the program to display in menus.',
                 },
            'program location':
                {'kind': Path, 'validator': gd.executable_file_validator, 'initial': '',
                 'tooltip':
                     'The full path to the program on disk.'},
            '':
                {'kind': 'text',
                 'validator': None,
                 'initial': 'Full path to program executable'},
        }, window_title="Helper application to add")
        if not data:
            self._root().set_window_status_to_canceled(True, True)
            return
        utils.prefs.set_helper_app(executable_path=data['program location'],
                                   executable_description=data['displayed name'])
        gu.redirect_prints_to_status_bar(self._root().status_bar, True, utils.prefs.save_preferences)
        self.rebuild_helper_apps_menu()

    @gu.trap_and_report_errors
    def do_remove_helper_apps(self) -> None:
        """View and edit the list of helper apps.
        """
        data = gd.get_data_from_user(data_needed={name: {'kind': bool, 'initial': True,
                                                         'tooltip': f'Uncheck to remove {name} from menu.',
                                                         } for name in utils.prefs['helper-apps']})
        if data:
            utils.prefs['helper-apps'] = {key: value for key, value in utils.prefs['helper-apps'].items() if data[key]}
        utils.prefs.save_preferences()
        self.rebuild_helper_apps_menu()

    def rebuild_helper_apps_menu(self) -> None:
        """Build the 'Open with ...' helper applications menu that appears when an entry in
        the file list is right-clicked.
        """
        self.helper_app_menu.delete(0, 'end')
        for name, prog in pref('helper-apps').items():
            self.helper_app_menu.add_command(label=name, command=lambda p=prog: self.do_execute_with(Path(p)))
        self.helper_app_menu.add_separator()
        self.helper_app_menu.add_command(label="Add helper application ...", command=self.do_add_new_helper_app)
        self.helper_app_menu.add_command(label="View/edit helper applications ...", command=self.do_remove_helper_apps)

    def make_popups(self) -> None:
        """Make the popup menu that's held in reserve for right-clicks.
        """
        self.column_body_popup = tk.Menu(self, tearoff=False)
        self.column_body_popup.add_command(label="Play video(s)", command=self.do_view_media)
        self.column_body_popup.add_command(label="Video info ...", command=self.do_video_info)
        self.helper_app_menu = tk.Menu(self.column_body_popup, tearoff=False)
        self.column_body_popup.add_cascade(menu=self.helper_app_menu, label="Open with")
        self.rebuild_helper_apps_menu()
        self.column_body_popup.add_separator()
        self.column_body_popup.add_command(label="Delete video(s) ...", command=self.handle_delete)
        self.column_body_popup.add_command(label="Rename video(s) ...", command=self.handle_rename)
        self.column_body_popup.add_separator()
        self.column_body_popup.add_command(label="Tag video(s) ...", command=self.handle_tag)
        self.column_body_popup.add_command(label="Clear tags ...", command=self.clear_tags)
        self.column_body_popup.add_command(label="Move to front of incremental tagging queue",
                                           command=self.tag_this_now)
        self.column_body_popup.add_separator()
        self.column_body_popup.add_command(label="Create playlist for selected videos ...",
                                           command=self.handle_create_playlist)
        self.column_body_popup.add_separator()

        schedule_menu = tk.Menu(self.column_body_popup, tearoff=pref('allow-tearoffs'))
        self.column_body_popup.add_cascade(menu=schedule_menu, label="Schedule processing")
        schedule_menu.add_command(label="Schedule audio removal ...", command=self.schedule_audio_removal)
        schedule_menu.add_command(label="Schedule audio downsampling ...", command=self.schedule_audio_downsampling)
        schedule_menu.add_command(label="Schedule video resize ...", command=self.schedule_video_resize)
        schedule_menu.add_separator()

        resize_menu = tk.Menu(schedule_menu, tearoff=pref('allow-tearoffs'))
        schedule_menu.add_cascade(menu=resize_menu, label="Rotate and flip")
        resize_menu.add_command(label=" 90° clockwise", command=lambda: self.schedule_video_rotation(how_much=90))
        resize_menu.add_command(label="180° clockwise", command=lambda: self.schedule_video_rotation(how_much=180))
        resize_menu.add_command(label="270° clockwise", command=lambda: self.schedule_video_rotation(how_much=270))
        resize_menu.add_separator()
        resize_menu.add_command(label="Flip horizontally",
                                command=lambda: self.schedule_video_flip(horizontal=True, vertical=False))
        resize_menu.add_command(label="Flip vertically",
                                command=lambda: self.schedule_video_flip(horizontal=False, vertical=True))

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Change aspect ratio ...", command=self.schedule_aspect_ratio_change)
        schedule_menu.add_separator()

        volume_menu = tk.Menu(schedule_menu, tearoff=pref('allow-tearoffs'))
        schedule_menu.add_cascade(menu=volume_menu, label="Adjust volume")
        volume_menu.add_command(label="Adjust volume ...", command=self.do_adjust_volume)
        volume_menu.add_command(label="Normalize volume", command=self.do_normalize_volume)


        schedule_menu.add_separator()
        schedule_menu.add_command(label="Cut beginning of video ...", command=self.schedule_beginning_cut)
        schedule_menu.add_command(label="Cut end of video ...", command=self.schedule_end_cut)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Trim video frame size ...", command=self.schedule_frame_trim)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Add raw FFmpeg parameters ...", command=self.schedule_raw_ffmpeg_params)
        schedule_menu.add_separator()
        schedule_menu.add_command(label="Clear scheduled processing ...", command=self.clear_scheduled_processing)
        self.column_body_popup.bind("<FocusOut>", lambda *args: self.column_body_popup.unpost())
        gu.FlexibleTreeview.make_popups(self)

    def popup(self, event: tk.Event):
        """Pop up the popup menu.
        """
        region = self.identify("region", event.x, event.y)
        if region == "heading":
            gu.FlexibleTreeview.popup(self, event)     # headings are handled by the superclass!
        else:
            self.column_body_popup.post(event.x_root, event.y_root)
            self.column_body_popup.focus_set()

    def filter(self, filter_proc: Optional[Callable] = None):
        """Override the superclass's method: we inherit, then update the status bar after
        filtering is done.
        """
        if not self._root().displayed_database:        # not yet displaying a database? nothing needs to be filtered.
            return
        gu.FlexibleTreeview.filter(self, filter_proc)
        self._root().set_default_window_status()


class VideoFilter(ttk.Frame):
    """A filter box for the top of the database window, to help find items quickly.
    """
    def __init__(self,
                 parent: ttk.Frame,
                 *pargs, **kwargs):
        ttk.Frame.__init__(self, parent, *pargs, **kwargs)
        # self.config(background="#eee")        # FIXME!
        self.last_keystroke = None      # Or a float from time.monotonic()

        # now, make widgets that occur in this Frame.
        ttk.Label(self, text="Quick filter: ").pack(side=tk.LEFT, expand=tk.NO)

        self.match_mode = gu.WrappedCombobox(self)
        self.match_mode.filter_procs = {'Exact phrase': self._filter_list_by_exact_phrase,
                                        'All words': self._filter_list_by_every_word,
                                        'Any word': self._filter_list_by_any_word,
                                        'RegEx': self._filter_list_by_regex,
                                        }
        self.match_mode['values'] = list(self.match_mode.filter_procs.keys())
        self.match_mode.current(0)
        self.match_mode.state(['readonly'])
        self.match_mode.bind('<<ComboboxSelected>>', self.change_match_mode)
        self.match_mode.pack(side=tk.RIGHT, expand=tk.NO)
        ttk.Label(self, text="Match by: ").pack(side=tk.RIGHT, expand=tk.NO)

        self.entry_text = gu.WrappedEntry(self)
        self.entry_text.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)
        self.entry_text.bind('<Key>', func=lambda e: self.respond_to_entry_change(e))
        self.entry_text.bind('<Escape>', func=lambda e: self.after(pref('filter-delay'), self.clear_filter, e))
        self.pack(side=tk.TOP, expand=tk.NO, fill=tk.X)

    def _filter_list_by_exact_phrase(self, match_text: str,
                                     item_id: str) -> bool:
        """Loosely matches the items in the MainTreeView against the string MATCH_TEXT.
        More specifically, MATCH_TEXT matches an item with ID if MATCH_TEXT case-
        insensitively and without considering diacritics appears in the name of that
        item, or if that string appears in a tag attached to that item. If it matches,
        then _filter_list_by_exact_phrase() returns FALSE, meaning that the item *should
        not be filtered out* (because it matches); otherwise, it returns True,
        indicating that the item *should be filtered out* (because it does not match).
        This is perhaps counterintuitive: it is the boolean inverse of the question
        "should this item be in the list?"
        """
        assert item_id in self._root().displayed_database
        comparator = utils.default_sort(match_text).strip()
        if comparator in item_id:
            return False
        for t in self._root().displayed_database.get_tags(Path(item_id)):
            if comparator in utils.default_sort(t).strip():
                return False
        return True

    def _filter_list_by_any_word(self, match_text: str,
                                 item_id: str) -> bool:
        """Loosely matches the items in the MainTreeView against the string MATCH_TEXT,
        where MATCH_TEXT is a whitespace-separated list of words, any of which may (case
        insensitively) match the file name or any tag associated with the file.

        The return value has the same meaning as in the other filterbar filter
        functions: True means it SHOULD BE FILTERED (i.e., not appear on the list), and
        False means it SHOULD NOT BE FILTERED (i.e., should appear on the list).
        """
        assert item_id in self._root().displayed_database
        comparator = set([utils.default_sort(i).strip() for i in utils.default_sort(match_text).strip().split()])
        title = Path(item_id)
        title_comparator = set([title.parent] + [utils.default_sort(i).strip()
                                                 for i in utils.default_sort(title.name).strip().split()])
        if comparator.intersection(title_comparator):
            return False
        if comparator.intersection(set([utils.default_sort(i).strip()
                                        for i in self._root().displayed_database.get_tags(Path(item_id))])):
            return False
        return True

    def _filter_list_by_every_word(self, match_text: str,
                                   item_id: str) -> bool:
        """Loosely matches the items in the MainTreeView against the string MATCH_TEXT,
        where a match means that every word in the whitespace-separated textual list
        provided in the string MATCH_TEXT appears in either the name of the file or the
        tags associated with the file.

        The return value has the same meaning as in the other filterbar filter
        functions: True means it SHOULD BE FILTERED (i.e., not appear on the list), and
        False means it SHOULD NOT BE FILTERED (i.e., should not appear on the list).
        """
        assert item_id in self._root().displayed_database
        id_comparator = utils.default_sort(item_id).strip()
        tags_comparator = [utils.default_sort(i).strip()
                           for i in self._root().displayed_database.get_tags(Path(item_id))]
        for w in [utils.default_sort(i).strip() for i in utils.default_sort(match_text).strip().split()]:
            if w in id_comparator:
                continue
            if w not in tags_comparator:
                return True
        return False

    def _filter_list_by_regex(self, match_text: str,
                              item_id: str) -> bool:
        """(Non-loosely) matches the items in the MainTreeView against the regular
        expression MATCH_TEXT, where "a match" means that the MATCH_TEXT regex matches
        either the filename or else one of the tags associated with that file in the
        database. Unlike other filterbar matching modes, this function matches the exact
        regex entered without pre-processing it in any way whatsoever: no stripping of
        whitespace from the ends, no caefolding -- all of these could change the meaning
        of the regex.

        The return value has the same meaning as in the other filterbar filter
        functions: True means it SHOULD BE FILTERED (i.e., not appear on the list), and
        False means it SHOULD NOT BE FILTERED (i.e., should not appear on the list).
        """
        assert item_id in self._root().displayed_database
        regex = utils.regex_compile(match_text)
        if re.match(regex, Path(item_id).name):
            return False
        for t in self._root().displayed_database.get_tags(Path(item_id)):
            if re.match(regex, t):
                return False
        return True

    def _get_quick_filter_func(self) -> Callable:
        """Get the function that should be used to filter the file list, based on the
        current selection in the "Match type:" menu.
        """
        return self.match_mode.filter_procs[self.match_mode._getval()]

    def handle_entry_change(self, event: Optional[tk.Event] = None,
                            ignore_keystroke_time: bool = False) -> None:
        """Handle a change to the content of the filter.

        Consumes an EVENT, if one is passed, but does nothing with it. If
        IGNORE_KEYSTROKE_TIME is False (the default), does nothing unless it's been
        "long enough" (as configured in the global preferences) since the last
        keystroke.
        """
        # First: if it last keystroke wasn't long ago enough, don't filter: there are other filtration events scheduled.
        if (not ignore_keystroke_time) and (time.monotonic() < (self.last_keystroke + (pref('filter-delay')/1000))):
            return

        # Next: avoid re-filtering a full list to full based on an empty filter.
        scroller = self._root().video_list.scroller.the_list
        if not self.entry_text.get().strip():   # if the text field is empty ...
            if not scroller.filtered:           # and the list of entries that have been filtered out is also empty ...
                return                          # we need not do anything.

        old_sel = scroller.selection() or None

        current_filter_text = self.entry_text.get().strip()
        current_filter = self._get_quick_filter_func()
        if current_filter_text:
            scroller.filter(filter_proc=lambda item: current_filter(current_filter_text, item))
        else:
            scroller.filter(None)

        if old_sel:
            new_sel = tuple(set(old_sel).intersection(set(scroller.get_children())))
            if new_sel:
                scroller.selection_set(new_sel)
                scroller.see(scroller.selection())

    def _apply_immediately(self) -> None:
        """Forces the current filter to definitely be re-applied to the list of displayed
        files immediately, without needing to wait long enough after a keystroke. This
        is handy, for instance, when rebuilding the list from scratch.
        """
        self.handle_entry_change(ignore_keystroke_time=True)

    def respond_to_entry_change(self, event: tk.Event) -> None:
        """Handle a keystroke event inside the filter bar. We do this by (a) letting
        existing keybindings put the letter into the box, and (b) scheduling a filter
        event (based on timing specified in the preferences) for the future.
        """
        self.last_keystroke = time.monotonic()
        self.after(5 + pref('filter-delay'), self.handle_entry_change)

    def clear_filter(self, event: tk.Event) -> None:
        """Handle events (e.g., the Esc key) that clear the quick filter list by clearing
        the quick filter.
        """
        self.entry_text.delete(0, 'end')
        self.handle_entry_change(event)

    def change_match_mode(self, *event) -> None:
        self.match_mode.selection_clear()
        self._apply_immediately()


class VideoScroller(ttk.Frame):
    """A Frame that implements the list of known media known in the currently displayed
    database. It includes references to the Treeview and Scrollbar widgets that it
    initializes.
    """
    def __init__(self, parent=None, *pargs, **kwargs):
        """Create the Frame, then populate it with subordinate widgets."""
        ttk.Frame.__init__(self, parent, *pargs, **kwargs)
        # self.config(height=8, width=60, background="white")       # FIXME!
        self.config(height=8, width=60)

        # now, make widgets inside the Frame.
        the_list = MainTreeview(self)
        the_list['show'] = 'headings'

        the_list.bind('<<TreeviewSelect>>', self.master.master.set_default_window_status)
        the_list.bind('<<TreeviewOpen>>', self.master.master.set_default_window_status)
        the_list.bind('<<TreeviewClose>>', self.master.master.set_default_window_status)

        sbar = ttk.Scrollbar(self)
        sbar.config(command=the_list.yview)
        the_list.config(yscrollcommand=sbar.set)
        sbar.pack(side=tk.RIGHT, fill=tk.Y)
        sbar = ttk.Scrollbar(self, orient='horizontal')
        sbar.config(command=the_list.xview)
        the_list.config(xscrollcommand=sbar.set)
        sbar.pack(side=tk.BOTTOM, fill=tk.X)
        the_list.pack(side=tk.LEFT, expand=tk.YES, fill=tk.BOTH)
        the_list.bind('<Double-1>', self.view_media)
        the_list.bind('<Return>', self.view_media)
        self.the_list = the_list

        self.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.BOTH)

    def re_sort_list(self, new_sort=None) -> None:
        """Method that re-sorts the VideoList.

        This just delegates the call to the list widget's own method for handling this
        action, and it's always safe to just call the lower-level function directly
        instead. See that docstring for an explanation of the NEW_SORT tuple.
        """
        self.the_list.re_sort_list(new_sort=new_sort)

    def view_media(self, event: tk.Event) -> None:
        """Handle a double click on an item in the Treeview by identifying which media
        file is being double-clicked, then opening it in the configured external editor.
        """
        if not event:
            return

        region = self.the_list.identify("region", event.x, event.y)
        if region == "heading":                 # Double-clicking on a column heading should do nothing but sort twice.
            return
        try:
            gu.view_media(self.master.master.entire_current_selection())
        except (IndexError,):                                   # nothing selected? nothing to do.
            pass

    def do_delete(self) -> None:
        """Pass this on to the embedded widget that handles it.
        """
        self.the_list.handle_delete()

    def empty_list(self) -> None:
        """Empty the underlying Treeview, probably before rebuilding it.
        """
        self.the_list.filter(None)
        self.the_list.filtered = list()
        self.the_list.columns = tuple()
        self.the_list.config(displaycolumns=tuple())
        self.the_list.delete(*self.the_list.get_children())

    def _entry_data(self, which_file: str,
                    which_database: Optional[mh.MetadataStore] = None) -> List[str]:
        """Get the list of items that need to be inserted into the VideoScroller list for
        WHICH_FILE. It looks up the data in WHICH_DATABASE, which may be omitted; if it
        is omitted, it will be looked up from the root window's 'displayed_database'
        attribute. This takes time, though, so specifying it manually is wise in a time-
        critical operation, such as when rebuilding the entire database.
        """
        if not which_database:
            which_database = self.master.master.displayed_database
        file_data = which_database[which_file]

        ret = [Path(which_file).name, ]
        try:
            ret.append(str(which_database[which_file]['movie_metadata']['length']).strip())
        except (KeyError,):
            ret.append('')
        except (Exception,) as errrr:
            pass

        ret.append(', '.join(which_database.get_tags(Path(which_file))))

        try:  # file last modified time
            ret.append(str(datetime.datetime.fromtimestamp(file_data['file_metadata']['st_mtime'])))
        except Exception as errrr:
            ret.append('')

        try:  # file size
            ret.append(str(file_data['file_metadata']['cache_size']))
        except Exception as errrr:
            ret.append('')

        ret.append(str(Path(which_file).parent) + os.path.sep)  # Add the file location.

        return ret

    def change_item_iid(self, old_iid: str,
                        new_iid: str) -> None:
        """Change the IID for the item with OLD_IID to have the IID NEW_IID.
        VideoDataStore uses the relative-path-qualified file name as the internal ID
        (iid) of each database-entry list item, so if the file name changes (i.e., if
        the file is renamed), the iid also needs to change, because the rest of the
        code assumes that a filename is an iid.

        The problem is that Tkinter Treeviews don't have a way to change the iid that
        they use internally to refer to a row in a Treeview, so what we do instead is
        delete the old row and create a new one that uses NEW_IID instead of OLD_IID.
        Along the way, it actually rebuilds the list item in question, in case any of
        the metadata happens to have changed. It also inserts it into the correct
        position, according to the current sort, in the Treeview; and, if the old row
        was part of the current highlight, the new row will be, too.

        Since VideoDataScroller assumes that the iid is the filename of the file being
        represented, it's REALLY IMPORTANT not to "change" an iid except as part of a
        file-renaming process!
        """
        assert isinstance(old_iid, str)
        assert isinstance(new_iid, str)

        if new_iid == old_iid:
            return              # nothing to do here!

        prev_sel = old_iid in self.the_list.selection()

        try:                # we SHOULD be calling this AFTER renaming ...
            prev_vis = bool(self.the_list.bbox(new_iid))    # can't find data based on new name? check old name too.
        except (KeyError, tk.TclError) as errrr:
            prev_vis = bool(self.the_list.bbox(old_iid))

        try:
            data = list(self._entry_data(new_iid))
        except (KeyError, Exception) as errrr:          # FIXME! Do we EVER catch anything other than KeyError?
            data = list(self._entry_data(old_iid))
        data[0] = Path(new_iid).name

        cur_sort = self.the_list.default_sort
        sort_col = self.the_list['columns'].index(cur_sort[0])

        self.the_list.delete(old_iid)
        sort_data = [self.the_list.item(i, 'values')[sort_col].casefold() for i in self.the_list.get_children()]

        # bisect.bisect() doesn't have a REVERSED parameter, so reverse the list if it's currently reverse-sorted.
        if cur_sort[1]:
            sort_data = sort_data[::-1]

        ins_pt = bisect.bisect(sort_data, data[sort_col].casefold())
        if cur_sort[1]:
            ins_pt = len(sort_data) - ins_pt
        self.the_list.insert(parent='', index=ins_pt, iid=new_iid, text=str(new_iid), values=data)

        if prev_sel:
            self.the_list.selection_add(new_iid)
        if prev_vis:
            self.the_list.see(new_iid)

    def rebuild_single_list_item(self, which_file: str,
                                 which_database: Optional[mh.MetadataStore] = None) -> None:
        """Rebuild the display of a single item, representing WHICH_FILE, in the list
        that represents WHICH_DATABASE. If WHICH_DATABASE is None, it is (more slowly)
        computed from the main program window's displayed database.
        """
        self.the_list.item(which_file, text=which_file, values=self._entry_data(which_file, which_database))

    def delete_list_item(self, which_file: Union[str, Path]) -> None:
        """Delete a single entry from the list.
        """
        self.the_list.delete(str(which_file))

    def rebuild_list(self) -> None:
        """There are two similarly-named methods, rebuild_lit() and _rebuild_list().
        Code that just wants the list rebuilt should call no-underscore rebuild_list().
        It (and similarly named methods throughout the class hierarchy) route the call
        up through the Tkinter object hierarchy to a method on the MainWindow class
        that sets the status of the main window's status bar before calling back to
        _rebuild_list(), which rebuilds the list.

        On large databases, rebuilding the list can take a while, so it's important that
        the user have a visual indication of why the application isn't responding.
        """
        self._root().rebuild_list()

    def _rebuild_list(self) -> None:
        """Rebuild the list, based on the contents of the root window's displayed_database
        MetadataStore.

        # FIXME: preserve the current selection.
        """
        self.empty_list()

        columns = list(MainTreeview.columns)
        columns.sort(key=lambda k: pref('column-weights')[k] if k in pref('column-weights') else 0)

        self.the_list.configure(columns=columns)

        display_cols = [c for c in columns if c not in pref('hidden-columns')]
        self.the_list['displaycolumns'] = display_cols

        for col in columns:
            self.the_list.column(col, anchor="w", stretch=True)
            self.the_list.heading(col, text=col, command=lambda _col=col: self.the_list.sort_by_column(_col))

        db = self.master.master.displayed_database
        for key, value in self.master.master.displayed_database.items():
            self.the_list.insert('', 'end', iid=key, text=str(value), values=self._entry_data(key, db))

        self.master.entry_filter._apply_immediately()
        self.re_sort_list()


class VideoList(ttk.Frame):
    """A Frame that handles the interaction between a Treeview and a Scrollbar.
    """
    def __init__(self, parent: ttk.Frame, *pargs, **kwargs):
        """Create the new VideoList, pack it, and create its subordinate widgets.
        """
        ttk.Frame.__init__(self, parent, *pargs, **kwargs)
        self.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.BOTH)

        # make subordinate widgets for this Frame.
        self.entry_filter = VideoFilter(self)
        self.scroller = VideoScroller(self)

    def do_view_media(self) -> None:
        """Route a request to view the selected media to the embedded scroller.
        """
        self.scroller.view_media(None)

    def re_sort_list(self, new_sort=None) -> None:
        """Method that re-sorts the VideoList.

        This just delegates the call to the list widget's own method for handling this
        action, and it's always safe to just call the lower-level function directly
        instead. See that docstring for an explanation of the NEW_SORT tuple.
        """
        self.scroller.re_sort_list(new_sort=new_sort)

    def empty_list(self) -> None:
        """Empty the VideoList's data by emptying its underlying Treeview. Called when
        creating a new database, and before loading an existing database.
        """
        self.scroller.empty_list()

    def rebuild_list(self) -> None:
        """Rebuild the list. Just delegates to the specific widget's function.
        """
        self._root().rebuild_list()

    def do_delete(self):
        """Pass this on to the embedded widget that handles it.
        """
        self.scroller.do_delete()


class MainWindowStatusBar(gu.StatusBar):
    """Overriding the superclass to get more detailed default text.
    """
    def __init__(self, parent: Type[tk.Widget],
                 *pargs,
                 initial_messages: Iterable[str] = ('No file open.',),
                 default_text: Optional[str] = None,
                 **kwargs):
        assert default_text is None, "Specifying DEFAULT_TEXT when creating the main window status bar is not allowed!"
        gu.StatusBar.__init__(self, parent, *pargs, initial_messages=initial_messages,
                              default_text=default_text, **kwargs)

    def set_default_text(self, immediate: bool = False) -> None:
        """Set the status bar text to its default value, which gives the user some stats
        about current database size, number of currently displayed (not-filtered-out)
        items, and number of currently selected rows.

        If IMMEDIATE is True, empty the messages-to-display queue and then force the
        default text to be displayed immediately.
        """
        if self.master.displayed_database and len(self.master.displayed_database):
            cur_sel = self.master.video_list.scroller.the_list.selection()
            len_cur_sel = len(cur_sel)
            shown_items = len(self._root().video_list.scroller.the_list.get_children())
            total_items = len(self._root().displayed_database)

            if len_cur_sel == 1:
                text = f'"{cur_sel[0]}" selected'
            elif len_cur_sel > 1:
                text = f"{len_cur_sel} files selected"
            else:
                text = ''

            if shown_items < total_items:
                addl_text = f'showing {shown_items} of {total_items} media files in this database'
            else:
                addl_text = f"{total_items} media files in this database"

            if text:
                text = f"{text} ({addl_text})."
            else:
                text = addl_text + '.'

        else:
            text = "No media files in this database."
        self.set_text(text, immediate=immediate)


class MainWindow(tk.Tk, gu.BasicWindowMessagingMixIn):
    """A top-level window that displays a MetadataStore object.
    """
    def __init__(self, database_to_display: Optional[Path] = None,
                 *pargs, **kwargs):
        if database_to_display:
            assert isinstance(database_to_display, Path)

        tk.Tk.__init__(self, *pargs, **kwargs)
        self.minsize(150, 100)
        self.wm_geometry(pref('window-geometry'))
        self.displayed_database = None  # type: mh.MetadataStore
        self.dirty = False              # True if unsaved changes have been made.
        self.subwindows = {'incremental tag': None, 'tags in database': None, 'database info': None,
                           'files to process': None, 'preferences': None}
        self.set_title()
        self.make_top_level_menu()
        self.status_bar = MainWindowStatusBar(self)
        self.status_bar.pack(side=tk.BOTTOM, fill=tk.X)

        self.video_list = VideoList(self)
        self.video_list.scroller.the_list.bind("<Control-a>", self.do_sel_all)
        self.video_list.scroller.the_list.bind("<Delete>", lambda *e: self.video_list.do_delete())
        self.video_list.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.BOTH)

        self.protocol("WM_DELETE_WINDOW", self.quit)
        self.scheduler = gu.TaskScheduler(self)
        self.batch_processing_thread = None
        if database_to_display:                     # FIXME! We should be accepting a structured Python object here,
            self._load_file(database_to_display)    # rather than only now reading in a file! Making the CLI file-
        # handling more flexible requires moving file-reading logic earlier to typecheck CLI arguments.

    def _destroy_subwindows(self) -> None:
        """Destroy any sub-windows for the MainWindow, in preparation for opening a new
        file.
        """
        for w in self.subwindows:
            if self.subwindows[w]:
                self.subwindows[w].destroy()
                self.subwindows[w] = None

    def set_window_status(self, what_text: str,
                          immediate: bool = False,
                          then_set_default: bool = False) -> None:
        """Convenience function to set the status in the status bar without having to
        dereference the SELF object to get its STATUS_BAR attribute.
        """
        self.status_bar.set_text(what_text=what_text, immediate=immediate)
        if then_set_default:
            self.set_default_window_status(immediate=False)

    # And now the handlers for app events required by BasicWindowMessagingMixIn
    def EVENT_file_deleted(self, which_file: Path) -> None:
        """Remove the deleted file from the file list.
        """
        assert isinstance(which_file, Path)
        self.video_list.scroller.delete_list_item(str(which_file))

    def EVENT_filename_changed(self, old_name: Path,
                               new_name: Path) -> None:
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)

        old_iid = str(utils.relative_to_with_name(self.displayed_database.file_location, old_name))
        new_iid = str(utils.relative_to_with_name(self.displayed_database.file_location, new_name))
        self.video_list.scroller.change_item_iid(old_iid, new_iid)

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """Rebuild the entry list for WHICH_FILE.
        """
        assert isinstance(which_file, Path)
        self.video_list.scroller.rebuild_single_list_item(str(which_file), self.displayed_database)

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """Rebuild the entry list for WHICH_FILE.
        """
        assert isinstance(which_file, Path)
        self.video_list.scroller.rebuild_single_list_item(str(which_file), self.displayed_database)

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        """Currently, we don't deal with processing parameters in any way.
        """
        pass        # intentionally ignore this event.

    def set_default_window_status(self, immediate=True):
        """Convenience function to set the window's status bar to its default text.
        """
        self.status_bar.set_default_text(immediate=immediate)

    def set_window_status_to_canceled(self, immediate=True, then_set_default=True):
        """Convenience function to set the window's status bar to display the text
        "Canceled."
        """
        self.set_window_status("Canceled.", immediate=immediate, then_set_default=then_set_default)

    def yield_current_selection(self) -> Generator[Path, None, None]:
        """Generator function that yields index-IDs for the currently selected rows in
        SELF's scrolling listbox.
        """
        for ret in self.video_list.scroller.the_list.selection():
            yield Path(ret)

    def entire_current_selection(self) -> List[Path]:
        """Returns a list of all index-IDs for the currently selected rows in a main
        window's scrolling listbox. This is the same as yield_current_selection(), above,
        except that it returns a list of all IDs all at once instead of yielding them one
        by one.
        """
        return list(self.yield_current_selection())

    def re_sort_list(self, new_sort=None) -> None:
        """Method that re-sorts the VideoList. NEW_SORT is a new sort type, as explained
        in the underlying list object's method docstring.

        This just delegates the call to the list widget's own method for handling this
        action, and it's always safe to just call the lower-level function directly
        instead.
        """
        self.video_list.re_sort_list(new_sort=new_sort)

    def rebuild_list(self) -> None:
        """Rebuilds the main Video-displaying list in the window. Just a convenience
        function to make this visible from the level of the main application window:
        it delegates to the lower-level underlying list object.
        """
        self.set_window_status("Rebuilding media list ...", immediate=True, then_set_default=False)
        self.video_list.scroller._rebuild_list()
        self.set_default_window_status(immediate=True)

    def do_sel_all(self, *event) -> None:
        """Convenience function to select all items in the displayed scroller list.
        """
        self.video_list.scroller.the_list.do_select_all()

    def do_sel_none(self, *event) -> None:
        """Convenience function to deselect all items in the displayed scroller list.
        """
        self.video_list.scroller.the_list.do_select_none()

    def do_inv_sel(self, *event) -> None:
        """Convenience function to invert the selection of items in the displayed
        scroller list.
        """
        self.video_list.scroller.the_list.do_invert_selection()

    def set_title(self) -> None:
        """Set the window title based on document title and whether the current data needs
        to be saved.
        """
        db = self.displayed_database
        title = "* " if self.dirty else ""
        if db and db.file_location:
            title += (db.file_location.parent.name + '/' + db.file_location.name + " ﹘ ")
        title += "{} ({})".format(utils.__progname__, utils.__version__)
        self.title(title)

    def tarnish(self) -> None:
        """Mark the window's data as having changed in a way that requires saving.
        """
        if not self.dirty:
            self.dirty = True
            self.set_title()

    def untarnish(self) -> None:
        """Mark the window's data as being "clean", in the sense of not needing to be saved.
        """
        if self.dirty:
            self.dirty = False
            self.set_title()

    def update_GUI_prefs(self) -> None:
        """Handle updates to volatile UI preferences, like window position; then save the
        current user preferences to whatever prefs file is appropriate.
        """
        utils.prefs['window-geometry'] = self.wm_geometry()
        utils.prefs.save_preferences()

    def confirm_close(self) -> bool:
        """Confirm that unsaved data should be dumped before a do_close-type operation (e.g.,
        before loading a new file, or creating a new database). Returns True if the
        operation causing the do_close should continue, i.e. if the user agrees to save,
        or chooses to discard changes; it returns False if the user chooses to cancel
        the operation.
        """
        try:
            filename = self.displayed_database.file_location.name or "{untitled file}"
        except (AttributeError,):
            filename = "{untitled file}"
        ans = tk_dialog.Dialog(self, title="Discard changes?", bitmap='questhead', default=0,
                               text="Save changes to {} before closing?".format(filename),
                               strings=('Save', 'Discard', 'Cancel'))
        if ans.num == 0:
            self.do_save()
            return True
        elif ans.num == 1:
            self.set_window_status('Changes discarded.', True, True)
            return True
        elif ans.num == 2:
            self.set_window_status_to_canceled(True, True)
            return False

    def quit(self, event=None) -> None:
        """Handle the QUIT event by overriding the superclass's .quit() method to do some
        processing. Checks to see if we need to save. Saves any changed preferences,
        then inherits from the superclass to destroy the application.
        """
        if self.dirty and not self.confirm_close():
            return
        self.update_GUI_prefs()
        try:
            if self.batch_processing_thread is not None:
                self.set_window_status("Waiting for batch processing to finish ...", immediate=True)
                self.batch_processing_thread.join()
        except (AttributeError,):
            pass
        try:
            self.set_window_status("Waiting for save to finish ...", immediate=True)
            self.displayed_database.writer.join()
        except (AttributeError,):
            # If .writer is None, or becomes None, never mind: we don't need to wait for the writer to finish.
            pass
        tk.Tk.quit(self)

    def do_quit(self, *pargs) -> None:
        """Friendly interface to the Tkinter .quit() method for nomenclatural consistency.
        """
        self.quit(*pargs)

    def open_file_info_window(self, which_file: Path) -> None:
        """Open a FileInfoWindow displaying information about WHICH_FILE. Unless one
        already exists, in which case just have the window manager lift it up to the
        top.
        """
        assert isinstance(which_file, Path)
        which_file = utils.relative_to_with_name(self.displayed_database.file_location, which_file)

        if which_file not in self.displayed_database:
            self.displayed_database.add_file_to_database(which_file)

        if which_file in self._root().subwindows:
            self._root().subwindows[which_file].lift()
        else:
            self.subwindows[which_file] = subwindows.FileInfoWindow(which_file=which_file, parent_window=self,
                                                                    which_database=self.displayed_database)

    def _copy_multi_file_data(self, single_file_data_generator: Callable[[Union[Path, str]], str],
                              multi_file_data_generator: Callable[[Iterable[Union[Path, str]]], str]) -> None:
        """Copies various types of data about files to the clipboard. This is a wrapper
        function that handles the general logic for this case so there don't need to be
        a bunch of ten-line functions where a one- or two-line function will do.

        SINGLE_FILE_DATA_GENERATOR is a function that takes a single filename, as
        reported by the database, and returns a string that constitutes the data that
        should be copied to the clipboard. MULTI_FILE_DATA_GENERATOR does the same, but
        must operate on an iterable of filenames. Both must accept filenames as either
        str or Path -- this is not currently necessary, but may be required by future
        changes.

        COPY_MULTI_FILE_DATA is arguably a misnomer: it can also copy data for a single
        file. The name is long enough already that it's worth avoiding making it longer,
        though.
        """
        sel = self.entire_current_selection()
        if len(sel) == 0:
            self.set_window_status("Nothing to copy: no files selected!", True, True)
            return
        elif len(sel) == 1:
            data = single_file_data_generator(sel[0])
        else:
            data = multi_file_data_generator(sel)
        gu.text_to_clipboard(self, data)
        self.set_window_status("Data copied.", True, True)

    def do_copy_name(self) -> None:
        """Copy the name(s) of any selected files to the clipboard.
        """
        self._copy_multi_file_data(lambda d: d.name,
                                   lambda d: pprint.pformat([f.name for f in d], compact=True))

    def do_copy_name_and_path(self) -> None:
        """Copy the name(s) of any selected files to the clipboard, including the fully-
        qualified canonical paths to those names.
        """
        self._copy_multi_file_data(lambda d: str(d.resolve()),
                                   lambda d: pprint.pformat([str(f.resolve()) for f in d], compact=True))

    def do_copy_tags_as_quoted_string(self) -> None:
        """Copy the tags associated with the selected file(s) to the clipboard. If a single
        file is selected, the tags are copied as a shell-quoted string. If multiple
        files are selected, what is copied is a JSON-encoded dictionary mapping
        filenames to shell-quoted strings representing the tags for those files.
        """
        self._copy_multi_file_data(lambda d: shlex.join(self.displayed_database.get_tags(d)),
                                   lambda d: utils.jsonify({str(f): shlex.join(self.displayed_database.get_tags(f))
                                                            for f in d}))

    def do_copy_JSON_data(self) -> None:
        """Copy the full data stored about any selected files in the database to the
        clipboard, in JSON format for easy parsing by other applications.
        """
        self._copy_multi_file_data(lambda d: utils.jsonify(dict(self.displayed_database[d])),
                                   lambda d: utils.jsonify({str(f): self.displayed_database[f] for f in d}))

    @gu.trap_and_report_errors
    def do_new(self, event=None) -> None:
        """Handle the File/New command by closing the current document (saving first if
        necessary), emptying the displayed list, and resetting file-based information
        about the current save file.
        """
        if self.dirty and not self.confirm_close():        # Unsaved data? check with user before proceeding!
            return
        loc = filedialog.askdirectory(title="Directory to save metadata in", parent=self)
        if not loc:
            return
        self.displayed_database = mh.MetadataStore(file_location=Path(loc), ignore_existing_and_overwrite=True)
        options = gd.get_data_from_user({
            'Search subdirectories': {
                'kind': bool, 'validator': None, 'initial': False,
                'tooltip':
                    'If checked, VideoDataStore will descend recursively into subdirectories, finding all files of '
                    'appropriate types that it can. If not checked, VDS will simply scan the current directory.',
            },
            'Include video files': {
                'kind': bool, 'validator': None, 'initial': True,
                'tooltip':
                    'If checked, video files found during the initial scan will be added to the database '
                    'automatically, and future scans for this database will also search for video files. (What "counts '
                    'as" a video file is determined by the file\'s extension; file extension settings can be edited '
                    'in the Preferences window.',
            },
            'Include audio files': {
                'kind': bool, 'validator': None, 'initial': False,
                'tooltip':
                    'If checked, audio files found during the initial scan will be added to the database '
                    'automatically, and future scans for this database will also search for audio files. (What "counts '
                    'as" an audio file is determined by the file\'s extension; file extension settings can be edited '
                    'in the Preferences window.',
            },
            'Include other files': {
                'kind': bool, 'validator': None, 'initial': False,
                'tooltip':
                    'If checked, files other than audio and video files will be added to the database during its '
                    'initial scan, and future scans for this database will also include non-audio, non-video files. '
                    'Which files are considered to be "audio" and "video" files is determined by file extension, and '
                    'can be configured in the Preferences window.'
            }},
            window_title="Add files to new database")
        if options:
            db = self.displayed_database
            recurse = options['Search subdirectories']
            db.settings['include video'] = options['Include video files']
            db.settings['include audio'] = options['Include audio files']
            db.settings['include others'] = options['Include other files']
            db.populate_all_data(Path(loc), recurse=recurse,
                                 include_video_files=db.settings['include video'],
                                 include_audio_files=db.settings['include audio'],
                                 include_other_files=db.settings['include others'])
            db.write_data()
        self.rebuild_list()
        self.set_title()
        self.untarnish()
        self.set_window_status('New empty file created.', True, False)

    def say_save_finished(self) -> None:
        """Convenience function to post the phrase "Saved." to the status bar. Used as a
        completion function passed to MetadataStore.write_data().
        """
        self.scheduler.queue_task(self.set_window_status, what_text='Saved.', immediate=True, then_set_default=True)

    @gu.trap_and_report_errors
    def do_save(self, event=None) -> None:
        """Handle the File/Save command (and other pathways that do the same thing) by
        writing the database to disk. Unlike very early versions, this pushes the
        actual data-writing operation to another thread so the main application thread
        can remain responsive.
        """
        if not self.displayed_database:
            self._save_as()           # We'll wind up back here after the file dialog ...
            return                    # ... so by the time that call returns, we will have already saved.
        try:
            self.set_window_status("Saving ...", immediate=True)
            self.displayed_database.write_data(completion_notification_func=self.say_save_finished)
            self.untarnish()
            self.update_GUI_prefs()
        except (Exception,) as errrr:
            gu.error_message_box("Unable to save file!", errrr)

    def _save_as(self) -> None:
        """Handle the File/Save As command by asking the user where to save the
        currently displayed data, setting the relevant attributes in the object's
        instance to point to data-storage parameters, then issuing a do_save() call.

        Note that there is currently no GUI interface directly to this function: there
        should be no direct File/Save As ... command, because moving the MetadataStore
        to a new location requires a full File / Move ... operation.
        """
        filename = filedialog.asksaveasfilename(parent=self)
        if filename:
            self.displayed_database.file_location = Path(filename)
            self.dirty = True
            self.do_save()
            self.set_title()
        else:
            self.set_window_status_to_canceled(True, True)

    @gu.trap_and_report_errors
    def do_open(self, *event) -> None:
        """Handle the File/Open command by choosing a new file and opening it.
        """
        if self.dirty and not self.confirm_close():
            return                                  # Unsaved data? check with user first to see if we want to proceed!
        filename = filedialog.askopenfilename(parent=self)
        if filename:
            self._destroy_subwindows()              # Destroy any existing subwindows for the previous document.
            self._load_file(Path(filename))

    def _load_file(self, which_file: Path) -> None:
        """Load WHICH_FILE into the main window and rebuild the main window list.
        """
        assert isinstance(which_file, Path)
        try:
            self.set_window_status(f"Opening {which_file.name} ...", immediate=True)
            old_size = self.wm_geometry()
            self.video_list.empty_list()
            self.video_list.entry_filter.entry_text.delete(0, 'end')
            self.displayed_database = mh.MetadataStore(file_location=which_file)
            self.rebuild_list()
            self.wm_geometry(old_size)
            self.untarnish()
            self.set_window_status(f'File opened. {len(self.displayed_database)} media files in this database.',
                                   immediate=True, then_set_default=True)
            self.set_title()
        except (Exception,) as errrr:
            self.set_window_status('Unable to open file.', True, True)
            gu.error_message_box("Unable to open file!", errrr)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_revert(self) -> None:
        """Handle the Revert command."""
        if gu.confirm_prompt("Discard all changes and revert to saved version?", parent=self):
            self._load_file(self.displayed_database.file_location)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_move_database(self) -> None:
        """Relocate the database to a new location, adjusting the internal references
        to external files to match their new relative location.
        """
        new_path = filedialog.asksaveasfilename(parent=self)
        if new_path:
            self.displayed_database.move_database(new_location=Path(new_path))
            self.rebuild_list()
        else:
            self.set_window_status_to_canceled(True, True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_recontainerize_deprecated(self) -> None:
        """Schedule recontainerization of all files in "deprecated" formats, a decision
        made by looking at the 'deprecated_containers' prefs key.
        """
        data = gd.get_data_from_user({'Re-containerize even if size increases':
                                          {'kind': bool, 'initial': True, 'validator': None,
                                           'tooltip':
                                               'Normally, if VideoDataStore discovers that putting the data into a new '
                                               'container increases the overall file size, it instead keeps the old '
                                               'file in its old container. If this box is checked, it will instead '
                                               'keep the new file even if that file is larger on disk.'}})
        if data:
            self.set_window_status("Scheduling re-containerization of videos in deprecated formats ...", True, False)
            gu.redirect_prints_to_status_bar(self.status_bar, True, mh.schedule_deprecated_recontainerizing,
                                             self.displayed_database, data['Re-containerize even if size increases'],
                                             add_callback=lambda f: gu.BasicWindowMessagingMixIn.dispatch(
                                                 'EVENT_file_processing_parameters_change', f))
            self.set_default_window_status(False)
        else:
            self.set_window_status_to_canceled(True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.textbox_redirector()
    def do_schedule_large_downsizing(self) -> None:
        """Schedule resizing of known videos that exceed the prefs-configured maximum
        size.
        """
        data = gd.get_data_from_user({
            'maximum width': {'kind': int, 'validator': gd.positive_integer_validator_func,
                              'initial': pref('max_video_width'),
                              'tooltip':
                                  'Videos wider than this number (in pixels) will be scheduled for downsizing.'},
            'maximum height': {'kind': int, 'validator': gd.positive_integer_validator_func,
                               'initial': pref('max_video_height'),
                               'tooltip':
                                   'Videos taller than this number (in pixels) will be scheduled for downsizing.'},
            "even if size goes up": {'kind': bool, 'validator': None, 'initial': True,
                                     'tooltip':
                                         'In the unlikely event that downsizing a file INCREASES the file size, '
                                         'VideoDataStore normally keeps the larger-dimensions, smaller-file-size file. '
                                         'If this box is checked, VideoDataStore will prefer the smaller-dimensions '
                                         'file even if its size on disk is larger.'},
            "even if already recompressed": {'kind': bool, 'validator': None, 'initial': False,
                                             'tooltip':
                                                 'Normally, VideoDataStore skips downsizing videos if it knows that it '
                                                 'has already recompressed them, because every time a file is '
                                                 'recompressed, quality is lost. Check this box if you want to '
                                                 'downsize large files even if VDS has already recompressed them, even '
                                                 'though this will result in further quality reduction.'},
            "": {'kind': 'text', 'validator': None,
                 'initial': 'Videos larger in either dimension will be resized to fit within the specified size'},
        })
        if data:
            mh.schedule_large_downsizing(which_store=self.displayed_database, max_width=data['maximum width'],
                                         max_height=data['maximum height'],
                                         even_if_size_increase=data['even if size goes up'],
                                         add_callback=lambda p: gu.BasicWindowMessagingMixIn.dispatch(
                                             'EVENT_file_processing_parameters_change', p))
        else:
            self.set_window_status_to_canceled(True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.textbox_redirector()
    def do_schedule_hi_bitrate_downsizing(self) -> None:
        """Schedule re-encoding of "high bitrate" videos, a determination made based on
        the Kush-gauge characteristics defined in the preferences.
        """
        data = gd.get_data_from_user({"even if size goes up": {'kind': bool, 'validator': None, 'initial': False,
                                                               'tooltip':
                                                                   'Normally, if VideoDataStore detects that the size '
                                                                   'of the file on disk has gone up after trying to '
                                                                   'process it, it keeps the old version instead of '
                                                                   'the new one. If this is checked, VDS will keep the '
                                                                   'new version even if it is larger on disk.'}})
        if data:
            mh.schedule_hi_bitrate_downsizing(self.displayed_database, data['even if size goes up'],
                                              add_callback=lambda p: gu.BasicWindowMessagingMixIn.dispatch(
                                                  'EVENT_file_processing_parameters_change', p,
                                                  exception_windows=(self,)))
        else:
            self.set_window_status_to_canceled(True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_schedule_spurious_stream_elimination(self) -> None:
        """Find videos with more streams than expected and check with the user to see which
        streams to keep when the next processing run happens.
        """

        def gui_cancel_chooser(which_file: Path) -> Literal['this', 'all']:
            """Asks the user to choose whether they want to cancel just this single file, or
            all files.
            """
            if "yes" == tk_msgbox.askquestion(message="Cancel the entire rest of the operation? ('No' just "
                                                      "skips this single file.)"):
                return 'all'
            else:
                return 'this'

        def gui_stream_chooser(header_phrase: str,
                               streams: List[str]) -> Iterable[int]:
            """Given STREAMS, a list of textual descriptions of audio/video/subtitle/other
            streams, produced by FFMPEG, prompt the user to decide which streams to keep.
            The STREAMS list should be list of every stream in the video file.

            Returns a list of strings that represents the zero-indexed numeric stream IDs
            for the streams to be kept during reprocessing. For instance, if the user
            selects the first, fourth, and ninth streams, returns ['0', '3', '8'].
            """
            data_needed = dict()
            for i, s in enumerate(sorted(streams)):
                data_needed[s] = {
                    'kind': bool,
                    'tooltip': 'If the box is checked, keep this stream; if unchecked, remove  it.',
                    'initial': True,
                }
            data = gd.get_data_from_user(data_needed, window_title=header_phrase)
            if not data:
                return list()

            return sorted({i for i, v in enumerate(data) if v})

        mh.schedule_spurious_stream_elimination(self.displayed_database, stream_chooser=gui_stream_chooser,
                                                cancel_chooser=gui_cancel_chooser)

    @gu.trap_and_report_errors
    @gu.textbox_redirector(window_title='Codec information')
    def do_display_codec_info(self, kind: Literal['audio', 'video', 'subtitle', 'all'],
                              direction: Literal['encode', 'decode', 'all']) -> None:
        """Display information about which codecs the currently installed version of
        FFmpeg can handle.
        """
        c_data = fu.get_codecs(kind=kind, direction=direction)

        # we are unconcerned with column zero, which we do not display to the user.
        # we are prepping a two-column layout no more than 80 characters wide, with two spaces between columns.
        col1_width = min(max(len(c[1]) for c in c_data), 20)
        col2_width = min(max(len(c[2]) for c in c_data), 78 - col1_width)

        for c in c_data:
            c1 = f"{c[1]}{' ' * (col1_width - len(c[1]))}"
            col2 = textwrap.wrap(c[2], width=col2_width, replace_whitespace=False, expand_tabs=False,
                                 drop_whitespace=False)
            printed_c1 = False
            for c2_line in col2:
                if not printed_c1:
                    print(f"{c1}  {c2_line}")
                    printed_c1 = True
                else:
                    print(f"{' ' * (2 + col1_width)}{c2_line}")

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_incremental_tag(self) -> None:
        """Display the Incremental Tagging window: create one if there isn't yet one, or
        bring the existing one to the top, if one does already exist.
        """
        if self.subwindows['incremental tag']:
            self.set_window_status("Already engaged in incremental tagging!", True, True)
            self.subwindows['incremental tag'].lift()
        else:
            files_to_tag = gd.get_incremental_tag_mode(self.displayed_database)
            if not files_to_tag:
                return
            db = self.displayed_database
            pruned_files = [f for f in files_to_tag if ('incrementally_tagged' not in db[f]) or
                            (not db[f]['incrementally_tagged'])]
            if not pruned_files:
                gu.error_message_box('There are no files that need to be incrementally tagged in this database!', None)
                return
            w = subwindows.MultiFileTaggingWindow(files_to_tag=pruned_files,
                                                  db=self.displayed_database,
                                                  num_already_tagged_files=(len(files_to_tag) - len(pruned_files)),
                                                  parent_window=self,
                                                  preferred_files=self.entire_current_selection())
            self.subwindows['incremental tag'] = w

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def display_tags_window(self) -> None:
        """Display the "tags in database" window, creating a new one if it doesn't exist,
        or lifting it to the top of the window list if it does.
        """
        if self.subwindows['tags in database']:
            self.subwindows['tags in database'].lift()
        else:
            self.set_window_status("Gathering tags ...", True)
            self.subwindows['tags in database'] = subwindows.TagsInDatabaseWindow(self.displayed_database, self)
            self.status_bar.queue_default_text()

    def do_mark_all_inc_tagged(self) -> None:
        """Mark all files in the database as incrementally tagged.
        """
        if not gu.confirm_prompt("Really mark all files as having been incrementally tagged?", parent=self):
            self.set_window_status_to_canceled(immediate=True, then_set_default=True)
            return
        self.set_window_status('Marking all files as incrementally tagged ...', immediate=True)
        for i, f in enumerate(self.displayed_database, start=1):
            if i % 100 == 0:
                self.set_window_status(f"Marked {i} files incrementally tagged", immediate=True)
            self.displayed_database[f]['incrementally_tagged'] = True
        self.set_window_status("Marked all files as incrementally tagged!", immediate=True)
        self.do_save()

    def do_mark_all_not_inc_tagged(self) -> None:
        """Mark all files in the database as not incrementally tagged.
        """
        if not gu.confirm_prompt("Really mark all files as not being incrementally tagged?", parent=self):
            self.set_window_status_to_canceled(immediate=True, then_set_default=True)
            return
        self.set_window_status('Marking all files as not incrementally tagged ...', immediate=True)
        for i, f in enumerate(self.displayed_database, start=1):
            if i % 100 == 0:
                self.set_window_status(f"Marked {i} files not incrementally tagged", immediate=True)
            self.displayed_database[f]['incrementally_tagged'] = False
        self.set_window_status("Marked all files as not incrementally tagged!", immediate=True)
        self.do_save()

    def do_mark_any_tagged_as_inc_tagged(self) -> None:
        """Mark every file in the database that has any tags at all as being incrementally
        tagged.
        """
        if not gu.confirm_prompt("Really mark all tagged files as having already been incrementally tagged?",
                                 parent=self):
            self.set_window_status_to_canceled(immediate=True, then_set_default=True)
            return
        self.set_window_status("Marking all files with tags as incrementally tagged", immediate=True)
        previously_marked = len([f for f in self.displayed_database if (('incrementally_tagged' in self.displayed_database[f]) and self.displayed_database[f]['incrementally_tagged'])])
        for f in self.displayed_database:
            if self.displayed_database.get_tags(f):
                self.displayed_database[f]['incrementally_tagged'] = True
        now_marked = len([f for f in self.displayed_database if (('incrementally_tagged' in self.displayed_database[f]) and self.displayed_database[f]['incrementally_tagged'])])
        self.set_window_status(f'Marked {now_marked - previously_marked} as incrementally tagged.', immediate=True)
        self.do_save()

    def do_mark_untagged_as_not_inc_tagged(self) -> None:
        """Mark every file in the database that has no tags as being not incrementally
        tagged. Probably not particularly useful, since it's generally impossible to
        mark a file "incrementally tagged" without assigning tags to it, but it's
        possible to remove tags from a file without changing its "incrementally tagged"
        status, so may come in handy occasionally.
        """
        if not gu.confirm_prompt("Really mark all untagged files as not being incrementally tagged?", parent=self):
            self.set_window_status_to_canceled(immediate=True, then_set_default=True)
            return
        self.set_window_status("Marking all files without tags as not incrementally tagged ...", immediate=True)
        previously_marked = len([f for f in self.displayed_database if (('incrementally_tagged' in self.displayed_database[f]) and self.displayed_database[f]['incrementally_tagged'])])
        for f in self.displayed_database:
            if not self.displayed_database.get_tags(f):
                self.displayed_database[f]['incrementally_tagged'] = False
        now_marked = len([f for f in self.displayed_database if (('incrementally_tagged' in self.displayed_database[f]) and self.displayed_database[f]['incrementally_tagged'])])
        self.set_window_status(f'Marked {previously_marked - now_marked} as not incrementally tagged.', immediate=True)
        self.do_save()

    @staticmethod
    def _get_copy_params(include_copy_symlinks_option: bool = True) -> dict:
        """Utility function for the next few methods: get the copy-or-symlink-to parameters
        and return them to the calling method.
        """
        data_needed={
            'Destination': {'kind': "pathdir", 'validator': gd.directory_exists_validator, 'initial': None,
                            'tooltip':
                                'The directory into which files will be copied'},
            '': {'kind': 'text', 'validator': None, 'initial': 'Relative positions of files will be lost in copying.'},
            'Only files with these tags': {'kind': gu.FileTaggerPane, 'validator': None, 'initial': None,
                                           'tooltip':
                                               'Only copy files that are tagged with at least one of these tags.'},
            'Skip files with these tags': {'kind': gu.FileTaggerPane, 'validator': None, 'initial': None,
                                           'tooltip':
                                               'Skip copying files that are tagged with at least one of these tags.'},
            'Copy symlinks as symlinks (not files)': {'kind': bool, 'validator': None, 'initial': False,
                                                      'tooltip':
                                                          'By default, when VideoDataStore encounters a symbolic link '
                                                          'to a file, it will copy the file that symlink points to '
                                                          'instead of the symlink itself. If this is checked, VDS will '
                                                          'copy the symlink instead of the file it points to.'},
            'Infer tags from file names': {'kind': bool, 'validator': None, 'initial': False,
                                           'tooltip':
                                               'If this is checked, VideoDataStore will treat filenames that contain '
                                               'any words that are also used as tags in the database as if those files '
                                               'were tagged with those words when evaluating files using the "Only '
                                               'files with these tags" and "Skip files with these tags" options for '
                                               'this copy operating. (This can be helpful if not all of the files in '
                                               'the database have been tagged yet.)\n\nIf this is not checked, then '
                                               'VDS will only look at the explicit tags that have been attached to '
                                               'files while not looking at file names at all.'},
        }
        if not include_copy_symlinks_option:
            del data_needed['Copy symlinks as symlinks (not files)']
        return gd.get_data_from_user(data_needed, window_title="Files to copy?")

    def _do_copy_or_whatever(self, which_files: List[Union[str, Path]],
                             new_dest: Path,
                             copy_func: Callable[[Path, Path], None],
                             action_name: str) -> None:
        """Selectively copy, or symlink or whatever, files to/in another location.

        WHICH_FILES is a list of files to be copied. NEW_DEST is the directory into
        which the files are to be copied. COPY_FUNC is the function that does the actual
        copying or whatever. COPY_FUNC is a function that takes two arguments, src
        and dest, and does the actual copying or whatever from src to dest. ACTION_NAME
        is a verb in the present participle that describes, in English, what action is
        being taken.
        """
        assert isinstance(new_dest, Path)

        self.set_window_status(f"{len(which_files)} files found. {action_name} files ...", immediate=True)
        try:
            for f in which_files:
                new_loc = new_dest / Path(f).name
                skip = False

                while (not skip) and new_loc.exists():
                    new_loc = filedialog.asksaveasfilename(initialdir=new_dest, parent=self,
                                                           initialfile=str(new_loc.name),
                                                           title=f"{f.name} already exists. New name?")
                    if new_loc:
                        new_loc = Path(new_loc)
                    else:
                        if tk_dialog.Dialog(self, title="Skip or cancel?", bitmap='questhead', default=0,
                                            text="Skip just this file, or cancel the whole process",
                                            strings=('Skip this file', 'Cancel the process')) == 0:
                            skip = True     # ensure we skip this file when loop ends
                            continue
                        else:
                            return
                if not skip:
                    copy_func(f, new_loc)
        finally:
            self.set_window_status(f'{action_name} finished', immediate=True, then_set_default=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket('Gathering files ...')
    def do_selective_copy(self) -> None:
        """Selectively copy files to another location. This collapses any directory
        structure that might exist amongst the relative files. If any files already exist
        with the same names in the destination directory, the user can rename the files to
        be copied, skip the files, or cancel the process.

        In some cases, esoteric file metadata is not copied, in the standard way of things
        that Python's shutil.copy2 routine can't copy, especially under Windows.
        """
        data = self._get_copy_params(True)
        if data:
            which_files = list(self.displayed_database.complex_video_select_by_tags(data['Only files with these tags'],
                                                                                    data['Skip files with these tags'],
                                                                                    data['Infer tags from file names']))
            self._do_copy_or_whatever(which_files, data['Destination'], action_name="Copying",
                                      copy_func=lambda src, dest: shutil.copy2(str(src), str(dest), follow_symlinks=not data['Copy symlinks as symlinks (not files)']))
        else:
            self.scheduler.queue_task(self.set_window_status_to_canceled, immediate=True)

    @gu.only_if_DB_loaded
    @gu.not_under_windows('Microsoft Windows does not support symbolic links.')
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket('Gathering files ...')
    def do_create_selected_symlinks(self) -> None:
        """Create symlinks to selected files in another location.
        """
        data = self._get_copy_params(False)
        if data:
            which_files = list(self.displayed_database.complex_video_select_by_tags(data['Only files with these tags'],
                                                                                    data['Skip files with these tags'],
                                                                                    data['Infer tags from file names']))
            self._do_copy_or_whatever(which_files, data['Destination'], action_name="Linking",
                                      copy_func=lambda src, dest: dest.symlink_to(src))
        else:
            self.scheduler.queue_task(self.set_window_status_to_canceled, immediate=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket("Gathering files ...")
    def do_create_windows_batch_file(self) -> None:
        """Create a Windows batch script to handle copying selected files under Windows.
        """
        data = self._get_copy_params(False)
        if data:
            which_files = list(self.displayed_database.complex_video_select_by_tags(data['Only files with these tags'],
                                                                                    data['Skip files with these tags'],
                                                                                    data['Infer tags from file names']))
            batch_file_name = filedialog.asksaveasfilename(initialfile='script.bat', title="Save batch file as ...", parent=self)
            if not batch_file_name:
                self.set_window_status_to_canceled(immediate=True, then_set_default=True)
                return
            destination = str(data['Destination'].resolve()).strip()
            if not destination.endswith(os.path.sep):
                destination += os.path.sep
            with io.open(batch_file_name, mode='wt', encoding='utf-8', newline='\r\n') as batch_file:      # use Windows line endings, regardless of current OS
                batch_file.write('@ECHO OFF\r\n\r\n')
                batch_file.writelines([f'copy {shlex.quote(str(Path(f).resolve()))} {shlex.quote(destination)}\r\n' for f in which_files])
                batch_file.write('\r\n\r\nECHO "Copying finished!')
            self.scheduler.queue_task(self.set_window_status, 'Windows batch file created!', immediate=True,
                                      then_set_default=True)
        else:
            self.scheduler.queue_task(self.set_window_status_to_canceled, immediate=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket("Gathering files ...")
    def do_create_sh_script(self) -> None:
        """Get a set of parameters for selective by-tag copying, as in the previous few
        methods, and then, instead of copying, create a .sh script that will do the
        copying. Unlike the Windows version, this is unlikely to be particularly useful:
        the Windows version is occasionally useful because Windows system utilities can
        copy esoteric data that Python cannot. This is much less a problem under POSIX-
        type systems because they are typically designed from the ground up to be open
        to the user and programmable.

        But it's provided anyway in case there are obscure situations in which this
        generalization does not hold.
        """
        data = self._get_copy_params(False)
        if data:
            which_files = list(self.displayed_database.complex_video_select_by_tags(data['Only files with these tags'],
                                                                                    data['Skip files with these tags'],
                                                                                    data['Infer tags from file names']))
            batch_file_name = filedialog.asksaveasfilename(initialfile='script.sh', title="Save copying script as ...", parent=self)
            if not batch_file_name:
                self.scheduler.queue_task(self.set_window_status_to_canceled, immediate=True, then_set_default=True)
                return
            destination = str(data['Destination'].resolve()).strip()
            if not destination.endswith(os.path.sep):
                destination += os.path.sep
            with open(batch_file_name, mode='wt', encoding='utf-8') as batch_file:
                batch_file.write('#!/bin/sh\n\n')
                batch_file.writelines([f'cp {shlex.quote(str(Path(f).resolve()))} {shlex.quote(destination)}\n' for f in which_files])
                batch_file.write('\n\necho "Copying finished!')
        else:
            self.scheduler.queue_task(self.set_window_status_to_canceled, immediate=True, then_set_default=True)

        # Now make it world-executable.
        os.chmod(batch_file_name, os.stat(batch_file_name).st_mode | 0o111)
        self.scheduler.queue_task(self.set_window_status, "Script created!", immediate=True, then_set_default=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_clear_processing_queue(self) -> None:
        """Clear the processing queue, so that we no longer have any plans to process any
        videos.
        """
        previously_scheduled_to_process = self.displayed_database.videos_to_process
        if gu.confirm_prompt("Are you sure you want to empty the processing queue and forget all plans to reprocess videos?", "Really Empty Queue?", parent=self):
            self.displayed_database.empty_processing_queue(confirm=True)
            self.set_window_status("Processing queue emptied.", True, True)
        for f in previously_scheduled_to_process:
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', f,
                                                  exception_windows=(self,))

    def _video_processing_update(self, num_items: int,
                                 which_file: Union[str, Path]) -> None:
        """Handle the intermittent requests to update the status of the overall process.
        We handle this by doing two things: broadcasting the fact that a file has
        changed on disk to every window that listens for file-related events; and by
        running update_idletasks() to encourage the GUI to update.

        This calls Tkinter code directly and indirectly, so it's important that code
        that might be running in a thread other than the thread that started Tkinter
        not call this function directly! Instead, add it to the global scheduler item
        at self.scheduler with self.scheduler.queue_task().
        """
        # Intentionally refrain from excluding SELF from the message broadcast: we want to trigger that processing.
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_change_on_disk', which_file=which_file)
        # FIXME: eventually, we'll want to update a progress bar here, no?
        self.update_idletasks()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket('Analyzing database ...')
    def show_processing_queue(self) -> None:
        if self.subwindows['files to process']:
            self.subwindows['files to process'].lift()
        else:
            self.subwindows['files to process'] = subwindows.ProcessingQueueWindow(database_to_display=self.displayed_database,
                                                                                   parent_window=self)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket("Processing queued videos ...")
    def do_process_videos(self) -> None:
        """Go ahead and start the video-processing process.
        """
        old_stdout, old_stderr = sys.stdout, sys.stderr

        try:
            batch_win = subwindows.BatchProcessingProgressWindow(self)
            batch_win.lift()
            batch_win.focus_set()
            batch_win.update_idletasks()
            sys.stdout = batch_win.redirector
            sys.stderr = sys.stdout
            mh.process_videos_in_separate_thread(self, batch_win, setup_proc=batch_win.set_cancel_func,
                                                 teardown_proc=batch_win.enable_ok_button,
                                                 update_proc=lambda *a, **kwa: self.scheduler.queue_task(self._video_processing_update, *a, **kwa))
        finally:
            sys.stdout, sys.stderr = old_stdout, old_stderr
            self.rebuild_list()     # FIXME: ultimately, it would be better to update info during processing
            if ('files to process' in self.subwindows) and (self.subwindows['files to process']):
                self.subwindows['files to process'].rebuild_processing_list()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_scan_for_new_files(self) -> None:
        """Go ahead and scan known directories to see if new files have appeared. Also
        remove any files in the database that are beneath any ignore directories.
        """
        oldlen = len(self.displayed_database)
        gu.redirect_prints_to_status_bar(self.status_bar, False, self.displayed_database.scan_for_new_files)
        self.rebuild_list()     # FIXME: really, we should add files one by one as they are found.
        self.set_window_status(f"Added {len(self.displayed_database) - oldlen} files!", True, True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.status_bar_announce_bracket("Pruning missing files from database ...")
    @gu.textbox_redirector()
    def do_prune_missing_files(self) -> None:
        """Go ahead and prune files that have disappeared from the database. Also remove
        files in the database that are beneath any ignore directories.
        """
        def handle_pruned(which_file: Path) -> None:
            self.video_list.scroller.the_list.delete_video(which_file, suppress_write=True)
            self.dispatch(message='EVENT_file_deleted', which_file=which_file, exception_windows=(self,))

        oldlen = len(self.displayed_database)
        self.displayed_database.prune_missing_files(remove_handler=handle_pruned)
        self.do_save()
        self.scheduler.queue_task(self.set_window_status, f"Pruned {oldlen - len(self.displayed_database)} entries!",
                                  immediate=True, then_set_default=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.textbox_redirector()
    def do_force_update_file_status(self) -> None:
        """Force all the data in the database to match what's on disk in the same-named
        files, except for the file mtime, which is forced to match the file mtime that's
        already stored in the database.

        This is useful in a processing chain that goes like this:

        1. Update database so information is current, using File/Re-Scan ... and
           File/Prune ...
        2. Process files using some other application, which alters their sizes and mtimes.
        3. Use this method (File/Force-update ...) to update the database's stored file
           sizes (and any other relevant data), and to update the file's mtime on disk
           to match what's in the database.

        Crucial to the workflow above is that filenames must not change in step 2, nor
        be changed at any other point, ever, outside the database application itself.
        """
        if not gu.confirm_prompt("Are you SURE you want to force-update the database to match the files as they exist on disk? (Read the manual to see what you're getting into before doing this.)", parent=self):
            self.set_window_status_to_canceled(True, True)
            return
        self.set_window_status("Force-updating database ...", True)
        db = self.displayed_database
        for i, f in enumerate(db):
            try:
                old_mtime = db[f]['file_metadata']['st_mtime']
                db.add_file_to_database(Path(f))
                os.utime(f, times=(os.stat(f).st_atime, old_mtime))
            except Exception as errrr:
                print(f"Can't operate on {Path(f).name}: the system said: {errrr}")
            if ((i + 1) % 1000) == 0:
                self.set_window_status(f"Updated {i+1} entries.", True)
        self.rebuild_list()
        self.set_window_status("Finished!", True, False)
        self.do_save()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_clean_db_filenames(self) -> None:
        """Rename the files in the database to have 'clean' filenames, after prompting the
        user for specifications about what a 'clean' filename should be.

        Much, but not all, of this work is handled by utils.clean_name().
        """
        opts = gd.get_data_from_user(data_needed={
            'Remove whitespace from beginning and end of name': {'kind': bool, 'initial': True},
            'Remove strip phrases from beginning and end of name': {'kind': bool, 'initial': True},
            'Limit filename length': {'kind': gd.DataGetter,
                                      'initial': {'how many characters': {'kind': int,
                                                                          'validator': gd.positive_integer_validator_func},
                                                  'split on': {'kind': list,
                                                               'initial': ['Character', 'Word']}}},
            'All-caps names': {'kind': list, 'initial': ['Leave all caps', 'Make all lowercase', 'Make title case']},
            'No extra period before extension': {'kind': bool, 'initial': True},
            'Extension must be lowercase': {'kind': bool, 'initial': True},
            ' ': {'kind': gd.Separator},
            'Handle filename conflicts by': {'kind': list, 'initial': ['Prompt', 'Make new unique name']}
        }, window_title='Clean Filenames in Database')

        if not opts:
            self.set_window_status_to_canceled()
            return

        num_renamed = 0
        try:
            auto_rename = opts['Handle filename conflicts by'] == 'Make new unique name'
            if opts['Remove strip phrases from beginning and end of name']:
                fixes = self.displayed_database.settings['strip phrases']
            else:
                fixes = list()

            for entry in self.displayed_database:
                p = Path(entry)
                parent = p.parent
                new_name = p.name

                if opts['Extension must be lowercase']:
                    new_name = p.stem + p.suffix.lower()

                if opts['Remove whitespace from beginning and end of name']:
                    new_name = p.stem.strip() + p.suffix

                if opts['All-caps names'] == 'Make all lowercase':
                    if ''.join([c for c in new_name if c.isalpha()]).isupper():
                        new_name = new_name.lower()
                elif opts['All-caps names'] == "Make title case":
                    if ''.join([c for c in new_name if c.isalpha()]).isupper():
                        new_name = new_name.title()

                if opts['No extra period before extension']:
                    while new_name.endswith('.'):
                        new_name = new_name[:-1]

                p = parent / new_name

                # We need to run it through clean_name() if we are stripping fixes.
                if fixes:
                    p = utils.clean_name(suggested_name=(parent / new_name), fixes_to_strip=fixes, auto_rename=False)
                    new_name = p.name

                if opts['Limit filename length']:
                    acceptable = False
                    while not acceptable:
                        if len(p.name) > opts['Limit filename length']['how many characters']:
                            if opts['Limit filename length']['split on'] == 'Character':
                                new_name = p.stem[:-1] + p.suffix
                            else:
                                new_name = ' '.join(p.name.split())[:-1] + p.suffix
                            p = parent / new_name
                        else:
                            acceptable = True
                    else:
                        acceptable = True

                # If we haven't made any changes to the name, no need to do anything else
                new_name = str(parent / new_name)
                p = Path(new_name)
                if p.exists() and p.samefile(Path(entry)):
                    continue

                # If we're handling filename conflicts manually, and there is a conflict, then P is merely a
                # suggestion, not the final new name: prompt the user for the real name, and handle cancellation
                # sensibly. If we're not handling conflicts manually, and there's a conflict, just auto-generate
                # a clean name.
                if p.exists() and (p != Path(entry)):
                    if auto_rename:
                        new_name = utils.clean_name(suggested_name=p).name
                    else:
                        new_name = filedialog.asksaveasfilename(title=f"Rename {Path(entry).name} to ...",
                                                                initialfile=p.name,
                                                                initialdir=Path(entry).resolve().parent, parent=self)
                        if not new_name:
                            if "yes" == tk_msgbox.askquestion(message="Cancel the entire rest of the name-cleaning "
                                                                      "operation? ('No' just skips this single file.)"):
                                self.set_window_status_to_canceled()
                                return
                            else:
                                self.set_window_status(f"Skipped renaming file {entry}.",
                                                       immediate=True, then_set_default=True)
                                continue

                p = utils.relative_to_with_name(self.displayed_database.file_location, Path(new_name))
                if p.exists() and p.samefile(Path(entry)):
                    return

                self.displayed_database.rename_file(Path(entry), p, suppress_write=True)
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_filename_changed',
                                                      old_name=Path(entry), new_name=p)
                self.set_window_status(f"Renamed file {entry} to {p}.", immediate=True, then_set_default=True)
                num_renamed += 1

                if (num_renamed % 20) == 0:
                    self.displayed_database.write_data()

        finally:
            self.displayed_database.write_data()
            self.set_window_status(f"Renamed {num_renamed} file{'s' if (num_renamed != 1) else ''}!",
                                   immediate=False, then_set_default=True)


    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_edit_ignore_directories(self) -> None:
        """View the list of "directories to ignore" for this database.
        """
        data = gd.get_data_from_user(data_needed={'Directories': {
            'kind': gu.EditableListPane,
            'validator': None,
            'addfunc': filedialog.askdirectory,
            'initial': list(self.displayed_database.settings['skipped_dirs']),
            'tooltip':
                'List of directories whose contents will be ignored by the current database when scanning for new'
                'files.',}},
            window_title="Add/Remove Ignore Directories")
        if data:
            new_dirs = [utils.relative_to(self.displayed_database.file_location, Path(p)) for p in data['Directories']]
            self.displayed_database.settings['skipped_dirs'] = new_dirs
            self.displayed_database.write_data()
            self.set_window_status("Updated list of directories to ignore", True, True)
        else:
            self.set_window_status_to_canceled()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_specify_holding_directory(self) -> None:
        """Specify a "holding directory," the directory from which files are moved.
        """
        dir = filedialog.askdirectory(title="Directory to move files from", parent=self)
        if dir:
            dir = utils.relative_to(self.displayed_database.file_location.resolve(), Path(dir).resolve())
            self.displayed_database.settings['holding directory'] = str(dir)
            self.set_window_status(f"Set holding directory for imported files to {dir}", True, True)
        else:
            self.set_window_status_to_canceled(True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_forget_holding_directory(self) -> None:
        """Forget the already-specified 'holding directory,' if there is one.
        """
        if not self.displayed_database.settings['holding directory']:
            self.set_window_status('No holding directory set! Nothing to forget ...', True, True)
        elif gu.confirm_prompt(f"Forget existing holding directory {self.displayed_database.settings['holding directory']}?", parent=self):
            self.displayed_database.settings['holding directory'] = None
            self.set_window_status("Forgot database's holding directory!", True, True)
        else:
            self.set_window_status_to_canceled(True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_import_strip_phrases_from_file(self) -> None:
        """Import a list of strip phrases from a text file. The text file must be a list of
        phrases to add to the "strip phrases" list for the database, one per line.
        Leading and trailing whitespace are stripped from the strip phrases before
        they're added to the database list.

        It's just a plain-text list of strip phrases, not regexes or anything else
        fancy. For now, anyway.         # FIXME
        """
        which_file = filedialog.askopenfilename(title="File to import strip phrases from", parent=self)
        if which_file:
            with open(Path(which_file), 'rt') as strips_file:
                strips = [l.strip() for l in strips_file.readlines() if (l.strip()) and not (l.strip().startswith('#'))]
                for s in sorted(strips, key=str.casefold):
                    self.displayed_database.add_strip_phrase(s, suppress_write=True)
            self.set_window_status(f'Added {len(strips)} strip phrases to database.', True, True)
            self.displayed_database.write_data()
        else:
            self.set_window_status_to_canceled(True, True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_edit_strip_phrases(self) -> None:
        """Add, view (and, optionally, remove) the strip phrases that are associated with the
        current database.
        """
        def get_new_strip_phrase() -> Union[str, Literal[False]]:
            return simpledialog.askstring(title="Add phrase", prompt="Enter new phrase: ")

        data = gd.get_data_from_user(data_needed={
            'Phrases':
                {
                    'kind': gu.EditableListPane,
                    'validator': None,
                    'addfunc': get_new_strip_phrase,
                    'initial': list(self.displayed_database.settings['strip phrases']),
                    'tooltip':
                        'Add phrases to repeatedly strip from the beginnings and endings of filenames with the "Add" '
                        'button; remove these phrases with the "Delete" button. Also repeatedly stripped will be any '
                        'characters configured to strip on the "file handling" pane of the Preferences window.'}
                },
            window_title="Add/Remove Phrases to Strip")
        if data:
            self.displayed_database.settings['strip phrases'] = sorted(data['Phrases'])
            self.displayed_database.write_data()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_move_files_from_holding_directory(self) -> None:
        """Move all files (of appropriate type) from the designated holding directory into
        a directory specified by the user, and track them in the current database.
        """
        if not self.displayed_database.settings['holding directory']:
            if not gu.ask_yes_or_no('No holding directory is specified! Choose one now?',
                                    title='Specify Holding Directory?', parent=self):
                self.set_window_status('Importing files from holding directory canceled.', True, True)
            self.do_specify_holding_directory()
            if not self.displayed_database.settings['holding directory']:
                self.set_window_status('Importing files from holding directory canceled.', True, True)

        sort_types = {
            'By file date': lambda item: utils.get_mtime(Path(item)),
            'Alphabetic': lambda item: Path(item).resolve().name,
            'Random': lambda item: random.random(),
            'Folder by folder': str,
            'By video play time': lambda item: utils.get_video_play_length(Path(item)),
            'By file size': lambda item: utils.get_file_size(Path(item)),
            'By video frame size': lambda item: utils.get_video_frame_size(Path(item)),
        }

        data = gd.get_data_from_user(
            {
                'Directory to move files to':
                    {'kind': 'pathdir', 'validator': gd.directory_exists_validator,
                     'tooltip':
                         'The directory to move the files into.'},
                'separator 2': {'kind': gd.Separator, },
                'Auto-generate new filename':
                    {'kind': bool, 'validator': None, 'initial': True,
                     'tooltip':
                         'If checked, VideoDataStore will generate a unique name for files relative to the directory '
                         'into which they\'re being moved.'},
                'Remove strip phrases from file names':
                    {'kind': bool, 'validator': None, 'initial': True,
                     'tooltip':
                         'If checked, VideoDataStore will remove any "strip phrases" you have configured from the '
                         'beginnings and ends of filenames (as well as any strippable characters configured in the '
                         'Preferences dialog). "Strip phrases" can be configured by choosing "Add/Remove phrases to '
                         'strip" or "Import phrases to strip" from the "Import from holding directory" submenu of the '
                         'File menu.'},
                'Manually approve file names during move':
                    {'kind': list, 'validator': None, 'initial': ['Conflicts only', 'All', 'None'],
                     'tooltip':
                         'If "All" is chosen, VideoDataStore will ask you to approve the name for each file before '
                         'moving it into the chosen directory and adding it to the database (after cleaning strip '
                         'phrases and ensuring uniqueness, if those options are checked) . If "None" is chosen, '
                         'VDS will honor the other choices you have made relating to filename changes, but will not '
                         'give you the opportunity to approve the resulting file name, and may overwrite existing files'
                         ' if "Auto-generate new filenames" is not chosen. If "Conflicts only" is chosen, you will only'
                         ' be asked to approve the new filename if the new filename would conflict with an existing '
                         'file name would conflict with an existing file in the same directory.'},
                'separator 3': {'kind': gd.Separator, },
                'Include video files':
                    {'kind': bool, 'validator': None, 'initial': self.displayed_database.settings['include video'],
                     'tooltip':
                         'Include video files when moving files out of the holding directory. If this is checked and '
                         'the database is not already tracking video files, you will be asked if you want the database '
                         'to start tracking video files, and if so, known folders will be rescanned for video files.'},
                'Include audio files':
                    {'kind': bool, 'validator': None, 'initial': self.displayed_database.settings['include audio'],
                     'tooltip':
                         'Include audio files when moving files out of the holding directory. If this is checked and '
                         'the database is not already tracking audio files, you will be asked if you want the database '
                         'to start tracking audio files, and if so, known folders will be rescanned for video files.'},
                'Include other files':
                    {'kind': bool, 'validator': None, 'initial': self.displayed_database.settings['include others'],
                     'tooltip':
                         'Include non-audio, non-video files when moving files out of the holding directory. If this '
                         'is checked and the database is not already tracking non-audio, non-video files, you will be '
                         'asked if you want the database to start tracking non-audio, non-video files, and if so, '
                         'known folders will be rescanned for non-audio, non-video files.'},
                'separator 4': {'kind': gd.Separator, },
                'Incrementally tag files after moving':
                    {'kind': bool, 'validator': None, 'initial': True,
                     'tooltip':
                         'If checked, after importing all files from the holding directory, VideoDataStore will start '
                         'an incremental tagging process just to tag the imported files.'},
                'separator 1': {'kind': gd.Separator, },
                'Limit imported files':
                    {'kind': gd.DataGetter,
                     'initial': {
                         'number of files': {'kind': int, 'validator': gd.positive_integer_validator_func,
                                             'tooltip': 'Maximum number of files to import.'},
                         'import order': {'kind': list, 'initial': list(sort_types.keys()),
                                          'tooltip': 'How to choose the files to be imported.',},
                         'reverse order': {'kind': bool, 'initial': False,
                                           'tooltip': 'If checked, import the files in reverse order.',}}
                     },
            })
        if not data:
            self.set_window_status_to_canceled(True)
            return

        tracking_settings_modified = False
        for dialog_key, settings_key, textual_desc in (('Include video files', 'include video', 'video'),
                                                       ('Include audio files', 'include audio', 'audio'),
                                                       ('Include other files', 'include others',
                                                        'non-audio, non-video files')):
            if data[dialog_key] and not self.displayed_database.settings[settings_key]:
                if gu.ask_yes_or_no(f"You checked '{dialog_key}', but this database is not currently tracking {textual_desc} files. Do you want to start tracking {textual_desc} files? (Answering 'no' will skip this time, too.)", title="Track New Files?", parent=self):
                    tracking_settings_modified, self.displayed_database.settings[settings_key] = True, True
                    self.set_window_status(f'Database is now tracking {textual_desc} files.', True, True)
        if tracking_settings_modified:
            self.set_window_status('Scanning for files of newly specified types.', True, False)
            self.do_scan_for_new_files()
            self.status_bar.queue_default_text()

        dir_to_move_files_to = Path(data['Directory to move files to'])
        files_to_move = list()
        for f in Path(self.displayed_database.settings['holding directory']).glob('*'):
            if f.suffix.lower() in pref('known_video_types') and data['Include video files']:
                files_to_move.append(f)
            elif f.suffix.lower() in pref('known_audio_types') and data['Include audio files']:
                files_to_move.append(f)
            elif data['Include other files']:
                files_to_move.append(f)

        if not files_to_move:
            self.set_window_status('No files to move!', True, True)

        if data['Remove strip phrases from file names']:
            phrases = self.displayed_database.settings['strip phrases']
        else:
            phrases = None

        if data['Limit imported files']:
            file_count_lim = data['Limit imported files']['number of files']
            files_to_move.sort(key=sort_types[data['Limit imported files']['import order']],
                               reverse=data['Limit imported files']['reverse order'])
        else:
            file_count_lim = None

        if data['Manually approve file names during move'] == 'All':
            proc = gu.confirm_each_file_renaming_proc
        elif data['Manually approve file names during move'] == 'Conflicts only':
            proc = gu.confirm_before_overwriting_renaming_proc
        elif data['Manually approve file names during move'] == 'None':
            proc = None
        else:
            raise RuntimeError("Somehow got an invalid value for the 'Manually approve file names during move' option!")

        def do_del(full_path: Path):
            rel_path = utils.relative_to_with_name(self.displayed_database.file_location, full_path)
            self.video_list.scroller.the_list.delete(str(rel_path))

        moved_files = mh.move_files_from_holding_dir(files_to_move=files_to_move,
                                                     dir_to_move_files_to=dir_to_move_files_to,
                                                     which_database=self.displayed_database,
                                                     strip_phrases=phrases,
                                                     auto_rename=data['Auto-generate new filename'],
                                                     renaming_proc=proc,
                                                     parent_window=self,
                                                     max_files_moved=file_count_lim,
                                                     sort_func=None,
                                                     replace_func=do_del)

        if moved_files and data['Incrementally tag files after moving']:
            if self.subwindows['incremental tag']:
                if not gu.confirm_prompt("Tagging the imported files will end the incremental tagging operation "
                                         "already in progress. Do you want to do that?",
                                         parent=self):
                    return
                self.subwindows['incremental tag'].done_tagging()
            self.subwindows['incremental tag'] = subwindows.MultiFileTaggingWindow(files_to_tag=moved_files,
                                                                                   db=self.displayed_database,
                                                                                   num_already_tagged_files=0,
                                                                                   parent_window=self)
        self.rebuild_list()     # FIXME: would be better to add files to list as they are added to the DB.

    def do_prefs(self) -> None:
        """Create and display a Preferences dialog box. If there already is a Preferences
        dialog box, raise it to the top of the window stack instead of creating a new
        one,
        """
        if not self.subwindows['preferences']:
            self.subwindows['preferences'] = pwin.PrefsWindow()
        self.subwindows['preferences'].lift()

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    @gu.textbox_redirector()
    def do_scan_new_directory(self) -> None:
        """Ask the user for a directory to scan recursively for files, then scan it
        recursively, adding files found to the database.
        """
        dir = filedialog.askdirectory(title="Directory to add", parent=self)
        if dir:
            old_len = len(self.displayed_database)
            self.displayed_database.populate_all_data(which_folder=Path(dir), recurse=True,
                                                      include_audio_files=self.displayed_database.settings['include audio'],
                                                      include_video_files=self.displayed_database.settings['include video'],
                                                      include_other_files=self.displayed_database.settings['include others'])
            self.rebuild_list()         # FIXME: would be better to add files to list as they are added to DB.
            self.set_window_status(f"Added {len(self.displayed_database) - old_len} files!", True, True)
        else:
            self.set_window_status_to_canceled(immediate=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_export_JSON(self) -> None:
        """Export the database in JSON format.
        """
        file_to_save = filedialog.asksaveasfilename(parent=self)
        if file_to_save:
            self.set_window_status(f"Exporting database to {file_to_save} ...", True, False)
            self.displayed_database.export_metadata(Path(file_to_save))
            self.set_window_status(f"Exported database to {file_to_save}.", True, True)
        else:
            self.set_window_status_to_canceled(immediate=True)

    @gu.only_if_DB_loaded
    @gu.trap_and_report_errors
    def do_merge_exported_JSON(self) -> None:
        """Merge an exported JSON file into the database.
        """
        file_to_merge = filedialog.askopenfilename(parent=self)
        if file_to_merge:
            oldlen = len(self.displayed_database)
            self.displayed_database.import_metadata(Path(file_to_merge))
            self.rebuild_list()
            self.set_window_status(f"Added {len(self.displayed_database) - oldlen} files by merging {file_to_merge}", True, True)
        else:
            self.set_window_status_to_canceled(immediate=True)

    @gu.trap_and_report_errors
    @gu.textbox_redirector(window_title='Debug output')
    def do_current_debugging_command(self) -> None:
        """A frequently rewritten harness that just calls whatever code is needed to help
        debug whatever's currently being debugged.
        """
        self.video_list.scroller.change_item_iid(self.video_list.scroller.the_list.selection()[0], 'HELLO')
        # print('Here is a breakpoint, do with it as you will')

    def make_top_level_menu(self) -> None:
        """Set up the top-level menus, linking them to callback handlers.
        """
        menubar = tk.Menu(self)

        if utils.running_under_macos():
            # handle macOS-specific menu stuff; the ".apple" (actually, application) menu should appear first
            apple_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'), name="apple")
            apple_menu.add_command(label=f'About {utils.__progname__} ...', command=gu.do_about_box)
            apple_menu.add_command(label=utils.__progname__ + ' website ...', command=gu.open_program_website)
            apple_menu.add_separator()
            menubar.add_cascade(menu=apple_menu)

            self.createcommand('tk::mac::ShowPreferences', self.do_prefs)

        file_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'))
        file_menu.add_command(label="New", command=self.do_new, accelerator="Ctrl+N")
        file_menu.bind_all("<Control-n>", self.do_new)
        file_menu.add_command(label="Open...", command=self.do_open, accelerator="Ctrl+O")
        file_menu.bind_all("<Control-o>", self.do_open)
        file_menu.add_command(label="Save", command=self.do_save, accelerator="Ctrl+S")
        file_menu.bind_all("<Control-s>", self.do_save)
        file_menu.add_separator()
        file_menu.add_command(label="Revert", command=self.do_revert)
        file_menu.add_command(label="Export metadata as JSON ...", command=self.do_export_JSON)
        file_menu.add_command(label="Merge exported metadata ...", command=self.do_merge_exported_JSON)
        file_menu.add_separator()
        file_menu.add_command(label="Re-scan known directories for new files ...",
                              command=self.do_scan_for_new_files)
        file_menu.add_command(label="Prune missing files from database ...", command=self.do_prune_missing_files)
        file_menu.add_command(label="Force-update database info to current file status ...",
                              command=self.do_force_update_file_status)
        file_menu.add_command(label="Rename files in database to clean names ...", command=self.do_clean_db_filenames)
        file_menu.add_separator()

        tracked_dirs_menu = tk.Menu(file_menu, tearoff=pref('allow-tearoffs'))
        tracked_dirs_menu.add_command(label="Track new directory, adding files ...",
                                      command=self.do_scan_new_directory)
        tracked_dirs_menu.add_separator()
        tracked_dirs_menu.add_command(label="Add/remove directories to ignore ...",
                                      command=self.do_edit_ignore_directories)
        file_menu.add_cascade(label="Track/ignore directories ...", menu=tracked_dirs_menu)
        file_menu.add_separator()

        holding_directory_menu = tk.Menu(file_menu, tearoff=pref('allow-tearoffs'))
        holding_directory_menu.add_command(label="Specify holding directory ...",
                                           command=self.do_specify_holding_directory)
        holding_directory_menu.add_command(label="Forget holding directory ...",
                                           command=self.do_forget_holding_directory)
        holding_directory_menu.add_separator()
        holding_directory_menu.add_command(label="Add/remove phrases to strip from imported files ...",
                                           command=self.do_edit_strip_phrases)
        holding_directory_menu.add_command(label="Import phrases to strip from text file ...",
                                           command=self.do_import_strip_phrases_from_file)
        holding_directory_menu.add_separator()
        holding_directory_menu.add_command(label="Move files from holding directory to ...",
                                           command=self.do_move_files_from_holding_directory)
        file_menu.add_cascade(label="Import from holding directory ...", menu=holding_directory_menu)
        file_menu.add_separator()

        file_menu.add_command(label="Move database ...", command=self.do_move_database)

        file_menu.add_command(label='Database info ...',
                              command=lambda *e: subwindows.edit_database_info(self), accelerator='Ctrl+I')
        file_menu.bind_all('<Control-i>', lambda *e: subwindows.edit_database_info(self))
        file_menu.add_separator()
        file_menu.add_command(label="Quit", command=self.do_quit, accelerator="Ctrl+Q")
        file_menu.bind_all("<Control-q>", self.do_quit)
        menubar.add_cascade(label="File", menu=file_menu)

        edit_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'))
        copy_menu = tk.Menu(edit_menu, tearoff=pref('allow-tearoffs'))
        copy_menu.add_command(label="File name", command=self.do_copy_name)
        copy_menu.add_command(label="File name and full path", command=self.do_copy_name_and_path)
        copy_menu.add_command(label="Tags as quoted string", command=self.do_copy_tags_as_quoted_string)
        copy_menu.add_command(label="All database info as JSON", command=self.do_copy_JSON_data)
        edit_menu.add_cascade(label="Copy", menu=copy_menu)

        edit_menu.add_separator()
        select_menu = tk.Menu(edit_menu, tearoff=pref('allow-tearoffs'))
        # This next cannot yet be bound: the VideoScroller does not yet exist. MainWindow.__init__() binds it.
        select_menu.add_command(label="Select All", command=self.do_sel_all, accelerator="Ctrl+A")
        select_menu.add_command(label="Select None", command=self.do_sel_none)
        select_menu.add_command(label="Invert Selection", command=self.do_inv_sel)
        edit_menu.add_cascade(label="Select", menu=select_menu)

        edit_menu.add_separator()
        edit_menu.add_command(label="Preferences ...", command=self.do_prefs)
        menubar.add_cascade(label="Edit", menu=edit_menu)

        processing_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'))
        processing_menu.add_command(label="Process scheduled files", command=self.do_process_videos)
        processing_menu.add_command(label="Show processing queue ...", command=self.show_processing_queue)
        processing_menu.add_separator()
        processing_menu.add_command(label="Re-containerize deprecated formats ...",
                                    command=self.do_recontainerize_deprecated)
        processing_menu.add_command(label="Schedule large-video downsizing ...",
                                    command=self.do_schedule_large_downsizing)
        processing_menu.add_command(label="Schedule high-bitrate downsizing ...",
                                    command=self.do_schedule_hi_bitrate_downsizing)
        processing_menu.add_command(label="Schedule extra stream elimination ...",
                                    command=self.do_schedule_spurious_stream_elimination)
        processing_menu.add_separator()

        codec_info_menu = tk.Menu(edit_menu, tearoff=pref('allow-tearoffs'))
        codec_info_menu.add_command(label="Video codecs (for decoding)",
                                    command=lambda: self.do_display_codec_info('video', 'decode'))
        codec_info_menu.add_command(label="Video codecs (for encoding)",
                                    command=lambda: self.do_display_codec_info('video', 'encode'))
        codec_info_menu.add_command(label="Audio codecs (for decoding)",
                                    command=lambda: self.do_display_codec_info('audio', 'decode'))
        codec_info_menu.add_command(label="Audio codecs (for encoding)",
                                    command=lambda: self.do_display_codec_info('audio', 'encode'))
        processing_menu.add_cascade(label="FFmpeg codec info", menu=codec_info_menu)
        menubar.add_cascade(label="Processing", menu=processing_menu)

        utils_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'))

        tag_utils_menu = tk.Menu(utils_menu, tearoff=pref('allow-tearoffs'))
        tag_utils_menu.add_command(label="Incremental tagging ...", command=self.do_incremental_tag)
        tag_utils_menu.add_command(label="Tags in database ...", command=self.display_tags_window)
        tag_utils_menu.add_separator()

        tag_utils_marking_menu = tk.Menu(tag_utils_menu, tearoff=pref('allow-tearoffs'))
        tag_utils_marking_menu.add_command(label="... all files as incrementally tagged",
                                           command=self.do_mark_all_inc_tagged)
        tag_utils_marking_menu.add_command(label="... all files as not incrementally tagged",
                                           command=self.do_mark_all_not_inc_tagged)
        tag_utils_marking_menu.add_separator()
        tag_utils_marking_menu.add_command(label="... all files with any tags as incrementally tagged",
                                           command=self.do_mark_any_tagged_as_inc_tagged)
        tag_utils_marking_menu.add_command(label="... all files without any tags as not incrementally tagged",
                                           command=self.do_mark_untagged_as_not_inc_tagged)
        tag_utils_menu.add_cascade(label="Mark files ...", menu=tag_utils_marking_menu)
        utils_menu.add_cascade(label="Tagging utilities", menu=tag_utils_menu)

        selective_copy_menu = tk.Menu(utils_menu, tearoff=pref('allow-tearoffs'))
        selective_copy_menu.add_command(label="Copy selected files to ...", command=self.do_selective_copy)
        selective_copy_menu.add_command(label="Create symlinks to selected files ...",
                                        command=self.do_create_selected_symlinks)
        selective_copy_menu.add_separator()

        copy_script_menu = tk.Menu(selective_copy_menu, tearoff=pref('allow-tearoffs'))
        copy_script_menu.add_command(label="batch file (Windows)", command=self.do_create_windows_batch_file)
        copy_script_menu.add_command(label=".sh script (macOS/Linux/other Unix)", command=self.do_create_sh_script)

        selective_copy_menu.add_cascade(label='Create copying script', menu=copy_script_menu)
        utils_menu.add_cascade(label="Selective copying", menu=selective_copy_menu)

        utils_menu.add_separator()
        utils_menu.add_cascade(label="Plug-ins", menu=plugins.init_plugins(self, utils_menu))
        menubar.add_cascade(label="Utilities", menu=utils_menu)

        if pref('debugging'):
            debugging_menu = tk.Menu(menubar, tearoff=True)
            debugging_menu.add_command(label="Force-rebuild file list", command=self.rebuild_list)
            debugging_menu.add_command(label="Show preferences ...",
                                       command=lambda: gu.redirect_prints_to_text_box(pprint.pprint, dict(
                utils.prefs.data), indent=2))
            debugging_menu.add_separator()
            debugging_menu.add_command(label="Current test harness",
                                       command=lambda: subwindows.do_volume_change(self.entire_current_selection()[0],
                                                                                   which_db=self.displayed_database,
                                                                                   parent=self))
            menubar.add_cascade(label="Debugging", menu=debugging_menu)

        help_menu = tk.Menu(menubar, tearoff=pref('allow-tearoffs'), name='help')
        if not utils.running_under_macos():
            # under macOS, these items appear under the Apple menu.
            help_menu.add_command(label=f'About {utils.__progname__} ...', command=gu.do_about_box)
            help_menu.add_command(label=f'{utils.__progname__} website ...', command=gu.open_program_website)

        help_menu.add_command(label=f"{utils.__progname__} documentation ...", command=gu.not_implemented_GUI)  #FIXME!
        if utils.running_under_macos():
            menubar.add_cascade(menu=help_menu)    # FIXME: do we actually need to omit the label= parameter on macOS?
            self.createcommand('tk::mac::ShowHelp', gu.not_implemented_GUI)     #FIXME!
            self.createcommand('tk::mac::Quit', self.do_quit)

        else:
            menubar.add_cascade(label="Help", menu=help_menu)

        # macOS requires that certain menus be added to the menubar before the menubar is attached to the window,
        # which is why we've waited this long to attach it.
        self.config(menu=menubar)


def setup(main_window_datafile: Optional[Path] = None) -> MainWindow:
    """Do all required setup before the main program runs. Mostly, this means finding
    and opening the relevant database, possibly one specified on the command line,
    and, if the command-line file(s) aren't a database, travels upwards from the
    first specified file to try to find a database that can contain information for
    the files on the command line. If it doesn't, it quits with an error; if it
    does, it pops up a FileInfoWindow for each CLI-specified file, adding it to the
    database if necessary.

    #FIXME: future plans: should we be more careful about whether the relevant files
    come from different folders? Should we offer to create a database for them if we
    can't find one?
    """
    if main_window_datafile:
        assert isinstance(main_window_datafile, Path)

    ret = MainWindow(database_to_display=main_window_datafile)
    # any other setup goes here!
    return ret


def parse_args(args: Sequence[str]) -> MainWindow:
    """Parse the command-line arguments, creating the main window if a database can be
    found, and searching upward in the tree for databases if the command-line
    arguments don't immediately specify the database. If a database is specified or
    found, it is opened in the main window; if any non-database files are specfied
    and a database is found, FileInfoWindows are opened for each, adding them to the
    found database as necessary. If files are specified and a database is not, and a
    database cannot be found, quits with an error.

    Only one database can be specified in a single invocation. Attempting to specify
    a second database from the command line after already specifying one will cause
    the program to quit with an error. All other files specified will have an info
    window opened for them, adding them to the single specified database if necessary.
    """
    parser = argparse.ArgumentParser(description='GUI for VideoDataStore, a utility program to manage a collection of videos.')
    parser.add_argument('file', nargs='+', metavar='FILE', type=Path,
                        help='files to open; at most one can be a database, and it has to come first')
    args = parser.parse_args(args)
    params = list(args.file)
    if not params:
        return setup()

    first = params.pop(0)       # If a database is specified, it must be the first item.

    try:
        _ = mh._unpack_database(first)
        del _               # Could be a big chunk of memory, might was well free it as rapidly as possible.
        db, guessed = first, False
    except (Exception,):
        db, guessed = mh.find_metadata_file(first), True
        # And now put the argument back into the PARAMS list: we need to pop up a window for it!
        params = [first] + params

    files = list()
    while params:
        current = params.pop(0)
        try:
            if mh._unpack_database(current):        # FIXME! This is a shabby proxy: uses "Can de-jsonify (or maybe unpickle) a bzipped data file" to determine if something is a VDS database!
                print(f"ERROR! Only one database can be specified on the command line.\n{current} is the second database specified.")
                if guessed:
                    print(f"    (Note: the first database was {db}, which was inferred automatically from your first command-line argument, {first}.")
                    print(f"    (If this is incorrect, try specifying the database explicitly as the first parameter.)")
                sys.exit(3)
        except (Exception,):        # mh._unpack_database can raise several errors if the file is not a database.
            pass                    # All of which we ignore, because we only accept non-database files at this point.
        files.append(current)

    # we held off from opening the window earlier so we can open zero windows if we're going to call sys.exit(3).
    ret = setup(main_window_datafile=db)
    while files:
        f = files.pop(0)
        assert isinstance(f, Path)
        ret.scheduler.queue_task(ret.open_file_info_window, which_file=f)

    return ret


if __name__ == "__main__":
    force_test = False
    if force_test:
        parse_args([  # '/home/patrick/Photos/.metadata.dat',
            '/home/patrick/Photos/2019-04-16/Failure - "Counterfeit Sky".MP4',
            '/home/patrick/Photos/2022-08-27/20220827_213524.mp4',
            '/home/patrick/Photos/2022-08-27/20220827_210541.mp4',
        ]).mainloop()
    elif len(sys.argv) == 1:                                  # no file specified
        setup().mainloop()
    else:
        parse_args(sys.argv[1:]).mainloop()                 # parse_args will call & return the result of setup()
