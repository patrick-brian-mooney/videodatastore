#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Command-line interface to the batch-processing functionality of
VideoDataStore.

Makes a single batch-processing pass through all files references by
METADATA_FILE that need to be batch-processed. May not fully batch-process all
files: if an error is encountered, VideoDataStore will attempt to adjust the
parameters so that it is more likely to succeed next time. It does not,
however, automatically make another pass through the files referenced by the
database that still need processing.

This program is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import argparse
import time
import sys

from pathlib import Path
from typing import List


import vds.metadata_handling as mh
import vds.utils as utils


def parse_args(args: List[str]) -> None:
    """Parse command-line arguments and kick off the batch-processing operation.
    """
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('--version', '--vers', '--ver', action='version', version=f'{utils.__progname__} {utils.__version__}, running under Python {sys.version}', help="Display version information and exit.")
    parser.add_argument('--status', '--stat', action='store_true', help="Display batch-processing status for the specified database and quit without doing any processing")

    parser.add_argument('--quit-after', '--quit', '-q', action='store', help="Check whether at least this many seconds have passed after each file is processed, and if so, stop processing.", type=float)
    parser.add_argument('--max-success', '--max', '-m', action='store', help="Stop processing after this many successes.", type=int)
    parser.add_argument('--prune-first', '--prune', '-p', action='store_true', help="Prune missing files from the database before processing.")
    parser.add_argument('filename', type=Path, help="The database storing information about the files to be processed.")
    args = parser.parse_args(args)

    print(f"\n\nOpening {args.filename} ...")
    data = mh.MetadataStore(file_location=args.filename)
    print('... opened!\n')

    if args.status:
        print(f"\nDatabase: {data.file_location}")
        print(f"Real pathway: {data.file_location.resolve()}")
        print(f"Files tracked: {len(data)}")
        print(f"Files waiting to be processed: {data.num_videos_to_process}")
        sys.exit()

    if args.prune_first:
        print("Pruning missing files ...")
        data.prune_missing_files()

    start_time = time.monotonic()
    success, fail = mh.process_videos(data, max_videos_to_process=args.max_success,
                                      allotted_processing_seconds=args.quit_after)
    print(f"\n\n... finished!\n{success} files successfully processed (unable to process {fail} files)")
    print(f"... processing took {(time.monotonic() - start_time)/60} minutes.\n\n\n")

    if data.writer:
        print("Waiting for save to finish ...")
        data.writer.join()


if __name__ == "__main__":
    if False:       # quick-and-dirty test harness
        parse_args(["--prune", """path to a MetadataStore"""])
        sys.exit()

    parse_args(sys.argv[1:])
