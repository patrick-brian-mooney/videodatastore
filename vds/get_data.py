#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Provides utility GUI code that assists in prompting the user for information
for the VideoDataStore program. Primarily, this code consists of
get_data_from_user(), a function that designs a dialog box to spec, based on
parameters that are passed in; and of the DataGetter class, a flexible pane
that is built to spect to be in that dialog box.

The module also includes utility functions and classes for working with
DataGetter panes, whether explicitly created or created automatically by
get_data_from_user(), and some widgets that are largely intended for the
benefit of DataGetter panes, but are also useful elsewhere.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import collections
import os
import random
import shlex
import textwrap
import warnings

import tkinter as tk
import tkinter.filedialog as filedialog
import tkinter.messagebox as tk_msgbox
import tkinter.simpledialog as simpledialog
import tkinter.ttk as ttk

from pathlib import Path
from typing import Any, Callable, Collection, Dict, Iterable, List, Literal, Optional, Type, Union

import vds.gui_utils as gu
import vds.ffmpeg_utils as fu
import vds.metadata_handling as mh

import vds.utils as utils
from vds.utils import pref


# Utility functions for get_data_from_user(), below, for validation, user-input massaging, and small-scale data entry.
def do_get_suffix() -> str:
    """Ask the user to enter a file suffix, which must be a string starting with a dot.
    Keep annoying the user until we get a string starting with a dot. Whitespace is
    automatically stripped from both ends.
    """
    ret = None
    while not ret:
        val = simpledialog.askstring(title="Extension", prompt="Enter a file extension.")
        if is_valid_extension(val):
            ret = val.strip()

    return ret


def sample_validator_func(value: Any) -> bool:
    """Accepts any VALUE passed to it. By the time this has been called, VALUE will
    already be known to be of the appropriate type. Returns True if the value is
    acceptable, or False if it is not.
    """
    return True


def is_valid_extension(value: Any) -> bool:
    """Return True if VALUE is a string that specifies a valid file extension, or False
    otherwise.
    """
    try:
        assert isinstance(value, str)
        assert value.strip().startswith('.')
        assert len(value.strip()) > 1
    except (AssertionError,):
        gu.error_message_box(f"A file extension needs to start with a dot and contain at least one more character!",
                             None)
        return False
    return True


def ffmpeg_preset_validator_func(value: Any) -> bool:
    try:
        if not isinstance(value, str):
            value = str(value)
        assert value.strip().casefold() in fu.get_ffmpeg_presets()
    except (AssertionError, ValueError, TypeError,):
        poss = utils.oxford_comma_list_of(fu.get_ffmpeg_presets(), quote_char='"')
        gu.error_message_box(f'The value "{value}" is not a valid ffmpeg preset!',
                             f"""Allowable presets are {poss}.""")
        return False
    return True


def is_valid_encoding_codec(value: Any, kind: Literal['audio', 'video', 'subtitle', 'all']) -> bool:
    try:
        if not isinstance(value, str):
            value = str(value)
        assert value.strip().casefold() in [i[1].strip().casefold() for i in fu.get_codecs(kind, 'encode')]
    except (AssertionError, ValueError, TypeError,):
        poss = utils.oxford_comma_list_of([i[1] for i in fu.get_codecs(kind, 'encode')],
                                          quote_char='"')
        gu.error_message_box(f'The value "{value}" is not a {kind} encodingcodec for the installed FFmpeg!',
                             f'Allowable codec names are {poss}.')
        return False
    return True


def window_geometry_validator(value: Any) -> bool:
    """A valid Tkinter window geometry string has the format "wxh±x±y".
    """
    try:
        assert isinstance(value, str), f"The value {value} is of type {value.__class__.__name__}, not a string!"
        assert 'x' in value, f"A valid geometry must contain an 'x', but the value {value} does not contain one!"
        assert '+' in value or '-' in value, (f"Valid geometries must include numbers separated by plus (+) or (-), but"
                                              f" value {value} contains neither!")

        value = value.strip().casefold()
        x_loc = value.index('x')
        seps = list()
        for sep in ('+', '-'):
            if sep in value:
                for i, c in enumerate(value[1 + x_loc:], start=(1 + x_loc)):
                    if c == sep:
                        seps.append(i)

        assert len(seps) == 2, (f"A valid geometry must have exactly two numbers preceded by plus (+) and minus (-) "
                                f"signs, but the value {value} has {len(seps)}!")
        seps.sort()
        assert x_loc < seps[0], (f"A valid geometry needs to place its plus (+) and minus (-) signs AFTER the numbers "
                                 f"separated by the 'x', but the value {value} does not honor this rule!")
        assert (seps[0] - x_loc) >= 2, (f"A valid geometry must have at least one digit between its 'x' character and "
                                        f"the first plus (+) or minus (-) sign!")
        assert (seps[1] - seps[0]) >= 2, (f"A valid geometry must have at least one digit between its two plus (+) "
                                          f"and/or minus (-) signs, but the value {value} does not honor this rule!")
        assert (seps[1] + 1) != len(value), f"A valid geometry cannot end with a plus (+) or minus (-) sign!"

    except (AssertionError, ValueError, TypeError) as errrr:
        gu.error_message_box(f'The value "{value}" is not a valid geometry description!', errrr)
        return False
    return True


def acceptable_audio_bitrate_validator_func(value: Any) -> bool:
    """Determines whether VALUE can be legitimately passed to ffmpeg as a plausible
    audio bitrate for resampling.

    Currently just uses a large list of acceptable values reported by the aac
    encoder for one version of ffmpeg.
    """
    allowed_values = fu.get_audio_codec_encoding_sample_rates(pref('default_audio_codec'))
    try:
        val = int(value)
        assert abs(val - float(value)) < 0.000001, "The audio bitrate must be an integer."
    except (ValueError, AssertionError, TypeError):
        gu.error_message_box(f"The audio bitrate must be an integer.", None)
        return False
    if val in allowed_values:
        return True
    else:
        gu.error_message_box(f"The only legal values for audio bitrates are {allowed_values}.", None)
        return False


def positive_integer_validator_func(value: Any) -> bool:
    """Determines whether VALUE is a positive integer, or can be type-coerced into
    a positive integer.
    """
    try:
        assert int(value) > 0
        assert abs(float(value) - int(value)) < 0.00000001
        return True
    except (ValueError, AssertionError) as errrr:
        gu.error_message_box(f"The value {value} must be a positive integer.", errrr)
        return False


def bounded_integer_validator_factory(lower: int, upper: int) -> Callable:
    """A factory function that returns functions that validate that the provided VALUE
    is an integer between LOWER and UPPER, inclusive.
    """
    def on_call(value: Any) -> bool:
        try:
            if not isinstance(value, int):
                value = int(value)
            if not (lower <= value <= upper):
                raise ValueError
        except (ValueError, TypeError) as errrr:
            gu.error_message_box(explanatory_text=f"The value {value} is not valid! It must be an integer between "
                                                  f"{lower} and {upper}.", system_error=errrr)
            return False
        return True

    return on_call


def bounded_float_validator_factory(lower: float, upper: float) -> Callable:
    """A factory function that returns functions that validate that the provided VALUE
    is a float between LOWER and UPPER, inclusive.
    """
    def on_call(value: Any) -> bool:
        try:
            if not isinstance(value, (int, float)):
                value = float(value)
            if not (lower <= value <= upper):
                raise ValueError
        except (ValueError, TypeError) as errrr:
            gu.error_message_box(explanatory_text=f"The value {value} is not valid! It must be between {lower} "
                                                  f"and {upper}.", system_error=errrr)
            return False
        return True

    return on_call


def nonnegative_integer_validator_func(value: Any) -> bool:
    """Like the above, but allows zero, as well.
    """
    try:
        assert int(value) >= 0
        assert abs(float(value) - int(value)) < 0.00000001
        return True
    except (ValueError, AssertionError) as errrr:
        gu.error_message_box(f"The value {value} must be a non-negative integer.", errrr)
        return False


def positive_number_validator_func(value: Any) -> bool:
    """Determines whether VALUE is a positive number (int or float), or can be coerced
    to be one.
    """
    try:
        assert float(value) > 0
        return True
    except (AssertionError, ValueError) as errrr:
        gu.error_message_box(f"The value {value} is not a positive number.", errrr)
        return False


def nonnegative_number_validator_func(value: Any) -> bool:
    """Like the above, but allows zero, too.
    """
    try:
        assert float(value) >= 0
        return True
    except (AssertionError, ValueError) as errrr:
        gu.error_message_box(f"The value {value} is not a positive number.", errrr)
        return False


def file_exists_validator(value: Any) -> bool:
    """Determine whether VALUE is a path to a regular file that actually exists (or a
    symlink to one). If this is not the case, complain to the user.
    """
    try:
        p = value if (isinstance(value, Path)) else Path(value)
        assert p.exists()
        assert p.is_file()
        return True
    except (TypeError, AssertionError) as errrr:
        gu.error_message_box(f"The value {value} is not a path to an existing file.", errrr)
        return False


def directory_exists_validator(value: Any) -> bool:
    """Determine whether VALUE is (or "effectively is," as in the case of symlinks) a
    Path to a real directory that actually exists, or can be type-coerced to one.
    """
    try:
        p = Path(value)
        assert p.exists()
        assert p.is_dir()
        return True
    except (TypeError, AssertionError) as errrr:
        gu.error_message_box(f"The value {value} is not a path to an existing directory.", errrr)


def executable_file_validator(value: Any) -> bool:
    """Determine whether VALUE is a path to an actually existing regular file that is
    executable, or a symlink to one.
    """
    if not file_exists_validator(value):            # Will have reported an error already.
        return False
    if not os.access(str(value), os.X_OK):
        gu.error_message_box(f"The file {value} does not seem to be an existing program!", None)
        return False
    return True


def blank_or_executable_file_validator(value: Any) -> bool:
    """Returns True if VALUE is a str/Path representing a path to an executable file,
    or if, when whitespace is stripped from VALUE, it is an empty string; returns
    False otherwise. Handy for dialogs that allow paths to be optiona, e.g. for
    optional files.
    """
    if isinstance(value, Path):
        value = str(value)
    if isinstance(value, str):
        if not value.strip():
            return True
    return executable_file_validator(value)


def blank_or_positive_int_validator(value: Any) -> bool:
    """Validates values for "must be a positive integer or None, which is indicated as
    blank entry" field.
    """
    if not str(value).strip():
        return True
    return positive_integer_validator_func(value)


def blank_or_nonnegative_int_validator(value: Any) -> bool:
    """Like the above, but also allows zero, in addition to None.
    """
    if not str(value).strip():
        return True
    return nonnegative_integer_validator_func(value)


def fill_selected_file_path_button_factory(text_field: Union[tk.Entry, ttk.Entry, gu.WrappedEntry]) -> Callable:
    """Factory function that produces a function to be used by a "Choose File" button
    in a DataGetter pane. The returned function, when called, will pop up a file
    opening dialog and, if a file is selected, fill its path in the TEXT_FIELD Entry
    field. TEXT_FIELD is saved in the outer scope of the returned function.
    """
    def wrapped() -> None:
        ret = filedialog.askopenfilename(title="Choose a file")
        if ret:
            text_field.delete(0, tk.END)
            text_field.insert(0, ret)
    return wrapped


def fill_selected_directory_path_button_factory(text_field: Union[tk.Entry, ttk.Entry, gu.WrappedEntry]) -> Callable:
    """Factory function that produces a function to be used by a "Choose Directory"
    button in a DataGetter pane. The returned function, when called, will pop up a
    directory-selection dialog and, if a file is selected, fill its path in the
    TEXT_FIELD Entry.
    """
    def wrapped() -> None:
        ret = filedialog.askdirectory(title="Which directory?")
        if ret:
            text_field.delete(0, tk.END)
            text_field.insert(0, ret)
    return wrapped


def none_or_str(value: Any) -> Union[str, None]:
    """If VALUE is an empty string, after stripping whitespace, returns None; otherwise
    returns VALUE, after stripping leading and trailing whitespace. This is handy
    when processing user input in the Prefs dialog, which sometimes wants a None
    value for "no entry," which a user can enter by leaving a field blank.
    """
    value = value if isinstance(value, str) else str(value)
    if not value.strip():
        return None
    else:
        return value.strip()


def none_or_int(value: Any) -> Union[int, None]:
    """If VALUE is (effectively, in the sense of none_or_str(), above) blank, returns
    None; otherwise, tries to return an integer from VALUE. Throws ValueError if the
    conversion cannot be made.
    """
    if (value if isinstance(value, str) else str(value)).strip():
        return int(value)
    else:
        return None


# DataGetter is a utility class that creates panes for requesting data from the user; it is primarily used by
# get_data_from user(), but also by the prefs dialog.
class DataGetter(ttk.Frame, gu.AbstractUnifiedWidget):
    """A Frame full of widgets that make up a dialog box for getting information from
    the user. Needs to be put into a dialog window, of course; get_data_from_user(),
    below, does that and handles the resulting user actions.
    """
    defaults_template = {'validator': None, 'masseuse': None, 'tooltip': None}

    def __init__(self, parent: Union[Type[ttk.Frame], Type[tk.Toplevel], Type[tk.Tk]],
                 data_needed: Dict[str, Any],
                 *pargs,
                 do_validate_defaults: bool = False,
                 **kwargs):
        """A Frame containing widgets that, together, prompt the user for the data_needed.
        A PARENT Frame must be passed in, and a data_needed dictionary, which has the
        same structure as is documented in the helper function get_data_from_user(),
        below.

        Creates the widgets by iterating over the (key, data) pairs in
        self.data_needed and constructs a form requesting that information.

        The supplied default values are validated iff DO_VALIDATE_DEFAULTS is True.
        This is a bad idea in general because there are times when the database
        stores, for instance, "???" instead of an integer because ffmpeg or
        os.stat() can't determine a real value, and we need the dialog-box UI to not
        barf at the data when this happens.

        We have a four-column layout here:

        Most datatypes:
            0   1               2               3
            (text label)        | <-     widget     -> |

        For file-choosing layouts:
            0   1               2               3
            (text label)        path-as-string  file-/directory-choice button

        For checkboxes (representing type `bool`):
            0   1               2               3
        (blank) checkbox (which is its own label, stretching through columns 1-3)

        For Separator:
            0   1               2               3
        (blank) (a ttk.Separator across both)   (blank)

        For embedded DataGetter panes:
            0   1               2               3
        (blank) (the embedded pane, spread across three columns)
        """

        assert data_needed, "No data requested from DataGetter.__init__()!"
        ttk.Frame.__init__(self, parent, *pargs, **kwargs)
        self.data_needed = data_needed

        # create the widgets for the DataGetter.
        grid = ttk.Frame(self)
        grid.pack(side=tk.LEFT, fill=tk.BOTH, expand=tk.YES)
        focus_given = False
        for row_num, (name, constraints) in enumerate(self.data_needed.items()):
            constraints = dict(collections.ChainMap(constraints, DataGetter.defaults_template))
            if constraints['tooltip']:
                assert isinstance(constraints['tooltip'], str)
            if 'addfunc' in constraints:
                assert constraints['kind'] == gu.EditableListPane, (f"ERROR! An addfunc parameter was specified to the "
                                                                    f"DataGetter constructor for the prompt '{name}', "
                                                                    f"but this prompt is supposed to be of type "
                                                                    f"{constraints['kind']}, not type "
                                                                    f"EditableListPane!")

            if constraints['kind'] not in (bool, DataGetter, Separator):    # these don't get a label
                label_field = ttk.Label(grid, text=(name.strip() + ": " if name.strip() else name.strip()))
                label_field.grid(column=0, row=row_num, sticky=(tk.E + tk.N + tk.S), columnspan=2)

            if constraints['kind'] in (float, int):
                content_field = gu.WrappedEntry(grid, width=16)
                content_field.grid(column=2, row=row_num, sticky=tk.W, columnspan=2)
                if not focus_given:
                    content_field.focus_set()
                    focus_given = True

            elif constraints['kind'] == str:
                content_field = gu.WrappedEntry(grid, width=40)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E), columnspan=2)
                if not focus_given:
                    content_field.focus_set()
                    focus_given = True

            elif constraints['kind'] == bool:
                ttk.Label(grid, text='     ').grid(column=0, row=row_num)
                content_field = gu.WrappedCheckbutton(grid, text=name.strip())
                content_field.grid(column=1, row=row_num, sticky=tk.W, columnspan=3)

            elif constraints['kind'] == 'text':
                content_field = gu.WrappedLabel(grid, text=constraints['initial'])
                content_field.grid(column=2, row=row_num, sticky=tk.W, columnspan=2)

            elif constraints['kind'] == Path:
                content_field = gu.WrappedEntry(grid, width=40)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E))
                choose_file_button = ttk.Button(grid, text="Choose ...",
                                                command=fill_selected_file_path_button_factory(content_field))
                choose_file_button.grid(column=3, row=row_num, sticky=(tk.W + tk.E))

            elif constraints['kind'] == 'pathdir':
                content_field = gu.WrappedEntry(grid, width=40)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E))
                choose_file_button = ttk.Button(grid, text="Choose ...",
                                                command=fill_selected_directory_path_button_factory(content_field))
                choose_file_button.grid(column=3, row=row_num, sticky=(tk.W + tk.E))

            elif constraints['kind'] == list:
                assert constraints['validator'] is None, (f"Item {name} requested from DataGetter is a ListBox, but was"
                                                          f" passed a VALIDATOR function, which is not allowed!")
                assert isinstance(constraints['initial'], Iterable), (f"Item {name} requested from DataGetter is "
                                                                      f"supposed to be a Combobox, but specified "
                                                                      f"Combobox options don't make sense")
                assert len(constraints['initial']) >= 1, (f"Item {name} is supposed to be a ListBox, but fewer than "
                                                          f"one option were provided to the constructor!")

                content_field = gu.WrappedCombobox(grid, height=min(5, len(constraints['initial'])),
                                                   exportselection=False)
                content_field['values'] = constraints['initial']
                content_field.state(['readonly'])
                content_field.bind('<<ComboboxSelected>>', lambda *args: content_field.selection_clear())
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E), columnspan=2)

            elif constraints['kind'] == gu.ProcessingParameterPane:
                content_field = gu.ProcessingParameterPane(grid)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E), columnspan=2)
                if not focus_given:
                    content_field.params_field.focus_set()
                    focus_given = True

            elif constraints['kind'] == gu.FileTaggerPane:
                content_field = gu.FileTaggerPane(grid, suppress_label=True)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.E), columnspan=2)
                if not focus_given:
                    content_field.params_field.focus_set()
                    focus_given = True

            elif constraints['kind'] == DataGetter:
                ttk.Label(grid, text='     ').grid(column=0, row=row_num)        # for spacing. Same as checkboxes.
                content_field = SwitchableDataGetter(parent=grid, checkbox_name=name,
                                                     data_needed=constraints['initial'])
                content_field.grid(column=1, row=row_num, sticky=(tk.W + tk.E), columnspan=3)

            elif constraints['kind'] == gu.EditableListPane:
                assert 'initial' in constraints, ("ERROR! DataGetter was asked to get an EditableListPane's worth of "
                                                  "items, but no initial values were provided!")
                addfunc = constraints['addfunc'] if ('addfunc' in constraints) else None
                content_field = gu.EditableListPane(parent=grid, items=[str(i) for i in constraints['initial']],
                                                    addfunc=addfunc)
                content_field.grid(column=2, row=row_num, sticky=(tk.W + tk.S + tk.E + tk.N), columnspan=2)

            elif constraints['kind'] == Separator:
                if 'validator' in constraints:
                    assert constraints['validator'] is None, (f"ERROR! Cannot supply a validation function for the "
                                                              f"Separator {name}!")
                if 'initial' in constraints:
                    assert constraints['initial'] is None, (f"ERROR! Tried to give a Separator an initial value of "
                                                            f"{constraints['initial']}!")
                content_field = Separator(grid, orient=tk.HORIZONTAL)
                content_field.grid(column=1, row=row_num, stick=(tk.W + tk.E), columnspan=2)

            else:
                raise RuntimeError(f"DataGetter was requested to get data of type {constraints['kind']} for field "
                                   f"{name}; no idea how to comply!")

            if constraints['tooltip']:  # convert tooltip text string to instantiated Tooltip object
                tooltip = '\n'.join(textwrap.wrap(constraints['tooltip'], width=pref('tooltip-width'),
                                                  replace_whitespace=False))
                constraints['tooltip'] = gu.Tooltip(content_field, tooltip)

            if ('initial' in constraints) and (constraints['initial'] is not None):
                if isinstance(constraints['initial'], str):
                    if isinstance(constraints['kind'], type):
                        if constraints['kind'] != str:
                            try:
                                _ = constraints['kind'](constraints['initial'])
                            except (ValueError,):
                                raise ValueError(f'DataGetter was requested to get data of type {constraints["kind"]} '
                                                 f'for label {name} but passed incompatible initial value '
                                                 f'{constraints["initial"]} of type '
                                                 f'{constraints["initial"].__class__.__name__}!')

                if do_validate_defaults:
                    if (constraints['validator']) and (not constraints['validator'](constraints['initial'])):
                        raise ValueError(f"DataGetter was passed default value {constraints['initial']} for label "
                                         f"{name}, but that value doesn't validate!")

                if constraints['kind'] == bool:
                    content_field._setval(bool(constraints['initial']))
                elif constraints['kind'] == list:
                    content_field.current(0)
                elif constraints['kind'] not in (gu.EditableListPane, 'text', DataGetter):
                    content_field.insert(0, str(constraints['initial']))

            constraints['widget'] = content_field       # keep a reference to the created widget
            self.data_needed[name] = constraints        # we've been working with a modified copy. Copy it back.

            # make non-Separator rows grow much more than Separator rows with window growth
            grid.rowconfigure(row_num, weight=4 if constraints['kind'] not in (Separator,) else 1)

        # middle columns can stretch; column 0 is a small amount of checkbox-spacing, and col 3 is a button.
        grid.columnconfigure(1, weight=1)
        grid.columnconfigure(2, weight=1)

    def validate_data(self) -> bool:
        """Validates that the data in the dialog constitutes an acceptable response. If it
        is, returns True. If it isn't, displays a message to the user and returns False.

        What "acceptable" means depends on the type of data being validated. At a minimum,
        it means that the data meets the promises made in the long docstring for the
        get_data_from_user() function, below, about what data types will be
        returned, and that any validator functions originally supplied in the
        data_needed dictionary validate the data.
        """
        for name, constraints in self.data_needed.items():
            try:
                if constraints['kind'] in (gu.FileTaggerPane, gu.ShellQuotedParametersPane, gu.EditableListPane,):
                    try:
                        _ = constraints['widget']._getval()
                    except (ValueError,) as errrr:
                        gu.error_message_box(explanatory_text=f'Cannot break the entered text '
                                                              f'"{constraints["widget"].params_field}" down into '
                                                              f'components.', system_error=errrr)
                        return False
                elif constraints['kind'] in ('pathdir',):
                    _ = Path(constraints['widget']._getval())
                elif constraints['kind'] == DataGetter:  # If the sub-pane doesn't validate, neither does its parent
                    if not constraints['widget'].validate_data():
                        return False
                elif constraints['kind'] not in (bool, list, 'text'):
                    _ = constraints['kind'](constraints['widget']._getval())
            except (ValueError,) as errrr:
                gu.error_message_box(explanatory_text=f"Parameter {name} needs to be of type "
                                     f"{constraints['kind'].__name__}!", system_error=errrr)
                return False
            except (BaseException,) as errrr:
                gu.error_message_box(explanatory_text="Unable to complete task!", system_error=errrr)
            if constraints['validator']:
                if not constraints['validator'](constraints['widget']._getval()):
                    return False
        return True

    def extract_data(self) -> Dict[str, Any]:
        """Extract the data encoded in the SELF pane and return it. By the time this
        function is called, the entered data has already been validated as acceptable
        by self.validate_data().
        """
        ret = dict()
        for name, constraints in self.data_needed.items():
            if constraints['kind'] in (list, 'text', gu.FileTaggerPane, gu.ProcessingParameterPane,
                                       gu.EditableListPane, DataGetter):
                data = constraints['widget']._getval()
            elif constraints['kind'] == 'pathdir':
                data = Path(constraints['widget']._getval())
            else:
                data = constraints['kind'](constraints['widget']._getval())
            if constraints['masseuse']:
                data = constraints['masseuse'](data)
            ret[name] = data
        return ret

    def _getval(self) -> Dict[str, Any]:
        """Alias this to extract_data() to maintain compatibility with the
        AbstractUnifiedWidget class's expected interface.
        """
        return self.extract_data()

    def activate(self) -> None:
        """Enable all the widgets in this pane. Useful if this is a sub-pane of another
        DataGetter pane, and that pane's main controlling checkbox has just been turned
        back on: all the widgets, which were previously disabled, need to be enabled
        again.

        The exception to this rule, of course, is that if this pane itself has
        sub-panes, they themselves are only activated if their controlling checkbox is
        checked.
        """
        for i in self.data_needed.values():
            i['widget'].activate()

    def deactivate(self) -> None:
        """Disable all widgets in this pane. Useful if this is a sub-pane of another
        DataGetter pane, and that pane's main controlling checkbox has just been turned
        off: all the widgets need to be set to "disabled" status.
        """
        for i in self.data_needed.values():
            i['widget'].deactivate()


class Separator(ttk.Separator, gu.AbstractUnifiedWidget):
    """This class can be used as a 'kind' in get_data_from_user() to produce a visible
    separation at that point in the dictionary.
    """
    def _getval(self) -> Any:
        """This isn't really a control and doesn't have a value. Nevertheless, it
        needs to behave like a control.
        """
        return None

    def _setval(self, value: Any) -> None:
        warnings.warn(f"Attempt to set the value of {self} to {value}; ignored! Separators do not have a value")

    def activate(self) -> None:
        """Intentionally do nothing.
        """
        pass

    def deactivate(self) -> None:
        """Intentionally do nothing
        """
        pass


class SwitchableDataGetter(ttk.Frame, gu.AbstractUnifiedWidget):
    """A DataGetter subclass that is "switchable" in the sense that it can be enabled
    or disabled using a checkbox that turns the whole DataGetter pane off and on.
    This allows us to have one DataGetter inside another DataGetter that is only
    enabled if a checkbox is checked: that is, it allows for an option to be turned
    on that enables sub-options to be entered if the primary option is enabled. This
    makes it possible to set up DataGetters with "do X, yes or no? If yes, how?"
    sequences.

    The switchable state starts off with the button checked if ENABLED is True, or
    disabled if ENABLED is false. DATA_NEEDED is the dictionary specifying the data
    to be requested from the user, in the same format as a regular DataGetter frame
    requires.
    """
    def __init__(self, parent: Union[ttk.Frame, tk.Toplevel, tk.Tk],
                 checkbox_name: str,
                 data_needed: Dict[str, Any],
                 *pargs,
                 enabled: bool = True,
                 **kwargs):
        ttk.Frame.__init__(self, master=parent, *pargs, **kwargs)

        self.checkbox = gu.WrappedCheckbutton(self, text=checkbox_name, command=self.toggle_activation)
        self.checkbox._setval(enabled)
        self.checkbox.grid(row=0, column=0, sticky=tk.W, columnspan=2)

        ttk.Label(self, text='     ').grid(column=0, row=1)      # blank label for spacing
        self.embedded_pane = DataGetter(parent=self, data_needed=data_needed)
        self.embedded_pane.grid(row=1, column=1, sticky=tk.W)
        self.columnconfigure(1, weight=1)
        self.rowconfigure(1, weight=1)

    def toggle_activation(self) -> None:
        """Callback for the checkbox embedded in the widget: when the box is checked
        or unchecked, activates or deactivates the embedded DataGetter pane.
        """
        if self.checkbox._getval():
            self.activate()
        else:
            self.deactivate()

    def _getval(self) -> Union[Literal[False], Dict[str, Any]]:
        if self.checkbox._getval():
            return self.embedded_pane._getval()
        else:
            return False

    def activate(self) -> None:
        """Pass the activation request onto the embedded DataGetter pane.
        """
        self.embedded_pane.activate()

    def deactivate(self) -> None:
        """Pass the deactivation request onto the embedded DataGetter pane.
        """
        self.embedded_pane.deactivate()

    def validate_data(self) -> bool:
        """If the checkbox is checked, and therefore the embedded pane is active, then
        pass the validation request on to the embedded pane. If the checkbox is
        unchecked, then the SwitchableDataGetter is valid, and we should return True to
        indicate this (and its value, incidentally, is False).
        """
        if self.checkbox._getval():
            return self.embedded_pane.validate_data()
        else:
            return True


def get_data_from_user(data_needed: Dict[str, dict],
                       window_title: Optional[str] = "Parameters",
                       supers: Optional[Iterable[Type[type]]] = (),
                       ) -> Union[bool, Dict[str, Any]]:
    """Get some information, specified by the DATA_NEEDED dictionary (format described
    below), from the user. This is a general routine that assembles a query pane
    specifying what information is needed and how to present it; the structure of
    the data is described below. It assembles a general dialog box, shows it to the
    user, validates it, and then returns the requested data. It also handles user
    interactions along the way. Returns False if the user cancels the operation.

    If SUPERS is specified, it must be a list of mix-in classes used to make the
    data-requesting dialog. This allows the window to be customized by making it
    possible for, say, BasicWindowMessagingMixIn to intervene in the window's
    behavior. However, the current underlying structure of the window is not
    particularly amenable to being mucked about with by mix-in classes. (# FIXME)

    In any case, to add additional superclasses to the dialog window, simply specify
    them as, say, supers=(BWMImplementationMixIn, AnotherMixIn). In that case,
    those classes will be added to the base class in the superclass list of the
    class instantiated to create the dialog window. That list of classes must
    contain all of the needed implementation code to do what it does (and therefore
    using BasicWindowMessagingMixIn directly is a bad idea, because it is an
    abstract class that expects to have some methods overridden. Instead of being
    used, it should be subclassed by a class that overrides its abstract methods).

    The data_needed dictionary:
        { 'label':
            { 'kind': type,         # currently allowed: float, int, str, bool, list, Path, Separator, FileTaggerPane,
                                        # ProcessingParameterPane, EditableListPane, DataGetter, the strings
                                        # "text" and "pathdir,"
              'validator': func,    # described below; None is also a valid option.
              'initial': value,     # initial value for this label. Need not be specified.
              'masseuse': func,     # a function that, if present, is called to "massage" the entered data as the last
                                        # step in data extraction; useful, to override automatic type coercion that the
                                        # DataGetter pane normally already does. (This will still happen; but the
                                        # 'masseuse' function will have the final say in what the data looks like.)
                                        # Not called during validation; only called when extracting values from the
                                        # pane, when validation has already succeeded. Need not be present.
              'addfunc': func,      # only for EditableListPane: a function that returns a new value to add to the
                                        # list after the currently selected item. Specifying None is equivalent
                                        # to not supplying the parameter at all.
              'tooltip': str,       # a string that will be displayed in a pop-up window if the user hovers the mouse
                                        # over the data-entry widget associated with the label. If not supplied, no
                                        # tooltip is created.
            },
          'another label':
            [another dictionary],
          [...]
        }

    The function assembles a pane to request the data and validates that it is of
    the requested type, or can be coerced to the specified type, before returning it
    (after type coercion, if necessary).

    If 'kind' is the list type, then the 'initial' key *must* be specified, and must
    be a list. A combobox showing stringified versions of the elements of that list
    will be presented to the user, who will be made to choose exactly one item. The
    returned value will be a string regardless of the types of the items originally
    in the list. Validator functions (see below) are not allowed for list items.

    If 'kind' is the Path type, an interface for selecting a file will be presented,
    giving both a "choose file ..." selection button on an entry widget allowing the
    user to enter a full pathname. Automatic validation is performed (without
    needing to provide an additional validator function) to ensure that the data
    entered is a path to an actually existing regular file (or something that can be
    resolved to one, such as a valid symlink pointing to an actually existing
    regular file).

    If 'kind' is the (case-sensitive) string "pathdir", then there will be an
    interface for selecting a directory, much as with Path selection of individual
    files, above. Just as Path interfaces require an actually existing regular
    file, the result of a "pathdir" query must be an actually existing actual
    directory or something that Python thinks is equivalent.

    If 'kind' is the (case-sensitive) string "text", then there will be a non-
    editable label created instead of a control or text-entry field. In this case,
    'validator' must be None, and 'initial' must be the text of the label to be
    displayed. The displayed label will still have a two-column setup, like the rest
    of the dialog box, along the form of [text of 'label']: [text of 'initial']. (If
    [test of "label"] is a blank or whitespace-only string, it will be omitted, and
    so will the colon; but the resulting [text of "initial"] text will still only
    be in the second column.)  The return value for the 'text' label will be the
    contents of the 'initial' field.

    If 'kind' is the FileTaggerPane or ProcessingParameterPane types, that pane will
    be created and its relevant data will be returned.

    If 'kind' is the DataGetter class, then the "control" created will be another
    DataGetter pane that is a sub-pane of the top-level pane being created. In that
    case, the 'initial' value must be a dictionary that is a valid constructor
    dictionary for the DataGetter class. That is, it must be structured in the way
    described by this long docstring.

    If 'kind' is the EditableListPane class, then a multi-selection pane will be
    created, presenting the options given in the 'initial' parameter. These
    (stringified) options will be presented, in order, in a ListBox widget, next to
    a set of buttons allowing individual items to be moved up and down, or deleted.
    Optionally, an 'addfunc' parameter may be specified; if given, it must be a
    function that returns a new string to be added after the (last) currently
    selected item.

    If 'kind' is Separator, a ttk.Separator will be generated at that point in the
    dialog. It does not display its label, so its label is irrelevant; and it
    returns the value None when the dialog box is dismissed with the OK button.

    If the VALIDATOR key is specified, that function will be called to validate the
    user's selection when the user presses OK and passed the value entered by the
    user; it should return True if the value is acceptable, or False if it is not.
    Validator functions are not allowed for list-type items; all options in the list
    are assumed to be valid selections. Validator functions returning False should
    also display a message to the user explaining what is wrong with the data
    entered by the user so that the user knows how to correct the problem.

    If 'initial' isn't specified, the default value for the type's constructor is
    used (zero, False, empty string). If it IS specified, it must pass validation
    according to the 'validator' parameter. Note that 'initial' has a different
    meaning if 'kind' is the list type: it specifies the options available in the
    combobox that is presented to the user.
    """
    assert data_needed, "No data requested in request to get_data_from_user()!"

    class DataEntryWindow(*([tk.Toplevel] + list(supers))):
        pass

    data_form_window = DataEntryWindow()
    returned_data: Optional[Dict] = None

    def handle_ok():
        """Handle the OK button: destroy the window and, if there have been any changes,
        set the nonlocal EDIT_SUCCESSFUL flag so the new info can be integrated into the
        database.
        """
        nonlocal returned_data
        if data_form_window.data_input_frame.validate_data():
            returned_data = data_form_window.data_input_frame.extract_data()
            data_form_window.destroy()

    def handle_cancel():
        """Handle window-closing events that don't constitute "successful editing that
        results in changed information."
        """
        data_form_window.destroy()

    data_form_window.title(window_title)
    button_frame = ttk.Frame(data_form_window)
    ttk.Button(button_frame, text="OK", command=handle_ok).pack(side=tk.RIGHT)
    ttk.Button(button_frame, text="Cancel", command=handle_cancel).pack(side=tk.RIGHT)
    button_frame.grid(row=1, column=0, sticky=(tk.S + tk.E))
    data_form_window.data_input_frame = DataGetter(parent=data_form_window, data_needed=data_needed)
    data_form_window.data_input_frame.grid(row=0, column=0, sticky=(tk.S + tk.E + tk.W + tk.N), ipadx=15, ipady=10)
    data_form_window.columnconfigure(0, weight=1)
    data_form_window.rowconfigure(0, weight=1)
    data_form_window.bind('<Escape>', lambda e: handle_cancel())
    data_form_window.bind('<Return>', lambda e: handle_ok())
    data_form_window.lift()
    data_form_window.focus_set()
    data_form_window.grab_set()
    data_form_window.wait_window()

    if returned_data:                           # User entered valid data, then hit OK!
        return returned_data
    else:                                       # User canceled!
        return False


def do_tag_files(which_files: Collection[Path]) -> Union[List[str], bool, None]:
    """Assemble a dialog box to ask for tags that will be applied to each file in
    WHICH_FILES. The tags are entered in a single text field, space-separated;
    POSIX-shell-like quoting allows whitespace inside of tags.

    If the user enters tags and clicks OK, returns a list of strings; these strings
    are the tags that should be applied to each file.

    If the user clicks Cancel, or leaves the field blank and clicks OK, returns
    False. (The function *never* returns the boolean value True.)

    If WHICH_FILES is empty, returns None, as a special case.
    """
    if not which_files:         # Passed an empty list? Silently do nothing.
        return

    multiple = (len(which_files) > 1)
    data = get_data_from_user(data_needed={
        '':
            {'kind': 'text',
             'validator': None,
             'initial': f"Enter tags for {'these' if multiple else 'this'} {len(which_files)} "
                        f"file{'s' if multiple else ''}",
             },
        "Tags to add":
            {'kind': gu.FileTaggerPane, 'validator': None, 'initial': None,
             'tooltip':
                 'Tag(s) entered here will be applied to every selected file.'},
    }, window_title=f"Enter tags ...")

    if not data:
        return False        # User canceled!
    return data['Tags to add']


def schedule_video_resize(which_database: mh.MetadataStore,
                          which_file: Path) -> None:
    """Friendly GUI-centered interface to video-resizing-scheduling operations that's
    more abstract than the function actually called directly from various GUI
    elements, but more configurable than the methods directly on the MetadataStore.
    """
    data = get_data_from_user(data_needed={
                'height': {
                    'kind': int,
                    'validator': positive_integer_validator_func,
                    'initial': which_database[which_file]['movie_metadata']['frame height']
                },
                'width': {
                    'kind': int,
                    'validator': positive_integer_validator_func,
                    'initial': which_database[which_file]['movie_metadata']['frame width']
                }}, window_title=f"New video dimensions for "
                                 f"{utils.relative_to_with_name(which_database.file_location, which_file)}")
    if data:
        which_database.schedule_video_resize(which_file, new_height=data['height'],
                                             new_width=data['width'], suppress_write=True)


def schedule_audio_downsampling(which_database: mh.MetadataStore,
                                which_file: Path,
                                suppress_write: bool = False) -> None:
    """Friendly, GUI-centric interface to scheduling audio downsampling.
    """
    dn = {'bitrate':
              {'kind': int, 'validator': acceptable_audio_bitrate_validator_func,
               'initial': which_database[which_file]['movie_metadata']['audio sample rate']}
          }
    rel_path = utils.relative_to_with_name(which_database.file_location, which_file)
    data = get_data_from_user(data_needed=dn,
                              window_title=f'New audio bitrate for {rel_path}')
    if data:
        which_database.schedule_audio_downsampling(which_file, bitrate=data['bitrate'], suppress_write=suppress_write)


@gu.trap_and_report_errors
def schedule_aspect_ratio_change(database: mh.MetadataStore,
                                 which_files: Iterable[Union[str, Path]]) -> None:
    """Schedule a change of aspect ratio, either "soft" (i.e., simply encode a new
    aspect ratio in the video's metadata, which not all players will honor, but
    which doesn't require re-encoding the video stream); or else "hard" (fully
    re-encodes the video stream, with all that that implies, including that it's
    time-consuming and necessarily reduces quality, but is of course honored by all
    video players).
    """
    changed = False

    try:
        for f in which_files:
            movie_metadata = database[f]['movie_metadata']
            current_aspect_ratio = movie_metadata['frame width'] / movie_metadata['frame height']
            data = get_data_from_user({
                'current aspect ratio': {'kind': 'text', 'validator': None, 'initial': f"{current_aspect_ratio:.4f}"},
                'new aspect ratio': {'kind': list,
                                     'validator': None,
                                     'initial': ['16:9', '4:3', '1:1', '16:10', '2.21:1', '2.35:1', '2.39:1', '5:4',
                                                 '9:16', '3:4', '10:16', '1:2.21', '1:2.35', '1:2.39', '4:5'],
                                     'tooltip':
                                         'The new (width-to-height) aspect ratio for the video, after it '
                                         'is processed.'},
                're-encode video': {'kind': bool, 'validator': None, 'initial': True,
                                    'tooltip':
                                        'Re-encoding video is time-consuming and lossy, but works in all video '
                                        'players.'},
            }, window_title=f"New aspect ratio for {f.name}")
            if data:
                changed = True
                new_aspect_ratio = data['new aspect ratio']
                relative_width, relative_height = new_aspect_ratio.strip().split(':')
                new_aspect_ratio = float(relative_width) / float(relative_height)
                new_height = int(movie_metadata['frame height'])
                new_width = round(new_height * new_aspect_ratio)
                database.schedule_aspect_ratio_change(which_file=f, new_height=new_height, new_width=new_width,
                                                      fully_reencode=data['re-encode video'])
                gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', f)
    finally:
        if changed:
            database.write_data()


def _schedule_cut(database: mh.MetadataStore,
                  which_files: Iterable[Union[str, Path]],
                  beginning_or_end: str,
                  command_line_switch: str,
                  suppress_write: bool = False,) -> None:
    """Schedule the elimination of frames from the beginning or end of a video to occur
    on the next processing run. Prompts the user for necessary info for each file,
    then schedules that processing to happen on the next processing run.

    DATABASE is the MetadataStore containing the file(s) to be trimmed. WHICH_FILES
    is a list (or other iterable) of the files to be trimmed. BEGINNING_OR_END is
    either the string "beginning" or "end". COMMAND_LINE_SWITCH is the switch to be
    used as a processing parameter.

    Writes the database every ten files, and when done processing, unless
    SUPPRESS_WRITE is True.
    """
    just_saved = True           # If we process no files, spare ourselves a pointless dB write on the way out.
    for i, f in enumerate(which_files):
        just_saved = False
        database.ensure_next_processing(f)
        old_params = database[f]['next_processing']
        already_reencoding = not fu.is_only_copying_video(old_params)
        selected_vcodec = fu.specified_video_codec(old_params)
        dn = {'seconds to trim': {'kind': str, 'initial': 0,
                                  # FIXME! should be possible to enter a HH:MM:SS.frac string!
                                  'validator': positive_number_validator_func,
                                  'tooltip':
                                      'Re-encoding is slow and degrades quality, but ensures the cut is exactly at the '
                                      'specified time; choosing not to re-encode might mean the cut has to be moved '
                                      'slightly.'},
              }
        data = get_data_from_user(data_needed=dn,
                                  window_title=f"How much to trim from the {beginning_or_end} of {f.name}?", )
        if not data:
            if "yes" == tk_msgbox.askquestion(message="Abort, and cancel trimming other files? ('No' just skips this "
                                                      "single file.)"):
                return
            else:
                continue
        new_params = old_params[:]
        if already_reencoding:
            if selected_vcodec not in (None, "copy"):       # FIXME! This should be an info box, not an error.
                gu.error_message_box("You chose not to re-encode the video, but processing options requiring "
                                     "re-encoding have already been specified as part of another processing operation "
                                     "for this file. The video will be re-encoded.")
        else:
            if data['re-encode']:
                new_params = fu.set_argument_parameter(new_params, '-vcodec', pref('default_video_codec'))
            else:
                new_params = fu.set_argument_parameter(new_params, '-vcodec', 'copy')

        new_params = fu.set_argument_parameter(new_params, command_line_switch, data['seconds to trim'])
        database[f]['next_processing'] = new_params
        if ((i + 1) % 10) == 0:
            if not suppress_write:
                database.write_data()
                just_saved = True
    if not just_saved:                      # Don't save again if we just saved
        if not suppress_write:
            database.write_data()


@gu.trap_and_report_errors
def schedule_beginning_cut(database: mh.MetadataStore,
                           which_files: Iterable[Union[str, Path]],
                           suppress_write: bool = False) -> None:
    """Schedule the elimination of frames from the beginning of a video to occur on the
    next processing run.
    """
    _schedule_cut(database, which_files, 'beginning', '-ss', suppress_write)


@gu.trap_and_report_errors
def schedule_end_cut(database: mh.MetadataStore,
                     which_files: Iterable[Union[str, Path]],
                     suppress_write: bool = False) -> None:
    """Schedule the elimination of frames from the end of the video to occur on the
    next processing run.
    """
    _schedule_cut(database, which_files, 'end', '-to', suppress_write)


def generic_change_ffmpeg_parameters(which_database: mh.MetadataStore,
                                     which_file: Path) -> None:
    """Prompt the user to free-enter parameters to be passed to ffmpeg on the next
    processing run. Does no validation whatsoever.
    """
    if 'next_processing' in which_database[which_file]:
        already_sched_params = which_database[which_file]['next_processing']
    else:
        already_sched_params = list()

    data = get_data_from_user(data_needed={
        'parameters':
            {'kind': str,
             'validator': None,
             'initial': shlex.join(already_sched_params) or "",
             'tooltip': 'The exact processing parameters that FFmpeg will use on the next batch processing run.'},
        'Warning':
            {'kind': 'text',
             'validator': None,
             'initial': 'Incorrect parameters will prevent ffmpeg from processing this file!'},
    }, window_title=f'Add raw ffmpeg parameters for {which_file.name} to next processing run')
    if data:
        which_database[which_file]['next_processing'] = shlex.split(data['parameters'])


def rename_tag(database: mh.MetadataStore, main_window: 'MainWindow',
               suppress_main_window_update=False,
               tags_window: Optional['TagsInDatabaseWindow'] = None,
               suppress_tags_window_update=False,
               tag_to_rename: str = '',
               new_tag_after_renaming: str = '') -> bool:
    """Prompt the user for an existing tag and a tag to replace it. The existing tag
    will be replaced with the new tag in all tags for every file throughout the
    database.

    Returns True if any file was successfully renamed, or False if none was.
    """
    data = get_data_from_user(data_needed={
        'tag to rename':
            {'kind': 'text', 'validator': None, 'initial': tag_to_rename,
             'tooltip':
                 'All occurrences of this tag, attached to any file in the database, will be renamed.'},
        'case-insensitive match':
            {'kind': bool, 'validator': None, 'initial': False,
             'tooltip':
                 'If unchecked, exact matching will be performed; if checked, the all tags that match '
                 'case-insensitively will be replaced with the (exact, case-sensitive) tag specified below.'},
        'rename to': {'kind': str, 'validator': None, 'initial': new_tag_after_renaming or tag_to_rename,
                      'tooltip':
                          'The replacement for any tag(s) fthat match the tag to rename specified above.'},
        '': {'kind': 'text', 'validator': None, 'initial': "Replacement tags are always applied case-sensitively."},
    }, window_title="Tag to rename ...")

    if not data:
        main_window.set_window_status_to_canceled(True, True)
        return False

    if data['tag to rename'] == data['rename to']:
        main_window.set_window_status("Cannot rename tag to same original name! Canceling.", True, True)
        return False

    changed = 0
    if data['case-insensitive match']:
        cmp_func = utils.case_insensitive_tag_comparator
    else:
        cmp_func = utils.case_sensitive_tag_comparator
    to_rename, rename_to = data['tag to rename'], data['rename to']

    which_files = database.get_files_with_tag(to_rename)
    for f in which_files:
        cmp_tags = [cmp_func(s) for s in database.get_tags(f)]
        if cmp_func(to_rename) in cmp_tags:
            where = cmp_tags.index(cmp_func(to_rename))                     # preserve relative location of tag
            database[f]['tags'][where] = rename_to
            # passing this through the OrderedDict constructor eliminates duplicates while preserving order.
            database.set_tags(f, collections.OrderedDict.fromkeys(database[f]['tags']).keys(), suppress_write=True)
            main_window.video_list.scroller.rebuild_single_list_item(which_file=f, which_database=database)
            changed += 1
            windows_to_ignore = list()
            if suppress_main_window_update:
                windows_to_ignore.append(main_window)
            if tags_window:             # We'll explicitly update later if not suppress_tags_window_update.
                windows_to_ignore.append(tags_window)               # Either way, don't trigger it here.
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_tags_change', which_file=Path(f),
                                                  exception_windows=windows_to_ignore)
    if changed:
        main_window.set_window_status(f"Changed tag {to_rename} to {rename_to} in {changed} files", True)
        if tags_window and not suppress_tags_window_update:
            tags_window.rebuild_tags_list()
        database.write_data()
        main_window.status_bar.queue_default_text()
    else:
        main_window.set_window_status(f"Tag '{to_rename}' not found in any file!", True, True)

    return changed != 0


def delete_tag(database: mh.MetadataStore,
               main_window: 'MainWindow',
               tags_window: Optional['TagsInDatabaseWindow'] = None,
               suppress_tags_window_update: bool = False,
               tag_to_delete: str = '') -> None:
    """Delete a specified tag from every file in the database that has it.
    """
    data = get_data_from_user(data_needed={
        'tag to delete':
            {'kind': str, 'validator': None, 'initial': tag_to_delete,
             'tooltip':
                 'Which tag should be deleted from every file in the database that is currently associated with it.'},
        'case-insensitive match':
            {'kind': bool, 'validator': None, 'initial': False,
             'tooltip':
                 'If checked, match will be performed case-insensitively, instead of exactly.'},
    }, window_title="Tag to delete ...")
    if data and data['tag to delete'].strip():
        if gu.confirm_prompt(f"Really delete tag {data['tag to delete']} from all files in database?"):
            changed = 0
            cmp_func = utils.case_insensitive_tag_comparator if data['case-insensitive match'] else utils.case_sensitive_tag_comparator
            to_delete = cmp_func(data['tag to delete'])
            for f in database:
                cmp_tags = [cmp_func(t) for t in database[f]['tags']]
                if to_delete in cmp_tags:
                    where = cmp_tags.index(to_delete)
                    del database[f]['tags'][where]
                    changed += 1
                    main_window.video_list.scroller.rebuild_single_list_item(which_file=f, which_database=database)
            if changed:
                main_window.set_window_status(f"Deleted tag {to_delete} from {changed} files", True, True)
                if tags_window and not suppress_tags_window_update:
                    tags_window.rebuild_tags_list()
                database.write_data()
                main_window.status_bar.queue_default_text()
        else:
            main_window.set_window_status_to_canceled(True, True)
    else:
        main_window.set_window_status('Nothing to do!', True, True)


def get_incremental_tag_mode(which_store: mh.MetadataStore) -> Iterable[Union[str, Path]]:
    """Presents the user with a dialog box asking the order in which they want to see
    files listed as those files are being incrementally_tagged. Returns a function
    that sorts a list of those files.

    The function must take a single parameter, a MetaDataStore; it must return a list of
    items to be examined, sorted in whatever way it needs to be sorted.
    """
    sort_types = {
        'By file date': lambda item: which_store[item]['file_metadata']['st_mtime'],
        'Alphabetic': lambda item: Path(item).resolve().name,
        'Random': lambda item: random.random(),
        'Folder by folder': str,
        'By video play time': which_store.get_video_play_time,
        'By file size': lambda item: which_store.get_video_file_size(Path(item)),
        'By video frame size': lambda item: which_store.get_video_frame_size(Path(item)),
    }

    options = get_data_from_user(data_needed={
        'Video order': {
            'kind': list,
            'validator': None,
            'initial': list(sort_types.keys()),
            'tooltip': 'How to sort the files that need to be tagged during this incremental tagging session.',
        },
        'Reverse order': {
            'kind': bool,
            'validator': None,
            'initial': False,
            'tooltip': 'If checked, sort the files in reverse order.'
        }
    })
    if not options:
        return list()

    return sorted([i for i in which_store], key=sort_types[options['Video order']],
                  reverse=options['Reverse order'])


def get_loudness_normalization_params(num_files: int) -> Union[dict, Literal[False]]:
    """Get the parameters for a loudness normalization operation on NUM_FILES files.
    """
    opts = get_data_from_user(data_needed={
        'Loudness target': {'kind': float, 'initial': -24.0, 'validator': bounded_float_validator_factory(-70, -5),
                            'tooltip': 'The Integrated Loudness Target is an overall measure of how loud the audio '
                                       'is, relative to the maximum possible loudness for the trck.'},
        'Loudness range': {'kind': float, 'initial': 7, 'validator': bounded_float_validator_factory(1, 50),
                           'tooltip': 'Target variation in loudness throughout the audio track.'},
        'True peak': {'kind': float, 'initial': -2.0, 'validator': bounded_float_validator_factory(-9, 0),
                      'tooltip': 'The desired true peak value of the resulting audio stream, between -9 and 0.'},
        'Dual mono': {'kind': bool, 'initial': False,
                      'tooltip': 'Calculate single-channel audio streams more accurately.'}
    }, window_title=f"Loudness normalization for {'this' if num_files==1 else 'these'} "
                    f"{num_files if (num_files > 1) else ''} file{'s' if (num_files > 1) else ''}")

    return opts or False


if __name__ == "__main__":
    print("Sorry, no self-test code in this module!")
