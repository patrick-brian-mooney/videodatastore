#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Defines an object that associates metadata with files, stores that information
in metadata files, and provides an ability to export it as JSON; and a (basic,
Python-based) CLI/UI for editing that metadata.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import bz2
import collections
import datetime
import functools
import json
import numbers
import os
import pickle
import re
import shlex
import shutil
import subprocess
import threading
import time
import uuid

from typing import Any, Callable, Dict, Generator, Iterable, Iterator, List, Literal, Optional, Tuple, Set, Union

from pathlib import Path

import vds.ffmpeg_utils as fu

import vds.utils as utils
from vds.utils import pref          # used often enough to benefit from a shorter synonym.


class StopProcessingThisFile(Exception):
    """Raise this to stop processing on the current video during the current run.
    """
    pass


# We're going to define some constants to match against when evaluating how to respond to FFmpeg errors.
ERR_Q_TOO_SMALL = "too many packets buffered for output stream"
ERR_EXPERIMENTAL_CODEC_USAGE = re.compile("(.*) in (.*) support is experimental")           # codec, container
ERR_TAG_NOT_FOUND = re.compile("could not find tag for codec (.*) in stream")               # codec
ERR_NONSTANDARD_MUXING = re.compile(r"muxing (.*) at (\d*)hz is not standard")              # container, audio bitrate
ERR_BAD_HEIGHT = 'height not divisible by'
ERR_CANNOT_FILTER_WHEN_COPYING = "filtering and streamcopy cannot be used together"
ERR_NO_OUTPUT_FORMAT_FOUND = "unable to find a suitable output format"
ERR_BAD_WIDTH_OR_HEIGHT = re.compile(r"invalid too big or non positive size for width '(\d*)' or height '(\d*)'")


# And also specify which codecs apply to which stream.
known_audio_codecs, known_video_codecs, _ = fu.get_codec_lists()


def default_cmp_func(what: str) -> str:
    """Default function to find the "comparison form" of a string. This is the default
    function used for tag comparison if another comparison function is not supplied
    to methods that take a CMP_FUNC parameter.
    """
    assert isinstance(what, str)
    return what.strip().casefold()


def _needs_codec_change(ffmpeg_output: str) -> Tuple[bool, bool]:
    """Given FFMPEG_OUTPUT, a string that contains the casefold version of the
    output of an FFmpeg invocation, returns a 2-tuple (video, audio) of booleans
    indicating whether the video codec and the audio codec (respectively) need to be
    switched to the default video and audio codec specified in the preferences.
    """
    ret_video, ret_audio = False, False
    bad_codecs = set()
    for res in ERR_EXPERIMENTAL_CODEC_USAGE.findall(ffmpeg_output):
        codec, container = res
        if ']' in codec:
            codec = codec[1 + codec.rfind(']'):].strip()
        bad_codecs.add(codec)
    for codec in ERR_TAG_NOT_FOUND.findall(ffmpeg_output):
        bad_codecs.add(codec)
    if ERR_NONSTANDARD_MUXING.findall(ffmpeg_output):
        ret_audio = True

    for codec in bad_codecs:
        if codec in known_audio_codecs:
            ret_audio = True
        elif codec in known_video_codecs:
            ret_video = True
        else:
            print(f"WARNING! Cannot determine whether codec '{codec}' stores audio or video "
                  f"information! Assuming 'neither' ...")
    return ret_video, ret_audio


def _handle_processing_error(which_db: 'MetadataStore',
                             which_file: Path,
                             ffmpeg_output: str) -> None:
    """If processing with ffmpeg results in an error, try to adjust the ffmpeg
    parameters for the next run so that the error does not occur again then.
    """
    assert isinstance(which_file, Path)
    which_db.ensure_next_processing(which_file)
    params = which_db[which_file]['next_processing']

    handled = False
    low_output = ffmpeg_output.casefold()
    print('Attempting to adjust parameters to try to prevent errors for this file on next processing run ...')

    if ERR_Q_TOO_SMALL in low_output:
        # If ffmpeg can't detect the input format, the size of the muxing queue may be too small
        current_q_size = fu.get_argument_parameter(params, '-max_muxing_queue_size')
        new_q_size = (2 * int(current_q_size)) if current_q_size else 1024
        params = fu.set_argument_parameter(params, '-max_muxing_queue_size', new_q_size)
        print(f'   ... adjusted -max_muxing_queue_size to f{new_q_size}')
        handled = True

    change_video_codec, change_audio_codec = _needs_codec_change(low_output)

    if change_audio_codec:
        params = fu.set_argument_parameter(params, '-acodec', pref('default_audio_codec'))
        params = fu.set_argument_parameter(params, '-q:a', '2')
        print('   ... specified -acodec ' + pref('default_audio_codec') + ' and a quality option!')
        handled = True

    if change_video_codec:
        params = fu.set_argument_parameter(params, '-vcodec', pref('default_video_codec'))
        print('   ... specified -vcodec ' + pref('default_video_codec'))
        handled = True

    if ERR_BAD_HEIGHT in low_output:
        print("   ... explicitly specifying allowable height and width!")
        chain = fu.get_video_filterchain(params)
        new_width, new_height = None, None
        if chain.as_str().strip():
            scale = chain['scale']
            try:
                new_width, new_height = scale.split(':')
                new_height += 1
            except (ValueError,):
                pass
        else:
            new_width = which_db[which_file]['movie_metadata']['frame width']
            new_height = which_db[which_file]['movie_metadata']['frame height']
        which_db.schedule_video_resize(which_file, new_width=new_width, new_height=new_height)
        handled = True

    if ERR_CANNOT_FILTER_WHEN_COPYING in low_output:
        if fu.is_only_copying_video(params):
            params = fu.set_argument_parameter(params, '-vcodec', pref('default_video_codec'))
        if fu.is_only_copying_audio(params):
            params = fu.set_argument_parameter(params, '-acodec', None)
            params.extend(['-acodec', pref('default_audio_codec'), '-q:a', '2'])

    if ERR_NO_OUTPUT_FORMAT_FOUND in low_output:
        # If we haven't found another strategy for dealing with this problem yet, give up on processing this file.
        if not handled:
            print("   ... abandoning attempt to process file!")
            params = list()
            handled = True

    if re.search(ERR_BAD_WIDTH_OR_HEIGHT, low_output):
        print("   ... validating crop rectangle ...")
        old_params = params[:]
        params = which_db.validate_crop_dimensions(which_file)
        if params != old_params:
            print("      ... altered crop rectangle!")
        handled = True

    if handled:
        which_db[which_file]['next_processing'] = params
        which_db.validate_codec_specs(which_file)
    else:
        print("   ... no strategy for handling this problem, sorry!")


def _report_ffmpeg_error(which_db: 'MetadataStore',
                         ffmpeg_output: str,
                         return_code: int,
                         which_file: Path,
                         outfile: Path) -> None:
    """When ffmpeg reports an error, pass the report on to the user, along with an
    explanation of what it means for processing.
    """
    assert isinstance(which_file, Path)
    assert isinstance(outfile, Path)
    print("ERROR! -- reported by ffmpeg with return code %d" % return_code)
    print("Command output:\n" + ffmpeg_output)
    _handle_processing_error(which_db=which_db, which_file=which_file, ffmpeg_output=ffmpeg_output)
    if outfile.exists() and outfile.is_file():
        outfile.unlink()
    print()
    raise StopProcessingThisFile


def _size_is_acceptable(new_size: int,
                        old_size: int,
                        allow_size_increase: bool,
                        ignore_size_of_decrease: bool = False) -> bool:
    """Decide whether the NEW_SIZE of a processed file is acceptable, based on its
    OLD_SIZE and whether to ALLOW_SIZE_DECREASE. If IGNORE_SIZE_OF_DECREASE is True,
    any decrease, or equality of OLD_SIZE with NEW_SIZE, is considered to be good
    enough; otherwise, the amount by which NEW_SIZE has to be less than OLD_SIZE to
    be acceptable is controlled by the max_postcomp_size_ratio preference setting.
    """
    if allow_size_increase:
        return True
    if new_size > old_size:
        return False
    if ignore_size_of_decrease:
        return True
    if new_size <= (pref('max_postcomp_size_ratio') * old_size):
        return True
    return False


def _simple_video_process(which_db: 'MetadataStore',
                          which_file: Path,
                          initial_arguments: List[str],
                          outfile: Path) -> bool:
    """Attempts a 'simple' video processing: just attempts to run the file through with
    -crf 23 (or whatever the current value of single_CRF_value is). If this is
    "good enough", replaces the file and returns True; otherwise, deletes the
    intermediate file and returns False.

    What "good enough" means is largely controlled by the constant
    MAX_POSTCOMP_SIZE_RATIO.
    """
    assert isinstance(which_file, Path)
    assert isinstance(outfile, Path)
    metadata_location = which_db.file_location.parent if which_db.file_location else Path.cwd()
    arguments, successful = initial_arguments[:], False

    if not fu.is_only_copying_video(arguments):
        arguments = fu.set_argument_parameter(arguments, '-crf', str(pref('single_CRF_value')))
        print(f"\nPROCESSING: re-encoding {which_file.absolute().relative_to(metadata_location)} at constant rate"
              f" factor {pref('single_CRF_value')}")
    else:
        print(f"\nPROCESSING: copying video data from {which_file.absolute().relative_to(metadata_location)} to "
              f"new container")
    arguments.append(str(outfile))

    completed_process = utils.run_process(arguments)
    if completed_process.returncode:
        _report_ffmpeg_error(which_db, ffmpeg_output=completed_process.stdout,
                             return_code=completed_process.returncode, which_file=which_file, outfile=outfile)
    if not (outfile.exists() and outfile.is_file()):    # Seems unlikely if we made it this far, but check anyway.
        print("ERROR! Unable to create file.")
        if outfile.exists():
            outfile.unlink()
        return False

    # Check for, and handle, success
    ignore = which_db[which_file]['next_processing_params']['allow_size_increase']
    if _size_is_acceptable(outfile.stat().st_size, which_db[which_file]['file_metadata']['st_size'],
                           allow_size_increase=False,
                           ignore_size_of_decrease=ignore):
        which_db[which_file]['next_processing'] = [][:]
        del which_db[which_file]['next_processing_params']
        successful = True
        if not fu.is_only_copying_video(arguments):
            which_db[which_file]['recompressed'] = True
    if successful:
        try:
            st_data = which_file.stat()
            outfile.replace(which_file)
            os.utime(which_file, (st_data.st_atime, which_db[which_file]['original_mdate']))
            _ = which_db.get_filesystem_metadata(which_file, suppress_write=True)       # Force metadata update.
            _ = which_db.get_movie_metadata(which_file, suppress_write=True)            # Ditto.
        except (OSError,):
            pass
    else:
        if (outfile.exists()) and (outfile.is_file()):
            outfile.unlink()
    return successful


def _two_pass_video_process(which_db: 'MetadataStore',
                            which_file: Path,
                            initial_arguments: List[str],
                            outfile: Path) -> bool:
    """Engages in two-pass video processing to hit a target bitrate. Returns True and
    swaps in the new file if it is successful; returns False and deletes any
    intermediate file if it is unable to process the file for some reason.
    """
    assert isinstance(which_file, Path)
    assert isinstance(outfile, Path)
    metadata_location = which_db.file_location.parent if which_db.file_location else Path.cwd()
    assert default_cmp_func(outfile.suffix) == pref('proc_file_ext')  # Currently, .mp4 is baked in below. #FIXME

    try:
        width, height = fu.get_video_dim_after_processing(which_db, which_file)
        bitrate = utils.kush_gauge(width, height)

        print(f"PROCESSING: first pass: {which_file.absolute().relative_to(metadata_location.parent)} at "
              f"bitrate {which_file.absolute().relative_to(metadata_location.parent)}")
        first_pass = initial_arguments + ["-c:v", pref('default_video_codec'), "-b:v", str(bitrate),
                                          "-pass", "1", "-an", "-f", "mp4", "/dev/null"]

        completed_process = utils.run_process(first_pass)
        print(completed_process.stdout)
        if completed_process.returncode:
            _report_ffmpeg_error(which_db, ffmpeg_output=completed_process.stdout,
                                 return_code=completed_process.returncode, which_file=which_file, outfile=outfile)

        print(f"PROCESSING: second pass: {which_file.absolute().relative_to(metadata_location.parent)} "
              f"at bitrate {bitrate}")
        second_pass = initial_arguments + ["-c:v", pref('default_video_codec'), "-b:v", str(bitrate),
                                           "-pass", "2", str(outfile)]
        completed_process = utils.run_process(second_pass)
        if completed_process.returncode:
            _report_ffmpeg_error(which_db, ffmpeg_output=completed_process.stdout,
                                 return_code=completed_process.returncode, which_file=which_file, outfile=outfile)
        if not (outfile.exists() and outfile.is_file()):  # Seems unlikely if we made it this far, but check anyway.
            print("ERROR! Unable to create file.")
            if outfile.exists():
                outfile.unlink()
            return False

        # If we get this far, we've been successful at re-encoding the video, even if we don't keep the new version.
        which_db[which_file]['next_processing'] = [][:]
        try:
            # Even if the video is larger, treat it as if it had been recompressed to prevent future attempts.
            if not fu.is_only_copying_video(initial_arguments):
                which_db[which_file]['recompressed'] = True
        except (ValueError, IndexError, KeyError):
            pass

        allow = which_db[which_file]['next_processing_params']['allow_size_increase']
        if _size_is_acceptable(new_size=outfile.stat().st_size,
                               old_size=which_db[which_file]['file_metadata']['st_size'],
                               allow_size_increase=allow,
                               ignore_size_of_decrease=True):
            try:
                st_data = which_file.stat()
                outfile.replace(which_file)
                os.utime(which_file, (st_data.st_atime, which_db[which_file]['original_mdate']))
                _ = which_db.get_filesystem_metadata(which_file, suppress_write=True)   # Force metadata update.
                _ = which_db.get_movie_metadata(which_file, suppress_write=True)        # Ditto.

                # OK, clean up the two-pass logfiles
                for f in utils.flatten_list([list(Path(os.getcwd()).rglob('ffmpeg2pass*.log')),
                                             list(Path(os.getcwd()).rglob('ffmpeg2pass*log.temp')),
                                             list(Path(os.getcwd()).rglob('ffmpeg2*log.mbtree'))]):
                    try:
                        os.unlink(f)
                    except (OSError,):
                        print("Warning! unable to delete the log file: %s" % f)
            except (OSError,):
                pass
            return True
        else:
            if os.path.getsize(outfile) >= os.path.getsize(which_file):
                # If, despite everything, we've produced a larger file, delete it and signal failure.
                outfile.unlink()
                print("ERROR! Recompressed file does not save any space! Keeping original ...")
                return False

    except Exception as errrr:
        print("Unable to process! The system said: %s" % errrr)
        try:
            if outfile.exists():
                outfile.unlink()
        except (IOError,) as errrr:
            pass
        return False


def process_video(which_db: 'MetadataStore',
                  which_file: Path,
                  suppress_write: bool = False) -> bool:
    """Process a single video, using the parameters in the 'next_processing' field
    of the stored metadata for the video. If processing is successful, empty the
    'next_processing' list. If video was recompressed, set the 'recompressed' flag
    in the metadata for the movie.

    Unless SUPPRESS_WRITE is True, write out the changed metadata store to disk.

    Returns True if the file was successfully processed, or False if something
    prevented the video from being processed successfully.
    """
    assert isinstance(which_file, Path)
    if which_file not in which_db:
        print(f"ERROR: No stored metadata for {shlex.quote(str(which_file))}!")
        return False
    if 'next_processing' not in which_db[which_file]:
        print(f"ERROR: no processing scheduled for {shlex.quote(str(which_file))}!")
        return False
    if not (which_file.exists() and which_file.is_file()):
        print(f"ERROR: {which_file} has disappeared from disk!")
        del which_db[which_file]
        return False

    old_size = os.path.getsize(which_file)
    success = False
    try:
        outfile = utils.clean_name(which_file.parent / (which_file.stem + '-processed' + which_file.suffix),
                                   new_suffix=pref('proc_file_ext'))
        if default_cmp_func(outfile.suffix) != pref('proc_file_ext'):
            outfile = outfile.with_suffix(pref('proc_file_ext'))
        if pref('nice_loc') and pref('nice_value'):
            initial_arg = [pref('nice_loc'), '-n', str(pref('nice_value'))]
        else:
            initial_arg = list()
        initial_arg.extend([pref('ffmpeg_loc'), '-y', '-threads', str(pref('num_ffmpeg_threads')),
                            "-i", str(which_file)] + which_db[which_file]['next_processing'])

        # Code that's about to be called makes a lot of assumptions that -acodec and -vcodec are only specified
        # once. First, check to make sure that's true, then check to make sure they're specified at all.
        initial_arg = fu.validate_codec_specs(initial_arg)
        if not fu.get_argument_parameter(initial_arg, '-vcodec'):
            if fu.get_argument_parameter(initial_arg, '-vf'):
                initial_arg = fu.set_argument_parameter(initial_arg, '-vcodec', pref('default_video_codec'))
            else:
                initial_arg = fu.set_argument_parameter(initial_arg, '-vcodec', 'copy')

        if not fu.get_argument_parameter(initial_arg, '-acodec'):
            if fu.get_argument_parameter(initial_arg, '-ab') or fu.get_argument_parameter(initial_arg, '-af'):
                initial_arg = fu.set_argument_parameter(initial_arg, '-acodec', pref('default_audio_codec'))
            else:
                initial_arg = fu.set_argument_parameter(initial_arg, '-acodec', 'copy')
        if not fu.is_only_copying_video(initial_arg):
            if '-preset' not in initial_arg:
                initial_arg = fu.set_argument_parameter(initial_arg, "-preset", pref('ffmpeg_preset'))

        print("Initial arguments to ffmpeg: " + shlex.join(initial_arg))

        if _simple_video_process(which_db, which_file, initial_arg, outfile):
            new_size = os.path.getsize(which_file)      # OUTFILE has already been removed-by-renaming at this point.
            if old_size >= new_size:
                print(f"Re-encoding saved {old_size - new_size} bytes, or "
                      f"{(100 * ((old_size - new_size) / old_size)):.3f}%\n")
            else:
                print(f"Re-encoding used an additional {new_size - old_size} bytes, or "
                      f"{(100 * ((new_size - old_size) / old_size)):.3f}%\n")
            success = True
            return True
        elif _two_pass_video_process(which_db, which_file, initial_arg, outfile):
            new_size = os.path.getsize(which_file)
            if old_size >= new_size:
                print(f"Re-encoding saved {old_size - new_size} bytes, or"
                      f" {100 * ((old_size - new_size) / old_size):.3f}%")
            else:
                print(f"Re-encoding used an additional {new_size - old_size} bytes, or"
                      f" {(100 * ((new_size - old_size) / old_size)):.3f}%\n")
            success = True
            return True
        else:
            return False

    except StopProcessingThisFile:  # Silently stop processing this video and return False. Any errors are assumed
        return False                # to have already been reported.

    except Exception as err:
        print("\tUnable to process! The system said: %s" % err)
        try:
            if outfile.is_file():
                outfile.unlink()
        except NameError:           # If we somehow managed not to bind the name OUTFILE,
            pass                          # never mind: no clean-up necessary.
        return False

    finally:
        if success:
            if which_file.suffix != outfile.suffix:
                # rename the file in the database
                new_name = which_file.with_suffix(outfile.suffix)
                which_db.rename_file(which_file, new_name, suppress_write=True)
                which_file = new_name

            # record the processing history for the file
            which_db[which_file]['process history'] = dict(collections.ChainMap(
                {datetime.datetime.now().isoformat(' '): initial_arg},
                which_db[which_file].get('process history', dict())
            ))
        which_db[which_file]['movie_metadata'] = _read_movie_metadata_from_file(which_file)
        if not suppress_write:
            which_db.write_data()


def process_videos(which_db: 'MetadataStore',
                   setup_proc: Optional[Callable[[Callable, int], Any]] = None,
                   teardown_proc: Optional[Callable] = None,
                   update_proc: Optional[Callable[[numbers.Real, Union[str, Path]], None]] = None,
                   max_videos_to_process: Union[int, None] = None,
                   allotted_processing_seconds: Union[numbers.Real, None] = None,
                   sort_proc: Callable = lambda what: default_cmp_func(str(what)),
                   ) -> Tuple[int, int]:
    """Starts a processing run for all videos that need processing. Determines whether
    a video needs processing based on whether there is a non-empty 'next_processing'
    list stored in the video's metadata. For each video that needs processing, this
    function processes it using the stored 'next_processing' parameters.

    If SETUP_PROC is provided, it will be called before processing begins, and will
    be passed the do_cancel() function. This is used by the GUI, which captures the
    do_cancel function and binds it to a "Cancel" button. do_cancel eats any
    parameters passed to it: it doesn't need any, but this lets it be used as a
    Tkinter callback. The second parameter passed to the SETUP_PROC is the total
    number of videos to be processed, which is useful when setting an upper limit on
    a status bar, for instance.

    If TEARDOWN_PROC is provided, it will be called with no arguments when the
    processing run has completed.

    UPDATE_PROC, if not None, is called after every video is processed and passed
    the total number of videos processed so far, which is useful if, for instance,
    updating a status bar.

    If MAX_VIDEOS_TO_PROCESS is not None, it specifies the maximum number of videos
    to SUCCESSFULLY process; unsuccessful processing does not count toward this
    number. After successfully processing this many videos, processing will stop
    even if there are still videos waiting in the processing queue.

    If ALLOTTED_PROCESSING_SECONDS is not None, then a check will be performed after
    every file is processed, whether that file's processing attempt wsa successful
    or not. If the total wall time since the processing run started equals or
    exceeds the specified value, the processing run will end even if there are still
    videos waiting to be processed. (It is not currently possible to prevent the
    run from exceeding the specified value, because the processing time for an
    individual file cannot be accurately predicted.)

    If SORT_PROC is specified, it will be used as a key function when sorting the
    list of files waiting to be processed. If it is not specified, a case-insensitive
    stringification of the files' path will be used to sort.

    Returns a 2-tuple:
        (# files processed successfully, # files whose processing resulted in an error)

    #FIXME: this can be run in a separate thread, but it updates WHICH_DB. The
    possibility of concurrent updates to the same attributes clobbering data is
    small, but not nonexistent. A future version should be more careful about gating
    attribute changes.
    """
    assert isinstance(which_db, MetadataStore)

    if setup_proc:
        assert isinstance(setup_proc, Callable)
    if teardown_proc:
        assert isinstance(teardown_proc, Callable)
    if update_proc:
        assert isinstance(update_proc, Callable)

    if max_videos_to_process:
        assert isinstance(max_videos_to_process, int)
        assert max_videos_to_process > 0, (f"ERROR! Cannot process videos, because MAX_VIDEOS_TO_PROCESS is "
                                           f"{max_videos_to_process}, not a positive integer!")
    if allotted_processing_seconds:
        assert isinstance(allotted_processing_seconds, numbers.Real)
        assert allotted_processing_seconds > 0, (f"ERROR! Attempt to allot {allotted_processing_seconds} for "
                                                 f"processing videos, and that is not a positive number!")

    consecutive_errors, total_successes, total_errors = 0, 0, 0
    cancel = False
    videos_to_process = sorted(which_db.videos_to_process, key=sort_proc)
    processing_start = time.monotonic()
    vid_num, orig_len = 0, len(videos_to_process)

    if not videos_to_process:
        print("No videos queued for processing!")
        return 0, 0

    def do_cancel(*e) -> None:
        nonlocal cancel
        cancel = True

    if setup_proc:  # FIXME! Check that function annotation in this function for setup_proc is correct!
        setup_proc(do_cancel, len(videos_to_process))

    print("Beginning to process video queue at: " + datetime.datetime.now().isoformat())
    if max_videos_to_process or allotted_processing_seconds:
        msg = "Limiting processing to "
        if max_videos_to_process:
            msg += f"{max_videos_to_process} successful media files "
            if allotted_processing_seconds:
                msg += "and "
        if allotted_processing_seconds:
            msg += f"{allotted_processing_seconds} seconds of processing time"
        msg = msg.strip() + "."
        print(msg)

    try:
        while videos_to_process and not cancel:
            vid_num += 1
            which_file = videos_to_process.pop(0)
            print(f"\n\n\nProcessing {vid_num} of {orig_len} videos: {which_file}.")
            if process_video(which_db, which_file, suppress_write=False):
                consecutive_errors = 0
                total_successes += 1
            else:
                consecutive_errors += 1
                total_errors += 1
                if pref('max-consecutive-batch-errors'):
                    if consecutive_errors >= pref('max-consecutive-batch-errors'):
                        raise RuntimeError(f"{pref('max-consecutive-batch-errors')} consecutive processing errors!")
            if update_proc:
                update_proc(vid_num, which_file)
            if max_videos_to_process and (total_successes >= max_videos_to_process):
                print(f"Successfully processed {total_successes} videos, stopping ...")
                break
            elif allotted_processing_seconds and ((time.monotonic() - processing_start) >= allotted_processing_seconds):
                print(f"Allotted processing time of {allotted_processing_seconds} seconds has passed, stopping ...")
                break
            elif max_videos_to_process or allotted_processing_seconds:
                msg = "Not stopping processing yet: quitting after "
                if max_videos_to_process:
                    msg += f"{max_videos_to_process - total_successes} more videos successfully processed "
                if max_videos_to_process and allotted_processing_seconds:
                    msg += "or "
                if allotted_processing_seconds:
                    msg += f"{allotted_processing_seconds - (time.monotonic() - processing_start)} seconds "
                print(msg.strip() + ".\n")
    finally:
        if teardown_proc:
            teardown_proc()
        return total_successes, total_errors


class BatchProcessingThread(threading.Thread):
    def __init__(self, parent: 'MainWindow',
                 setup_proc: Optional[Callable] = None,
                 teardown_proc: Optional[Callable] = None,
                 update_proc: Optional[Callable[[numbers.Real, Union[str, Path]], None]] = None):
        threading.Thread.__init__(self)
        self.parent = parent
        self.setup_proc = setup_proc
        self.teardown_proc = teardown_proc
        self.update_proc = update_proc

    def run(self) -> None:
        """By the time we get here, sys.stdout and sys.stderr need to be already
        redirected to something thread-safe if they need to be redirected to
        something thread-safe.
        """
        process_videos(which_db=self.parent.displayed_database, setup_proc=self.setup_proc,
                       teardown_proc=self.teardown_proc, update_proc=self.update_proc)
        self.parent.scheduler.queue_task(self.parent.set_window_status, "All files processed!", True, True)
        self.parent.scheduler.queue_task(setattr, self.parent, 'batch_processing_thread', None)


def process_videos_in_separate_thread(main_window: 'MainWindow',
                                      batch_window: Optional['BatchProcessingProgressWindow'] = None,
                                      setup_proc: Optional[Callable] = None,
                                      teardown_proc: Optional[Callable] = None,
                                      update_proc: Optional[Callable[[numbers.Real, Union[str, Path]], None]] = None,
                                      ) -> None:
    """Run process_videos(), but spin it off to a separate thread. This is really only
    useful from the GUI, and therefore we want to be careful about how it might
    trigger GUI updates, so it prints through thread_safe_text_box_redirector() to
    be sure. For that reason, and because the thread it spawns needs to be tracked
    by the GUI's MainWindow, it needs to be passed a MainWindow object as
    MAIN_WINDOW.
    """
    if main_window.batch_processing_thread:
        main_window.set_window_status("Already running a batch processing job!", True, True)
        return

    main_window.batch_processing_thread = BatchProcessingThread(main_window,
                                                                setup_proc=setup_proc,
                                                                teardown_proc=teardown_proc,
                                                                update_proc=update_proc)
    main_window.batch_processing_thread.run()


# Utilities for batch importing new items into a database
class StopHandlingThisFile(Exception):
    """An exception that can be raised during the file-moving process to stop handling
    the current file and move along to the next file that is waiting to be moved.
    """


class StopHandlingAllFiles(Exception):
    """An exception that can be raised to abort the entire fire-importing process.
    """
    pass


def just_choose_clean_name_renaming_proc(dir_to_move_files_to: Path,
                                         current_file: Path,
                                         strip_phrases: Iterable[str],
                                         auto_rename: bool,
                                         parent_window=None) -> Path:
    """A renaming procedure that just calls utils.clean_name to produce a suggested name
    to use when renaming files.
    """
    return utils.clean_name(suggested_name=dir_to_move_files_to / current_file.name,
                            fixes_to_strip=strip_phrases,
                            auto_rename=auto_rename)


def move_files_from_holding_dir(files_to_move: Iterable[Path],
                                dir_to_move_files_to: Path,
                                which_database: 'MetadataStore',
                                renaming_proc: Optional[Callable] = None,
                                strip_phrases: Optional[Iterable[str]] = None,
                                auto_rename: bool = False,
                                parent_window: 'MainWindow' = None,
                                max_files_moved: Optional[int] = None,
                                sort_func: Optional[Callable] = lambda f: default_cmp_func(str(f)),
                                replace_func: Optional[Callable] = None
                                ) -> List[Path]:
    """A high-level interface to the 'move files from holding directory and into the
    database' functionality.

    FILES_TO_MOVE is an Iterable of Paths for files to be moved into the database.

    DIR_TO_MOVE_FILES_TO is the Path to a directory into which the files will be
    moved before they are added to the database.

    WHICH_DATABASE is the MetadataStore that will track them after they are moved.

    RENAMING_PROC is the renaming procedure that will be used to rename the files
    immediately before moving them. If None, just_choose_clean_name_renaming_proc is
    used.

    STRIP_PHRASES is a list of strings that will be stripped from the beginning or
    end of the generated names, provided that the RENAMING_PROC supports that
    operation.

    AUTO_RENAME is a parameter passed to RENAMING_PROC that tells that function
    whether to generate a unique filename or allow a file to overwrite an existing
    file. If True, a unique filename is generated.

    PARENT_WINDOW should be specified if this function is called from a GUI, and
    should be the top-level window for the application. Some RENAMING_PROCs that are
    intended for GUI use only test to ensure it is not None.

    If MAX_FILES_MOVED is specified, the function will stop moving files after that
    number of files are successfully moved and added to the database, even if there
    are files in the FILES_TO_MOVE list that have not yet been imported.

    If SORT_FUNC is specified, it must be a key function to be used to sort
    FILES_TO_MOVE. If SORT_FUNC is None, files will be processed in exactly the
    order that they appear in FILES_TO_MOVE.

    If REPLACE_FUNC is specified, it will be called immediately before a file is
    overwritten by another file with the same name.

    Returns a list of (the new Paths of) the files that were successfully moved.
    """
    for f in files_to_move:
        assert isinstance(f, Path)
    assert isinstance(dir_to_move_files_to, Path)
    assert dir_to_move_files_to.is_dir()
    assert max_files_moved is None or isinstance(max_files_moved, int)
    if max_files_moved:
        assert max_files_moved > 0, (f"ERROR! move_files_from_holding_dir() got a max_files_moved parameter of "
                                     f"{max_files_moved}, which is not a positive integer!")
    renaming_proc = renaming_proc or just_choose_clean_name_renaming_proc

    moved_files, num_moved_files = list(), 0
    if sort_func is not None:
        files_to_move = sorted(files_to_move, key=sort_func)

    while files_to_move:
        try:
            current_file = files_to_move.pop(0)
            new_file_path = renaming_proc(dir_to_move_files_to, current_file, strip_phrases, auto_rename, parent_window)

            if new_file_path.exists():
                # we're replacing, so do necessary housekeeping .
                which_database[new_file_path]['recompressed'] = False
                which_database[new_file_path]['process history'] = dict(collections.ChainMap(
                    {datetime.datetime.now().isoformat(' '): 'replaced with new file'},
                    which_database[new_file_path].get('process history', dict())))

                if replace_func:
                    replace_func(new_file_path)

            new_file_path = shutil.move(src=current_file, dst=new_file_path)  # Move the file, get its new Path.
            new_file_rel_loc = utils.relative_to(from_where=which_database.file_location,
                                                 to_where=new_file_path)
            new_file_path = new_file_rel_loc / new_file_path.name
            moved_files.append(new_file_path)
            which_database.add_file_to_database(new_file_path)
            num_moved_files += 1
            if max_files_moved and (num_moved_files >= max_files_moved):
                break
        except (StopHandlingThisFile,):
            continue
        except (StopHandlingAllFiles,):
            break

    return moved_files


# Some file-handling utilities
def _update_version_zero_to_version_one(db_data: Dict) -> Dict:
    """Update a version-zero VideoDataStore file to a version-one file. Version-zero
    files were never produced by publicly released versions of VideoDataStore, so
    this is unlikely ever to be needed, but the safety net is there if that
    deduction ever turns out to be wrong.

    Version-zero files don't have a 'version', 'metadata', or 'settings' field and
    cannot store anything other than the metadata for the files that they are
    tracking. There is also no 'data' field: that data appears as the top-level
    dictionary in a version-zero file
    """
    return {
        'version': 1,
        'metadata': utils.blank_metadata_template(),
        'data': db_data,
        'settings': utils.blank_settings_template(),
    }


def _unpack_database(f: Path) -> dict:
    """Given F, a Path to a MetadataStore on disk, read that file and return it.

    Tries to read the database as a compressed JSON file first. If that fails and we
    are (based on the preferences) allowed to read pickle files, then try that before
    giving up.
    """
    try:
        with bz2.open(f, 'rb') as metadata_file:
            return json.load(metadata_file)
    except (json.JSONDecodeError, UnicodeDecodeError, OSError) as err:
        if pref('UNSAFE_allow_pickle_load'):
            try:
                with bz2.open(f, 'rb') as metadata_file:
                    return pickle.load(metadata_file)
            except (pickle.PickleError, pickle.PicklingError, OSError) as errrr:
                raise RuntimeError(f"File cannot be unpickled! The system said: {errrr}.")
        else:
            raise RuntimeError(f"File cannot be decoded! The system said: {err}.\n"
                               f"(Unpickling is not currently allowed.)")


def read_and_validate_database(from_where: Path) -> collections.abc.MutableMapping:
    """Read in a stored database file and make sure that it passes basic sanity checks.
    Returns the stored structure that was created by the MetaDataStore's
    _disk_representation() method. Note that this includes not only data stored in
    the database (which is metadata about media files) but also the database's own
    metadata (about itself) and its settings, plus a file version number.

    Basic sanity checks are rather anemic here, but it does make some effort to
    update the structure from older versions, and it tries to correct problems that
    have arisen in the past while debugging if that can be done automatically, and
    also tries to foresee a few possible problems that could conceivably arise.
    """
    assert isinstance(from_where, Path)
    file_data = _unpack_database(from_where)
    if 'version' not in file_data:
        file_data = _update_version_zero_to_version_one(file_data)

    if file_data['version'] > utils.file_version:
        raise RuntimeError(f"Cannot open {from_where.name}! It was produced by a newer version of {utils.__progname__}")

    for entry in file_data['data']:
        if 'file_metadata' not in file_data['data'][entry]:
            file_data['data'][entry]['file_metadata'] = _read_filesystem_metadata(Path(entry))
        for field in ('cache_time', 'st_size', 'cache_size', 'st_mtime'):
            if field not in file_data['data'][entry]['file_metadata']:
                file_data['data'][entry]['file_metadata'] = _read_filesystem_metadata(Path(entry))
                break
        if 'next_processing' not in file_data['data'][entry]:
            file_data['data'][entry]['next_processing'] = list()
        if file_data['data'][entry]['next_processing']:
            if 'next_processing_params' not in file_data['data'][entry]:
                file_data['data'][entry]['next_processing_params'] = fu.default_processing_goals()

        else:
            if 'next_processing_params' in file_data['data'][entry]:
                del file_data['data'][entry]['next_processing_params']
        if 'tags' not in file_data['data'][entry]:
            file_data['data'][entry]['tags'] = list()
        else:           # eliminate duplicates while preserving order
            new_tags = list(collections.OrderedDict.fromkeys(file_data['data'][entry]['tags']).keys())
            file_data['data'][entry]['tags'] = new_tags
        if ('incrementally_tagged' in file_data['data'][entry]) and (file_data['data'][entry]['incrementally_tagged']):
            if not file_data['data'][entry]['tags']:
                file_data['data'][entry]['incrementally_tagged'] = False

    if 'settings' not in file_data:
        file_data['settings'] = utils.blank_settings_template()
    for field in utils.blank_settings_template():
        if (field not in file_data['settings']) or (not file_data['settings'][field]):
            file_data['settings'][field] = utils.blank_settings_template()[field]

    if 'metadata' not in file_data:
        file_data['metadata'] = utils.blank_metadata_template()
    for field in utils.blank_metadata_template():
        if (field not in file_data['metadata']) or (not file_data['metadata'][field]):
            file_data['metadata'][field] = utils.blank_metadata_template()[field]

    return file_data


def find_metadata_file(starting_where: Path) -> Union[Path, None]:
    """Given STARTING_WHERE, a path to a directory, finds the metadata file in that
    directory, or else in the directory above that directory that's closest to the
    specified starting path, or else None, if it ascends to the filesystem root
    without finding a metadata file.
    """
    assert isinstance(starting_where, Path)
    if not starting_where.is_dir():
        starting_where = starting_where.parent
    try:
        current_dir = starting_where
        while len(current_dir.parts) > 1:
            current_test = current_dir / utils.metadata_name
            if current_test.is_file():
                return current_test
            else:
                if current_dir.resolve() == (current_dir / os.pardir).resolve():
                    return None         # We're at the filesystem (or drive) root. Give up.
                else:
                    current_dir = (current_dir / os.pardir).resolve()       # Move UP to parent directory
    except Exception:
        return None         # Explicitly return None if there are any problems preventing the function from finishing.


def _read_movie_metadata_from_file(which_file: Path) -> dict:
    """Get a dictionary encoding metadata about a movie file. The following fields
    are defined:

        length, duration, bitrate, video stream count, frame width,
        frame height, audio stream count, audio bitrate, audio channels,
        audio sample rate

    Also defined: 'cache_size', the file size when this data was initially read
    (and/or calculated). We don't detect cache staleness from file mtime, because
    we want to preserve initial mtime as the time the file was added to the
    collection in some cases. And 'cache_time', the time that the file was initially
    cached.

    This function does not interact with the movie metadata cache at all: it simply
    reads the metadata from the file.
    """
    assert isinstance(which_file, Path)
    ret = dict()

    try:
        ret['cache_size'] = which_file.stat().st_size
        ret['cache_time'] = time.time()
        data = utils.run_process([pref('ffprobe_loc'), str(which_file)], print_output=False,
                                 stderr=subprocess.PIPE, stdout=subprocess.PIPE).stderr

        assert "Input #" in data, "ERROR: ffprobe output does not conform to expected format!"
        data = data[data.find("Input #"):]  # Chop off the program info data at the beginning
        data = [line.strip() for line in data.split('\n')[1:]]    # omit header row, strip whitespace
        duration_line = [line for line in data if line.casefold().startswith('duration:')][0]
        d, _, br = [i.strip() for i in duration_line.split(',')]

        # FIXME! .lstrip() doesn't strip the specified string, but all the characters in it! Works for now, tho.
        ret['length'] = d[len("duration:"):].casefold().strip()
        ret['bitrate'] = br[len("bitrate:"):].casefold().strip()
        if ret['bitrate'].endswith('kb/s'):
            ret['bitrate'] = ret['bitrate'][:len('kb/s')].strip()

        #   Example string to be parsed: Stream #0:0(und): Video: h264 (Constrained Baseline) (avc1 / 0x31637661)
        stream_lines = [default_cmp_func(line) for line in data if default_cmp_func(line).startswith('stream #')]
        video_streams = [line for line in stream_lines if 'video: ' in line]
        ret['video_streams'] = {line[:line.find('video: ')].strip().strip(':').strip(): line[7 + line.find('video: '):]
                                for line in video_streams}
        #   example: Stream #0:1(und): Audio: aac (LC) (mp4a / 0x6134706D), 44100 Hz, stereo, fltp, 58 kb/s (default)
        audio_streams = [line for line in stream_lines if 'audio: ' in line]
        ret['audio_streams'] = {line[:line.find('audio: ')].strip().strip(':').strip(): line[7 + line.find('audio: '):]
                                for line in audio_streams}
        ret['all_streams'] = stream_lines

        # next, video and audio stream data.
        video_lines = [line for line in data if "stream #" in line.casefold() and "video:" in line.casefold()]
        ret['video stream count'] = len(video_lines)
        try:
            ret['frame width'], ret['frame height'] = re.findall(r"""[0-9]{2,}x[0-9]{2,}""",
                                                                 video_lines[0])[0].casefold().split('x')
            ret['frame width'], ret['frame height'] = int(ret['frame width']), int(ret['frame height'])
        except IndexError:
            ret['frame width'], ret['frame height'] = "???", "???"
        audio_lines = [line for line in data if "stream #" in line.casefold() and "audio:" in line.casefold()]
        ret['audio stream count'] = len(audio_lines)
        if audio_lines:
            try:
                audio_data = audio_lines[0].casefold()[audio_lines[0].casefold().find('audio:') + len('audio:'):]
                codec, samprate, channels, _, brate = audio_data.strip().split(',')
                ret['audio sample rate'] = int(''.join([c for c in samprate if c.isdigit()]))
                ret['audio channels'] = utils.float_channels_from_textdesc(channels)
                ret['audio bitrate'] = int(''.join([c for c in brate if c.isdigit()]))
            except Exception as err:
                components = default_cmp_func(audio_data)
                if 'audio sample rate' not in ret:
                    try:
                        which = [c for c in components if 'hz' in c][0]
                        ret['audio sample rate'] = int(''.join([c for c in which if c.isdigit()]))
                    except Exception as err:
                        ret['audio sample rate'] = str(err)
                if 'audio channels' not in ret:
                    try:
                        which = [c.strip() for c in components if default_cmp_func(c) in
                                 utils.channel_textual_descriptions][0]
                        ret['audio channels'] = utils.float_channels_from_textdesc(which)
                    except Exception as err:
                        ret['audio channels'] = str(err)
                if 'audio bitrate' not in ret:
                    print("ERROR! Unable to determine audio bitrate for %s!" % which_file)
        else:
            ret['audio sample rate'], ret['audio channels'], ret['audio bitrate'] = None, None, None
    except Exception as err:
        print("WARNING: unable to analyze file %s fully because: %s" % (shlex.quote(str(which_file)), err))
    finally:
        return ret


def _read_filesystem_metadata(from_where: Path) -> dict:
    """Stat the file and return some filesystem metadata in a dictionary with defined
    fieldnames. This call does not interact with the cached data in a MetadataStore
    in any way at all; it just reads the data from the file and returns it.

    The following fields are currently tracked and returned:
        st_size, st_mtime

    Also defined is 'cache_time', the Unix time when the data was read and cached.
    """
    assert isinstance(from_where, Path)

    ret = dict()
    ret['cache_time'] = time.time()
    try:
        fs_data = from_where.stat()
        ret.update({'st_size': fs_data.st_size, 'cache_size': fs_data.st_size, 'st_mtime': fs_data.st_mtime})
    except (OSError,):
        ret.update({'st_size': 0, 'cache_size': 0, 'st_mtime': 0})
    return ret


class MetadataWriter(threading.Thread):
    """A class whose objects can be created and run as threads to write data to disk
    while returning control to the main program thread. Keeps a queue of data
    waiting to be written; if new data comes in faster than it can be written out,
    older data is abandoned. Since we really only want to write the most recent data
    (plus the second-most-recent data as the .bak file), this helps to ensure
    that we do not queue up thousands of changes that are written only to be
    overwritten later.

    To use, create a new object, save a reference to it in the parent object, and
    call its run() method. Initializing the object requires a reference to the
    parent MetadataSore, plus the data to be written (a JSON-serialized stream).
    While the runner is running, data can be appended to its queue from the main
    program thread (or any other thread, really). The thread will keep running until
    the queue is empty, at which time the thread quits, blanking out the reference
    to it that the parent object holds. Creation and running of threads are
    abstracted away by the MetadataStore's .write_data() method, which handles
    the whole writing kerfuffle.
    """
    _max_writing_queue_length = 4      # Maximum number of to-write objects tracked. 4 is overkill already.

    def __init__(self, parent: 'MetadataStore',
                 data: Optional[dict] = None,
                 completion_notification_func: Optional[Callable] = None):
        if data:
            assert isinstance(data, dict)
        threading.Thread.__init__(self)
        self.parent = parent
        self.to_write = list()          # Unlikely ever to grow all that long, so performance shouldn't be an issue.
        self.mutex = threading.Lock()
        if data:
            self.queue_for_write(data, completion_notification_func=completion_notification_func)

    def queue_for_write(self, data: dict,
                        completion_notification_func: Optional[Callable] = None) -> None:
        """Adds DATA (a structured file record) to the to-write queue. We pickle it on
        adding, then unpickle right before writing, because we want a snapshot of the
        data when it enters the queue, not a reference to data that may continue to
        change, including right while we're serializing it to JSON. Queueing a pickled
        snapshot ensures we're getting a copy of the data, not a reference to it.

        JSON encoding is done by the _write method because it is relatively slow, and
        doing that in the background helps to keep the process responsive when dealing
        with large files.

        Along with the data itself and the completion notification function, we also
        queue a string representing the current time, which helps when overlooking the
        process during debugging.
        """
        with self.mutex:
            self.to_write.append((pickle.dumps(data, protocol=-1), completion_notification_func,
                                  datetime.datetime.now().isoformat(' ')))
            if len(self.to_write) > self._max_writing_queue_length:  # Grown too long? Just keep most recent entries.
                sorted_queue = sorted(self.to_write, key=lambda i: i[2])
                print(f"DEBUG: pruning save queue ... there are {len(sorted_queue)} entries queued from "
                      f"{sorted_queue[0][2]} to {sorted_queue[-1][2]}")
                self.to_write = self.to_write[-self._max_writing_queue_length:]
                sorted_queue = sorted(self.to_write, key=lambda i: i[2])
                print(f"    ... sorted! Queue now contains {len(sorted_queue)} entries, queued from "
                      f"{sorted_queue[0][2]} to {sorted_queue[-1][2]}")

    def _write(self) -> None:
        """Writes the data that is waiting to be written to disk.
        """
        completion_func = None
        while self.to_write:
            fname = None
            while (fname is None) or (fname.exists()):
                fname = self.parent.file_location.with_name(str(uuid.uuid4()))
            try:
                with self.mutex:
                    data = self.to_write.pop(0)
                completion_func = data[1] or completion_func        # Track the latest non-nil completion function.

                # Now, write the data either in slow, safe JSON format or in quick, unsafe pickle format, based on prefs
                if not pref('UNSAFE_use_pickle_save'):
                    with bz2.open(fname, 'wt') as metadata_file:
                        metadata_file.write(utils.jsonify(pickle.loads(data[0])))
                else:
                    with bz2.open(fname, 'wb') as metadata_file:
                        # If the protocol specified in the preferences is -1, great! That's what we've already got!
                        # Dump it to disk!
                        if pref('UNSAFE_pickle_version') == -1:
                            metadata_file.write(data[0])
                        # Otherwise, un-pickle and re-pickle the data in the desired format.
                        else:
                            pickle.dump(pickle.loads(data[0]), metadata_file, protocol=pref('UNSAFE_pickle_version'))
                if self.parent.file_location.exists():
                    new_name = Path(str(self.parent.file_location) + '.bak')
                    self.parent.file_location.rename(new_name)
                fname.rename(self.parent.file_location)
            except (IndexError,):   # Shouldn't be popping from an empty list: we're the only thread removing data
                pass                    # from the to_write queue. Still, be careful anyway.
            except (Exception,) as errrr:
                print(f"Unable to write file! The system said: {errrr}")
            finally:
                if fname.exists():  # If we got this far and the temp file still exists, delete it.
                    fname.unlink()
        if completion_func:
            completion_func()
        self.parent.writer = None

    def run(self) -> None:
        self._write()


# The metadata collection is a dictionary that maps file paths to information about those files. "file paths" means
# paths to the files, relative to the database location.

# Fields that have been defined so far:
#   'recompressed'          : boolean: has video been recompressed already?
#   'tags'                  : list of strings: tags associated with the file
#   'movie_metadata'        : dict: metadata about the movie file, extracted from the movie, along with the Unix
#                               timestamp saying when it was extracted.
#   'next_processing'       : list of strings: parameters to be passed to ffmpeg next time a movie re-processing run
#                               happens.
#   'next_processing_params': dict: indicates constraints on the file's process. These are not command-line parameters,
#                               but rather notes on goals that processing will try to achieve.
#   'file_metadata'         : dict: cached metadata from the filesystem.
#   'original_mdate'        : Unix timestamp for when the file was first indexed.
#   'incrementally_tagged'  : datetime: when video was tagged incrementally through the incremental tagging process.
#   'incremental_temp_skip' : datetime: when file was temporarily skipped in the incremental tagging process.


class MetadataStore(collections.abc.MutableMapping):
    """Object to store metadata about video files. It must be initialized with a
    path to a specific metadata-file location; it then reads this file and remembers
    the location.

    Calling WRITE_METADATA before the object is destroyed is NECESSARY; the object
    does not attempt to update automatically on do_close. Many convenience functions
    write on metadata change by default, but not every change automatically results
    in a write, so it's a FANTASTIC idea to explicitly write when all changes have
    been made.
    """
    def __init__(self, file_location: Optional[Path] = None,
                 data: Union[dict, Iterable] = (),
                 ignore_existing_and_overwrite: bool = False,):
        """Sets up the data store object. Reads data from disk, if present, unless
        IGNORE_EXISTING_AND_OVERWRITE is True. If data is successfully read from disk,
        produces a backup copy of that data. Updates that data from the supplied DATA
        parameter, if supplied. Builds a list of all tags in the database and caches it.

        If IGNORE_EXISTING_AND_OVERWRITE is True, a new blank store is created and will
        overwrite the old one when the data is next written out.
        """
        if file_location:
            assert isinstance(file_location, Path)

        self.mapping = {}                                       # The actual underlying file-tracking data
        self.settings = utils.blank_settings_template()         # Settings stored in the VideoDataStore itself
        self.metadata = utils.blank_metadata_template()         # Metadata for this VideoDataStore.
        self.writer = None

        if file_location:
            if file_location.is_dir():
                file_location = file_location / utils.metadata_name
        else:
            starting_position = (Path('..') / utils.metadata_name).resolve()
            file_location = find_metadata_file(starting_position) or starting_position
        self.file_location = file_location

        if (not ignore_existing_and_overwrite) and file_location.is_file():
            file_data = read_and_validate_database(file_location)
            self.update(file_data['data'])
            self.settings = file_data['settings']
            self.metadata = file_data['metadata']
            shutil.copy(file_location, file_location.with_suffix(file_location.suffix + '.bak'))

        os.chdir(file_location.parent)
        if data:
            self.update(data)

    def move_database(self, new_location: Path,
                      delete_old_database: bool = True) -> None:
        """Moves the SELF database to a new location, which must be specified as (of
        course) NEW_LOCATION. NEW_LOCATION can be either a full path to a new file, in
        which case that becomes the full name ond location of the new file; or else it
        can be the full pathname of the folder that is to contain the database; in that
        second case, the filename is the same as it previously was in the previous
        folder. Either option can be a relative (to the current working directory, which
        is the old location of the database) instead of absolute path.

        Moving the database does NOT move the files it refers to; it simply means that,
        henceforth, they will be referred to by a path relative to the new location of
        the database instead of relative to the old location. This of course means that
        every key in the database has to be re-indexed using its new relative index.

        Unless DELETE_OLD_DATABASE is False, the old database is deleted. This is almost
        certainly a sensible move in almost all circumstances.
        """
        assert isinstance(new_location, Path)
        assert self.file_location

        self.write_data()           # Have an on-disk copy of the latest info before we start monkeying with it.
        try:
            self.writer.join()      # Wait for the write to finish.
        except (AttributeError,):   # If displayed_database.writer is None, or becomes None, the writer is finished.
            pass

        old_dir = os.curdir
        backup = self.__dict__.copy()           # Something to restore from if something goes wrong
        old_file_loc = self.file_location

        if new_location.is_dir():
            new_location = new_location / self.file_location.name
        new_location = new_location.resolve()

        try:
            # Produce a duplicate list of the data indexed in SELF, only indexed by canonical, instead of relative, URL
            absolute_index = dict()
            for key in self:
                absolute_index[Path(key).resolve()] = self[key]
                del self[key]

            # also produce a list of the ignore directories, indexed by canonical full path
            absolute_ignores = [Path(f).resolve() for f in self.settings['skipped_dirs']]

            # we now have a dict of every file, indexed by canonical path, with the metadata tracked for that file.
            # we also have an empty database. We now change working directory and the path to the database and re-add
            # the data, indexed relative to the new location.

            # We need there to be an actual file there, or relative_to() won't work.
            new_location.write_text('', encoding='utf-8')
            self.file_location = new_location
            os.chdir(new_location.parent)
            for key, value in absolute_index.items():
                self[key] = value               # path conversion is automatically performed by the db's dunder methods.

            self.settings['skipped_dirs'] = [str(utils.relative_to(new_location, f)) for f in absolute_ignores]

            self.write_data()                   # Write the data to disk in the new location
            if delete_old_database:
                old_file_loc.unlink()
        except (Exception,) as errrr:
            print(f"Unable to move database! The system said: {errrr}.")
            os.chdir(old_dir)
            self.__dict__ = backup

    @property
    def _all_tags_yielder(self) -> Generator[str, None, None]:
        """Yields every tag applied to any file in the database, one at a time, each tag
        exactly once.
        """
        seen = set()
        for f in self:
            if 'tags' in self[f]:
                for t in self[f]['tags']:
                    if t not in seen:
                        seen.add(t)
                        yield t

    @property
    def all_tags(self) -> Set[str]:
        """Construct a set of every tag on every file in the database.
        """
        return set(self._all_tags_yielder)

    def all_tags_with_counts(self, sorted_order: bool = False) -> Dict[str, int]:
        """Return a dictionary mapping each tag used in the database to the number of times
        it was used. If SORTED_ORDER is True, the returned dict has its keys in
        increasing (case-insensitive) alphabetical order; otherwise the order of entries
        is undefined.

        There is no one-by-one yielder for this function; there's no point, because it
        needs to see every tag for every file before it can report counts.

        I guess we could *yield* tags as we come across them, then *return* a dict
        of counts corresponding to it, but there's no current need for this.
        """
        ret = collections.defaultdict(int)
        for f in self:
            if 'tags' in self[f]:
                for t in self[f]['tags']:
                    ret[t] += 1
        if sorted_order:
            return {key: ret[key] for key in sorted(ret.keys(), key=utils.default_sort)}
        else:
            return dict(ret)

    def __repr__(self) -> str:
        ret = "MetadataStore"
        try:
            ret = ret.strip() + f" (for {len(self)} objects) "
        except Exception as errr:
            pass
        try:
            ret = ret.strip() + f" (with {len(self.all_tags)} unique tags) "
        except Exception as errr:
            pass
        try:
            ret = ret.strip() + (f" (with {sum([len(self[f]['tags']) if ('tags' in self[f]) else 0 for f in self])} "
                                 f"total tags) ")
        except Exception as errr:
            pass
        try:
            to_process = self.num_videos_to_process
            if to_process:
                ret = ret.strip() + f" (with {to_process} videos to be processed)"
        except Exception as errr:
            pass
        try:
            ret = ret.strip() + f" (stored at {self.file_location}) "
        except Exception as errr:
            pass
        return "< %s >" % ret.strip()

    def _normalize_index(self, key: Union[Path, str]) -> str:
        """Given KEY, normalize it so that it can effectively be used as an index to
        SELF. This means (1) ensure that it is a Path; and (2) make sure that it is
        relative to the file location of the data store, then (3) convert it back to
        a string.

        #FIXME! the type of KEY is often Path->str->Path, which is inefficient,
        especially because it often returns the same str that is passed in. It would be
        nice to optimize this operation,.
        """
        assert isinstance(key, (Path, str))

        if not isinstance(key, Path):
            key = Path(key)
        if key == key.absolute():
            key = key.relative_to(self.file_location.parent)
        return str(key)

    def __getitem__(self, key: Union[str, Path]) -> Any:
        return self.mapping[self._normalize_index(key)]

    def __delitem__(self, key: Union[str, Path]) -> None:
        del self.mapping[self._normalize_index(key)]

    def __setitem__(self, key: Union[str, Path],
                    value: Any) -> None:
        self.mapping[self._normalize_index(key)] = value

    def __iter__(self) -> Iterator:     # Make sure we iterate over the object in alphabetical order.
        return iter(sorted(self.mapping, key=str.casefold))

    def __len__(self) -> int:
        return len(self.mapping)

    def validate_filesystem_metadata(self, which_file: Path,
                                     metadata: dict,
                                     suppress_write: bool = False) -> dict:
        """Checks to make sure that the filesystem metadata has the expected structure and
        contains the expected information. #FIXME: currently, this only actually fixes
        a small number of known problems, largely those that arose when further
        development added new information that didn't exist for already-cached files
        in existing metadata stores.

        Returns the metadata, modified if and as necessary.
        """
        assert isinstance(which_file, Path)
        changed = False
        try:
            if 'cache_size' not in metadata:
                metadata['cache_size'] = which_file.stat().st_size
                changed = True
            try:
                if not isinstance(metadata['frame width'], int):
                    metadata['frame width'] = int(metadata['frame width'])
                    changed = True
            except Exception as errrr:
                pass
            try:
                if not isinstance(metadata['frame height'], int):
                    metadata['frame height'] = int(metadata['frame height'])
                    changed = True
            except Exception as errrr:
                pass
            try:
                if not isinstance(metadata['bitrate'], int):
                    metadata['bitrate'] = int(metadata['bitrate'])
                    changed = True
            except Exception as errrr:
                pass
            try:
                if not isinstance(metadata['audio sample rate'], int):
                    metadata['audio sample rate'] = int(metadata['audio sample rate'])
                    changed = True
            except Exception as errrr:
                pass
            return metadata
        finally:                                # On the way out, write changes, if necessary and allowed.
            if changed:
                if not suppress_write:
                    self.write_data()

    def get_filesystem_metadata(self, which_file: Path,
                                suppress_write: bool = False) -> Dict[str, Any]:
        """Get some filesystem metadata about WHICH_FILE from the cache, if it is stored
        and not stale; if that is not the case, get the data from the file itself and
        cache it.

        If we update the cache and SUPPRESS_WRITE is not True, then update the stored
        data on disk.
        """
        assert isinstance(which_file, Path)
        try:
            if which_file in self:
                if 'file_metadata' in self[which_file]:
                    if 'cache_size' in self[which_file]['file_metadata']:
                        if self[which_file]['file_metadata']['cache_size'] == which_file.stat().st_size:
                            return self.validate_filesystem_metadata(which_file, self[which_file]['file_metadata'],
                                                                     suppress_write=suppress_write)
        except (AttributeError,):
            pass
        ret = _read_filesystem_metadata(which_file)
        if which_file in self:
            self[which_file]['file_metadata'] = ret
        else:
            self[which_file] = {'file_metadata': ret}
        if not suppress_write:
            self.write_data()
        return ret

    def get_movie_metadata(self, which_file: Path,
                           suppress_write: bool = False) -> dict:
        """Gets movie metadata about WHICH_FILE. First it tries the cache to see if the
        data is in there and not stale; if both are true, returns cached data.

        If not, it reads the data from the file, caches it, and returns it.

        If we update the cache and SUPPRESS_WRITE is not True, write the updated
        metadata cache to disk.
        """
        assert isinstance(which_file, Path)
        assert which_file.exists()

        if which_file in self:
            if 'movie_metadata' in self[which_file]:
                if 'cache_size' in self[which_file]['movie_metadata']:
                    if which_file.stat().st_size == self[which_file]['movie_metadata']['cache_size']:
                        return self[which_file]['movie_metadata']
        ret = _read_movie_metadata_from_file(which_file)
        if which_file in self:
            self[which_file]['movie_metadata'] = ret
        else:
            self[which_file] = {'movie_metadata': ret}
        if not suppress_write:
            self.write_data()
        return ret

    def _populate_filesystem_metadata(self, p: Path) -> None:
        """Populate the data in SELF with filesystem metadata for the file P.
        """
        assert isinstance(p, Path)
        if p.is_file():
            _ = self.get_filesystem_metadata(p, suppress_write=True)

    def _populate_movie_metadata(self, p: Path) -> None:
        """Populate the data in SELF with movie metadata for the files P.
        """
        assert isinstance(p, Path)
        if p.is_file():                     # Otherwise, silently skip it.
            _ = self.get_movie_metadata(p, suppress_write=True)

    def _populate_original_date(self, p: Path) -> None:
        """Populate the data in SELF with original mtimes for the file P.
        """
        assert isinstance(p, Path)
        if p.is_file():                     # Otherwise, silently skip it.
            if p not in self:
                self[p] = {'original_mdate': p.stat().st_mtime}
            elif 'original_mdate' not in self[p]:
                self[p]['original_mdate'] = p.stat().st_mtime

    def add_file_to_database(self, p: Path) -> None:
        """Add data for P to the SELF database, reading in its metadata from the filesystem
        and parsing its video information.
        """
        self._populate_original_date(p)
        self._populate_filesystem_metadata(p)
        self._populate_movie_metadata(p)
        if 'tags' not in self[p]:
            self[p]['tags'] = list()
        if 'recompressed' not in self[p]:
            self[p]['recompressed'] = False
        if 'next_processing' not in self[p]:
            self[p]['next_processing'] = list()

    def populate_all_data(self, which_folder: Path,
                          include_video_files: bool = True,
                          include_audio_files: bool = False,
                          include_other_files: bool = False,
                          recurse: bool = False,
                          exclude_dirs: Iterable = (),
                          visited: Iterable = (),
                          suppress_write: bool = True) -> None:
        """Populates all relevant data for files in the folder specified as WHICH_DIR.
        If INCLUDE_VIDEO_FILES is True (the default), populates data for video files.
        If INCLUDE_AUDIO_FILES is True, also populates data for audio files. If
        INCLUDE_OTHER_FILES is True, also populates data for non-audio, non-video files.
        All three of these "is it a ... file?" determinations are performed by comparing
        a file's extension to the extensions listed in 'known_video_files' and
        'known_audio_files' preferences from the current preferences store chain.

        If RECURSE is True, also descend recursively into subdirectories. If
        EXCLUDE_DIRS is not empty, it must consist of filesystem Paths that refer to
        directories; those directories will not be included in the recursive search.

        When finishing up, writes the modified metadata to disk unless SUPPRESS_WRITE is
        True. (Note that when re-entering the function recursively, SUPPRESS_WRITE is
        always True to avoid writing the whole database out over and over due to small
        or nonexistent updates.)

        Leave VISITED set to an empty iterable; it's used internally when recursing to
        avoid getting caught in an infinite loop due to links, weird mountpoint setups,
        etc.
        """
        assert isinstance(which_folder, Path)
        assert which_folder.is_dir()
        for i in exclude_dirs:
            assert isinstance(i, Path)
        for i in visited:
            assert isinstance(i, Path)
        if not recurse:
            assert not exclude_dirs
            assert not visited

        # bail out immediately if we've already visited this directory.
        for f in visited:
            if which_folder.samefile(f):
                return

        # also bail out if WHICH_FOLDER matches any EXCLUDE_DIRS.
        for f in exclude_dirs:
            if which_folder.samefile(f):
                return

        l = list(which_folder.glob('*'))
        if not l:
            return

        vid_exts, aud_exts = pref('known_video_types'), pref('known_audio_types')
        for i, p in enumerate(l):
            if (((i + 1) % 100 == 0) and ((i + 1) > 0)) or (i >= (len(l) - 3)):
                print(f'populating and validating metadata for file {1 + i} of {len(l)} in folder {which_folder.name}/')
                if ((1 + 1) % 1000 == 0) or (i == len(l) - 1):     # only write out every thousand files, or at end.
                    if not suppress_write:
                        self.write_data()
            if p.is_file():
                if (default_cmp_func(p.suffix) in aud_exts) and not include_audio_files:
                    continue
                if (default_cmp_func(p.suffix) in vid_exts) and not include_video_files:
                    continue
                if (default_cmp_func(p.suffix) not in vid_exts) and (default_cmp_func(p.suffix) not in aud_exts):
                    if not include_other_files:
                        continue
                self.add_file_to_database(p)
            elif p.is_dir() and recurse:
                self.populate_all_data(which_folder=p, recurse=True, include_video_files=include_video_files,
                                       include_audio_files=include_audio_files, include_other_files=include_other_files,
                                       exclude_dirs=exclude_dirs, visited=(list(visited) + [which_folder]),
                                       suppress_write=True)
        if not suppress_write:
            self.write_data()

    @property
    def _all_tracked_file_extensions(self) -> Iterable[str]:
        """Returns an iterable containing all file suffixes tracked by all files in SELF.
        """
        return set((Path(f).suffix for f in self))

    def _any_extension_occurs(self, which_exts: Iterable[str]) -> bool:
        """Returns True if any extension listed in WHICH_EXTS occurs as the file suffix
        for any file tracked in SELF, or False otherwise.
        """
        for e in [default_cmp_func(ext) for ext in self._all_tracked_file_extensions]:
            if e in which_exts:
                return True
        return False

    @property
    def _tracks_audio_files(self) -> bool:
        """Returns True if there are any audio files (as determined by the prefs key
        'known_audio_types') tracked in SELF, or False if there are not.
        """
        return self._any_extension_occurs(pref('known_audio_types'))

    @property
    def _tracks_video_files(self) -> bool:
        """Returns True if there are any video files (as determined by the prefs key
        'known_audio_types') tracked in SELF, or False if there are not.
        """
        return self._any_extension_occurs(pref('known_video_types'))

    @property
    def _tracks_non_audio_non_video_files(self) -> bool:
        """Returns True if there are any non-audio, non-video files (as determined by the
        relevant prefs keys) tracked in SELF.
        """
        for e in [default_cmp_func(ext) for ext in self._all_tracked_file_extensions]:
            if (e not in pref('known_video_types')) and (e not in pref('known_audio_types')):
                return True
        return False

    def add_strip_phrase(self, which_str: str,
                         suppress_write: bool = False) -> None:
        """Adds a "strip phrase" tracked by the database. "Strip phrases" are strings that
        can (optionally) be removed from the beginning and end of filenames when files
        are moved in from holding directories.

        Writes SELF to disk when done unless SUPPRESS_WRITE is True.
        """
        which_str = which_str.strip()
        if which_str not in self.settings['strip phrases']:
            self.settings['strip phrases'].append(which_str)
        if not suppress_write:
            self.write_data()

    def _is_ignorable_directory(self, p: Path) -> bool:
        """Given P, a Path, determine whether P represents a directory that is either
        on the "ignore directories" list for SELF, or else a subdirectory of such a
        directory. If so, returns True.

        Returns False in all other circumstances, including if P is not a directory.

        Makes no attempt to determine whether P is in SELF, contains any files in SELF,
        or bears any relationship whatsoever to SELF besides being on the ignore-
        directories list for SELF or a directory beneath it.
        """
        assert isinstance(p, Path)
        if not p.is_dir():
            return False

        for ignore in [Path(p).resolve() for p in self.settings['skipped_dirs']]:
            if (ignore in p.parents) or (ignore == p):
                return True

        return False

    def _is_file_in_ignorable_directory(self, p: Path) -> bool:
        """Returns True if P is tracked in SELF and is beneath any ignore directories
        for SELF.

        Also returns True if P does not exist or is not a file. Returns False if P is
        not tracked in SELF.
        """
        assert isinstance(p, Path)
        if not p.is_file():
            return True
        if p not in self:
            return False

        return self._is_ignorable_directory(p.parent.resolve())

    def _clean_ignored_files(self) -> None:
        """Go through all files tracked in SELF, removing tracking information for any file
        that is beneath a 'skipped directory.'
        """
        for f in [Path(p) for p in self]:
            if self._is_file_in_ignorable_directory(f):
                del self[f]

    def scan_for_new_files(self) -> None:
        """Scan for new media files in all the directories containing media files that
        this database knows about. Includes audio files if the database is configured to
        include them. Same for video and "other" (non-audio, non-video) files.

        Descends through all the known directories recursively. Checks first to
        eliminate any subdirectories of directories that are already going to be
        searched so that nothing is walked twice.

        Takes steps to ensure that no files wind up in the database that are in (at any
        level of remove) a directory we're supposed to skip scanning.
        """
        dirs, skips = set(), set()
        for f in (Path(f).resolve() for f in self):
            if f.is_file():
                p = f.parent
            elif f.is_dir():
                p = f
            else:
                continue
            if (p in skips) or (p in dirs):
                continue
            if self._is_ignorable_directory(p):
                skips.add(p)
            else:
                dirs.add(p)

        cleaned_paths = set()
        for d in sorted(dirs, key=lambda fil: len(list(fil.parents))):  # traverse in shortest-to-longest-path order.
            dup = False
            for par in d.parents:
                if par in cleaned_paths:
                    dup = True                          # If a parent dir is in the to-visit list, skip its child dir.
            if not dup:
                cleaned_paths.add(d)

        # Now, recurse through each of the unique directories we've found, adding any new files.
        oldlen = len(self)
        sett = self.settings
        track_video, track_audio, track_other = sett['include video'], sett['include audio'], sett['include others']
        for p in cleaned_paths:
            self.populate_all_data(p, track_video, track_audio, track_other, recurse=True,
                                   exclude_dirs=[Path(d) for d in self.settings['skipped_dirs']], suppress_write=True)

        # Finally, ensure that nothing in the database is in a directory we're supposed to skip scanning.
        self._clean_ignored_files()
        print(f"Scanned {len(cleaned_paths)} top-level directories and their subdirectories, and "
              f"added {len(self)-oldlen} files!")
        self.write_data()

    def prune_missing_files(self, remove_handler: Optional[Callable[[Path], None]]=None) -> None:
        """Goes through all the files tracked in self.mapping, removing entries for files that
        no longer exist. Writes the data store to disk after being done.

        If REMOVE_HANDLER is specified, it will be called for every file that is pruned.

        Also prunes any files that are beneath an any ignore directories.
        """
        orig_length = len(self)
        for p in [Path(f) for f in self]:
            pth = self.file_location.parent / p
            if not pth.exists() or not pth.is_file():
                del self[pth]
                if remove_handler:
                    remove_handler(pth)
        self._clean_ignored_files()
        if len(self) != orig_length:
            print("Pruned %d entries!" % (orig_length - len(self)))
            self.write_data()
        else:
            print("No files pruned!")

    def rename_file(self, which_file: Path,
                    new_name: Path,
                    suppress_write: bool = False) -> None:
        """Rename WHICH_FILE to NEW_NAME in the filesystem, while associating the data
        currently indexed with WHICH_FILE in SELF under the NEW_NAME instead.

        Will happily overwrite an existing file at NEW_NAME with no warning.

        Unless suppress_write is True, writes the updated SELF to disk.
        """
        assert isinstance(which_file, Path) and isinstance(new_name, Path)
        if which_file.resolve() == new_name.resolve():
            return      # Do nothing if old and new names are (effectively) the same.

        try:
            which_file.rename(new_name)
        except Exception as err:
            print("ERROR: unable to rename %s, because %s!" % (shlex.quote(str(which_file)), err))
            return
        if which_file not in self:                      # If we have no mapping tracked, no more work is necessary!
            return
        self[new_name] = self[which_file]
        del self[which_file]
        if not suppress_write:
            self.write_data()

    def _disk_representation(self) -> Dict[str, Union[int, float, str, dict]]:
        """Produce the on-disk representation of SELF, which is a dict that encodes the
        data. This contains, and, mostly but not entirely, consists of, the SELF.mapping
        underlying dictionary. It also includes a file-version number, which is not the
        same as the version of VideoDataStore.
        """
        return {
            'version': utils.file_version,
            'metadata': self.metadata,
            'data': self.mapping,
            'settings': self.settings,
        }

    def write_data(self, completion_notification_func: Optional[Callable] = None) -> None:
        """Write the file metadata to disk. Or, rather, serialize it using the json
        module, or the pickle module if the user has enabled unsafe saves, and enqueue
        the data for writing.
        """
        if self.writer:
            self.writer.queue_for_write(data=self._disk_representation(),
                                        completion_notification_func=completion_notification_func)
            # Avoid races: if self.writer has disappeared while we're adding the data to be queued, just try again.
            if not self.writer:
                self.write_data(completion_notification_func=completion_notification_func)
        else:
            self.writer = MetadataWriter(parent=self, data=self._disk_representation(),
                                         completion_notification_func=completion_notification_func)
            self.writer.start()

    def export_metadata(self, export_location: Optional[Path] = None) -> None:
        """Export the data store in JSON format.

        Currently, this exports only the file-related data, not any metadata or
        settings stored in the VideoDataStore. As of now, there is no plan to change
        this.
        """
        if export_location is None:
            export_location = self.file_location.with_name('metadata_export.json')
        else:
            assert isinstance(export_location, Path)
            assert not export_location.is_dir()
        export_location.write_text(utils.jsonify(self.mapping))

    def import_metadata(self, import_location: Optional[Path] = None,
                        suppress_write: bool = False) -> None:
        """Import a JSON file into the metadata store, supplementing but not replacing the
        data already there. (Duplicate keys are overwritten, however.)
        """
        if import_location is None:
            import_location = self.file_location.with_name('metadata_export.json')
        else:
            assert isinstance(import_location, Path)
            assert not import_location.is_dir()

        json_data = json.loads(import_location.read_text())
        for i, f in enumerate(json_data):
            if f in self:
                self[f].update(json_data[f])
            else:
                self[f] = json_data[f]
            if (((i + 1) % 1000) == 0) or ((i + 1) >= (len(json_data) - 3)):
                print("Imported %d of %d entries!" % (i + 1, len(json_data)))
        if not suppress_write:
            self.write_data()

    def add_tag(self, which_file: Path,
                tag: str,
                suppress_write: bool = False) -> None:
        """Adds a tag to the stored tags for a file.

        Writes the changed metadata to disk unless SUPPRESS_WRITE is True.
        """
        assert isinstance(which_file, Path)
        which_file = which_file.resolve()

        if 'tags' in self[which_file]:
            if tag not in self[which_file]['tags']:
                self[which_file]['tags'] += [tag]
        else:
            self[which_file]['tags'] = [tag]

        if not suppress_write:
            self.write_data()

    def add_tags(self, which_file: Path,
                 tags: Iterable[str],
                 suppress_write: bool = False) -> None:
        """Adds all tags in TAGS to the database entry for WHICH_FILE.

        Writes the database to disk, unless SUPPRESS_WRITE is True.
        """
        assert isinstance(which_file, Path)
        which_file = which_file.resolve()

        for t in tags:
            self.add_tag(which_file, t, suppress_write=True)
        if not suppress_write:
            self.write_data()

    def set_tags(self, which_file: Path,
                 tags: Iterable[str],
                 suppress_write: bool = False) -> None:
        """Sets the known tags for WHICH_FILE to be exactly and precisely TAGS, losing
        all other tags attached to WHICH_FILE that are not specified in TAGS.
        """
        assert isinstance(which_file, Path)
        assert isinstance(tags, Iterable)
        for t in tags:
            assert isinstance(t, str)

        self[which_file]['tags'] = list()
        self.add_tags(which_file, tags, suppress_write=True)
        if not suppress_write:
            self.write_data()

    def get_tags(self, which_file: Path) -> List[str]:
        """Returns a list of all tags associated with a given file.
        """
        assert isinstance(which_file, Path)
        if 'tags' not in self[which_file]:
            self[which_file]['tags'] = list()
        return self[which_file]['tags']

    def delete_all_tags(self, which_file: Path,
                        suppress_write: bool = True) -> None:
        """Delete all tags from WHICH_FILE.

        If SUPPRESS_WRITE is False, writes the database to disk. Default is True
        because this small operation is frequently called in loops.
        """
        assert isinstance(which_file, Path)
        self[which_file]['tags'] = list()
        if not suppress_write:
            self.write_data()

    def delete_tag_from_all(self, which_tag: str,
                            cmp_func: Optional[Callable] = None,
                            suppress_write: bool = False) -> None:
        """Deletes WHICH_TAG from every single file in the database which is currently
        tagged with it. Does not touch any other tags, or any files not currently tagged
        with WHICH_TAG. If WHICH_TAG is not currently attached to any files, silently
        does nothing.

        If a CMP_FUNC is specified, it is passed to get_files_with_tag when tags are
        found; if not, that function's default comparison function (which strips
        leading/trailing whitespace and compares case-insensitively) is used.

        Unless SUPRESS_WRITE is True, writes the changed database to disk.
        """
        cmp_func = cmp_func or default_cmp_func
        for f in self.get_files_with_tag(which_tag, cmp_func):
            tags = self.get_tags(f)
            cmp_tags = [cmp_func(t) for t in tags]
            while cmp_func(which_tag) in cmp_tags:
                idx = cmp_tags.index(cmp_func(which_tag))
                tags.pop(idx)
                cmp_tags.pop(idx)
            self.set_tags(f, tags, suppress_write=True)

        if not suppress_write:
            self.write_data()

    def delete_tags_from_all(self, which_tags: Iterable[str],
                             cmp_func: Optional[Callable] = None,
                             suppress_write: bool = False) -> None:
        """Convenience function: wraps delete_tags_from_all(), above, so that multiple tags
        can be deleted with a single call. WHICH_TAGS are of course the tags to delete,
        and CMP_FUNC is the comparison function passed along to single-tag deletions.

        Writes out the changed database unless SUPPRESS_WRITE is True.
        """
        for t in which_tags:
            self.delete_tag_from_all(t, cmp_func, suppress_write=True)
        if not suppress_write:
            self.write_data()

    def get_tags_starting_with(self, prefix: str) -> List[str]:
        """Get a list of all tags known by this object that begin with the string
        PREFIX. This tag-searching comparison is performed without regard to case and
        after all strings involved have leading and trailing whitespace stripped.
        """
        prefix = default_cmp_func(prefix)
        return [t for t in self.all_tags if default_cmp_func(t).startswith(prefix)]

    def auto_tag_by_file_name(self, directory_path: Path,
                              tag_name: str,
                              words_in_file_name: List[str]) -> None:
        """Searches NON-RECURSIVELY through the directory specified by DIRECTORY_PATH,
        looking for files that have (case-insensitively) any of the words in the
        WORDS_IN_FILE_NAME list in their name. Any files that do have any of these words
        in their name get the tag TAG_NAME added to their entries in the metadata file.

        #FIXME: currently unused!
        """
        assert isinstance(directory_path, Path)

        for f in directory_path.glob('*'):
            if f.is_file():
                for w in words_in_file_name:
                    if default_cmp_func(w) in default_cmp_func(str(f)):
                        self.add_tag(f, tag_name, suppress_write=True)
        self.write_data()

    def get_files_with_tag(self, which_tag: str,
                           cmp_func: Optional[Callable] = default_cmp_func) -> List[Path]:
        """Given a tag, return a list of all files known by the MetadataStore that have that
        tag associated with them. Comparisons on tags are performed without regard to
        case, unless a different CMP_FUNC is specified, in which case the supplied
        function does the string normalization.
        """
        ret = list()
        which_tag = cmp_func(which_tag)
        for f in self:
            if 'tags' in self[f]:
                if which_tag in [cmp_func(t) for t in self[f]['tags']]:
                    ret.append(Path(f))
        return ret

    def get_video_play_time(self, which_file: Union[str, Path]) -> float:
        """Get the cached video play time for WHICH_FILE, assuming it is cached in SELF.
        """
        try:
            ret = datetime.datetime.strptime(self[which_file]["movie_metadata"]["length"], '%H:%M:%S.%f')
            return (ret - datetime.datetime(1900, 1, 1, 0)).total_seconds()
        except (KeyError, ValueError,):
            return -1

    def get_video_file_size(self, which_file: Path) -> int:
        """Get the cached video file size for WHICH_FILE, assuming it is cached in SELF.

        If it is not cached, simply return 0, without making any effort to determine
        the correct answer.
        """
        assert isinstance(which_file, Path)

        try:
            return self[which_file]["file_metadata"]["cache_size"]
        except (KeyError,):
            return 0

    def get_video_frame_size(self, which_file: Path) -> int:
        """Get the cached area (in pixels) for WHICH_FILE, assuming it is cached in SELF.
        """
        assert isinstance(which_file, Path)

        try:
            meta = self[which_file]["movie_metadata"]
            return int(meta["frame height"]) * int(meta["frame width"])
        except (KeyError, ValueError, TypeError):
            return 0

    def get_video_frame_dimensions(self, which_file: Path) -> Union[Tuple[int, int], None]:
        """Return a (width, height) tuple representing the width and height of
        WHICH_FILE, as those stats are represented in SELF.

        If the data isn't cached or can't be interpreted, return None.
        """
        assert isinstance(which_file, Path)

        try:
            metadata = self[which_file]['movie_metadata']
            return int(metadata['frame height']), int(metadata['frame width'])
        except (KeyError, ValueError, TypeError):
            return None

    def ensure_next_processing(self, which_file: Path) -> None:
        """Makes ABSOLUTELY SURE that self[which_file] has a 'next_processing' list and a
        'next_processing_params' dictionary.
        """
        assert isinstance(which_file, Path)
        if ('next_processing' not in self[which_file]) or not isinstance(self[which_file]['next_processing'], list):
            self[which_file]['next_processing'] = list()
        if 'next_processing_params' not in self[which_file]:
            self[which_file]['next_processing_params'] = fu.default_processing_goals()
        if not isinstance(self[which_file]['next_processing_params'], dict):
            self[which_file]['next_processing_params'] = fu.default_processing_goals()

    def clear_next_processing(self, which_file: Path) -> None:
        """Make sure that WHICH_FILE is not scheduled for any work on the next processing
        run.

        Calling this function is preferable to simply setting a file's 'next_processing'
        attribute to an empty list, because at some point in the future more will be
        required than simply clearing the list of ffmpeg parameters to be used on the
        next run. When that happens, functions that call this method will be compliant,
        while functions that simply set 'next_processing' to an empty list will be
        introducing subtle problems to databases.

        GUI code calling this should dispatch EVENT_file_processing_parameters_change.
        """
        self[which_file]['next_processing'] = list()

    def set_argument_parameter(self, which_file: Path,
                               argument: str,
                               argument_parameter: Union[str, None],
                               only_if_absent: bool = False,
                               suppress_write: bool = True) -> None:
        """Sets the 'next_processing' argument and its parameter, as defined in
        fu.set_argument_parameter(). If ONLY_IF_ABSENT is specified to be True, then the
        function will do nothing instead of overwriting an existing value.

        Unless suppress_write is True (the default), writes the database to disk after
        the change is made. Because this is a small operation and the method is often
        called repeatedly, the default for SUPPRESS_WRITE, unusually, is True.

        GUI code calling this should dispatch EVENT_file_processing_parameters_change.
        """
        assert isinstance(which_file, Path)
        if only_if_absent and (argument in self[which_file]['next_processing']):
            return
        self.ensure_next_processing(which_file)
        fu.set_argument_parameter(self[which_file]['next_processing'], argument, argument_parameter)
        if not suppress_write:
            self.write_data()

    def vf_filterchain_as_dict(self, which_file: Path) -> fu.FilterChain:
        """Get an OrderedDict representing the filterchain for the currently specified
        -vf options for WHICH_FILE, or an empty dictionary if none are currently
        specified.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        return fu.get_video_filterchain(self[which_file]['next_processing'])

    def af_filterchain_as_dict(self, which_file: Path) -> fu.FilterChain:
        """Get an OrderedDict representing the filterchain for the currently specified
        -af options for WHICH_FILE, or an empty dictionary if none are currently
        specified.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        return fu.get_audio_filterchain(self[which_file]['next_processing'])

    def clear_vf_option(self, which_file: Path,
                        vf_key: str) -> None:
        """Removes all references to VF_KEY from the filterchain list. If VF_KEY is not in
        the filterchain list, silently does nothing.
        """
        self.ensure_next_processing(which_file)
        params = fu.get_video_filterchain(self[which_file]['next_processing'])
        if vf_key in params:
            fu.set_argument_parameter(self[which_file]['next_processing'], '-vf', params.as_str())

    def set_vf_option(self, which_file: Path,
                      vf_key: str,
                      vf_value: Union[str, None],
                      allow_repeated: bool = False,
                      before: Iterable = (),
                      suppress_write: bool = False) -> None:
        """Sets a filterchain option for the -vf switch for WHICH_FILE, which will be
        used on the next processing run. If the -vf switch is not yet used, adds it to
        the end of the 'next_processing' list. If the -vf switch is already used, adds
        the new option to the end of the filterchain list if VF_KEY does not already
        appear in the filterchain list; if VF_KEY does appear, VF_VALUE replaces the
        previously used value while keeping the key-value pair in its current location
        relative to the rest of the filterchain text.

        In all cases, setting VF_VALUE to None will remove the relevant key-value
        pair from the 'next_processing' list.

        If BEFORE is specified, it must be a list of option names that this option
        will ensure occur only after this option is added. That is, it adds the option
        in the middle, before any items that are in the list.

        Writes the modified metadata to disk unless SUPPRESS_WRITE is True.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)

        f = fu.get_video_filterchain(self[which_file]['next_processing'])
        f.set_key_value_pair(key=vf_key, value=vf_value, allow_repeated=allow_repeated, before=before)
        self[which_file]['next_processing'] = fu.set_argument_parameter(self[which_file]['next_processing'],
                                                                        '-vf', f.as_str())

        if not suppress_write:
            self.write_data()

    def set_noarg_vf_option(self, which_file: Path,
                            which_option: str,
                            allow_repeated: bool = False,
                            suppress_write: bool = False) -> None:
        """Just for the sake of being difficult, some ffmpeg options are flags that take
        no arguments; their presence IS the flag. This function adds WHICH_OPTION, a
        flag of this type, to the current filterchain list for WHICH_FILE. If
        WHICH_OPTION is already in the current filterchain list, does nothing, unless
        ALLOW_REPEATED is True.

        If changes are made, writes metadata to disk unless SUPPRESS_WRITE is True.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)

        filterchain_str = fu.get_argument_parameter(self[which_file]['next_processing'], '-vf')
        filterchain = fu.FilterChain(filterchain_str)
        filterchain.__setitem__(key=which_option, allow_repeated=allow_repeated)
        fu.set_argument_parameter(self[which_file]['next_processing'], '-vf', str(filterchain))

        if not suppress_write:
            self.write_data()

    def clear_noarg_vf_option(self, which_file: Path,
                              which_option: str,
                              suppress_write: bool = False) -> None:
        """Just for the sake of being difficult, some ffmpeg options are flags that take
        no arguments; their presence IS the flag. This function removes WHICH_OPTION, a
        flag of this type, from the current filterchain list for WHICH_FILE. If
        WHICH_OPTION is not in the current filterchain list, does nothing.

        If WHICH_OPTION occurs multiple times in the -vf filterchain, all
        occurrences are deleted.

        If changes are made, writes metadata to disk unless SUPPRESS_WRITE is True.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)

        filterchain_str = fu.get_argument_parameter(self[which_file]['next_processing'], '-vf')
        filterchain = fu.FilterChain(filterchain_str)
        filterchain.__delitem__(key=which_option)
        fu.set_argument_parameter(self[which_file]['next_processing'], '-vf', str(filterchain))

        if not suppress_write:
            self.write_data()

    def set_af_option(self, which_file: Path,
                      af_key: str,
                      af_value: Union[str, None],
                      suppress_write: bool = False):
        """Sets a filterchain option for the -af switch for WHICH_FILE, which will be
        used on the next processing run. Operates on the same principles and according
        to the same caveats as set_vf_option, except of course that -af is used (to
        specify audio filterchain options) instead of -vf (to specify video filterchain
        options.)
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)

        params = fu.get_audio_filterchain(self[which_file]['next_processing'])
        if af_value is not None:
            params[af_key] = af_value
        elif af_key in params:
            del params[af_key]
        fu.set_argument_parameter(self[which_file]['next_processing'], '-af', params.as_str())
        if not suppress_write:
            self.write_data()

    def _get_planned_audio_codec(self, which_file: Path) -> Union[str, None]:
        """Query the planned recompression for WHICH_FILE and determine which audio codec
        will be used. If no plans have yet been made in this regard, returns None.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        try:
            return fu.specified_audio_codec(self[which_file]['next_processing'])
        except (KeyError,):
            return None

    def _get_planned_video_codec(self, which_file: Path) -> Union[str, None]:
        """Query the planned recompression for WHICH_FILE and determine which video codec
        will be used. If no plans have yet been made in this regard, returns None.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        try:
            return fu.specified_video_codec(self[which_file]['next_processing'])
        except (KeyError,):
            return None

    def _get_planned_crop(self, which_file: Path) -> Union[utils.Rect, None]:
        """Query the plans for cropping, if there are any, and return them. If there is
        no plan to crop, return None.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        return fu.specified_crop_size(self[which_file]['next_processing'])

    def _set_planned_crop(self, which_file: Path,
                          new_crop: utils.Rect) -> None:
        """Schedule the file to be cropped to NEW_CROP on the next processing run. If there
        are already existing plans to crop, they are silently overwritten.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        self[which_file]['next_processing'] = fu.set_crop(self[which_file]['next_processing'], new_crop)

    def validate_crop_dimensions(self, which_file: Path) -> List[str]:
        """Check the planned crop rectangle for WHICH_FILE, if we have cropping scheduled
        for WHICH_FILE, and ensure that it's a valid cropping rectangle.

        Returns the entire set of "next_processing" params after validating the cropping
        rectangle is valid. Does no other validation of any kind.
        """
        assert isinstance(which_file, Path)

        old_crop = self._get_planned_crop(which_file)
        if old_crop:                                    # no crop planned? Nothing to do!
            new_crop = old_crop.copy()
            new_crop.constrain(utils.Rect(utils.Point(0, 0), utils.Point(*self.get_video_frame_dimensions(which_file))))
            if new_crop != old_crop:
                self._set_planned_crop(which_file, new_crop)
        return self[which_file]["next_processing"]

    def validate_codec_specs(self, which_file: Path) -> None:
        """Makes sure that the codec options specified in the 'next_processing' list for
        WHICH_FILE are valid, according to the criteria laid out above in
        fu.validate_codec_specs(), to which function this function is merely a friendly
        wrapper.
        """
        assert isinstance(which_file, Path)
        if 'next_processing' in self[which_file]:
            self[which_file]['next_processing'] = fu.validate_codec_specs(self[which_file]['next_processing'])
        else:
            self.ensure_next_processing(which_file)

    def schedule_audio_removal(self, which_file: Path,
                               suppress_write: bool = False):
        """Adds some parameters to the 'next_processing' list for WHICH_FILE that will
        cause the WHICH_FILE's audio track to be removed when the next movie-processing
        run happens.

        Like all the other schedule_* functions, this does not actually remove the audio
        on its own; it just schedules audio removal for when a processing run occurs.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        self.validate_codec_specs(which_file)

        self[which_file]['next_processing'].append('-an')
        self.validate_codec_specs(which_file)
        if '-ar' in self[which_file]['next_processing']:
            self.set_argument_parameter(which_file, '-ar', None)
        if '-acodec' in self[which_file]['next_processing']:
            self.set_argument_parameter(which_file, '-acodec', None)
        if not suppress_write:
            self.write_data()

    def schedule_audio_downsampling(self, which_file: Path,
                                    bitrate: int = 44100,
                                    audio_codec: Optional[str] = None,
                                    suppress_write: bool = False) -> None:
        """Schedules audio downsampling for WHICH_FILE for the next processing run.

        Writes the modified metadata store to disk unless SUPPRESS_WRITE is True. (This
        is handy for quick single-file changes but very time-consuming when many files
        are being updated at once.) The audio re-encoding will use AUDIO_CODEC if
        specified, or the default audio encoder specified in the preferences chain
        otherwise.

        Does not actually do the downsampling, just schedules it for the next run.
        """
        assert isinstance(which_file, Path)
        self.ensure_next_processing(which_file)
        self.set_argument_parameter(which_file, "-acodec", audio_codec or pref('default_audio_codec'))
        self.set_argument_parameter(which_file, "-ar", str(bitrate))
        self.validate_codec_specs(which_file)
        if not suppress_write:
            self.write_data()

    def schedule_audio_normalization(self, which_file: Path,
                                     target: float,
                                     range: float,
                                     true_peak: float,
                                     dual_mono: bool,
                                     suppress_write: bool = False) -> None:
        """Given the relevant parameters, schedule an audio normalization event for the
        next processing run.
        """
        assert isinstance(which_file, Path)

        params = f"""i={target},LRA={range},TP={true_peak}"""
        if dual_mono:
            params += ",dual_mono"

        self.set_af_option(which_file, 'loudnorm', params, suppress_write=True)
        if not suppress_write:
            self.write_data()

    def schedule_video_resize(self, which_file: Path,
                              new_width: Optional[int] = None,
                              new_height: Optional[int] = None,
                              max_width: Optional[int] = None,
                              max_height: Optional[int] = None,
                              suppress_write: bool = False) -> None:
        """Schedules video resizing for the next processing run. If NEW_WIDTH and/or
        NEW_HEIGHT are specified, uses those as the new height and width of the video,
        after making sure that they meet the basic parameters for allowed video
        dimensions: divisible by two, not too big or small. If neither is specified,
        just checks that the current video size meets those basic requirements and
        adjusts as necessary if not.

        Instead of specifying NEW_WIDTH and NEW_HEIGHT, a MAX_HEIGHT and MAX_WIDTH can
        be specified instead. In that case, a new size will be picked automatically that
        preserves the aspect ratio of the original video and maximizes its new size.
        NEW_HEIGHT and NEW_WIDTH cannot be specified at the same time as MAX_HEIGHT and
        MAX_WIDTH. If either of MAX_WIDTH and MAX_HEIGHT is specified, both must be.

        Writes the modified metadata store to disk unless SUPPRESS_WRITE is True.

        Does not actually resize the video, just schedules resizing for the next run.
        """
        assert isinstance(which_file, Path)
        if new_width is None:
            assert new_height is None
        if new_height is None:
            assert new_width is None
        if (max_width is not None) or (max_height is not None):
            assert max_height is not None
            assert max_width is not None
            assert new_width is None
            assert new_height is None
        self.ensure_next_processing(which_file)

        metadata = self.get_movie_metadata(which_file, suppress_write=suppress_write)
        if max_width is not None:
            # if we've reached this point and this is True, max_height must also not be None.
            # it must also be the case that a new width and height are not exactly specified.
            new_width, new_height = int(metadata['frame width']), int(metadata['frame height'])
            if (new_width <= max_width) and (new_height <= max_height):
                return                                  # ergo, nothing to do here!
            aspect_ratio = new_width / new_height
            height_based_on_width = max_width / aspect_ratio
            width_based_on_height = max_height * aspect_ratio
            if height_based_on_width > max_height:
                new_height, new_width = max_height, min(round(max_height * aspect_ratio), max_width)
            elif width_based_on_height > max_width:
                new_height, new_width = min(round(max_width / aspect_ratio), max_height), max_width
            else:
                area_maximizing_width = round(max_width * (max_width / aspect_ratio))
                area_maximizing_height = round(max_height * (max_height * aspect_ratio))
                if area_maximizing_width >= area_maximizing_height:
                    new_width, new_height = max_width, round(max_width / aspect_ratio)
                else:
                    new_width, new_height = round(max_height * aspect_ratio), max_height

        if new_width is None:
            new_width, new_height = int(metadata['frame width']), int(metadata['frame height'])
            aspect_ratio = new_width / new_height
            if new_width > pref('max_video_width'):
                new_width = pref('max_video_width')
                new_height = round(pref('max_video_width') / aspect_ratio)
            if new_height > pref('max_video_height'):
                new_height = pref('max_video_height')
                new_width = round(pref('max_video_height') * aspect_ratio)
        while new_height % pref('video_size_factor') != 0:
            new_height -= 1
        while new_width % pref('video_size_factor') != 0:
            new_width -= 1
        self.set_vf_option(which_file, 'scale', '%d:%d' % (new_width, new_height), suppress_write=True)
        if not suppress_write:
            self.write_data()

    def schedule_aspect_ratio_change(self, which_file: Path,
                                     new_width: int,
                                     new_height: int,
                                     fully_reencode: bool,
                                     suppress_write: bool = False) -> None:
        """Schedule an aspect ratio change for WHICH_FILE during the next processing run.
        NEW_WIDTH and NEW_HEIGHT must be specified. If FULLY_REENCODE is True, then the
        video will be re-encoded; this is slow and lossy but is honored by all video
        players, because it entirely re-creates the video stream. If FULLY_REENCODE is
        False, no re-encoding is performed; an "aspect ratio" header is instead written
        to the file. This is fast and involves no loss of quality, but not all video
        players honor it.
        """
        try:
            if fully_reencode:
                self.schedule_video_resize(which_file=which_file, new_width=new_width, new_height=new_height,
                                           suppress_write=True)
            else:
                self.ensure_next_processing(which_file)
                self.set_argument_parameter(which_file, argument='-aspect',
                                            argument_parameter=f"{new_width}:{new_height}", suppress_write=True)
        finally:
            if not suppress_write:
                self.write_data()

    def schedule_video_rotation(self, which_file: Path,
                                how_much: int,
                                soft_rotate: bool = False,
                                suppress_write: bool = False):
        """Schedules video rotation for WHICH_FILE on the next processing run. If
        SOFT_ROTATE is True, just encodes the rotation into the video's metadata; if
        False (the default), the video will actually be re-encoded with the new
        orientation. (This is the default because not all video players respect the
        video orientation stored in the metadata.)

        HOW_MUCH indicates how many degrees clockwise the video is to be rotated. It
        must be a multiple of 90 (degrees) and between 0 and 270. Notice that HOW_MUCH
        is NOT the parameter that is ultimately passed to ffmpeg; it is used to
        CALCULATE that parameter.

        Writes the modified metadata store to disk unless SUPPRESS_WRITE is True.

        Does not actually rotate the video; just schedules the rotation for the next
        processing run.
        """
        assert (0 <= how_much < 360) and (how_much % 90 == 0), "Valid rotation amounts are 0, 90, 180, and 270 degrees"
        if soft_rotate:
            switch = 'rotate'
            self.clear_vf_option(which_file, 'transpose')
        else:
            switch = 'transpose'
            self.clear_vf_option(which_file, 'rotate')
        for _ in range(how_much // 90):
            self.set_vf_option(which_file, switch, "1", allow_repeated=True, suppress_write=True)
        if not suppress_write:
            self.write_data()

    def schedule_video_flip(self, which_file: Path,
                            horizontal: bool = False,
                            vertical: bool = False,
                            suppress_write: bool = False) -> None:
        """Schedules a horizontal and/or vertical flip of the video image for the next
        processing run. Unless SUPPRESS_WRITE is True, writes the changed metadata file
        to disk when finished.
        """
        if (not horizontal) and (not vertical):       # Nothing to do? Bail out.
            return
        if horizontal:
            self.set_noarg_vf_option(which_file, 'hflip', suppress_write=True)
        if vertical:
            self.set_noarg_vf_option(which_file, 'vflip', suppress_write=True)
        if not suppress_write:
            self.write_data()

    @property
    def _raw_incrementally_tagged_list(self) -> Generator[str, None, None]:
        """Convenience function for the next two properties: gets the raw list of indexable
        keys corresponding to files that have been incrementally tagged. Used primarily
        by other properties that massage the data it returns.
        """
        return (f for f in self if ('incrementally_tagged' in self[f] and (self[f]['incrementally_tagged'])))

    @property
    def _videos_that_have_been_incrementally_tagged(self) -> List[Path]:
        """Returns the list of videos that have been incrementally tagged.
        """
        return [Path(f) for f in self._raw_incrementally_tagged_list]

    @property
    def num_incrementally_tagged_files(self) -> int:
        """Returns the number of files that have been incrementally tagged.
        """
        return len(list(self._raw_incrementally_tagged_list))

    @property
    def _raw_incrementally_skipped_list(self) -> Generator[str, None, None]:
        """Convenience function for the next two properties. Returns the list of indexable
        keys corresponding to files that have been skipped during the current round of
        incremental tagging. used primarily by other properties that massage the data it
        returns.
        """
        return (f for f in self if ('incremental_temp_skip' in self[f] and self[f]['incremental_temp_skip']))

    @property
    def _videos_skipped_in_incremental_tagging(self) -> List[Path]:
        """Returns the list of videos that have been skipped during incremental tagging.
        """
        return [Path(f) for f in self._raw_incrementally_skipped_list]

    @property
    def _num_incrementally_skipped_files(self) -> int:
        """Returns the number of files that have been skipped during incremental tagging.
        """
        return len(list(self._raw_incrementally_skipped_list))

    def complex_video_select_by_tags(self, must_include_one_of_these_tags: Iterable[str] = (),
                                     may_not_include_any_of_these_tags: Iterable[str] = (),
                                     infer_tags_from_filenames: bool = False,
                                     ) -> Generator[Union[Path, str], None, None]:
        """Return a list of videos in the database that meet the specified criteria. This
        list includes no files tagged with any of the tags specified in the list
        MAY_NOT_INCLUDE_ANY_OF_THESE_TAGS. If MUST_INCLUDE_ONE_OF_THESE_TAGS is not
        empty, then all files specified must be tagged with at least one of those tags.
        (Otherwise, the returned file set includes all files in the database, excluding
        those excluded by the MAY_NOT_INCLUDE_ ... parameter.) If both iterables are
        empty, the function returns every file in the database.

        If INFER_TAGS_FROM_FILENAMES is True, then any chunk of a filename that is also
        a tag on any file in the database counts as a tag for the file including that
        chunk in its name. For instance, if "music" is a tag on at least one file in the
        database, and INFER_TAGS_ ... is True, then any file with the string "music" as
        part of its name will be treated as if it were tagged "music". This can be handy
        if the database has been inadequately or incompletely tagged.

        Tag comparison is always performed case-insensitively. It also strips whitespace
        from both ends of all tags, both those on files and those supplied as search
        parameters.

        Though some attention has been paid to making this routine as efficient as
        it can easily be made, more work could probably be done to make it more
        efficient. It is always, however, going to be relatively slow.
        """
        @functools.lru_cache(maxsize=None)
        def cmp_form(w: Union[str, Path]) -> str:
            if not isinstance(w, str):
                w = str(w)
            return default_cmp_func(w)

        must_include_one_of_these_tags = set(must_include_one_of_these_tags)
        may_not_include_any_of_these_tags = set(may_not_include_any_of_these_tags)

        if infer_tags_from_filenames:
            all_tags_in_db = set(self.all_tags)
        else:
            all_tags_in_db = set()

        for p in self:
            if 'tags' in self[p]:
                tags_for_current_file = set([cmp_form(t) for t in self[p]['tags']])
            else:
                tags_for_current_file = set()

            found_in_name = False
            tag_overlap = must_include_one_of_these_tags.intersection(tags_for_current_file)
            if not tag_overlap:
                for t in (all_tags_in_db.intersection(must_include_one_of_these_tags)):
                    if cmp_form(t) in cmp_form(p):
                        found_in_name = True
                        break

            if (not must_include_one_of_these_tags) or found_in_name or tag_overlap:
                if not may_not_include_any_of_these_tags:
                    yield p
                else:
                    found_in_name = False
                    tag_overlap = may_not_include_any_of_these_tags.intersection(tags_for_current_file)
                    if tag_overlap:
                        continue
                    if not tag_overlap:
                        for t in (all_tags_in_db.intersection(may_not_include_any_of_these_tags)):
                            if cmp_form(t) in cmp_form(p):
                                found_in_name = True
                                break
                    if not found_in_name:
                        yield p

    @property
    def _raw_video_process_list(self) -> Generator[str, None, None]:
        """Convenience function for the next two properties: gets the raw list of indexable
        keys corresponding to files that need to be processed. Used primarily by other
        properties that massage the data it returns.
        """
        return (f for f in self if ('next_processing' in self[f]) and (self[f]['next_processing']))

    @property
    def videos_to_process(self) -> List[Path]:
        """Returns the list of videos that need to be processed on the next processing run.
        """
        return [Path(f) for f in self._raw_video_process_list]

    @property
    def num_videos_to_process(self) -> int:
        """Returns the number of videos that need to be processed on the next run.
        """
        return len(list(self._raw_video_process_list))

    def _select_videos_with_movie_metadata(self, field_name: str,
                                           desired_type: type) -> List[str]:
        """Convenience function: returns all files indexed in SELF that have metadata with
        a 'movie_metadata' field, and whose 'movie_metadata' data includes a field
        FIELD_NAME with data that can be interpreted as DESIRED_TYPE.
        """
        assert isinstance(field_name, str), ("ERROR! The FIELD_NAME requested from _select_videos_with_movie_metadata()"
                                             " function must be a string!")
        assert isinstance(desired_type, Callable), ("ERROR! The DESIRED_TYPE passed to _select_videos_with_movie_"
                                                    "metadata() function must be, well, a type!")

        ret = list()
        for i in self:
            try:
                _ = desired_type(self[i]['movie_metadata'][field_name])        # Try to index and convert
                ret.append(i)                                                  # If successful, add to list of results.
            except (KeyError, ValueError):
                pass
        return ret

    def select_videos_from_movie_metadata(self, filter_func: Callable,
                                          movie_field_name: str = None,
                                          desired_type: type = None,
                                          exclude_recompressed_files: bool = False) -> List[Path]:
        """Finds all videos matching the criteria operating in FILTER_FUNC. Returns a list
        of all videos that satisfy FILTER_FUNC's criteria. That is, it passes the
        necessary data about each file to FILTER_FUNC and passes back a list of all
        files for which FILTER_FUNC returns True. If EXCLUDE_RECOMPRESSED_FILES is True,
        files that have the 'recompressed' field set in their metadata, indicating that
        they are already recompressed, are excluded from the set of files passed to
        FILTER_FUNC, and therefore do not appear in the list returned by this function.

        If either of DESIRED_TYPE or MOVIE_FIELD_NAME is specified, then both must be.
        Specifying these parameters pre-filters the list of videos so that it only
        includes videos whose 'movie_metadata' has a field MOVIE_FIELD_NAME that can
        be coerced to DESIRED_TYPE.

        This function only operates on cached metadata. It makes no attempt to check for
        files that exist amongst the set but that have no metadata stored, nor does it
        detect files whose metadata has become stale.

        FILTER_FUNC is a function that takes two parameters: in order, a Path
        representing the file, and the movie metadata dictionary containing the movie
        metadata. It should return True if the file matches the criteria for which the
        function searches, or False otherwise.
        """
        if desired_type:
            assert movie_field_name is not None
        if movie_field_name:
            assert desired_type is not None

        if desired_type:
            available = self._select_videos_with_movie_metadata(field_name=movie_field_name, desired_type=desired_type)
        else:
            available = self.mapping.keys()
        available = [i for i in available if 'movie_metadata' in self[i]]
        if exclude_recompressed_files:
            ret = [][:]
            for i in available:
                if 'recompressed' not in self[i]:
                    ret.append(i)
                elif not self[i]['recompressed']:
                    ret.append(i)
            available = ret
        return list([Path(p) for p in available if filter_func(Path(p), self[p]['movie_metadata'])])

    def empty_processing_queue(self, suppress_write: bool = False,
                               confirm: bool = False) -> None:
        """Set "next processing" plans for every video in the set to "no plans, thanks."
        As a safety measure, CONFIRM must explicitly be True. Writes metadata when
        finished unless SUPPRESS_WRITE is True.
        """
        if not confirm:
            print("WARNING: did not empty processing queue because the CONFIRM argument to empty_processing_queue "
                  "was not specified as True!")
            return
        for v in self:
            self[v]['next_processing'] = list()
            if 'next_processing_params' in self[v]:
                del self[v]['next_processing_params']
        if not suppress_write:
            self.write_data()


# Utility functions for scheduling work to be done on files.
def sample_add_callback_func(which_file: Path) -> None:
    """This is how to declare an appropriate callback function for the ADD_CALLBACK
    parameter to various functions below.
    """
    assert isinstance(which_file, Path)  # etc.


def schedule_deprecated_recontainerizing(which_store: MetadataStore,
                                         even_if_size_increase: bool = False,
                                         add_callback: Optional[Callable] = None) -> None:
    """Take any Flash videos with the .flv extension and transfer their data to an
    .mp4 container. Also do the same for containers in other deprecated formats, as
    defined in the 'deprecated_containers' prefs key. If EVEN_IF_SIZE_INCREASE is
    True, notes that for processing. If ADD_CALLBACK is specified, it should be a
    function that takes a single parameter, a Path to the file that's been added or
    modified for the next processing run. It will be called for every file added to the
    processing queue or whose parameters have been modified.

    On the first pass, we just try copying the internal codec data, though of course
    if that doesn't work a second pass with re-encoding will be scheduled.
    """
    @functools.lru_cache(maxsize=None)
    def decide_if_deprecated(extension: str,
                             cmp_func: Optional[Callable] = default_cmp_func) -> bool:
        """Utility function: decide, based on extension, if this file needs to be
        recontainerized.
        """
        extension = default_cmp_func(extension.strip().lstrip('.'))
        if len(extension) > 7:
            return True
        return cmp_func(extension) in [cmp_func(c) for c in pref('deprecated_containers')]

    total = 0
    for p in which_store:
        p = Path(p)
        if decide_if_deprecated(p.suffix):
            which_store.ensure_next_processing(p)
            which_store.validate_codec_specs(p)
            which_store.set_argument_parameter(p, '-acodec', 'copy', only_if_absent=True)
            which_store.set_argument_parameter(p, '-vcodec', 'copy', only_if_absent=True)

            # On this next setting: if the answer is "No," we don't want to turn it off for this file if another
            # processing step has already turned it on.
            allow_increase = even_if_size_increase or which_store[p]['next_processing_params']['allow_size_increase']
            which_store[p]['next_processing_params']['allow_size_increase'] = allow_increase
            total += 1
            if add_callback:
                add_callback(p)
    if total:
        print("Scheduled %d videos in deprecated formats for recontainerization!" % total)


def schedule_large_downsizing(which_store: MetadataStore,
                              max_width: int = None,
                              max_height: int = None,
                              even_if_size_increase: bool = False,
                              even_if_recompressed: bool = False,
                              add_callback: Optional[Callable] = None) -> None:
    """Convenience function: Go through videos with cached metadata and schedule those
    that are large for downsampling on the next processing run.

    MAX_HEIGHT and MAX_WIDTH are the maximum height and width. If they are None or
    otherwise Falsey, then the values specified in the preferences are used instead.
    Already-recompressed videos are excluded unless EVEN_IF_RECOMPRESSED is True.
    Video recompression that increases the size of the file will be abandoned unless
    EVEN_IF_SIZE_INCREASE is True.

    If ADD_CALLBACK is specified, it must be a function that takes a single
    parameter -- the path to a file -- and which will be called with the path to
    every file added to the to-process list.
    """
    if not max_width:
        max_width = pref('max_video_width')
    if not max_height:
        max_height = pref('max_video_height')

    l1 = which_store.select_videos_from_movie_metadata(lambda p, rec: (int(rec['frame height']) > max_height),
                                                       'frame height', int, even_if_recompressed)
    l2 = which_store.select_videos_from_movie_metadata(lambda p, rec: (int(rec['frame width']) > max_width),
                                                       'frame width', int, even_if_recompressed)
    large_videos = sorted([i for i in list(set(l1 + l2)) if i.exists()])
    for i, f in enumerate(large_videos):
        which_store.schedule_video_resize(f, max_width=max_width, max_height=max_height, suppress_write=True)

        # On this next setting: if the answer is "No," we don't want to turn it off for this file if another
        # processing step has already turned it on.
        allow_increase = even_if_size_increase or which_store[f]['next_processing_params']['allow_size_increase']
        which_store[f]['next_processing_params']['allow_size_increase'] = allow_increase

        if add_callback:
            add_callback(f)

        if ((i + 1) % 100 == 0) or ((i + 1) >= len(large_videos) - 3):
            print("Scheduled %d of %d videos for downsampling!" % (i + 1, len(large_videos)))
    which_store.write_data()


def schedule_hi_bitrate_downsizing(which_store: MetadataStore,
                                   even_if_size_increase: bool = False,
                                   add_callback: Optional[Callable] = None) -> None:
    """Convenience function: Go through videos with cached metadata and schedule those
    that use a high bitrate for downsampling on the next processing run.

    If ADD_CALLBACK is specified, it must be a function that takes a single
    parameter -- the path to a file -- and which will be called with the path to
    every file added to the to-process list.
    """
    def find_hi_bitrate(p: Path, rec: dict) -> bool:
        """Evaluates whether a given video, based on its metadata, is exceeding its target
        bitrate by a large enough amount to be worth re-encoding for space savings.

        Sanity checks: if we don't know the height or width, or can't make integers of
        them, we shouldn't touch the video. This is obviously not nearly enough error
        checking, and yet it's all we're currently doing.
        """
        if ('frame height' not in rec) or ('frame width' not in rec) or ('bitrate' not in rec):
            return False
        try:
            _ = int(rec['frame height'])
            _ = int(rec['frame width'])
            _ = int(rec['bitrate'])
        except (ValueError,):
            return False

        w, h = rec['frame width'], rec['frame height']
        if (utils.kush_gauge(w, h) * pref('max_kush_gauge_leeway')) <= int(rec['bitrate']):
            return True

        return False

    hi_bitrate_videos = which_store.select_videos_from_movie_metadata(find_hi_bitrate, exclude_recompressed_files=True)
    for i, f in enumerate(hi_bitrate_videos):
        which_store.schedule_video_resize(f, suppress_write=True)  # 'resize' is just as good as 'recompress' here.
        if even_if_size_increase:
            which_store[f]['next_processing_params']['allow_size_increase'] = True
        if add_callback:
            add_callback(f)
        if ((i + 1) % 100 == 0) or ((i + 1) >= len(hi_bitrate_videos) - 3):
            if i > 0:
                which_store.write_data()
            print("Scheduled %d of %d videos for recompression!" % (i + 1, len(hi_bitrate_videos)))
    which_store.write_data()


def cli_cancel_chooser(which_file: Path) -> Literal['this', 'all']:
    """Asks the user to choose whether they want to cancel just this single file, or
    all files.
    """
    opt = utils.menu_choice({'O': 'Only skip this file', 'C': 'Cancel the entire operation'},
                            prompt=f"You canceled removing streams from {which_file.name}. Do you want to stop "
                                   f"the whole stream-removing operation on all files, or just skip this particular "
                                   f"file?")
    if opt.strip().casefold() == 'o':
        return 'this'
    else:
        return 'all'


def cli_stream_chooser(header_phrase: str,
                       streams: List[str]) -> List[int]:
    """Given STREAMS, a list of textual descriptions of audio/video/subtitle/other
    streams, produced by FFMPEG, prompt the user to decide which streams to keep.
    The STREAMS list should be list of every stream in the video file.

    Returns a list of strings that represents the zero-indexed numeric stream IDs
    for the streams to be kept during reprocessing. For instance, if the user
    selects the first, fourth, and ninth streams, returns ['0', '3', '8'].

    If the user cancels the choice, returns an empty list.
    """
    done, keepers = False, list()
    print(f"\n{header_phrase}")
    for s in streams:
        print(s)
    try:
        if default_cmp_func(input("Keep just first two? ")).startswith('y'):
            keepers = [0, 1]
        else:
            while not done:
                keepers = input("Enter comma-separated list of zero-indexed streams to keep (blank to cancel): ")
                if not keepers.strip():
                    print("  cancelling ...\n\n")
                    done = True
                try:
                    stripped_keepers = [k.strip() for k in keepers.split(',')]
                    int_keepers = [int(k) for k in stripped_keepers]
                    if all([(0 <= k < len(streams)) for k in int_keepers]):
                        done = True
                        keepers = int_keepers
                except Exception:
                    print(f"Sorry, {keepers} is not a comma-separated list I can understand!\nEach stream ID must be "
                          f"between 0 and 1 less than the total number of streams.\nStream IDs must be integers.")
                    continue
    finally:
        return keepers


def schedule_spurious_stream_elimination(which_store: MetadataStore,
                                         stream_chooser: Callable[[str, List[str]], Iterable[int]] = cli_stream_chooser,
                                         cancel_chooser: Callable[[Path], Literal['this', 'all']] = cli_cancel_chooser,
                                         ) -> None:
    """Most of the time, what we want is for a video file to have a single audio stream
    and a single video stream. This goes through metadata for all known video files
    and asks the user what to do about files that have more streams than that.

    STREAM_CHOOSER is a Callable that has the same call and return signature as
    cli_stream_chooser(), above.
    """
    def find_polyploid_videos(p: Path, rec: dict) -> bool:
        try:
            if len(rec['all_streams']) > 2:
                return True
            if len(rec['video_streams']) > 1:
                return True
            if len(rec['audio_streams']) > 1:
                return True
            if len(rec['all_streams']) > (len(rec['video_streams']) + len(rec['audio_streams'])):
                return True
        except (KeyError,):
            return False  # Don't schedule for recompression if we can't interpret the data
        return False  # If none of the criteria above are met, the video file is not polyploid.

    polyploid_videos = which_store.select_videos_from_movie_metadata(find_polyploid_videos,
                                                                     exclude_recompressed_files=False)
    polyploid_videos = [v for v in polyploid_videos if '-map' not in which_store[v]['next_processing']]
    changed = False

    try:
        for i, f in enumerate(sorted(polyploid_videos)):
            which_store.ensure_next_processing(f)
            keepers = stream_chooser(f"Spurious streams: {f.name} ({i} of {len(polyploid_videos)})",
                                     which_store[f]['movie_metadata']['all_streams'])

            if not keepers:     # blank response means user canceled.
                if cancel_chooser(f) == 'this':
                    continue
                else:
                    return

            if len(set(keepers)) == len(set(which_store[f]['movie_metadata']['all_streams'])):
                continue    # Keeping all streams? Nothing to do.

            for stream in keepers:
                # Don't use .set_argument_parameter(), which would only allow one mapping
                which_store[f]['next_processing'].extend(['-map', f'0:{stream}'])
                changed = True

            if (i % 20) == 0:
                which_store.write_data()
                changed = False

    finally:
        if changed:
            which_store.write_data()


def schedule_audio_recompression(which_store: MetadataStore) -> None:
    """Finds "wasteful" audio and schedules the audio for recompression.

    FIXME: constants for what constitutes "wasteful" are hard-coded!

    FIXME: BROKEN: this sometimes results in re-encoded audio being silent!
    """
    print("WARNING! This scheduler is broken: it damages audio streams!")

    def find_wasteful_audio(p: Path, rec: dict) -> bool:
        if ('audio stream count' in rec) and (isinstance(rec['audio stream count'], int)):
            if rec['audio stream count'] > 1:
                print("WARNING: %s has multiple audio streams, not touching audio ..." % p)
                return False
        try:
            if int(rec['audio sample rate']) > 48000:
                return True
        except (KeyError, IndexError, ValueError, TypeError):
            pass
        try:
            if rec['audio channels'] > 2:
                return True
        except (KeyError, TypeError, ValueError):
            pass
        try:
            # Chances are, we've already caught this with the tests above, but check anyway.
            if (rec['audio bitrate'] / rec['audio channels']) > 96:
                return True
        except (KeyError, TypeError, ValueError):
            pass
        return False

    wasteful_audio_videos = which_store.select_videos_from_movie_metadata(find_wasteful_audio,
                                                                          exclude_recompressed_files=False)
    for i, f in enumerate(wasteful_audio_videos):
        which_store.ensure_next_processing(f)
        if int(which_store[f]['movie_metadata']['audio channels']) > 2:
            # do "nightmode" downsampling, see
            # https://superuser.com/questions/852400/properly-downmix-5-1-to-stereo-using-ffmpeg
            fu.set_argument_parameter(which_store[f]['next_processing'], '-acodec', 'dca')
            fu.set_argument_parameter(which_store[f]['next_processing'], '-af',
                                      "pan=stereo|FL=FC+0.30*FL+0.30*BL|FR=FC+0.30*FR+0.30*BR")

        if int(which_store[f]['movie_metadata']['audio sample rate']) > 48000:
            fu.set_argument_parameter(which_store[f]['next_processing'], '-ar', '48000')

        m_meta = which_store[f]['movie_metadata']
        if (m_meta['audio bitrate'] / m_meta['audio channels']) > 96:
            fu.set_argument_parameter(which_store[f]['next_processing'], '-ab',
                                      str(round(96 * which_store[f]['movie_metadata']['audio channels'])))
    which_store.write_data()


if __name__ == "__main__":
    force_test = False
    if force_test:
        import sys
        sys.exit()

    print("Sorry, no self-test code in this module!")
