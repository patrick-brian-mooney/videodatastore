#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Namespace for utilities used throughout the VideoDataStore code.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import collections
import datetime
import functools
import json
import locale
import numbers
import os
import platform
import tkinter as tk
import re
import shutil
import subprocess
import textwrap
import unicodedata

from pathlib import Path
from typing import Any, Collection, Generator, Hashable, Iterable, Iterator, List, Mapping, Optional, Tuple, Union


# The metadata store itself, and other filesystem data that's necessary
metadata_name = '.metadata.dat'


__progname__ = 'VideoDataStore'
__version__ = "alpha version"

__program_loc__ = Path(__file__).parent

file_version = 1

prefs_file_name = __progname__ + " preferences"

website_URL = "https://bitbucket.org/patrick-brian-mooney/videodatastore/"

defaults = {
    # Some constants that affect decision-making in video processing
    'max_video_width': 1366,            # maximum width to target when resizing videos.
    'max_video_height': 676,            # maximum height to target when resizing videos.
    'video_size_factor': 2,             # Constrain video height and width to be a multiple of this when resizing.

    'single_CRF_value': 23,             # What CRF value to use when making an attempt at one-pass encoding
    'max_postcomp_size_ratio': 0.90,    # Controls how big a file can be after processing and still be considered to
                                        # be a meaningful reduction in size. A ratio of 0.85, for instance, means a
                                        # recompressed video may be at most 85% of its original size.

    'kush_gauge_motion_prediction': 15, # How much to anticipate motion within a video when calculating upper bitrate?
                                        # Larger numbers are needed to avoid degrading quality for videos with more
                                        # motion.
    'max_kush_gauge_leeway': 1.1,       # Controls how tightly constrained by the Kush gauge calculation the "is this
                                        # video's bitrate within acceptable parameters" is. 1.2 means: if a video's
                                        # bitrate is more than 20% over what its Kush gauge value says the bitrate
                                        # should be, it's a "high bitrate" video.


    # parameters used to control how the ffmpeg process spawned, and what it creates.
    'num_ffmpeg_threads': 2,            # Number of threads ffmpeg is allowed to use.
    'ffmpeg_preset': "slow",
    'proc_file_ext': '.mp4',            # Extension to use on re-encoded videos. Anything but .mp4 is VERY experimental.
    'default_video_codec': 'libx264',   # codec name (as understood by ffmpeg) to use when re-encoding video
    'default_audio_codec': 'aac',       # codec name (as understood by ffmpeg) to use when re-encoding audio
    'nice_value': 15,                   # `nice` value to use when launching ffmpeg on systems with `nice` installed.
    'max-consecutive-batch-errors': 20, # If > 0, stop batch processing after many consecutive processing failures.

    # general parameters that control what extensions are treated as different general kinds of files.
    'deprecated_audio_codecs': ['adpcm_', 'pcm_'],
    'known_video_types': ['.3gp', '.3g2', '.avi', '.fid', '.flv', '.iso', '.m4v', '.mkv', '.mov', '.mp4', '.mpg',
                          '.mpeg', '.ms-wmv', '.ogm', '.rmvb', '.sfv', '.swf', '.webm', '.wmv', ],
    'known_audio_types': ['.flac', '.m4a', '.mp3', '.wav', '.wma', '.ogg', '.aa', '.m4p', '.ape'],
    'never_process_types': ['.iso', ],

    'deprecated_containers': ['3gp',    # extensions for container types that will be replaced with the preferred
                              '480', '720', 'flv', 'mpg',       # container type with the 're-containerize deprecated
                              'avi', 'fid', 'mpeg', 'ms-wmv',   # formats' command.
                              'webm', 'wmv'],

    'strippable_separators': "-—@",     # iterable of chars to strip when stripping prefixes/suffixes from file names.

    # locations of things on disk.
    'ffmpeg_loc': None,                 # if not overridden in another config file, try to find it with WHICH.
    'ffprobe_loc': None,                # As above.
    'nice_loc': None,                   # As above, for the Unix `nice` program.
    'plugins_loc': 'plugins/',          # plugins directory, either absolute, or relative to the script's home dir

    'UNSAFE_use_pickle_save': False,    # Use much faster, but unsafe, save format.
    'UNSAFE_allow_pickle_load': False,  # Allow pickled save files to be loaded. Must be True if the above is.
    'UNSAFE_pickle_version': -1,        # Zero to five (as of Py 3.8), or else -1 to use the highest version.

    # Now, some preferences for the GUI
    'debugging': False,                 # Currently adding debugging code? Also adds "debug" menu to main menubar.
    'allow-tearoffs': False,            # Use ugly old-style tearoff menus?
    'column-weights': {'Name': -100, 'Tags': 0, 'Location': 100},   # Each column's affinity for the RIGHT side
    'hidden-columns': [],               # columns to hide by default.
    'output-font': 'TkFixedFont',       # Bug with TkFixedFont prevents also specifying size and style.
    'title-font': ('system', 14, 'bold'),           # Font for graphically significant titles, e.g. in about box.
    'label-font': ('system', 10, 'bold'),           # Font info for GUI labels.
    'small-label-font': ('system', 8, 'italic'),    # Font info for GUI labels.
    'tooltip-font': ('system', 8, 'italic'),        # Font info for GUI labels.
    'tooltip-width': 70,  # characters

    'window-geometry': "300x200+40+40",
    'right-click-mode': 0,              # 0 for auto-detect, 1 for X11/Windows, 2 for macOS/Aqua.

    'statusbar-delay': 10,              # seconds that a message will be displayed in the GUI status bar before
                                        # rotating to the next queued message, if there is one.
    'statusbar-font': ('system', 8, 'italic'),      # Font info for the status bar label.

    'media-viewer': 'vlc',              # viewer to open from GUI to view media.

    'confirm_delete_db_entry': True,    # whether to confirm removing a file from the database
    'delete_file_with_db_entry': True,  # whether to also delete the file represented by the DB entry
    'confirm_file_deletion': True,      # whether to confirm before deleting a file
    'auto-play-when-tagging': True,     # whether to auto-play new file when advancing during incremental tagging

    'helper-apps': dict(),              # helper applications displayed in the Open With ... right-click menu. Format
                                        # of dict is name: path to executable.
    'max-windows-opened-unwarned': 10,  # maximum number of GUI windows to pop up at once without confirming
                                        # the user really wants to open {however many} windows.


    'default-vis-width': 800,
    'default-vis-height': 600,

    'filter-delay': 1100,               # delay, in milliseconds, between last keystroke and when list gets re-filtered.
}


config_directories = [                  # directories in which we search for configuration files.
    Path('/etc') / __progname__,                                            # Unix
    Path('/Library/Preferences') / __progname__,                            # macOS
    Path(__file__).resolve().parent.parent,                                 # application installation directory
    Path(os.path.expanduser('~')),                                          # any
    Path(os.path.expanduser('~')) / 'Library/Preferences' / __progname__,   # macOS
    Path(os.path.expanduser('~')) / ('.' + __progname__),                   # Unix
    Path(os.path.expanduser('~')) / '.config' / __progname__,               # Unix
    Path(os.environ['XDG_CONFIG_HOME']) / __progname__ if os.environ.get('XDG_CONFIG_HOME') else None,  # Unix
    Path(os.environ['LOCALAPPDATA']) / __progname__ if os.environ.get('LOCALAPPDATA') else None,        # Windoze
]


# FIXME! we need a constant representing /dev/null, because on Windows, of course, it's just NUL to ffmpeg.


def blank_settings_template() -> dict:
    """Create an empty settings template to track the settings for a new VideoDataStore.
    """
    return {
        'skipped_dirs': list(),
        'include video': True,
        'include audio': False,
        'include others': False,
        'holding directory': None,
        'strip phrases': list(),
    }


def blank_metadata_template() -> dict:
    return {
        'program': __progname__,
        'URL': website_URL,
        'database title': '',
        'database maintainer': '',
        'database description': '',
        'database created': datetime.datetime.now().isoformat(' '),
    }


# Next, some utility functions.
def running_under_windows() -> bool:
    """Return True if we're running under Microsoft Windows, False otherwise.
    """
    return os.name in ['nt', 'ce']


def running_under_macos() -> bool:
    """Returns True if we're running under macOS. Note that this is not the same
    thing as determining whether we're running under the Aqua window mananger for
    macOS, a function for which is provided in gui_utils.
    """
    return "darwin" in platform.system().casefold()


def running_under_posix() -> bool:
    """Determine whether we are running under a POSIX operating system. Note that this
    does not necessarily mean Linux/BSD/etc. only; macOS is a POSIX OS, and
    sometimes we need to check that case first and fall back to this check.
    """
    return 'posix' in os.name.casefold()


def _flatten_list(the_list: Iterable[Any]) -> Generator[Any, None, None]:
    """Regardless of how deep the list (or other iterable) L is, yield the non-list (or
    other iterable) atoms that compose L (and its sub-iterables, if any, to any
    depth). No matter how deeply nested L is, the yielded elements list will not
    contain any lists, tuples, or other iterables, but only the atoms of those lists.

    For purposes of the above paragraph, strings (and bytes objects) are considered
    to be elements, not iterables.

    Note that this actually returns a generator expression, not a list; the
    similarly named convenience wrapper flatten_list, below, may be a better choice
    if an actual list is desired (i.e., usually).
    """
    for elem in the_list:
        if isinstance(elem, collections.Iterable) and not isinstance(elem, (str, bytes)):
            for sub in _flatten_list(elem):
                yield sub
        else:
            yield elem


def flatten_list(the_list: Iterable[Any]) -> Iterable[Any]:
    """Convenience function to wrap _flatten_list and return an actual list. More often
    than not, this is what's desired.
    """
    return list(_flatten_list(the_list))


def jsonify(what: Any, *,
            indent: int = 2,
            ensure_ascii: bool = True,
            sort_keys: bool = True,
            **kwargs) -> str:
    """Takes WHAT and serializes it as a JSON string. WHAT must obey the general rules
    for JSON serializability.

    This is just a utility function that wraps json.dumps so that other code doesn't
    have to specify the same parameters over and over. Additional parameters not
    specified here can be passed in as **kwargs.
    """
    return json.dumps(what, indent=indent, ensure_ascii=ensure_ascii, sort_keys=sort_keys, **kwargs)


def clean_name(suggested_name: Path,
               fixes_to_strip: Optional[Iterable[str]] = None,
               auto_rename: bool = True,
               new_suffix: Optional[str] = None) -> Path:
    """Cleans up SUGGESTED_NAME so that it meets the criteria specified in the
    parameters passed in.

    If AUTO_RENAME is True, then the returned name will be a unique Path to a file
    in the same directory specified, but that does not exist and whose name uses
    SUGGESTED_NAME as a base. If SUGGESTED_NAME already exists in the specified
    location, sequentially tries appending (1), (2), (3) ... to SUGGESTED NAME,
    until it finds a unique name, keeping the same filename suffix (extension) as
    SUGGESTED_NAME.

    If AUTO_RENAME is False, then no numeric suffixes are ever automatically
    attached, and the returned name is not necessarily unique in its directory.

    If NEW_SUFFIX is not None, then the new filename returned has the specified new
    suffix instead of the old file's suffix, and the uniqueness calculation is
    performed for the file taking that new suffix into account rather than the old
    filename's suffix. (This is handy during a batch processing run, when videos
    being processed may wind up in a new format with a new filename extension.)

    If FIXES_TO_STRIP is not None, then, before a unique name is found,
    SUGGESTED_NAME is modified by repeatedly (and case-insensitively) stripping each
    item from both the beginning and end of the file until none of them are found.
    Whitespace is also removed from the beginnings and ends of files, and so are any
    strings in STRIPPABLE_SEPARATORS, a constant defined elsewhere.

    If FIXES_TO_STRIP *is* None, no stripping of being/end phrases is done at all,
    not even whitespace of the contents of the STRIPPABLE_SEPARATORS constant.
    """
    new_name, suffix = suggested_name.stem, suggested_name.suffix
    if new_suffix:
        assert new_suffix.startswith('.')
        suffix = new_suffix

    if fixes_to_strip:
        changed = True  # Go through the strips-comparison loop at least once.
        while changed:
            changed = False
            for strip in fixes_to_strip:
                if new_name.casefold().startswith(strip.casefold()):
                    new_name = new_name[len(strip):].strip().strip(pref('strippable_separators')).strip()
                    changed = True
                if new_name.casefold().endswith(strip.casefold()):
                    new_name = new_name[:len(new_name) - len(strip)]
                    new_name = new_name.strip().strip(pref('strippable_separators')).strip()
                    changed = True

    if auto_rename:
        count = 0
        base_name = new_name
        while suggested_name.with_name(new_name + suffix).exists():  # Vulnerable to race conditions. What can you do?
            count += 1
            new_name = f"{base_name} {'(' + str(count) + ')'}".strip()

    return suggested_name.with_name(new_name + suffix)


def kush_gauge(width, height) -> int:
    """Return the 'Kush Gauge' value for what the bitrate of a video of dimensions
    WIDTH x HEIGHT 'should be'. MOTION_PREDICTION is a value indicating how much to
    account for motion in the calculation. Common values for streaming are 1, 2, and
    4.
    """
    return int(width * height * pref('kush_gauge_motion_prediction') * 0.07)


channel_textual_descriptions = {'mono': 1, 'stereo': 2, "5.1": 6}


def float_channels_from_textdesc(textdesc: str) -> Union[float, None]:
    """ffmpeg describes audio channels in a human-friendly form, as in 'mono' or
    'stereo,' etc. But what we really want to know is a floating-point number: 1,
    2, 6, or something. (Note that 5.1 really means 6, so we return 6.)

    If we cannot determine a channel number from a textual description, return None.
    """
    try:
        return channel_textual_descriptions[textdesc.lower().strip()]
    except (KeyError,):
        return None


def time_str_from_timestamp(stamp: float) -> str:
    """Convenience function: takes STAMP, a Unix timestamp, and produces an
    ISO-formatted timestamp string that's intended to be human-readable.
    """
    return datetime.datetime.fromtimestamp(stamp).isoformat(' ')


def unicode_of(what: Union[str, bytes]) -> str:
    """Just force WHAT to be a Unicode string, as much as we can possibly automate
    that. We start by using the system default, then trying UTF-8, and then, if
    either or both is installed, tries UnicodeDammit and chardet. If all else fails,
    decodes to Latin-1, which should always not fail although it may munge data. If
    even *that* doesn't work or some unknown godawful reason, the error will
    propagate upwards.

    This comes in handy when interacting with external programs, which may just barf
    up data without caring about encoding.
    """
    if isinstance(what, str):
        return what

    try:
        what = what.decode()
    except (UnicodeDecodeError,):
        try:
            what = what.decode('utf-8')
        except (UnicodeDecodeError,):
            try:
                from bs4 import UnicodeDammit       # https://www.crummy.com/software/BeautifulSoup/bs4/doc/
                return UnicodeDammit(what).unicode_markup
            except (ImportError, UnicodeDecodeError):
                try:
                    import chardet
                    char_info = chardet.detect(what)
                    what = what.decode(char_info['encoding'])
                except (ImportError, UnicodeDecodeError):
                    what = what.decode('latin-1')   # last resort: pretend it's this common encoding that never fails
    return what


@functools.lru_cache(maxsize=None)
def case_insensitive_tag_comparator(s: str) -> str:
    """Convenience function to produce and cache tag comparisons.
    """
    assert isinstance(s, str)
    return s.strip().casefold()


@functools.lru_cache(maxsize=None)
def case_sensitive_tag_comparator(s: str) -> str:
    """Convenience function to produce and cache tag comparisons.
    """
    return s.strip()


def strip_diacritics(the_input: str) -> str:
    """Strip diacritical marks and convert to ASCII text by decomposing the Unicode
    string passed in, then removing non-spacing marks.
    """
    return ''.join([c for c in unicodedata.normalize('NFKD', the_input) if unicodedata.category(c) != 'Mn'])


@functools.lru_cache(maxsize=None)
def default_sort(the_input: str) -> str:
    """The default sorting transformation algorithm when we need to sort text. This
    strips Unicode diacritics and lowercases the input.
    """
    return strip_diacritics(the_input).casefold()


@functools.lru_cache(maxsize=None)
def regex_compile(the_input: str, *args, **kwargs) -> re.Pattern:
    """Utility function that caches the results of re.compile(), a potentially slow
    function. There's no useful point in cacheing information that will definitely
    never be needed again, but it does take up memory for the rest of the program's
    run, so it's only worth using when the same pattern is likely to nned to be
    compiled multiple times.
    """
    return re.compile(the_input, *args, **kwargs)


def path_of(what: str) -> Union[str, None]:
    """Given WHAT, the name of an executable, try to find that executable and return the
    filesystem path to it as a string. If we cannot determine the filesystem path,
    return WHAT unmodified, in the hope that it represents an executable in the
    system $PATH.
    """
    try:
        return shutil.which(what)
    except Exception as err:
        return None


def relative_to(from_where: Path,
                to_where: Path) -> Path:
    """Describe the path leading from the file or directory FROM_WHERE to the file or
    directory TO_WHERE, even if FROM_WHERE is not an ancestor of TO_WHERE.

    Relies largely on os.path.relpath(), but that's not *quite* what we want here;
    if FROM_WHERE is a file, we don't want the initial '../' that means "move up
    from that file to its parent directory"; users expect '../' to mean "move up
    from one directory to another," not "take the parent directory of this file,"
    which is a rather pedantic interpretation from a UI perspective even if it's
    perfectly correct from a path-operation perspective.

    In fact, os.path.relpath() assumes both components are directories, which is
    kind of weird, so we test and correct both FROM_WHERE and TO_WHERE.
    """
    assert isinstance(from_where, Path), (f"relative_to() was passed {from_where} as a FROM_WHERE argument, and that "
                                          f"is of type {type(from_where)}, not a Path!")
    assert isinstance(to_where, Path), (f"relative_to() was passed {to_where} as a TO_WHERE argument, and that is of "
                                        f"of type {type(to_where)}, not a Path!")

    if from_where.is_file():
        from_where = from_where.parent
    if to_where.is_file():
        to_where = to_where.parent

    return Path(os.path.relpath(to_where, from_where))


def relative_to_with_name(from_where: Path,
                          to_where: Path) -> Path:
    """Same as relative_to() above, but returns a Path with a terminal filename, not
    just the relative directory structure.
    """
    assert isinstance(from_where, Path)
    assert isinstance(to_where, Path)

    ret = Path(relative_to(from_where, to_where))
    if ret.name != to_where.name:
        ret = ret / to_where.name
    return ret


class PrefsTracker(collections.abc.MutableMapping):
    """A set of routines to manage the global preferences store, bound up with data
    about that store's state.

    The global prefs store is built from any appropriate config files found in any
    of the possible locations for config files (defined above). The global prefs
    store also tracks which prefs file we are currently writing to when the user
    updates preferences. It manages to only update the necessary (changed) bits
    during a run by keeping the changed-on-this-run bits of the preferences in the
    front mapping of the ChainMap that it uses to track the multiple files; ChainMaps
    are by default always updated by writing changes to the front ChainMap in the list.

    Code which may update the preferences is responsible for manually writing changes
    (by calling save_preferences()) when it's done making them. This object assumes
    that changes have already been written by the time it is destroyed.
    """
    def __init__(self, *args, **kwargs):
        """Go through the list of possible locations for preferences files, reading
        any that are found and assembling them into a ChainMap. Start the ChainMap with
        the embedded defaults so we have the hard-coded fallback if there are no prefs
        files anywhere along the search path.
        """

        self.data = collections.ChainMap(defaults)  # The furthest-back map is the hardcoded application defaults.
        for p in [loc for loc in config_directories if loc]:  # Filter out any Nones from the list
            this_config = p / prefs_file_name
            if this_config.exists():
                try:
                    self.data = self.data.new_child(json.loads(this_config.read_text(encoding='utf-8')))
                except (OSError, json.JSONDecodeError) as errrr:
                    print("WARNING! Unable to open prefs file {}. The system said: {}".format(this_config, errrr))

        # The frontmost map in the ChainMap is a new empty dict, which will catch all assignments to the preferences
        # structure. This means that we can track just what's changed and write just the changes to an on-disk
        # preferences file that integrates into our preferences file hierarchy.
        self.changed_data = dict()                  # This is the frontmost map
        self.data = self.data.new_child(self.changed_data)

        # Now copy references to mutable objects to the frontmost map to make sure that, when they are modified,
        # the modifications are saved. (__getitem__() lookups will find items deeper in the ChainMap than the frontmost
        # map; altering those items won't result in changes to the frontmost map and therefore won't be saved. We
        # avoid this by copying all mutable data to the frontmost map every single time we load the preferences from
        # on-disk files.)

        for key, value in self.data.items():
            if isinstance(value, (dict, list)):
                self.changed_data[key] = value

        self.writeable_prefs_dir = None             # Or a Path, once we discover one.

        # Next, write some summary data to the backmost map in the chain, so it's not written out in a prefs file
        # (and therefore -- intentionally -- has to be recalculated on every run).
        all_known_types = self['known_video_types'] + self['known_audio_types']
        all_processable_types = [i for i in all_known_types if i not in self['never_process_types']]
        self.data.maps[-1].update({'all_processable_types': all_processable_types})
        if not self.data['ffmpeg_loc']:
            self.data.maps[-1]['ffmpeg_loc'] = path_of('ffmpeg') or 'ffmpeg'
        if not self.data['ffprobe_loc']:
            self.data.maps[-1]['ffprobe_loc'] = path_of('ffprobe') or 'ffprobe'
        if not self.data['nice_loc']:
            self.data.maps[-1]['nice_loc'] = path_of('nice')
        if self.data['media-viewer'] and not Path(self.data['media-viewer']).exists():
            self.data.maps[-1]['media-viewer'] = path_of(self.data['media-viewer'])

        # Finally, if we got passed any dict-type constructor arguments, update us.
        self.update(dict(*args, **kwargs))
        self.validate_settings()

    def __getitem__(self, key: Hashable) -> Any:
        """Makes the object indexable by key so we don't have to keep referring to
        [object].data['something']
        """
        return self.data[key]

    def __setitem__(self, key: Hashable,
                    value: Any) -> None:
        self.data[key] = value

    def __delitem__(self, key: Hashable) -> None:
        del self.data[key]

    def __iter__(self) -> Iterator:
        return iter(self.data)

    def __len__(self) -> int:
        return len(self.data)

    def save_preferences(self) -> None:
        """Goes backwards through the list of CONFIG_DIRECTORIES, trying to create a
        preferences file in any existing directory that it finds. When it finds a
        location to which it can write a preferences file, it writes the first map from
        the PREFERENCES ChainMap -- those key-value pairs that have been assigned or
        changed just during this run -- to the location it found.

        If there is already a preferences file in that location, the preferences file
        that overwrites it incorporates any data in the prefs file being overwritten,
        except (of course) for those things that have changed during this run.
        """
        if not self.changed_data:           # No changes made? No work needs be done.
            return

        saved = False
        dirs_to_check = ([self.writeable_prefs_dir] if self.writeable_prefs_dir else [][:]) + config_directories[::-1]

        while not saved and dirs_to_check:
            d = dirs_to_check.pop(0)
            if not d:
                continue  # Skip everything else if dir is None, i.e. doesn't exist on this machine.
            if not d.exists():                    # If the directory doesn't exist on this system, try to create it.
                try:
                    d.mkdir(parents=True)
                except (OSError,):                  # Trap errors, including the frequent PermissionError.
                    continue
            if not d.exists():                    # If we didn't manage to create it, move along.
                continue
            p = d / prefs_file_name
            old_data = dict()
            if p.exists():
                try:
                    old_data = json.loads(p.read_text(encoding='utf-8'))
                except (IOError, json.JSONDecodeError):
                    pass
            try:
                data_to_write = dict(collections.ChainMap(self.changed_data, old_data))
                p.write_text(jsonify(data_to_write, default=repr), encoding='utf-8')
                saved = True
                self.writeable_prefs_dir = d      # Keep track of where we last saved prefs.

            except (IOError,):
                pass

        if not saved:
            print('Warning! unable to save preferences anywhere!')

    def change_column_visibility(self, column_name: str,
                                 visible: bool) -> None:
        """Record a change in the visibility of the data columns. This should always be
        called instead of assigning directly to the underlying ChainMap.

        COLUMN_NAME is the name of the column being shown or hidden. VISIBLE indicates
        whether the column is being shown (True) or hidden (False).
        """
        if visible:
            if column_name in self['hidden-columns']:
                self['hidden-columns'].remove(column_name)
        else:
            if column_name not in self['hidden-columns']:
                self['hidden-columns'].append(column_name)

    def change_column_position_weight(self, column_name: str, delta_weight: float) -> None:
        """Record the change in the column's affinity for the right side of the window.
        Columns are identified solely by their name, so every column with the same name
        in every part of the UI shares the same affinity value; this generally makes
        sense, given how the program actually uses columns. Moving a column to the left
        in the UI *decreases* its weight, whereas moving it to the right *increases* it.
        (We track the columns' affinity for the *right* side because this makes the math
        work out in a way that is consistent with a standard 'number line'
        visualization.)

        COLUMN_NAME specifies the name of the column whose weight to change.
        DELTA_WEIGHT is the amount to change the weight of the column.
        """
        try:
            self['column-weights'][column_name] += delta_weight
        except (KeyError,):
            self['column-weights'][column_name] = delta_weight

    def set_helper_app(self, executable_path: Union[Path, str],
                       executable_description: str) -> None:
        """Add a new helper application to the list of helper applications.
        """
        assert isinstance(executable_path, (str, Path))
        self['helper-apps'][executable_description] = str(Path(executable_path).resolve())
        self.save_preferences()

    def validate_settings(self, raise_error: bool = False) -> None:
        """Checks whether all settings are compatible and sensible. Does not
        exhaustively find all problems, but does attempt to avoid certain known issues.
        Called automatically when prefs are loaded, and at certain other times when data
        has been changed.

        If RaiseError is True, raises ValueError with an appropriate message when
        incompatibilities are detected; otherwise, prints a warning to stdout.
        """
        try:
            if self['UNSAFE_use_pickle_save'] and not self['UNSAFE_allow_pickle_load']:
                raise ValueError("Unsafe pickle files are set to be used to save data, but the program is not allowed "
                                 "to load them!")
            if not self['plugins_loc']:
                raise ValueError("Plugins directory is not specified in Preferences!")
            self['plugins_loc'] = str(Path(self['plugins_loc']).resolve())
        except ValueError as errrr:
            if raise_error:
                raise errrr
            else:
                print(f"Unable to validate preferences data! The system said: {errrr}.")


prefs = PrefsTracker()


def pref(key: Hashable) -> Any:
    """Given KEY, a by-name prefs index, return the data that the PrefsTracker has
    indexed under that KEY.

    Just a convenience function to make referring to preferences by name more
    convenient.
    """
    return prefs[key]


prefs_path = Path(os.getcwd())


# Now, some basic utilities for dealing with file metadata. The MetadataStore object handles much of this in
# aggregate when a new file is added, but sometimes we need to just check one attribute while processing without
# looking at the cached value in a MetadataStore object.
def get_mtime(which_file: Path) -> float:
    """Get the file-modified time for a file on disk, specified by WHICH_FILE. Does
    not interact with any MetadataStore objects at all, just checks the file on
    disk.

    Returns -1 if the file modified time cannot be determined
    """
    assert isinstance(which_file, Path)

    try:
        return which_file.stat().st_mtime
    except (OSError, AttributeError):
        return -1


def get_video_play_length(which_file: Path) -> str:
    """Use ffprobe to determine the video play length for WHICH_FILE. If the play time
    cannot be determined, return an empty string.

    FIXME! We should probably be returning a numeric duration of some kind.
    """
    data = run_process([pref('ffprobe_loc'), str(which_file)], print_output=False,
                       stderr=subprocess.PIPE, stdout=subprocess.PIPE).stderr
    if "Input #" not in data:
        return ""

    data = data[data.find("Input #"):]  # Chop off the program info data at the beginning
    data = [line.strip() for line in data.split('\n')[1:]]  # omit header row, strip whitespace
    duration_line = [line for line in data if line.casefold().startswith('duration:')][0]
    d, _, br = [i.strip() for i in duration_line.split(',')]

    # FIXME: .lstrip() doesn't strip the specified string, but the characters in it! Works for now, tho.
    return d.casefold().lstrip("duration:").strip()


def get_file_size(which_file: Path) -> int:
    """Get the size of WHICH_FILE on disk.

    If the size cannot be determined, returns -1.
    """
    assert isinstance(which_file, Path)
    assert which_file.is_file()

    try:
        return which_file.stat().st_size
    except (IOError,):
        return -1


def get_video_frame_size(which_file: Path) -> int:
    """Get the area of the video frames in this video, as reported by ffprobe.

    If the frame size cannot be determined, returns -1
    """
    assert isinstance(which_file, Path)
    assert which_file.exists()

    try:
        data = run_process([pref('ffprobe_loc'), str(which_file)], print_output=False, stderr=subprocess.PIPE,
                           stdout=subprocess.PIPE).stderr

        assert "Input #" in data, "ERROR: ffprobe output does not conform to expected format!"
        data = data[data.find("Input #"):]  # Chop off the program info data at the beginning
        data = [line.strip() for line in data.split('\n')[1:]]  # omit header row, strip whitespace
        video_lines = [line for line in data if "stream #" in line.casefold() and "video:" in line.casefold()]

        f_width, f_height = re.findall(r"""[0-9]{2,}x[0-9]{2,}""", video_lines[0])[0].casefold().split('x')
        f_width, f_height = int(f_width), int(f_height)

        return f_width * f_height

    except (IOError, AttributeError, ValueError, TypeError):
        return -1


# Now, some miscellaneous utilities for dealing with terminal-related stuff, mostly used by batch.py.
def terminal_width(default=80):
    """Do the best job possible of figuring out the width of the current terminal.
    Fall back on a default width if it cannot be determined.
    """
    try:
        width = shutil.get_terminal_size()[0]
    except Exception:
        width = default
    if width == -1:
        width = default
    return width


def menu_choice(choice_menu: Mapping,
                prompt: str) -> str:
    """Takes a menu description, passed in as CHOICE_MENU (see below for format),
     and asks the user to make a choice between the options. It then passes back
     the user's choice to the caller.

    CHOICE_MENU is a dict-like preserving order (including regular dicts in Python
    3.7+) mapping a list of options to be typed (short strings, each of which is
    ideally a single letter) to a full description of what that option means (a
    longer string). For example:

        {
            'a': 'always capitalize',
            'y': 'yes',
            'n': 'never'
        }

    As a special case, if both parts of an entry in the OrderedDict are two hyphens,
    that entry is not a valid menu choice; it is printed as-is, as a visual
    separator, but is not a selectable option.

    PROMPT is a direct request for input, printed after all the menu options have
    been displayed.

    Returns a string, the response the user typed that was validated as an allowed
    choice.
    """
    max_menu_item_width = max(len(x) for x in choice_menu)
    menu_column_width = max_menu_item_width + len("  [ ") + len(" ]")
    spacing_column_width = 3
    options_column_width = terminal_width() - (menu_column_width + spacing_column_width + 1)

    # OK, let's print this menu.
    print()
    for option, text in choice_menu.items():
        if (option == '--') and (text == '--'):
            current_line = '  --  ' + ' ' * (max_menu_item_width - len('--')) + ' ' * spacing_column_width + '-----'
        else:
            current_line = '[ %s ]%s%s' % (option, ' ' * (max_menu_item_width - len(option)),
                                           ' ' * spacing_column_width)
            text_lines = textwrap.wrap(text, width=options_column_width, replace_whitespace=False,
                                       expand_tabs=False, drop_whitespace=False)
            if len(text_lines) == 1:
                current_line = current_line + text_lines[0]
            else:
                current_line = current_line + text_lines.pop(0)     # Finish line with first line of the description
                left_padding = '\n' + (' ' * (menu_column_width + spacing_column_width))
                current_line = current_line + left_padding + left_padding.join(text_lines)     # Add in remaining lines
        print(current_line)
    print()

    # Now, get the user's choice
    choice = 'not a legal option'
    legal_options = [k.casefold() for k, v in choice_menu.items() if ((k != '--') or (v != '--'))]
    tried_yet = False
    while choice.casefold() not in legal_options:
        if tried_yet:           # If the user has got it wrong at least once...
            prompt = prompt.strip() + " [ %s ] " % ('/'.join(legal_options))
        choice = input(prompt.strip() + " ").strip()
        tried_yet = True
    return choice


# A few miscellaneous functions for dealing with miscellaneous OS-related stuff.
def produce_playlist(which_files: List[Path],
                     playlist_location: Path,
                     fmt: str,
                     absolute_paths: bool) -> None:
    """Produce a playlist consisting of WHICH_FILES, in one of the recognized formats.
    The resulting playlist will be created in PLAYLIST_LOCATION. FORMAT must be a
    recognized playlist format, as generated by MainTreeView.handle_create_playlist
    in gui.py. If ABSOLUTE_PATHS is True, all paths will be fully resolved, from the
    filesystem or drive root; if it is False, the paths are included as-is.

    Currently only .m3u (system default encoding) or .m3u8 (UTF-8 encoding) are
    supported. Both of these are merely "simple" .m3u (filenames with paths only),
    not "extended m3u," as it contains no #EXTINF or #EXTM3U data.

    See http://forums.winamp.com/showthread.php?t=65772 for the informal description
    of the .m3u format.
    """
    assert isinstance(playlist_location, Path)
    for p in which_files:
        assert isinstance(p, Path)
    assert fmt in ['m3u', 'm3u8'], f"ERROR! Unknown playlist format '{fmt}' passed to produce_playlist!"

    if absolute_paths:
        which_files = [p.resolve() for p in which_files]
    else:
        which_files = [relative_to_with_name(playlist_location, p) for p in which_files]

    file_encoding = 'utf-8' if (fmt == 'm3u8') else locale.getpreferredencoding()
    with open(playlist_location, 'wt', encoding=file_encoding) as playlist_file:
        playlist_file.writelines([f"{str(p).strip()}\n" for p in which_files])


def run_process(arguments: List[str], *args,
                stderr=subprocess.STDOUT,
                stdout=subprocess.PIPE,
                print_output: bool = True, **kwargs) -> subprocess.CompletedProcess:
    """Run an external program. The calling thread (which will never be the main GUI
    thread, when calling from a GUI, which calls process_videos_in_separate_thread()
    to start the batch processing task) blocks until the process is finished
    running. (But if we're driving this process from a Python REPL, or if, gods
    forbid, we ever build a CLI for it, it will run in the same thread as everything
    else in the program, which is also as intended.)

    The upshot is that the batch processing thread is blocked, but the main GUI
    thread is not, and batch processing can happen in the background while the GUI
    remains usable.

    STDERR and STDOUT should be streams or similar appropriate objects; they will be
    passed unmodified on to subprocess.run(). By default, both stderr & stdout are
    captured by the underlying wrapped subprocess.run() call, but specifying
    alternatives allows this to be changed. Other *args and **kwargs can be passed
    to subprocess.run(), as well.

    If PRINT_OUTPUT is True, stdout and stderr will be echoed to stdout, if they are
    captured, after the subprocess.run() call is returned.

    #FIXME: ultimately we want to spawn this off to a separate process using the multiprocessing module.

    #FIXME: assuming this only used for batch processing is no longer true! Docstring needs a major rewrite!
    """
    ret = subprocess.run(arguments, *args, stderr=stderr, stdout=stdout, **kwargs)
    # Next, convert ret.stdout from bytes to str, using our best guess as to encoding
    if ret.stdout:
        ret.stdout = unicode_of(ret.stdout)
        if print_output:
            print(ret.stdout)
    if ret.stderr:
        ret.stderr = unicode_of(ret.stderr)
        if print_output:
            print(ret.stderr)
    return ret


# Some utilities to help deal with abstracting geometry. Primarily used in relation to cropping.
class Point(object):
    def __init__(self, x: int, y: int):
        assert isinstance(x, int)
        assert isinstance(y, int)

        self.x = x
        self.y = y

    def __repr__(self) -> str:
        return f"< {self.__class__.__name__}: ({self.x}, {self.y}) >"


class Rect(object):
    def __init__(self, e1: Union[tk.Event, Point],
                 e2: Union[tk.Event, Point]):
        left, right, top, bottom = e1.x, e2.x, e1.y, e2.y

        assert isinstance(left, int)
        assert isinstance(top, int)
        assert isinstance(right, int)
        assert isinstance(bottom, int)

        if left > right:
            left, right = right, left
        if top > bottom:
            top, bottom = bottom, top
        self.top, self.left, self.bottom, self.right = top, left, bottom, right

    def __repr__(self) -> str:
        return f"< {self.__class__.__name__}: ({self.left}, {self.top}, {self.right}, {self.bottom}) >"

    def __eq__(self, other: 'Rect') -> bool:
        if not isinstance(other, Rect):
            return False
        return all([self.top == other.top, self.left == other.left,
                    self.bottom == other.bottom, self.right == other.right])

    @property
    def width(self) -> int:
        return self.right - self.left

    @property
    def height(self) -> int:
        return self.bottom - self.top

    @staticmethod
    def from_tlbr(top: int,
                  left: int,
                  bottom: int,
                  right: int) -> 'Rect':
        """Convenience function: creates an instance representing the specified
        coordinates.
        """
        return Rect(Point(left, top), Point(right, bottom))

    def as_tlbr(self) -> Tuple[numbers.Real, numbers.Real, numbers.Real, numbers.Real]:
        """Convenience function: decompose an instance into a (top, left, bottom, right)
        tuple.
        """
        return self.top, self.left, self.bottom, self.right

    @staticmethod
    def from_colon_delim(dimensions: str) -> 'Rect':
        """Convenience function: create an instance from the given ffmpeg-like colon-
        delimited string representation.
        """
        assert isinstance(dimensions, str)
        assert dimensions.count(':') == 3

        width, height, left, top = (int(i.strip()) for i in dimensions.split(':'))
        return Rect(Point(left, top), Point(left+width, top+height))

    def as_colon_delim(self) -> str:
        """Generate a colon-separated width:height:x:y string, for using as a parameter
        string to ffmpeg -vf crop.
        """
        return f"{self.width}:{self.height}:{self.left}:{self.top}"

    def copy(self) -> 'Rect':
        """Return a new Rect with the same coordinates as the old one.
        """
        return Rect.from_tlbr(self.top, self.left, self.bottom, self.right)

    def constrain(self, bounds: 'Rect') -> None:
        """Check whether SELF falls entirely on or within the boundaries BOUNDS. If not,
        crops SELF in-place so that all of it falls within BOUNDS.

        SELF should already (largely) be within BOUNDS, because all this function does
        is limit SELF to the overlap of (pre-modification) SELF and BOUNDS. If SELF and
        BOUNDS are entirely discontinuous, SELF will have zero height and zero width
        after this runs.
        """
        if self.top < bounds.top:       # FIXME: also check self.top vs. self.bottom!
            self.top = bounds.top
        if self.left < bounds.left:
            self.left = bounds.left
        if self.bottom > bounds.bottom:
            self.bottom = bounds.bottom
        if self.right > bounds.right:
            self.right = bounds.right
        if self.left > self.right:
            self.left = self.right


def oxford_comma_list_of(what: Collection[str],
                         conjunction: str = "and",
                         quote_char: Optional[str] = None) -> str:
    """Assemble a textual list of the elements of WHAT, separated by commas according
    to the Oxford-comma convention if WHAT has more than two elements. The
    conjunction before the final element can be specified with the CONJUNCTION
    parameter. If QUOTE_CHAR is specified, it is used as both the beginning and
    ending quotation mark surrounding each element and (except for the last element)
    its comma.
    """
    assert isinstance(what, Iterable)
    assert all([isinstance(elem, str) for elem in what])

    ret = ''
    for i, elem in enumerate(what, start=1):
        word = elem if (i == len(what)) else f"{elem},"
        if quote_char:
            word = f"{quote_char}{word}{quote_char}"
        ret += f"{word}{'' if (i == len(what)) else ' '}"

        if i == (len(what) - 1):
            ret += f"{conjunction} "

    return ret
