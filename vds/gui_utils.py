#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Provides utility GUI code for the VideoDataStore program.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import abc
import collections
import functools
import platform
import random
import shlex
import subprocess
import sys
import tempfile
import threading
import uuid
import weakref
import webbrowser

from pathlib import Path
from typing import Any, Callable, Container, Iterable, List, Optional, Tuple, Type, Union

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog as filedialog
import tkinter.messagebox as tk_msgbox
import tkinter.scrolledtext


import vds.ffmpeg_utils as fu
import vds.metadata_handling as mh

import vds.utils as utils
from vds.utils import pref


# Utility decorators
def only_if_DB_loaded(func: Callable) -> Callable:
    """A decorator for methods of the MainWindow class. It checks to see whether the
    MainWindow has a video database associated with it; if not, it prints a warning
    to stdout and returns without doing anything visible other than printing that
    warning.

    If multiple decorators are applied to a method, this should be the outermost:
    eventually, menu items will be auto-enabled and -disabled when decorated with
    this decorator. #FIXME: make that happen.
    """
    @functools.wraps(func)
    def on_call(inst: 'MainWindow', *args, **kwargs) -> Any:
        if inst.displayed_database is not None:
            return func(inst, *args, **kwargs)
        else:
            inst.set_window_status(f"WARNING! {func.__name__} called when no database was loaded! Doing nothing ...",
                                   True, True)
    return on_call


def trap_and_report_errors(func: Callable) -> Callable:
    """A decorator for methods (primarily of the MainWindow class, but should be useful
    for other classes as well). It runs the FUNC method call, trapping any uncaught
    errors and reporting them by popping up a dialog box.

    This is a rough way to report errors, but it's better than nothing.
    """
    @functools.wraps(func)
    def on_call(*args, **kwargs) -> Any:
        try:
            return func(*args, **kwargs)
        except Exception as errrr:
            error_message_box("Unable to complete task!", errrr)
    return on_call


def not_under_windows(message: str = 'It is not possible to do that under Microsoft Windows.') -> Callable:
    """A decorator for methods that are not worth bothering to try to run under Windows.
    Under windows, simply displays an error message.

    Based partly on https://www.geeksforgeeks.org/decorators-with-parameters-in-python/
    """
    def windoze(func) -> Callable:
        def wrapper(*args, **kwargs) -> None:
            error_message_box(message)
        return wrapper

    def non_windoze(func) -> Callable:
        def wrapper(*args, **kwargs) -> Any:
            return func(*args, **kwargs)
        return wrapper

    return windoze if utils.running_under_windows() else non_windoze


def status_bar_announce_bracket(message: str) -> Callable:
    """A utility decorator that modifies FUNC so that, when called, it immediately puts
    MESSAGE in the main window's status bar, then calls the original FUNC, then
    immediately sets the main window's status bar to the default when FUNC has
    finished running.

    Only works on methods of objects: requires a SELF instance (any widget) to call
    Tkinter methods on.

    The only status bar it can set is the status bar on the main application window.
    """
    def returner(func: Callable) -> Callable:
        def wrapper(self, *args, **kwargs) -> Any:
            self._root().set_window_status(message, immediate=True, then_set_default=False)
            try:
                return func(self, *args, **kwargs)
            finally:
                self._root().set_default_window_status(immediate=True)
        return wrapper
    return returner


# small and medium utility functions next: first, dialog boxes
def error_message_box(explanatory_text: str, system_error: Optional[Union[Type[BaseException], str]] = None) -> None:
    """Show an error message box, reporting the system error as well.
    #FIXME: ugly dialog box.
    """
    if system_error:
        tk_msgbox.showerror('Error', message=explanatory_text, detail=str(system_error))
    else:
        tk_msgbox.showerror("Error", message=explanatory_text)


def confirm_prompt(message: str,
                   title: str = "Confirm",
                   parent: Optional[Union[Type[tk.Tk], Type[tk.Toplevel], Type[tk.BaseWidget]]] = None) -> bool:
    """Ask the user to conform the action they're about to take.
    """
    return tk_msgbox.askokcancel(title=title, message=message, parent=parent)


def ask_yes_or_no(message: str,
                  title: str,
                  parent: Optional[Union[tk.Tk, tk.Toplevel]] = None) -> bool:
    """Ask the user whether they want to do something.
    """
    return tk_msgbox.askyesno(title=title, message=message, parent=parent)


def not_implemented_GUI() -> None:
    """Complains that a function is not yet implemented in code.
    """
    tk_msgbox.showwarning('Sorry', 'That function is not implemented yet.')


def do_about_box() -> None:
    """Display an about box.
    """
    box = tk.Toplevel()
    content = ttk.Frame(box, padding=5)

    title_label = ttk.Label(content, text='{} ({})'.format(utils.__progname__, utils.__version__))
    title_label.config(font=utils.prefs['title-font'])
    title_label.grid(columnspan=5, row=0)
    # URL_label = ttk.Label(box, text=utils.website_URL + '\n', fg='blue', font=utils.prefs['statusbar-font'])
    url_label = ttk.Label(content, text=utils.website_URL + '\n', font=utils.prefs['statusbar-font'])
    url_label.bind('<1>', func=lambda *e: open_program_website())
    url_label.grid(columnspan=5, row=1)

    # FIXME: this tk.Message looks odd against a ttk.Frame.
    subheading = tk.Message(content, text='Metadata tracker and batch-processing utility for video files.\n\nCopyright '
                                          '©2021-24 by Patrick Mooney. This program is released under the GNU GPL, '
                                          'either version 3 or (at your option) any later version. See the file '
                                          'LICENSE.txt for details.',
                            width=500, justify=tk.LEFT, padx=10, pady=5)
    subheading.grid(row=2, column=0, columnspan=5, sticky=tk.W)

    ttk.Label(content, text='\nDetails:').grid(row=3, columnspan=2, sticky=tk.E)

    this_label = ttk.Label(content, text='Python version:')
    this_label.grid(sticky=tk.E, columnspan=2, row=4)
    this_label.config(font=utils.prefs['label-font'])
    ttk.Label(content, text=f'{platform.python_implementation()} ({platform.python_version(),}; '
                            f'{platform.python_build()})').grid(row=4, column=2, columnspan=3, sticky=tk.W)

    this_label = ttk.Label(content, text='Tcl/Tk version:')
    this_label.grid(sticky=tk.E, columnspan=2, row=5, column=0)
    this_label.config(font=utils.prefs['label-font'])
    ttk.Label(content, text=f'{tk.TclVersion} / {tk.TkVersion}').grid(row=5, column=2, columnspan=3, sticky=tk.W)

    this_label = ttk.Label(content, text='Platform:')
    this_label.grid(sticky=tk.E, columnspan=2, row=6, column=0)
    this_label.config(font=utils.prefs['label-font'])
    ttk.Label(content, text=f'{platform.platform(aliased=True)}').grid(row=6, column=2, columnspan=3, sticky=tk.W)

    this_label = ttk.Label(content, text='Windowing system:')
    this_label.grid(sticky=tk.E, columnspan=2, row=7, column=0)
    this_label.config(font=utils.prefs['label-font'])
    ttk.Label(content, text="{box.tk.call('tk', 'windowingsystem')}".format).grid(row=7, column=2, columnspan=3,
                                                                                  sticky=tk.W)

    this_label = ttk.Label(content, text='FFmpeg version:')
    this_label.grid(sticky=tk.E, columnspan=2, row=8, column=0)
    this_label.config(font=utils.prefs['label-font'])
    ttk.Label(content, text=fu.get_ffmpeg_version()).grid(row=8, column=2, columnspan=3, sticky=tk.W)

    next_row = 20
    if any(utils.flatten_list(platform.java_ver())):
        ttk.Label(content, text='Java version:').grid(sticky=tk.E, columnspan=2, row=next_row, column=0)
        ttk.Label(content, text=f'{platform.java_ver()}').grid(row=next_row, columnspan=3, sticky=tk.W,
                                                               column=2)
        next_row += 1
    if any(utils.flatten_list(platform.win32_ver())):
        ttk.Label(content, text='Windows version:').grid(sticky=tk.E, columnspan=2, row=next_row, column=0)
        ttk.Label(content, text=f'{platform.win32_ver()}').grid(columnspan=3, sticky=tk.W, row=next_row,
                                                                column=2)
        next_row += 1
    if any(utils.flatten_list(platform.mac_ver())):
        ttk.Label(content, text='macOS version:').grid(sticky=tk.E, columnspan=2, row=next_row, column=0)
        ttk.Label(content, text=f'{platform.mac_ver()}').grid(columnspan=3, sticky=tk.W, row=next_row,
                                                              column=2)
        next_row += 1

    ttk.Button(content, text="Got it", command=box.destroy).grid(sticky=tk.E, column=3, columnspan=2)

    content.columnconfigure(0, weight=1)
    content.grid(row=0, column=0, sticky=(tk.W + tk.N + tk.E + tk.S))
    box.columnconfigure(0, weight=0)
    box.columnconfigure(1, weight=1)
    box.columnconfigure(2, weight=3)
    box.columnconfigure(3, weight=1)
    box.columnconfigure(4, weight=0)

    box.bind('<Return>', lambda *e: box.destroy())
    box.bind('<Escape>', lambda *e: box.destroy())

    box.lift()
    box.focus_set()
    box.grab_set()
    box.wait_window()


# small and medium utilities: non-dialog box code
def open_program_website() -> None:
    """Open the program's website in the user's default web browser.
    """
    webbrowser.open(utils.website_URL)


def running_under_aqua(w: Type[tk.Widget]) -> bool:
    """Determines whether we are running under Aqua, the macOS window manager. Note that
    this is not quite the same thing as determining whether we're running under
    macOS, which can theoretically use other window managers, including X11.
    """
    return w.tk.call('tk', 'windowingsystem') == 'aqua'


def right_click_bind(w: Type[tk.Widget], callback: Callable) -> None:
    """Bind an appropriate callback to the 'right-click' event on a widget, taking into
    account that macOS has special needs here: local convention there is to use
    button 2, not 3, and to also bind the menu popup to Ctrl-click, for people with
    a one-button mouse.

    #FIXME: we need to test this under both Windows and macOS.
    """
    def honor_macos_customs():
        """Determine whether we're following macOS conventions on "right-clicking".
        """
        if pref('right-click-mode') == 2:     # prefs specify macOS-type behavior, explicitly
            return True
        elif pref('right-click-mode') == 1:  # prefs specify Windows/X11-type behavior, explicitly.
            return False
        else:                                       # auto-detect
            return running_under_aqua(w)

    if honor_macos_customs():
        w.bind('<2>', callback)
        w.bind('<Control-1>', callback)
    else:
        w.bind('<3>', callback)


# Utility functions that provide an abstract interface to database functionality that's still more friendly and
# GUI-centric than directly calling database functions.
def confirm_each_file_renaming_proc(dir_to_move_files_to: Path,
                                    current_file: Path,
                                    strip_phrases: Iterable[str],
                                    auto_rename: bool,
                                    parent_window: 'MainWindow') -> Path:
    """A renaming procedure to pass to mh.move__files_from_holding_dir that uses the
    GUI to get the user to validate the name chosen by the cleaning algorithm.
    """
    assert parent_window is not None

    new_file_path = mh.just_choose_clean_name_renaming_proc(dir_to_move_files_to, current_file,
                                                            strip_phrases, auto_rename)
    if not current_file.exists():  # File disappeared since we started? Move along.
        raise mh.StopHandlingThisFile
    new_file = filedialog.asksaveasfilename(initialdir=dir_to_move_files_to, parent=parent_window,
                                            initialfile=str(new_file_path.name),
                                            title=f"Move {current_file.name} to ...")
    if not new_file:
        parent_window.set_window_status_to_canceled(True, True)
        if "yes" == tk_msgbox.askquestion(message="Cancel the entire rest of the file-moving operation? ('No' just "
                                                  "skips this single file.)"):
            raise mh.StopHandlingAllFiles
        else:
            raise mh.StopHandlingThisFile
    return Path(new_file)


def confirm_before_overwriting_renaming_proc(dir_to_move_files_to: Path,
                                             current_file: Path,
                                             strip_phrases: Iterable[str],
                                             auto_rename: bool,
                                             parent_window: 'MainWindow') -> Path:
    """A renaming procedure to pass to mh.move__files_from_holding_dir that approves
    filenames without manual intervention unless they clash with an existing filename,
    in which case it uses the GUI to get the user to validate the name chosen by the
    cleaning algorithm.
    """
    new_file_path = utils.clean_name(suggested_name=dir_to_move_files_to / current_file.name,
                                     fixes_to_strip=strip_phrases,
                                     auto_rename=False)

    if not new_file_path.exists():
        return new_file_path
    else:
        return confirm_each_file_renaming_proc(dir_to_move_files_to=dir_to_move_files_to,
                                               current_file=current_file,
                                               strip_phrases=strip_phrases,
                                               auto_rename=auto_rename,
                                               parent_window=parent_window)


@trap_and_report_errors
def view_media(which_files: List[Union[str, Path]]) -> None:
    """Open the files in WHICH_FILES using the editor configured in the preferences
    chain.

    Files listed in WHICH_FILES must either be absolute paths or relative to the
    current working directory.
    """
    for p in which_files:
        assert isinstance(p, (str, Path))
    files = [(str(p) if not isinstance(p, str) else p) for p in which_files]
    subprocess.Popen([pref('media-viewer')] + files)


def text_to_clipboard(source_widget: Type[tk.Widget],
                      the_text: str) -> None:
    """Push THE_TEXT to the system clipboard. Requires that a widget be specified
    because that's how the Tkinter bindings work in Python.
    """
    assert isinstance(the_text, str)
    source_widget.clipboard_clear()
    source_widget.clipboard_append(the_text)
    source_widget.update_idletasks()


def delete_video(which_database: mh.MetadataStore,
                 which_file: Path,
                 parent_window: Union[Type[tk.Tk], Type[tk.Toplevel]],
                 suppress_write: bool = False) -> None:
    """Delete the entry for WHICH_FILE from the WHICH_DATABASE, permanently (after
    confirming, if we are allowed to confirm).

    Sends out a 'file deleted' notification to all windows that are subclasses of
    BasicWindowMessagingMixIn except for PARENT_WINDOW.
    """
    assert isinstance(which_file, Path)
    try:
        if which_file not in which_database:
            return
        if pref('confirm_delete_db_entry'):
            if not confirm_prompt("Do you really want to to delete this entry from the database?"):
                return
        del which_database[which_file]       # FIXME: offer to re-add if delete fails
        if not pref('delete_file_with_db_entry'):
            return
        if pref('confirm_file_deletion'):
            if not confirm_prompt(f"Do you also want to delete the file {which_file} from disk ?"):
                return
        which_file.unlink()
        BasicWindowMessagingMixIn.dispatch('EVENT_file_deleted', which_file=which_file,
                                           exception_windows=[parent_window,] if parent_window else ())
        parent_window._root().set_window_status(f"Deleted {which_file}.", True, True)
    finally:
        if not suppress_write:
            which_database.write_data()


def rename_video(old_name: Path,
                 which_database: mh.MetadataStore,
                 parent_window: Optional[Union[tk.Tk, tk.Toplevel]] = None) -> Union[Path, bool]:
    """Handle a "rename video" event.
    """         # CHECKED! This code path properly results in a call to change_item_iid!
    new_name = filedialog.asksaveasfilename(title=f"Rename {old_name.name} to ...", initialfile=str(old_name.name),
                                            initialdir=old_name.resolve().parent, parent=parent_window)
    if new_name:
        which_database.rename_file(Path(old_name), Path(new_name))
        BasicWindowMessagingMixIn.dispatch('EVENT_filename_changed', old_name=old_name, new_name=Path(new_name),
                                           exception_windows=(parent_window,))
        return Path(new_name)
    else:
        return False


# Mix-in class for application windows that allows them to respond to events, such as file renaming, in other windows.
class BasicWindowMessagingMixIn(abc.ABC):
    """Mix this class in to declare that an application window supports receiving
    messages about events that occur in other windows. This class provides default
    methods that do nothing and need to be overridden in subclasses. It also provides
    a useful message-passing call that allows messages to be promulgated to all
    instances, and a central list, INSTANCES, in which all instances of
    BasicWindowMessagingMixIn and its subclasses are tracked with a WeakSet (to prevent
    this list from otherwise preventing dead objects from being garbage collected).

    It is very important that any subclass that overrides __new__() also calls back to
    the original so that the relevant instance is checked. So the message for a changed
    file might be sent with something like:

        cls.dispatch('EVENT_filename_changed', old_name='version 3.txt', new_name='version 4.txt'),

    where `cls` is BasicWindowMessagingMixIn or any subclass, or an instance of any of
    those, and the message "the file 'version 3.txt'"' has been renamed to
    'version 4.txt'" will be sent to all instances of any subclass of
    BWMessagingMixIn.

    Note that the main framework currently only issues these notifications for
    events related to ITEMS TRACKED IN THE CURRENT DATABASE, and does not make any
    attempt to issue notifications for every single time it touches a disk.
    Nevertheless, some subclasses of BWMMixIn do respond to events for files
    relevant to them even when those files are not items in the database, if any
    cooperative code wants to issue such notifications.

    Any piece of code may DISPATCH a message with cls.dispatch(); dispatching code
    need not be in a method of an object that is a subclass of this class. Only
    windows that want to RECEIVE messages need to be subclasses of this class.
    """
    instances = weakref.WeakSet()

    @staticmethod
    def __new__(cls, *args, **kwargs):
        instance = super().__new__(cls)
        BasicWindowMessagingMixIn.instances.add(instance)
        return instance

    @classmethod
    def dispatch(cls, message: str,
                 *args,
                 exception_windows: Container[Union[Type[type], Type['BasicWindowMessagingMixIn']]] = (),
                 **kwargs) -> None:
        """Dispatch MESSAGE, a message, to all interested windows, along with any required
        additional information in *ARGS and **KWARGS. MESSAGE is the name of the method to
        be invoked on the relevant window objects; it should be the name of one of the
        abstract procedures below.

        EXCEPTIONS is a list of windows NOT to dispatch MESSAGE to. (This allows a
        window to avoid sending a message to itself, causing it to check to see if it
        needs to perform the action it performed right before it sent the message.) It
        can contain references to individual windows, and can also contain classes, in
        which case no windows of those classes will receive the message.
        """
        assert isinstance(message, str)
        assert hasattr(cls, message), ("ERROR! the MESSAGE passed to BasicWindowMessagingMixIn.dispatch() must be "
                                       "an attribute of that class!")
        assert callable(getattr(cls, message)), ("ERROR! the MESSAGE passed to BasicWindowMessagingMixIn.dispatch() "
                                                 "must represent a callable method!")
        assert isinstance(exception_windows, Iterable)

        found_dead = False
        for wind in BasicWindowMessagingMixIn.instances:
            if wind not in exception_windows:
                if not any([(type(t) == type(type)) and (isinstance(wind, t)) for t in exception_windows]):
                    if not wind.winfo_exists():     # weakrefs aren't always cleaned up immediately. Check.
                        found_dead = True
                    else:
                        meth = getattr(wind, message, None)
                        if meth:
                            meth(*args, **kwargs)
        if found_dead:
            # Force a garbage collection to clean up out-of-date weakrefs. Not necessary, but helpful if we're
            # dispatching more messages in the near future.

            # Not all Pythons support GC, and those that do support it in different ways. GC-handling code is
            # wrapped in this try/except block to let alternate Pythons ignore this particular optimization.
            try:
                import gc
                gc.collect()
            except (Exception,) as err:
                pass

    def _print_warning(self, func_name: str) -> None:
        print(f"WARNING! Class {self.__class__.__name__} inherits method {func_name} from BasicWindowMessagingMixIn!")
        print("This is bad: the function should be explicitly declared and not call its superclass method.")
        print("Crashing now ...\n")
        raise NotImplementedError

    @abc.abstractmethod
    def EVENT_filename_changed(self, old_name: Union[str, Path],
                               new_name: Union[str, Path]) -> None:
        """Function called when a file is renamed by the program.
        """
        BasicWindowMessagingMixIn._print_warning(self, 'EVENT_filename_changed')

    @abc.abstractmethod
    def EVENT_file_deleted(self, which_file: Path) -> None:
        """Function called when the program deletes a file.
        """
        BasicWindowMessagingMixIn._print_warning(self, 'EVENT_file_deleted')

    @abc.abstractmethod
    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """Function called when the program changes a file on disk.
        """
        BasicWindowMessagingMixIn._print_warning(self, 'EVENT_file_change_on_disk')

    @abc.abstractmethod
    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """Function called when the tags associated with a database item change.
        """
        BasicWindowMessagingMixIn._print_warning(self, 'EVENT_file_tags_change')

    @abc.abstractmethod
    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        """Function called when the processing parameters to be used on the next processing
        run are changed.
        """
        BasicWindowMessagingMixIn._print_warning(self, 'EVENT_file_processing_parameters_change')


# Wrapper classes to make Tkinter controls have a consistent interface so they can be called abstractly, and
# some compound "widgets" used for data entry in gd.get_data_from_user(); some are also used elsewhere.
class AbstractUnifiedWidget(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def _getval(self) -> Any:
        """Routine to get the current value of the widget. Must be implemented by all
        descendant classes.
        """

    def activate(self) -> None:
        """Enable the widget. For most widgets, this simple implementation will suffice,
        but some compound widgets have more complex needs.
        """
        self.state(['!disabled'])

    def deactivate(self) -> None:
        """Enable the widget. For most widgets, this simple implementation will suffice,
        but some compound widgets have more complex needs.
        """
        self.state(['disabled'])


class WrappedEntry(ttk.Entry, AbstractUnifiedWidget):
    """Just a wrapper for an Entry field that presents a consistent interface for use
    with a DataGetter frame.
    """
    def _getval(self) -> str:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the widget, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return self.get()

    def _setval(self, text: Any) -> None:
        """Set the current text displayed in SELF to be the stringified version of TEXT,
        replacing any text that's already in the widget.
        """
        self.delete(0, tk.END)
        self.insert(0, str(text))


class WrappedCheckbutton(ttk.Checkbutton, AbstractUnifiedWidget):
    """Wrapper for the TK Checkbutton class that automatically creates the necessary
    Tkinter variable to track its state and provides a _getval() method to get its
    state. It also provides a _setval() method to set its state.
    """
    def __init__(self, *pargs, **kwargs):
        assert 'variable' not in kwargs, ("The WrappedCheckbutton constructor cannot take a VARIABLE parameter! Use "
                                          "a standard Tkinter Checkbutton if you want to do that.")
        self.underlying_variable = tk.IntVar()
        ttk.Checkbutton.__init__(self, variable=self.underlying_variable, *pargs, **kwargs)

    def _getval(self) -> bool:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the control, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return bool(self.underlying_variable.get())

    def _setval(self, value: bool = True) -> None:
        """Set the current value of the Checkbutton to True (checked) or False (not
        checked).
        """
        assert isinstance(value, bool)
        self.underlying_variable.set(int(value))


class WrappedListbox(tk.Listbox, AbstractUnifiedWidget):
    """Just a wrapper for a Listbox that presents a consistent interface for use with
    a DataGetter frame.
    """
    def __init__(self, *pargs,
                 options: Iterable[Any],
                 **kwargs):
        tk.Listbox.__init__(self, *pargs, **kwargs)
        opts = [str(item) for item in options]
        self.insert(tk.END, *opts)
        self.selection_set(0)

    def _getval(self) -> str:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the control, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return self.get(self.curselection())


class WrappedText(tk.Text, AbstractUnifiedWidget):
    """Just a wrapper for a Text field that presents a consistent interface for use
    with dialog boxes.
    """
    def __init__(self, parent, wrap=tk.WORD, *pargs, **kwargs):
        tk.Text.__init__(self, parent, wrap=wrap, *pargs, **kwargs)

    def _getval(self) -> str:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the widget, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return self.get("1.0", tk.END)

    def _setval(self, text: Any) -> None:
        """Set the current text displayed in SELF to be the stringified version of TEXT,
        replacing any text that's already in the widget.
        """
        self.delete(0, tk.END)
        self.insert(0, str(text))


class WrappedLabel(ttk.Label, AbstractUnifiedWidget):
    """Just a wrapper for a Label widget that presents a consistent interface for use
    with a DataGetter frame.
    """
    def _getval(self) -> str:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the widget, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return self.cget('text')

    def _setval(self, text: Any) -> None:
        """Set the current text displayed in SELF to be the stringified version of TEXT,
        replacing any text that's already in the widget.
        """
        self.config(text=text if isinstance(text, str) else str(text))


class WrappedCombobox(ttk.Combobox, AbstractUnifiedWidget):
    """Just a wrapper for a Combobox widget that consists a consistent interface for use
    with dialog boxes.
    """
    def __init__(self, *pargs, **kwargs):
        assert 'variable' not in kwargs, ("The WrappedCombobox constructor cannot take a VARIABLE parameter! "
                                          "Use a standard Tkinter ttk.Combobox if you want to do that.")
        self.underlying_variable = tk.StringVar()
        ttk.Combobox.__init__(self, textvariable=self.underlying_variable, *pargs, **kwargs)

    def _getval(self) -> str:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the control, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return self.underlying_variable.get()

    def _setval(self, value: Any) -> None:
        """Set the current value of the Checkbutton to True (checked) or False (not
        checked).
        """
        self.underlying_variable.set(value if isinstance(value, str) else str(value))


class ShellQuotedParametersPane(ttk.Frame):
    """A pane that contains a few widgets allowing the user to specify a series of
    string-type parameters in manner analogous to shell-quoting syntax. For
    instance, this is the ancestor of a pane allowing files to be tagged with space-
    separated tags that can be quoted to allow tags to contain spaces.
    """
    label_text = ''  # These are overridden by subclasses
    note_text = ''

    def __init__(self, parent: Optional[Union[ttk.Frame, tk.Toplevel, tk.Tk]] = None,
                 suppress_label: bool = False,
                 suppress_note: bool = False, *pargs, **kwargs):
        """A Frame containing widgets that, together, prompt the user for the data needed.
           A PARENT Frame must be passed in, and a data_needed dictionary, which has the same
           structure as is documented in the helper function get_data_from_user(), below.
           """
        ttk.Frame.__init__(self, parent, *pargs, **kwargs)

        if not suppress_label:
            self.label = ttk.Label(self, text=self.label_text)
            self.label.grid(row=0, column=0, sticky=tk.E)
        self.params_field = WrappedEntry(self, width=80)
        self.params_field.grid(row=0, column=1, sticky=(tk.W + tk.E))
        if not suppress_note:
            self.the_note = ttk.Label(self, text=self.note_text)
            self.the_note.config(font=pref('small-label-font'))
            self.the_note.grid(row=1, column=1, sticky=tk.W)
        self.columnconfigure(1, weight=1)

    def _extract_quoted_params(self) -> List[str]:
        """Extract the text from the tags field of SELF and turn it into a list of tags.

        shlex.split() may cause errors for any number of reasons, and code that calls
        this should trap at least ValueError.
        """
        return shlex.split(self.params_field.get())

    def _set_params(self, params: List[str]) -> None:
        """Set the value of the tags field to a properly expressed set of tag strings,
        where "properly expressed" means "quoted as necessary in a shell-like way."
        """
        self.params_field.delete(0, tk.END)
        if params:
            # space at end so thoughtless typing doesn't mutate last item unintentionally
            self.params_field.insert(0, shlex.join(params).strip() + " ")

    def activate(self) -> None:
        """Activate all the sub-widgets if this compound "widget" is activated.
        """
        self.label.state(['!disabled'])
        self.params_field.activate()
        self.the_note.state(['!disabled'])

    def deactivate(self) -> None:
        """Deactivate all the sub-widgets if this compound "widget" is deactivated.
        """
        self.label.state(['disabled'])
        self.params_field.deactivate()
        self.the_note.state(['disabled'])


class FileTaggerPane(ShellQuotedParametersPane, AbstractUnifiedWidget):
    """A frame containing widgets asking for tags to be applied to one or more files.
    Note that this is not itself a dialog box -- it does not contain OK/Cancel buttons
    or have a title bar -- but rather a pane that goes in a window.

    Has a _getval() method so it can be used in a DataGetter window.
    """
    label_text = "Tags: "
    note_text = "Separate tags with spaces. Shell-style quoting or escaping allows tags to contain whitespace."

    def extract_tags(self) -> List[str]:
        return list(collections.OrderedDict.fromkeys(self._extract_quoted_params()).keys())

    def _getval(self) -> List[str]:
        return self.extract_tags()

    def set_tags(self, tags: List[str]) -> None:
        self._set_params(tags)


class ProcessingParameterPane(ShellQuotedParametersPane, AbstractUnifiedWidget):
    label_text = "Parameters: "
    note_text = ("Enter parameters exactly as they will be passed to ffmpeg. Use quotes around parameters containing "
                 "spaces or special characters.")

    def extract_params(self) -> List[str]:
        return self._extract_quoted_params()

    def _getval(self) -> List[str]:
        return self.extract_params()

    def set_params(self, params: List[str]) -> None:
        self._set_params(params)


class EditableListPane(ttk.Frame, AbstractUnifiedWidget):
    """A pane displaying a list of options that can be reordered, removed, and,
    optionally, added to.
    """
    def __init__(self, *pargs,
                 parent: Optional[Union[ttk.Frame, tk.Toplevel, tk.Tk]] = None,
                 items: Iterable[str] = (),
                 addfunc: Optional[Callable] = None,
                 **kwargs):
        """Create the widgets composing the Pane. The pane layout consists of either six
        or seven rows, depending on whether an ADDFUNC is supplied (and therefore
        whether room needs to be made for an ADD button). The listbox is in the left
        column, with a scrollbar next to it, taking up all five (or seven) vertical
        rows; on the right side are buttons.

        Row layout:

        column          with ADDFUNC        without ADDFUNC
                        ------------        ---------------
        0               blank               blank
        1               up                  up
        2               down                down
        3               del                 blank
        4               blank               del
        5               add                 blank
        6               blank
        """
        assert items is not None, "ERROR! No items specified when trying to create an EditableListPane!"
        assert isinstance(items, Iterable), (f"ERROR! values supplied as ITEMS for an EditableListPane must be "
                                             f"supplied as an iterable of strings, but instead {items} "
                                             f"(type {type(items)}) was specified!!")
        for i in items:
            assert isinstance(i, str), (f"ERROR! Item {i} specified as an option for the EditableListPane is not "
                                        f"a string!")

        ttk.Frame.__init__(self, parent, *pargs, **kwargs)
        self.addfunc = addfunc

        num_rows = 7 if self.addfunc else 6

        self.listbox = WrappedListbox(self, options=items, selectmode='browse', height=4)
        self.listbox.grid(row=0, column=0, rowspan=num_rows, sticky=(tk.N + tk.W + tk.S + tk.E))
        self.columnconfigure(0, weight=1)

        self.sbar = ttk.Scrollbar(self, orient=tk.VERTICAL, command=self.listbox.yview)
        self.listbox.configure(yscrollcommand=self.sbar.set)
        self.sbar.grid(row=0, column=1, rowspan=num_rows, sticky=(tk.N + tk.W + tk.S + tk.E))

        # Which rows the buttons go in depends on whether there is an ADDFUNC associated with the pane.
        self.upbut = ttk.Button(self, text="Move Up", command=self.do_move_up)
        self.upbut.grid(row=1, column=2, sticky=(tk.N + tk.W + tk.S + tk.E))

        self.downbut = ttk.Button(self, text="Move Down", command=self.do_move_down)
        self.downbut.grid(row=2, column=2, sticky=(tk.N + tk.W + tk.S + tk.E))

        self.delbut = ttk.Button(self, text="Delete", command=self.do_delete)
        self.delbut.grid(row=3 if self.addfunc else 4, column=2, sticky=(tk.N + tk.W + tk.S + tk.E))

        if self.addfunc:
            self.addbut = ttk.Button(self, text="Add", command=self.do_add)
            self.addbut.grid(row=5, column=2, sticky=(tk.N + tk.W + tk.S + tk.E))
            self.rowconfigure(0, weight=1)
            self.rowconfigure(4, weight=1)
            self.rowconfigure(6, weight=1)

        else:
            self.rowconfigure(0, weight=1)
            self.rowconfigure(3, weight=1)
            self.rowconfigure(5, weight=1)

    def do_move_up(self) -> None:
        """Move the currently selected item up one space.
        """
        cur = self.listbox.curselection()
        if not cur:
            return

        assert len(cur) == 1, "ERROR! Somehow, more than one cell has become selected in the EditableListPane!"
        cur = cur[0]

        if cur == 0:        # First cell selected? Can't move it upwards, then.
            return

        self.listbox.select_clear(0, tk.END)
        val = self.listbox.get(cur)
        self.listbox.delete(cur)

        cur -= 1
        self.listbox.insert(cur, val)
        self.listbox.selection_set(cur)
        self.listbox.see(cur)

    def do_move_down(self) -> None:
        """Move the currently selected item down one space.
        """
        cur = self.listbox.curselection()
        if not cur:
            return

        assert len(cur) == 1, "ERROR! Somehow, more than one cell has become selected in the EditableListPane!"
        if cur[0] == len(cur):
            return

        cur = cur[0]
        self.listbox.select_clear(0, tk.END)
        val = self.listbox.get(cur)
        self.listbox.delete(cur)

        cur += 1
        self.listbox.insert(cur, val)
        self.listbox.selection_set(cur)
        self.listbox.see(cur)

    def do_delete(self) -> None:
        """Delete the currently selected value from the list.
        """
        cur = self.listbox.curselection()
        if not cur:
            return

        assert len(cur) == 1, "ERROR! Somehow, more than one cell has become selected in the EditableListPane!"
        cur = cur[0]
        self.listbox.delete(cur)

        # Make what was previously the next cell the currently selected cell.
        self.listbox.selection_set(cur)
        self.listbox.see(cur)

    def do_add(self) -> None:
        """Add a new value to the list after the currently selected value. If no value
        is currently selected, add to the end of the list.
        """
        if not self.addfunc:
            return

        newval = self.addfunc()
        if not newval:
            return

        assert isinstance(newval, str)

        cur = self.listbox.curselection()
        if (not cur) or (len(cur) == 0):
            cur = tk.END
        else:
            assert len(cur) == 1, "ERROR! Somehow, more than one cell has become selected in the EditableListPane!"
            cur = cur[0]

        self.listbox.insert(cur, newval)

    def _getval(self) -> List[str]:
        """Get the value of the widget. Annoyingly, Tkinter widgets don't all have a
        unified name for the routine that gets the current value of the widget, and
        though several use .get(), at least one control widget uses that name to mean
        something else, and at least one doesn't have a .get() method at all.
        """
        return list(self.listbox.get(0, self.listbox.size()))


# Other combination panes and widgets used as utility classes from multiple places within the code.
class FlexibleTreeview(ttk.Treeview):
    """A Treeview with a set of methods to help implement flexible column behavior.
    """
    def __init__(self, master: Union[Type[tk.BaseWidget], Type[tk.Tk]] = None,
                 *pargs, **kwargs):
        ttk.Treeview.__init__(self, master=master, selectmode='extended', *pargs, **kwargs)
        self.default_sort = None
        self.filtered = list()
        self.make_popups()

    def get_children(self, item=None) -> Tuple[str]:
        """Gatekeeper function, checking that we are in fact dealing with a Treeview
        having the expected zero-depth tree-hidden structure, or at least that the
        caller is not trying to do something incompatible with that assumption, before
        delegating to the superclass.
        """
        assert item is None
        return ttk.Treeview.get_children(self, item)

    def _children(self, item=None) -> List:      # FIXME: list of what?
        """Returns all children of the Treeview, including those currently filtered out.
        Mimics the syntax of get_children, but makes sure the user isn't trying to get
        the children of an individual item.
        """
        assert item is None
        return list(ttk.Treeview.get_children(self, item)) + self.filtered      # FIXME: annotate types

    def filter(self, filter_proc: Optional[Callable] = None) -> None:
        """Filter this Treeview using FILTER_PROC, a function which takes the ID of a
        row in the Treeview and returns False if the line should appear in the
        Treeview's visible list (i.e., if it should not be filtered out), or True if
        the Node should not appear (i.e, if it should be filtered out). Lines that are
        filtered out are detached from the tree and saved in the FlexibleTreeView's
        .filtered list.

        This function filters the top-level list by applying FILTER_PROC to ALL items
        in the database, including those that have previously been filtered out:
        calling .filter() on an already-filtered list applies a new filter to the whole
        list, not a secondary filter to a list that has already been filtered. There is
        no need to call .filter(None) manually before re-filtering, because this
        function already looks at all elements of the list, even if they're
        disconnected.

        If FILTER_PROC is None (the default), all lines are restored to the top level
        of the list, i.e. the list is now entirely unfiltered.
        """
        for i in self.filtered:
            try:
                self.reattach(i, '', -1)
            except Exception as errrrrrr:
                print(f"Warning! Could not reattach {i}! The system said: {errrrrrr}")
        self.filtered = list()

        if not filter_proc:
            self.re_sort_list()
            return

        # If we're actually filtering things out, start by clearing the list.
        for i in self.get_children():
            if filter_proc(i):
                self.detach(i)
                self.filtered.append(i)

        self.re_sort_list()

    def do_select_all(self) -> None:
        """Select every row in the TreeView.
        """
        self.selection_add(self.get_children())

    def do_select_none(self) -> None:
        """Ensure no rows in the TreeView are selected.
        """
        self.selection_remove(self.get_children())

    def do_invert_selection(self) -> None:
        """Invert the selection of the rows in the TreeView.
        """
        cur_sel = set(self.selection())
        for item in self.get_children():
            if item in cur_sel:
                self.selection_remove(item)
            else:
                self.selection_add(item)


    def empty_list(self) -> None:
        """Empty the list by deleting all the children.
        """
        self.delete(*self.get_children())

    def re_sort_list(self, new_sort: Optional[Tuple[str, bool]] = None) -> None:
        """Method that re-sorts the TreeView. If NEW_SORT is None, the previous default
        sort is used again; if there has never been a previous default sort, a sensible
        one is chosen and becomes the default sort for the future. If NEW_SORT *is*
        specified, that sorting method is used, and it also becomes the default sort for
        future sorts.

        If specified, NEW_SORT should be a 2-tuple: (column name, True or False),
        where the second parameter is a bool that is passed to the REVERSE parameter
        of the underlying list's .sort() method.
        """
        if len(self['columns']) < 1:                # No columns? There's no data loaded, & no point sorting.
            return
        if new_sort:                                # If a new sort is specified, use it (and make it the default)
            assert isinstance(new_sort, tuple)
            assert len(new_sort) == 2
            assert isinstance(new_sort[0], str)
            assert isinstance(new_sort[1], bool)
            self.sort_by_column(col=new_sort[0], reverse=new_sort[1])
        elif self.default_sort:                     # Otherwise, use the default sort, if there is one.
            self.sort_by_column(col=self.default_sort[0], reverse=self.default_sort[1])
        else:                                       # Otherwise, figure out a sensible default: first displayed column.
            try:
                new_sort = self['displaycolumns'][0]
            except (IndexError,):                   # Nothing displayed? Yeesh. Use first column found.
                new_sort = self['columns'][0]
            self.sort_by_column(col=new_sort, reverse=False)

    def sort_by_column(self, col: str, reverse: bool = False) -> None:
        """Sort the Treeview in COL order. If REVERSE is True, sort it in reversed COL
        order. Based partly on https://stackoverflow.com/a/1967793/5562328.
        """
        try:
            assert col in self['displaycolumns']
        except AssertionError:
            if len(self['displaycolumns']) > 0:
                col = self['displaycolumns'][0]
            else:
                col = self['columns'][0]
        self.default_sort = (col, reverse)              # Make this the new default sort for the list.

        line = [(self.set(k, col), k) for k in self.get_children()]
        if not line:   # don't bother trying to sort an empty database, which will throw errors anyway.
            return
        line.sort(key=lambda t: utils.default_sort(t[0]), reverse=reverse)

        for index, (_, k) in enumerate(line):           # rearrange items in sorted positions
            self.move(k, '', index)

        self.heading(col, command=lambda *args: self.sort_by_column(col, not reverse))      # next time, reverse sort

    def move_left(self) -> None:
        """Move the selected column to the left. Since this callback function doesn't get
        passed the column number, it relies on that number being stuffed into the
        Treeview widget as an attribute.
        """
        col_name = self.column(self._last_selected_column)['id']
        try:
            col_num = self['displaycolumns'].index(col_name)
            new_columns = list(self['displaycolumns'])
        except (ValueError,):
            new_columns = list(self['columns'])
            col_num = new_columns.index(col_name)
        if not col_num:     # If we can't find it, or it's column zero, do nothing
            return
        new_columns[col_num], new_columns[col_num-1] = new_columns[col_num-1], new_columns[col_num]
        self.config(displaycolumns=new_columns)
        utils.prefs.change_column_position_weight(col_name, -1)

    def move_right(self) -> None:
        """Move the selected column to the right. Since this callback function doesn't get
        passed the column number, it relies on that number being stuffed into the
        Treeview widget as an attribute.
        """
        col_name = self.column(self._last_selected_column)['id']
        try:
            col_num = self['displaycolumns'].index(col_name)
            new_columns = list(self['displaycolumns'])
        except (ValueError,):
            new_columns = list(self['columns'])
            col_num = new_columns.index(col_name)
        if col_num + 1 == len(new_columns):
            return              # If we got None, or were at the end of the list, do nothing.
        new_columns[col_num], new_columns[col_num+1] = new_columns[col_num+1], new_columns[col_num]
        self.config(displaycolumns=new_columns)
        utils.prefs.change_column_position_weight(col_name, 1)

    def hide_column(self, col_name: str) -> None:
        """Hide the column named COL_NAME. Also adjust column widths accordingly.
        """
        # First, get current widths of columns, and calculate total.
        widths = {c: self.column(c, 'width') for c in self['displaycolumns']}
        old_width = sum(widths.values())

        # change which columns are displayed.
        self['displaycolumns'] = [c for c in self['displaycolumns'] if c != col_name]

        # now enlarge each column, using new auto-width of widget to calculate enlargement ratio.
        new_width = sum([self.column(c, "width") for c in self['displaycolumns']])
        for c in self['displaycolumns']:
            self.column(c, width=round(widths[c] * (old_width/new_width)), stretch=True)
        utils.prefs.change_column_visibility(col_name, False)

    def show_column(self, col_name: str) -> None:
        """Show the column named COL_NAME, and adjust widths as appropriate.
        """
        # First, get current widths of columns, and calculate total.
        widths = {c: self.column(c, "width") for c in self['displaycolumns']}
        old_width = sum(widths.values())
        widths[col_name] = round(old_width / len(widths))

        self.column(col_name, width=widths[col_name])

        # change which columns are displayed
        self['displaycolumns'] = list(self['displaycolumns']) + [col_name]

        # now shrink each column, calculating shrink ratio.
        new_width = self.winfo_width()
        for c in self['displaycolumns']:
            self.column(c, width=round(widths[c] / (new_width/old_width)), stretch=True)
        utils.prefs.change_column_visibility(col_name, True)

    def toggle_column_visibility(self, col_name: str) -> None:
        """Show or hide the chosen column.

        The value in the StringVar associated with the column will already have been
        updated to indicate the current state of the menu item by the time this callback
        is executed.
        """
        if self.column_header_popup.column_status[col_name].get() == '0':
            self.hide_column(col_name)
        else:
            self.show_column(col_name)

    def rebuild_column_heading_popup_menu(self) -> None:
        """Build the varying part of the menu from the column labels in the Treeview."""
        if self.column_header_popup.index("end") >= 3:      # If we have existing column entries, remove them.
            self.column_header_popup.delete(3, "end")

        self.column_header_popup.column_status = {}
        for col in self['columns']:
            colvar = tk.StringVar()
            self.column_header_popup.column_status[col] = colvar
            self.column_header_popup.add_checkbutton(label=col, variable=colvar, onvalue=True, offvalue=False,
                                                     command=lambda _col=col: self.toggle_column_visibility(_col))
            self.column_header_popup.column_status[col].set(int(col in self['displaycolumns']))

    def make_popups(self) -> None:
        """Make a popup Menu associated with the column header of the Treeview, and save
        a reference to it in the Treeview's .column_header_popup attribute.

        If other popups are needed, this method should be overridden, and the overriding
        method should call this method to ensure that the column header popup is
        properly built and bound to the right-button event.
        """
        self.column_header_popup = tk.Menu(self, tearoff=False)
        self.column_header_popup.add_command(label="Move column left", command=self.move_left)
        self.column_header_popup.add_command(label="Move column right", command=self.move_right)
        self.column_header_popup.add_separator()
        self.rebuild_column_heading_popup_menu()
        self.column_header_popup.bind("<FocusOut>", lambda *args: self.column_header_popup.unpost())
        right_click_bind(self, self.popup)

    def popup(self, event) -> None:
        """Handle popping up the column header menu if the click was in the column header.
        Otherwise, do nothing. Note that un-popping the menu is a separate task handled
        when the menu loses focus. #FIXME: we really need to test this on a non-Linux
        system. (All of the popup menu code needs to be tested on another OS, really.)
        """
        region = self.identify("region", event.x, event.y)
        if region == "heading":
            self._last_selected_column = self.identify_column(event.x)
            self.rebuild_column_heading_popup_menu()
            self.column_header_popup.post(event.x_root, event.y_root)
            self.column_header_popup.focus_set()


# This scheduler stores and dispatches snippets of code. It's used by MainWindow to queue GUI-updating code that needs
# to be executed from the main execution thread but needs to be queued from another thread.
class TaskScheduler(object):
    """An object that schedules tasks in a queue and periodically executes them. It
    executes the tasks from whichever thread launches it, of course, and its primary
    purpose is to be started from the main Tkinter thread and allow other threads
    to schedule non-time-critical tasks for execution in the main Tkinter thread.
    That is, this object allows Tkinter actions to be scheduled from outside the main
    thread.

    It uses the Tkinter .after() method to execute at most a few actions per second,
    and only during idle time, so it's not suitable for actions that need to happen
    very soon. Still, actions that it executes should generally be quick.
    """
    def __init__(self, parent: 'MainWindow'):
        self.parent_window = parent
        self.to_execute = collections.deque()
        self.mutex = threading.Lock()
        self.parent_window.after(1, self.execute_one)       # schedule our first run as immediately as possible.

    def queue_task(self, task: Callable, *pargs, **kwargs) -> None:
        """Adds TASK, a callable function, to the to_execute queue, along with its
        positional and keyword arguments.

        JSON encoding is done by the _write method because it is relatively slow, and
        doing this in the background helps to keep the process responsive when dealing
        with large files.
        """
        with self.mutex:
            self.to_execute.append({'task': task, 'pargs': pargs, 'kwargs': kwargs})

    def execute_one(self) -> None:
        """Pops a task off of the queue and executes it, if the queue holds a waiting
        task. Executes only one task from the queue, no matter how many are waiting to be
        executed. Schedules itself to run again in a few hundred milliseconds.
        """
        with self.mutex:
            if len(self.to_execute) == 0:
                self.parent_window.after(200, self.execute_one)
                return

            data = self.to_execute.popleft()
            data['task'](*data['pargs'], **data['kwargs'])
            self.parent_window.after(5, self.execute_one)


# GUI tools for redirecting output streams to GUI widgets

# This first redirects streams to a text box. It is based on Mark Lutz, *Programming Python*, pp. 624-25.
# Following it are a convenience function for invocation and a convenience decorator for class methods.

# These functions cause GUI updates directly and are not safe to use from threads other than the main thread!
class TextBoxStreamRedirector(object):
    """Wraps a Tkinter ScrolledText object to manage output from a function printing to
    stdout/stderr.
    """
    def __init__(self, window_title: Optional[str] = None):
        self.text = None
        self.window_title = window_title

    def popupnow(self) -> None:
        if self.text:
            return
        parent = tk.Toplevel()
        if self.window_title:
            parent.title(self.window_title)
        self.text = tk.scrolledtext.ScrolledText(parent)
        self.text.config(font=pref('output-font'))
        self.text.pack(expand=True, fill=tk.BOTH)

    def write(self, text: str) -> None:
        self.popupnow()
        self.text.insert(tk.END, text)
        self.text.see(tk.END)
        self.text.update()

    def writelines(self, lines: List[str]) -> None:
        self.write('\n'.join(lines))


def redirect_prints_to_text_box(func: Callable,
                                *pargs,
                                window_title: Optional[str] = 'output',
                                stream_redirector_class: Callable = TextBoxStreamRedirector,
                                **kwargs) -> Any:
    """Call a function, re-routing stdout and stderr to a TextBoxStreamRedirector
     object (or, if specified, another, similar object). Note that, unlike in Lutz's
     example code, and perhaps counter-intuitively, stdin is not rerouted here.
    """
    save_streams = sys.stdout, sys.stderr
    try:
        sys.stdout = stream_redirector_class(window_title=window_title)
        sys.stderr = sys.stdout
        return func(*pargs, **kwargs)
    finally:
        sys.stdout, sys.stderr = save_streams


def textbox_redirector(window_title: Optional[str] = 'output') -> Callable:
    """Wraps the function redirect_prints_to_text_box in a decorator so it can easily
    be used to wrap functions and methods and director their text output to a pop-up
    window.
    """
    def decorator(func: Callable) -> Callable:
        def wrapper(*args, **kwargs):
            return redirect_prints_to_text_box(func, *args, window_title=window_title, **kwargs)
        return wrapper
    return decorator


# This next class is a version of TextBoxStreamRedirector that is safe to use from a thread other than the main
# GUI thread. It's followed by a convenience function that uses it to safely call redirect_prints_to_text_box from
# a thread other than the main GUI thread.
class ThreadSafeStreamRedirector(TextBoxStreamRedirector):
    def __init__(self, scheduler, *pargs, **kwargs):
        TextBoxStreamRedirector.__init__(self, *pargs, **kwargs)
        self.scheduler = scheduler

    def write(self, text: str) -> None:
        self.scheduler.queue_task(TextBoxStreamRedirector.write, self, text=text)


def thread_safe_text_box_redirector(func: Callable,
                                    *pargs,
                                    scheduler: TaskScheduler,
                                    **kwargs) -> Any:
    """Runs redirect_prints_to_text_box, but uses an alternative text-box class that
    routes updates through SCHEDULER.
    """
    assert isinstance(scheduler, TaskScheduler)
    return redirect_prints_to_text_box(func, *pargs,
                                       stream_redirector_class=lambda: ThreadSafeStreamRedirector(scheduler), **kwargs)


# This next is a selection_rect that redirects prints to a StatusBar, plus convenience invocation utilities.
class StatusBarStreamRedirector(object):
    """A stream to which stdout/stderr can be redirected. Instead of printing ot a
    console, output is directed to a StatusBar object that sits at the bottom of a
    window.

    Only writes one line at a time (if a multi-line string is passed in, it calls
    itself recursively to print one line after another).

    It strips whitespace from the end of each line it prints.
    """
    def __init__(self, the_status_bar: 'StatusBar'):
        self.the_status_bar = the_status_bar

    def write(self, text: str) -> None:
        text = text.strip()
        if '\n' in text:
            for line in text.split('\n'):
                self.write(line)
        else:
            if text:
                self.the_status_bar.set_text(text, immediate=True)

    def writelines(self, lines: List[str]) -> None:
        for line in lines:
            self.write(line)


def redirect_prints_to_status_bar(the_status_bar: 'StatusBar',
                                  also_redirect_stderr: bool,
                                  func: Callable, *pargs, **kwargs) -> Any:
    """Call a function, redirecting stdout (and, optionally, stderr) to a GUI status
    bar. As with redirect_prints_to_text_box(), above, stdin is not redirected.
    """
    assert isinstance(the_status_bar, StatusBar)
    save_streams = sys.stdout, sys.stderr
    try:
        sys.stdout = StatusBarStreamRedirector(the_status_bar)
        if also_redirect_stderr:
            sys.stderr = sys.stdout
        return func(*pargs, **kwargs)
    finally:
        sys.stdout, sys.stderr = save_streams
        the_status_bar.queue_default_text()


class StatusBarRedirector:
    """A decorator version of redirect_prints_to_status_bar. (Actually, any decorator
    that takes parameters is in fact a decorator FACTORY; many thanks to the people
    who helped me work this out.)

    https://www.reddit.com/r/learnpython/comments/tj6jz2/on_the_mechanics_of_function_decoration/

    Currently, this is unused: there's no way to use an instance attribute (the
    status bar attached to a MainWindow) or an instance (the MainWindow itself) as a
    parameter to the decorator when it decorates a class's method, because that data
    is not available at the time of decoration. D'oh!
    """
    def __init__(self, the_status_bar: 'StatusBar',
                 also_redirect_stderr: bool):
        self.the_status_bar = the_status_bar
        self.also_redirect_stderr = also_redirect_stderr

    def __call__(self, func):
        def on_call(*args, **kwargs) -> Any:
            return redirect_prints_to_status_bar(self.the_status_bar, self.also_redirect_stderr, func, *args, **kwargs)
        return on_call


class Tooltip:
    """A tooltip is an abstract object (not a widget) that encloses a widget and is
    attached to a widget, and causes the attached tooltip to pop up when the mouse
    pointer hovers over the widget it's attached to.

    Based on
    https://blog.finxter.com/5-best-ways-to-display-a-message-when-hovering-over-something-with-mouse-cursor-in-tkinter-python/
    """
    def __init__(self, widget, text):
        self.widget = widget
        self.text = text
        self.widget.bind("<Enter>", self.enter)
        self.widget.bind("<Leave>", self.leave)
        self.tooltip_window = None

    def enter(self, event=None):
        x, y, cx, cy = self.widget.bbox("insert")
        x += self.widget.winfo_rootx() + 25
        y += self.widget.winfo_rooty() + 20
        self.tooltip_window = tk.Toplevel(self.widget)
        self.tooltip_window.wm_overrideredirect(True)
        self.tooltip_window.wm_geometry("+%d+%d" % (x, y))
        label = tk.Label(self.tooltip_window, text=self.text, justify='left',
                         background='#ffffff', relief='solid', borderwidth=1,
                         font=pref('tooltip-font'))
        label.pack(ipadx=3)

    def leave(self, event=None):
        if self.tooltip_window:
            self.tooltip_window.destroy()


class StatusBar(ttk.Label):
    default_text = "Ready."

    def __init__(self, parent: Union[Type[tk.Widget], Type[tk.Toplevel], Type[tk.Tk]],
                 *pargs,
                 initial_messages: Iterable[str] = ('No file open.',),
                 default_text: Optional[str] = None,
                 **kwargs):
        ttk.Label.__init__(self, parent, *pargs, **kwargs, anchor='w', relief=tk.SUNKEN)
        self.default_text = default_text or self.default_text
        self.config(font=pref('statusbar-font'))
        self.receptive = True
        self.message_queue = collections.deque(initial_messages)
        self.rotate_text_queue()

    def rotate_text_queue(self) -> None:
        """If there's any text waiting to be displayed in the status bar, display the next
        bit of text and schedule the next rotation. If there's no text waiting to be
        displayed, leave any existing text where it is.
        """
        if len(self.message_queue) == 0:
            self.receptive = True
            return
        self.config(text=self.message_queue.popleft())
        self.receptive = False
        self.after(1000 * int(pref('statusbar-delay')), self.rotate_text_queue)

    def set_text(self, what_text: str,
                 immediate: bool = False) -> None:
        """Set the text in the window's status bar to WHAT_TEXT. If IMMEDIATE is True,
        the text is displayed immediately, pre-empting any other messages that are
        waiting to be displayed, and emptying the queue of waiting status messages;
        otherwise, it is queued, and will roll in when other messages have cycled
        through already.
        """
        if immediate:
            self.config(text='')
            self.message_queue.clear()
            self.receptive = True
        self.message_queue.append(what_text)
        if self.receptive:
            self.rotate_text_queue()
        self.update_idletasks()

    def set_default_text(self, immediate: bool = False) -> None:
        """The easiest way to set the default text is to specify it as a constructor
        parameter, or assign it to the relevant class-level attribute.
        """
        self.set_text(self.default_text, immediate=immediate)

    def queue_default_text(self) -> None:
        """Convenience function: more self-documenting code.
        """
        self.set_default_text(False)


# Here follows a series of classes used to display graphical information.
class ScrolledCanvas(tk.Canvas):
    """A scrollable frame, based on TKinter's Canvas widget. Based largely on Mark
    Lutz's example in *Programming Python*, 4th ed., ch. 11, p. 723ff.
    """

    def __init__(self, container: Union[tk.Frame, ttk.Frame, Type[tk.Toplevel], Type[tk.Tk]],
                 *pargs, **kwargs):
        tk.Canvas.__init__(self, master=container, *pargs, **kwargs)
        self.config(borderwidth=0)
        self.vbar = ttk.Scrollbar(container)
        self.hbar = ttk.Scrollbar(container, orient='horizontal')
        self.vbar.pack(side=tk.RIGHT, fill=tk.Y)
        self.hbar.pack(side=tk.BOTTOM, fill=tk.X)
        self.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
        self.vbar.config(command=self.yview)
        self.hbar.config(command=self.xview)
        self.config(yscrollcommand=self.vbar.set)
        self.config(xscrollcommand=self.hbar.set)


def get_video_thumbnail(which_video: Union[str, Path],
                        try_hard: bool = False) -> Union[Path, Type[BaseException]]:
    """Given WHICH_VIDEO, a Path (or string representation of a path) to a video file
    that ffmpeg can process, generate a thumbnail in an OS-appropriate temporary
    directory. If TRY_HARD is True, instructs ffmpeg to put extra effort into
    finding a better thumbnail even if it takes longer. Otherwise, just uses a
    random frame.

    Returns a Path to that thumbnail file, which the calling code is responsible for
    deleting if deletion is important.
    """
    tempdir = Path(tempfile.gettempdir())
    rendered_image_path = None
    while not rendered_image_path:  # Not totally immune to race conditions, but should make them pretty unlikely
        new_path = tempdir / str(Path(str(uuid.uuid4())).with_suffix('.png'))
        if not new_path.exists():
            rendered_image_path = new_path
    arguments = [pref('ffmpeg_loc'), '-ss', str(random.random() * 0.8 * fu.video_duration_from_ffprobe(which_video)),
                 '-i', str(which_video)]
    if try_hard:
        arguments += ['-vf', "thumbnail"]
    arguments += ['-frames:v', '1', str(rendered_image_path)]
    try:
        completed = utils.run_process(arguments, universal_newlines=True)
        if completed.returncode:
            raise RuntimeError(f"Got non-zero return code {completed.returncode} from ffmpeg! "
                               f"It said: {completed.stdout}.")
        if not rendered_image_path.exists():
            raise RuntimeError("Asked ffmpeg to produce a thumbnail, but it was unable to!")
    except (Exception,) as errrr:
        return errrr
    return rendered_image_path


class ProcessingTreeView(FlexibleTreeview):
    """A Treeview scroller used to display the list of files waiting to be processed in
    the next batch-processing run.
    """
    def __init__(self, *pargs, **kwargs):
        FlexibleTreeview.__init__(self, *pargs, **kwargs)
        self['show'] = 'headings'
        columns = ('file', 'parameters')
        self['columns'] = columns
        self['displaycolumns'] = columns
        self.column('file', anchor='w', stretch=True)
        self.column('parameters', anchor='e', stretch=True)
        self.heading('file', text='file', command=lambda: self.sort_by_column('file'))
        self.heading('parameters', text='parameters', command=lambda: self.sort_by_column('parameters'))

    def rebuild_list(self) -> None:
        """Rebuild the list, based on files scheduled for processing in the currently
        displayed database.
        """
        db = self._root().displayed_database
        self.empty_list()
        for f in [i for i in db if db[i]['next_processing']]:
            self.insert('', 'end', iid=f, text=f, values=(f, shlex.join(db[f]['next_processing'])))


if __name__ == "__main__":
    print("Sorry, no self-test code in this module!")
