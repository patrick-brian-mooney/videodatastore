#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Provides utility GUI code that creates windows other than the main window
for the VideoDataStore program.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import datetime
import numbers
import shlex
import subprocess
import sys
import textwrap
import warnings

import tkinter as tk
from tkinter import ttk
from tkinter import filedialog

from pathlib import Path
from typing import Callable, Iterable, List, Optional, Tuple, Type, Union

from PIL import Image, ImageTk

import vds.get_data as gd
import vds.gui_utils as gu
import vds.metadata_handling as mh

import vds.utils as utils
from vds.utils import pref


class GraphicDisplayWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """A window for displaying graphics -- not used directly by the main code, but
    useful for visualizations, etc.

    Based largely on sample code in Mark Lutz's *Programming Python*, 4th ed.,
    pp. 724ff.
    """
    def __init__(self, parent: Union[tk.Tk, tk.Toplevel, ttk.Frame, None],
                 image_location: Union[Path, str],
                 title: str,
                 forcesize: Tuple[int, int] = (),
                 *pargs, **kwargs):
        tk.Toplevel.__init__(self, master=parent, *pargs, **kwargs)
        self.parent_window = parent
        self.title(title)
        self.zoom_factor = 1.0
        self.make_top_level_menus()
        self.make_early_widgets()
        self.canvas = gu.ScrolledCanvas(self)
        self.set_image_from_file(image_location, forcesize)
        # self.graphic_object = PictureFrame(parent=self.canvas, image_loc=image_location)
        # self.graphic_object.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
        self.canvas.pack(side=tk.TOP, fill=tk.BOTH, expand=tk.YES)
        # self.graphic_object.image_panel.draw()
        self.protocol("WM_DELETE_WINDOW", self.handle_close)
        self.focus()
        # FIXME: Lutz's code binds various events here for UI usability purposes. Implement at least some of this.

    def set_image_from_file(self, image_location: Path,
                            forcesize: Tuple[int, int] = ()) -> None:
        """Set the image in this window to be the image specified by IMAGE_LOCATION.
        Then draw the image.
        """
        assert isinstance(image_location, Path)
        self.image_location = image_location
        self.original_image = Image.open(self.image_location)  # the original, never-touched copy of the image.
        self.working_image_copy = Image.open(self.image_location)  # The working copy of the image.
        self.draw_image(forcesize)

    def make_early_widgets(self) -> None:
        """Does nothing. Subclasses can override this method to put widgets in the window
        before other widgets are constructed.
        """
        pass

    def draw_image(self, forcesize: Tuple[int, int] = ()) -> None:
        """Actually draw the image. If FORCESIZE is specified, the image is drawn to that
        exact (w, h) size; otherwise, a reasonable size is computed.
        """
        self.save_photo = ImageTk.PhotoImage(self.working_image_copy)
        scrwide, scrhigh = forcesize or self.maxsize()
        imgwide = self.save_photo.width()
        imghigh = self.save_photo.height()

        fullsize = (0, 0, imgwide, imghigh)
        viewwide = min(imgwide, scrwide)
        viewhigh = min(imghigh, scrhigh)

        canvas = self.canvas
        canvas.delete('all')
        canvas.config(height=viewhigh, width=viewwide)
        canvas.config(scrollregion=fullsize)
        canvas.create_image(0, 0, image=self.save_photo, anchor=tk.NW)

        if imgwide <= scrwide and imghigh <= scrhigh:
            self.state('normal')
        elif sys.platform[:3] == 'win':
            self.state('zoomed')

    def size_to_display_side(self, scaler: Callable) -> None:
        """Uses SCALER to reize the image to its new size, then draws the image in its
        canvas. Selects an appropriate filtering algorithm. Updates the
        .working_image_copy of the image, not the no-touch original.
        """
        scrwide, scrhigh = self.maxsize()
        imgwide, imghigh = self.working_image_copy.size
        newwide, newhigh = scaler(scrwide, scrhigh, imgwide, imghigh)
        if (newwide * newhigh) < (imgwide * imghigh):
            fltr = Image.ANTIALIAS
        else:
            fltr = Image.BICUBIC
        self.working_image_copy = self.working_image_copy.resize((newwide, newhigh), fltr)
        self.draw_image()

    def on_size_to_display_height(self, event: tk.Event) -> None:
        """Resize, fitting to the computed new height of the embedded canvas.
        """
        def scale_high(scrwide: numbers.Real,
                       scrhigh: numbers.Real,
                       imgwide: numbers.Real,
                       imghigh: numbers.Real) -> Tuple[numbers.Real, numbers.Real]:
            newhigh = scrhigh
            newwide = int(scrhigh * (imgwide / imghigh))
            return newwide, newhigh

        self.size_to_display_side(scale_high)

    def on_size_to_display_width(self, event: tk.Event) -> None:
        """Resize, fitting ot the computed new width of the embedded canvas.
        """
        def scale_wide(scrwide: numbers.Real,
                       scrhigh: numbers.Real,
                       imgwide: numbers.Real,
                       imghigh: numbers.Real) -> Tuple[numbers.Real, numbers.Real]:
            newwide = scrwide
            newhigh = int(scrwide * (imghigh / imgwide))
            return newwide, newhigh

        self.size_to_display_side(scale_wide)

    def zoom(self, delta: float) -> None:
        """Zoom in or out, based on the value of DELTA, the amount to apply geometrically
        to SELF's current .zoom_factor. Typical values for DELTA are 0.9 or 1.1.
        """
        wide, high = self.original_image.size
        self.zoom_factor = self.zoom_factor * delta
        if self.zoom_factor < 1.0:
            filter_used = Image.ANTIALIAS
        else:
            filter_used = Image.BICUBIC
        self.working_image_copy = self.original_image.resize(
            (int(wide * self.zoom_factor), int(high * self.zoom_factor)), filter_used)
        self.draw_image()

    def on_zoom_in(self,
                   event: Optional[tk.Event] = None,
                   incr: float = 0.1) -> None:
        """High-level hook to be called to handle resizing when zooming in.
        """
        self.zoom(1.0 + incr)

    def on_zoom_out(self,
                    event: Optional[tk.Event] = None,
                    decr: float = 0.1) -> None:
        """High-level hook to be called to handle resizing when zooming out.
        """
        self.zoom(1.0 - decr)

    def png_export(self) -> None:
        """Export the visualization as a PNG file.
        """
        filename = tk.filedialog.asksaveasfilename(parent=self)
        if filename:
            Path(filename).write_bytes(self.image_location.read_bytes())

    def make_file_menu(self, top_level_menu: tk.Menu) -> None:
        """Add a File menu with very basic options: Save As and Close.
        """
        file_menu = tk.Menu(top_level_menu, tearoff=pref('allow-tearoffs'))
        file_menu.add_command(label="Save image ...", command=self.png_export)  # , accelerator="Ctrl+N")
        file_menu.add_separator()
        file_menu.add_command(label="Close", command=self.handle_close, accelerator="Ctrl+W")
        file_menu.bind_all("<Control-w>", lambda *a: self.handle_close())
        top_level_menu.add_cascade(label="File", menu=file_menu)

    def make_edit_menu(self, top_level_menu: tk.Menu) -> None:
        """Currently, this method does nothing, but we'll eventually want to be able to
        copy the visualization to the clipboard.
        """
        pass

    def make_view_menu(self, top_level_menu: tk.Menu) -> None:
        """Make a 'View' menu for the visualization window.
        """
        view_menu = tk.Menu(top_level_menu, tearoff=pref('allow-tearoffs'))
        view_menu.add_command(label="Zoom in", command=self.on_zoom_in)  # , accelerator="Ctrl-+"
        view_menu.bind_all("<Control-plus>", lambda *a: self.on_zoom_in())
        view_menu.add_command(label="Zoom out", command=self.on_zoom_out)  # , accelerator="Ctrl+-"
        view_menu.bind_all("<Control-minus>", lambda *a: self.on_zoom_out())
        top_level_menu.add_cascade(label="View", menu=view_menu)

    def make_top_level_menus(self):
        """Create the top-level menu for this window.
        """
        top_level_menu = tk.Menu(self)
        self.config(menu=top_level_menu)
        self.make_file_menu(top_level_menu)
        self.make_edit_menu(top_level_menu)
        self.make_view_menu(top_level_menu)

    def handle_close(self) -> None:
        """Handle the window-closing event by destroying the window. Subclasses may need to
        delete temporary files or remove the window from the MainWindow's subwindows
        list, or do other clean-up, before inheriting from the superclass.
        """
        self.destroy()

    # Now, the application-events handling mechanism.
    def EVENT_file_deleted(self, which_file: Path) -> None:
        """If the file we're displaying was deleted, destroy the window.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self._root().displayed_database.file_location, which_file)
        if which_file == self.image_location:
            self.handle_close()

    def EVENT_filename_changed(self, old_name: Path,
                               new_name: Path) -> None:
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)
        if old_name.samefile(self.image_location):
            self.image_location = new_name

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """Nothing to do here: we don't deal with file tags in any way.
        """
        assert isinstance(which_file, Path)        # intentionally ignore this event

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """If the file we're displaying changed on disk, reload it.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)
        if which_file == self.image_location:
            self.set_image_from_file(image_location=self.image_location)

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        """Nothing to do here: we don't deal with file processing parameters.
        """
        assert isinstance(which_file, Path)        # intentionally ignore this event


class DatabaseInfoWindow(tk.Toplevel):
    """A window that presents an overview of database info, including allowing editing
    of certain metadata.
    """
    def __init__(self, which_database: 'MetadataStore',
                 parent_window: Union[tk.Tk, tk.Toplevel, 'MainWindow']):
        tk.Toplevel.__init__(self)
        self.parent_window = parent_window
        self.which_database = which_database
        self.protocol("WM_DELETE_WINDOW", self.do_close)

        # now, make the window's widgets
        content = ttk.Frame(self, padding=5)

        ttk.Label(content, text="Location: ").grid(row=0, column=0, sticky=tk.E)
        self.location_label = ttk.Label(content)
        self.location_label.grid(row=0, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Created: ").grid(row=1, column=0, sticky=tk.E)
        self.when_created_label = ttk.Label(content)
        self.when_created_label.grid(row=1, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Files tracked: ").grid(row=2, column=0, sticky=tk.E)
        self.num_files_tracked_label = ttk.Label(content)
        self.num_files_tracked_label.grid(row=2, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Queued for processing: ").grid(row=3, column=0, sticky=tk.E)
        self.num_to_process_label = ttk.Label(content)
        self.num_to_process_label.grid(row=3, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Incrementally tagged: ").grid(row=4, column=0, sticky=tk.E)
        self.tagged_files_label = ttk.Label(content)
        self.tagged_files_label.grid(row=4, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Unique tags: ").grid(row=6, column=0, sticky=tk.E)
        self.num_tags_label = ttk.Label(content)
        self.num_tags_label.grid(row=6, column=1, sticky=tk.W)

        self.tracks_videos_checkbutton = gu.WrappedCheckbutton(content, text="Tracks videos")
        self.tracks_videos_checkbutton.grid(row=7, column=1, sticky=tk.W, columnspan=3)

        self.tracks_audio_checkbutton = gu.WrappedCheckbutton(content, text="Tracks audio files")
        self.tracks_audio_checkbutton.grid(row=8, column=1, sticky=tk.W, columnspan=3)

        self.tracks_other_checkbutton = gu.WrappedCheckbutton(content, text="Tracks other files")
        self.tracks_other_checkbutton.grid(row=9, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Database title: ").grid(row=10, column=0, sticky=tk.E)
        self.db_title_entry = gu.WrappedEntry(content, width=80)
        self.db_title_entry.insert(0, self.which_database.metadata['database title'])
        self.db_title_entry.grid(row=10, column=1, sticky=(tk.W + tk.E), columnspan=3)

        ttk.Label(content, text="Database maintainer: ").grid(row=11, column=0, sticky=tk.E)
        self.db_maintainer_entry = gu.WrappedEntry(content, width=80)
        self.db_maintainer_entry.insert(0, self.which_database.metadata['database maintainer'])
        self.db_maintainer_entry.grid(row=11, column=1, sticky=(tk.W + tk.E), columnspan=3)

        ttk.Label(content, text="Database description: ").grid(row=12, column=0, sticky=tk.E)
        self.db_desc_entry = gu.WrappedText(content, width=80, height=10)
        self.db_desc_entry.insert(tk.END, self.which_database.metadata['database description'])
        self.db_desc_entry.grid(row=12, column=1, sticky=(tk.W + tk.E), columnspan=3)

        ttk.Button(content, text="Cancel", command=self.handle_cancel).grid(row=100, column=2, sticky=tk.E)
        ttk.Button(content, text="OK", command=self.handle_ok).grid(row=100, column=3, sticky=tk.E)

        content.columnconfigure(1, weight=1)
        content.grid(row=0, column=0, sticky=(tk.W + tk.N + tk.E + tk.S))
        self.update_displayed_data()
        self.columnconfigure(0, weight=1)
        self.rowconfigure(0, weight=1)

        self.parent_window.subwindows['database info'] = self
        self.lift()
        parent_window.set_default_window_status(True)

    @gu.status_bar_announce_bracket('Analyzing database ...')
    def update_displayed_data(self) -> None:
        """Update the database info we're displaying. Called when the dialog box is set up,
        and sometimes when data-changed events occur.
        """
        self.location_label.config(text=str(self.which_database.file_location.resolve().parent))
        self.when_created_label.config(text=self.which_database.metadata['database created'])
        self.num_files_tracked_label.config(text=str(len(self.which_database)))
        self.num_to_process_label.config(text=str(self.which_database.num_videos_to_process))
        self.tagged_files_label.config(text=f"{self.which_database.num_incrementally_tagged_files} "
                                            f"({self.which_database._num_incrementally_skipped_files} "
                                            f"temporarily skipped)")
        self.num_tags_label.config(text=str(len(self.which_database.all_tags)))
        self.tracks_videos_checkbutton._setval(self.which_database.settings['include video'])
        self.tracks_audio_checkbutton._setval(self.which_database.settings['include audio'])
        self.tracks_other_checkbutton._setval(self.which_database.settings['include others'])

    def handle_ok(self) -> None:
        """Handle the OK button in the dialog box.
        """
        re_scan_files = False
        if self.which_database.settings['include video'] != self.tracks_videos_checkbutton._getval():
            re_scan_files = True
        if self.which_database.settings['include audio'] != self.tracks_audio_checkbutton._getval():
            re_scan_files = True
        if self.which_database.settings['include others'] != self.tracks_other_checkbutton._getval():
            re_scan_files = True

        self.which_database.settings['include video'] = self.tracks_videos_checkbutton._getval()
        self.which_database.settings['include audio'] = self.tracks_audio_checkbutton._getval()
        self.which_database.settings['include others'] = self.tracks_other_checkbutton._getval()
        self.which_database.metadata['database title'] = self.db_title_entry._getval().strip()
        self.which_database.metadata['database maintainer'] = self.db_maintainer_entry._getval().strip()
        self.which_database.metadata['database description'] = self.db_desc_entry._getval().strip()

        if re_scan_files:
            if gu.ask_yes_or_no("You have changed which types of files are being tracked in this database. "
                                "Would you like to re-scan the folders containing files and update the database?",
                                title="Update database?", parent=self):
                self.parent_window.set_window_status("Updating database ...", True, False)
                gu.redirect_prints_to_status_bar(self.parent_window.status_bar, False,
                                                 self.which_database.prune_missing_files)
                gu.redirect_prints_to_status_bar(self.parent_window.status_bar, False,
                                                 self.which_database.scan_for_new_files)
                self.parent_window.rebuild_list()
                self.parent_window.set_default_window_status(immediate=True)
            else:
                self.parent_window.set_window_status("Database not updated. Use File/Re-scan .. and File/Remove "
                                                     "missing ... to update manually.", True, True)
        self.which_database.write_data()
        self.do_close()

    def _is_changed(self) -> bool:
        """Returns True if any of the editable information in the dialog box has
        changed, or False otherwise
        """
        if self.which_database.settings['include video'] != self.tracks_videos_checkbutton._getval():
            return True
        if self.which_database.settings['include audio'] != self.tracks_audio_checkbutton._getval():
            return True
        if self.which_database.settings['include others'] != self.tracks_other_checkbutton._getval():
            return True
        if self.which_database.metadata['database title'] != self.db_title_entry._getval().strip():
            return True
        if self.which_database.metadata['database maintainer'] != self.db_maintainer_entry._getval().strip():
            return True
        if self.which_database.metadata['database description'] != self.db_desc_entry._getval().strip():
            return True
        return False

    def do_close(self) -> None:
        """Handles the window-destroying moment. Does not extract and update data. Does not
        ask the user if they want to cancel changes. Just destroys the window and
        updates the parent window's window list.
        """
        self.parent_window.subwindows['database info'] = None
        self.destroy()

    def handle_cancel(self) -> None:
        """Handle the cancel button in the dialog box (or the close-window event).
        """
        if self._is_changed() and not gu.ask_yes_or_no("Discard changes to file data?", "Cancel?", parent=self):
            return
        self.do_close()

    def EVENT_file_deleted(self, which_file: Union[str, Path]) -> None:
        """If we've deleted a file, we need to recalculate displayed stats.
        """
        self.update_displayed_data()

    def EVENT_filename_changed(self, old_name: Union[str, Path],
                               new_name: Union[str, Path]) -> None:
        """No need to worry here, we're not displaying any file names.

        #FIXME: Unless we're moving the database itself!
        """
        pass  # intentionally ignore this event

    def EVENT_file_tags_change(self, which_file: Union[str, Path]) -> None:
        """If a file's been re-tagged, we need to recalculate displayed stats.
        """
        self.update_displayed_data()

    def EVENT_file_change_on_disk(self, which_file: Union[str, Path]) -> None:
        """If a file-s changed on disk, we may need to recalculate displayed data.
        """
        self.update_displayed_data()


@gu.only_if_DB_loaded
@gu.trap_and_report_errors
def edit_database_info(main_window: 'MainWindow', event=None) -> None:
    """Display a "database info" informative box.
    """
    if main_window.subwindows['database info']:
        main_window.subwindows['database info'].lift()
    else:
        main_window.subwindows['database info'] = DatabaseInfoWindow(which_database=main_window.displayed_database,
                                                                     parent_window=main_window)


class BatchProcessingProgressWindow(tk.Toplevel):
    """A window used to show the progress of batch files when running in the GUI.

    When initializing, PARENT *must* be specified, and *must* be a MainWindow. This
    is not checked by the code because coding such an assert would require
    refactoring, but it's important anyway.
    """
    def __init__(self, parent, *pargs, **kwargs):
        tk.Toplevel.__init__(self, parent, *pargs, **kwargs)
        self.actual_cancel_func = None
        self.parent = parent

        # make the window's widgets
        button_pane = ttk.Frame(self)
        button_pane.pack(side=tk.BOTTOM, fill=tk.X)
        self.OK_button = ttk.Button(button_pane, text="OK", command=self.destroy)
        self.OK_button.state(['disabled'])
        self.OK_button.pack(side=tk.RIGHT, expand=tk.NO)
        self.cancel_button = ttk.Button(button_pane, text="Cancel", command=self.handle_cancel)
        self.cancel_button.pack(side=tk.RIGHT, expand=tk.NO)
        self.canceling_label = ttk.Label(self, text='')
        self.canceling_label.config(font=utils.prefs['label-font'])
        self.canceling_label.pack(side=tk.LEFT, expand=tk.NO)

        # self.redirector = gu.ThreadSafeStreamRedirector(scheduler=self.parent.scheduler, parent=self)
        self.redirector = gu.ThreadSafeStreamRedirector(scheduler=self.parent.scheduler)

    def handle_cancel(self) -> None:
        self.parent.set_window_status("Canceling batch processing ...", True, False)
        if self.actual_cancel_func:
            self.actual_cancel_func()
            self.parent.set_window_status("Batch processing canceled!", True, True)
        else:
            print('\n\n\n\nWARNING: Unable to cancel batch processing!')
            print('No cancellation procedure was supplied to the batch processing dialog box!')
            print('\n(This should never happen.)\n\n\n\n')
            self.parent.set_window_status("Unable to cancel batch processing!", True, True)

    def set_cancel_func(self, func: Callable,
                        num_videos_to_process: int) -> None:
        self.actual_cancel_func = func

    def enable_ok_button(self) -> None:
        self.parent.scheduler.queue_task(self.OK_button.state, ['!disabled'])


# Other special-purpose window-creation code.
def get_video_trim_size(parent_window: Optional[Union[Type[tk.Tk], Type[tk.Toplevel], Type[ttk.Frame]]],
                        video_location: Union[Path, str]
                        ) -> Union[None, utils.Rect]:
    """Display a graphical window with a video thumbnail, allowing the user to click
    and crag to choose a cropping rectangle to apply, on the next processing run, to
    the video at VIDEO_LOCATION. PARENT_WINDOW is the parent window for the window
    to be created.

    IF canceled, returns None; otherwise, returns a Rect indicating the crop dimensions
    relative to the video frame.

    # FIXME: try guessing initial cropping rectangle with ffmpeg -cropdetect!
    See https://www.bogotobogo.com/FFMpeg/ffmpeg_cropdetect_ffplay.php
    """
    assert video_location
    ret = None

    class TrimVideoSelectionWindow(GraphicDisplayWindow, gu.BasicWindowMessagingMixIn):
        """Display a thumbnail of a particular video and allow the user to drag to select
        a rectangular area that will be used as the boundaries at which the video file
        will be trimmed.

        #FIXME: Currently we disable zooming in/out on these windows, but it would be
        better to allow it.
        """
        def __init__(self, parent: Union[tk.Tk, tk.Toplevel, ttk.Frame, None],
                     video_loc: Union[Path, str],
                     title: str,
                     *pargs, **kwargs):
            self.which_video = video_loc
            image_location = gu.get_video_thumbnail(video_loc, True)
            GraphicDisplayWindow.__init__(self, parent=parent, image_location=image_location, title=title,
                                          *pargs, **kwargs)

            self.canvas.bind('<ButtonPress-1>', self.start_dragging)
            self.canvas.bind('<B1-Motion>', self.grow_selection)
            self.selection_rect = None              # An ID on self.canvas.
            self.selection_rect_begin = None

        def get_coordinates(self) -> Tuple[int, int, int, int]:
            """Return a (T, L, B, R) tuple of the selection coordinates represented in the
            window.
            """
            return (int(self.coords['top']._getval()), int(self.coords['left']._getval()),
                    int(self.coords['bottom']._getval()), int(self.coords['right']._getval()))

        def handle_ok(self) -> None:
            nonlocal ret
            ret = utils.Rect.from_tlbr(*self.get_coordinates())
            self.destroy()

        def handle_cancel(self) -> None:
            self.destroy()

        def handle_new_thumbnail(self) -> None:
            """Get a new thumbnail for the video being cropped. Display the new thumbnail.
            Delete the file for the previous thumbnail.
            """
            old_thumb = self.image_location
            try:
                tries, new_thumb = 0, None
                while not isinstance(new_thumb, Path):
                    new_thumb = gu.get_video_thumbnail(self.which_video)
                    if not isinstance(new_thumb, Path):
                        tries += 1
                        if tries > 10:
                            print(f"Unable to get a thumbnail for {self.which_video}! The most recent "
                                  f"error is:\n\n{new_thumb}")
                            self.destroy()
                            return
                self.set_image_from_file(new_thumb)
            finally:
                if old_thumb and old_thumb.exists():
                    old_thumb.unlink()
                if self.selection_rect:
                    self.draw_sel_rect_from_coords()

        def check_coords_entry_validity(self, event: tk.Event, which_entry: gu.WrappedEntry):
            if not hasattr(which_entry, 'prev_value'):
                which_entry.prev_value = '0'
            try:
                _ = int(which_entry._getval())
                which_entry.prev_value = which_entry._getval().strip()
            except (ValueError,):
                which_entry._setval(which_entry.prev_value)
                return
            y1, x1, y2, x2 = self.get_coordinates()
            self.set_coords_entries(utils.Rect(utils.Point(x1, y1), utils.Point(x2, y2)))
            self.draw_sel_rect_from_coords()

        def make_early_widgets(self) -> None:
            """Put a button or two on the bottom of the window before other widgets are built.
            """
            bottom_pane = ttk.Frame(self)
            bottom_pane.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.X)
            b = ttk.Button(bottom_pane, text="New thumbnail", command=self.handle_new_thumbnail)
            b.grid(column=0, row=0, sticky=(tk.W + tk.S + tk.E + tk.N))
            b = ttk.Button(bottom_pane, text="Zoom in", command=gu.not_implemented_GUI)
            b.grid(column=1, row=0, sticky=(tk.W + tk.S + tk.E + tk.N))
            b = ttk.Button(bottom_pane, text="Zoom out", command=gu.not_implemented_GUI)
            b.grid(column=2, row=0, sticky=(tk.W + tk.S + tk.E + tk.N))

            coords = {'top': gu.WrappedEntry(bottom_pane, width=8),
                      'left': gu.WrappedEntry(bottom_pane, width=8),
                      'bottom': gu.WrappedEntry(bottom_pane, width=8),
                      'right': gu.WrappedEntry(bottom_pane, width=8)}
            self.coords = coords
            for w in coords.values():
                w.bind('<FocusOut>', lambda e, widg=w: self.check_coords_entry_validity(e, widg))
            ttk.Label(bottom_pane, text="T").grid(column=10, row=0, sticky=(tk.S + tk.E + tk.N))
            coords['top'].grid(column=11, row=0, sticky=(tk.W + tk.S + tk.N))
            ttk.Label(bottom_pane, text="L").grid(column=12, row=0, sticky=(tk.S + tk.E + tk.N))
            coords['left'].grid(column=13, row=0, sticky=(tk.W + tk.S + tk.N))
            ttk.Label(bottom_pane, text="B").grid(column=14, row=0, sticky=(tk.S + tk.E + tk.N))
            coords['bottom'].grid(column=15, row=0, sticky=(tk.W + tk.S + tk.N))
            ttk.Label(bottom_pane, text="R").grid(column=16, row=0, sticky=(tk.S + tk.E + tk.N))
            coords['right'].grid(column=17, row=0, sticky=(tk.W + tk.S + tk.N))

            ttk.Label(bottom_pane, text="color: ").grid(column=18, row=0, sticky=(tk.S + tk.E + tk.N))
            self.current_color = 'black'
            self.current_color_widget = gu.WrappedEntry(bottom_pane, width=12)
            self.current_color_widget.grid(column=19, row=0, sticky=(tk.W + tk.S + tk.N))
            self.current_color_widget._setval(self.current_color)
            self.current_color_widget.bind('<FocusOut>', self.validate_and_change_selection_color)

            ttk.Button(bottom_pane, text="OK", command=self.handle_ok).grid(column=30, row=0,
                                                                            sticky=(tk.W + tk.S + tk.E + tk.N))
            ttk.Button(bottom_pane, text="Cancel", command=self.handle_cancel).grid(column=31, row=0,
                                                                                    sticky=(tk.W + tk.S + tk.E + tk.N))

            for r in (10, 19):
                bottom_pane.columnconfigure(r, weight=1)

        def validate_and_change_selection_color(self, event: tk.Event) -> None:
            """Callback that handles the focus-leaving-the-selection-color-chooser-widget
            event.
            """
            new_color = self.current_color_widget._getval().strip()
            try:
                self.canvas.winfo_rgb(new_color)        # Throw a TclError if new_color isn't a recognized color name
            except (tk.TclError,):
                # Color name unknown? Reset the widget's text to the previously understood color
                self.current_color_widget._setval(self.current_color)
                return
            if new_color == self.current_color:
                return

            self.current_color = new_color
            if self.selection_rect:
                self.canvas.itemconfig(self.selection_rect, outline=new_color)
            else:
                self.set_coords_entries(utils.Rect(utils.Point(0, 0),
                                                   utils.Point(self.canvas.winfo_width(), self.canvas.winfo_height())))
                self.draw_sel_rect_from_coords()

        def start_dragging(self, event: tk.Event) -> None:
            """Handle the beginning of a trim selection.
            """
            self.selection_rect_begin = event
            if self.selection_rect:
                self.canvas.delete(self.selection_rect)
            self.selection_rect = None

        def grow_selection(self, event: tk.Event) -> None:
            """Handle mouse movement while the button is held down by updating the
            selection rectangle and the displayed coordinates.
            """
            if self.selection_rect:
                self.canvas.delete(self.selection_rect)
            self.set_coords_entries(utils.Rect(self.selection_rect_begin, event))
            self.draw_sel_rect_from_coords()

        def draw_sel_rect_from_coords(self) -> None:
            """Draw the selection rectangle based on the contents of the .coords dictionary.
            Erase any existing rectangle. Update the self.selection_rect attribute as
            necessary.
            """
            if self.selection_rect:
                self.canvas.delete(self.selection_rect)
            y1, x1, y2, x2 = self.get_coordinates()
            self.selection_rect = self.canvas.create_rectangle(x1, y1, x2, y2, fill='', width=5,
                                                               outline=self.current_color)

        def set_coords_entries(self, the_coords: utils.Rect):
            """Set the entries in the .coords Entry fields to the coordinates in the
            THE_COORDS Rect.
            """
            self.coords['top']._setval(the_coords.top)
            self.coords['left']._setval(the_coords.left)
            self.coords['bottom']._setval(the_coords.bottom)
            self.coords['right']._setval(the_coords.right)

        def make_top_level_menus(self) -> None:
            """Don't inherit from the superclass. Don't do anything. There is no menu bar here.
            """
            pass

        def zoom(self, delta: float) -> None:
            """Don't inherit from the superclass. Don't do anything. We don't zoom in or out in
            these windows.
            """
            pass

        # and handlers for the BasicWindowMessagingMixIn protocol
        def EVENT_file_deleted(self, which_file: Path) -> None:
            """If the file we're displaying was deleted, destroy the window.
            """
            assert isinstance(which_file, Path)
            if which_file.samefile(self.which_video):
                self.handle_cancel()

        def EVENT_filename_changed(self, old_name: Path,
                                   new_name: Path) -> None:
            """If the name of the video we're displaying has been updated, or the
            current image thumbnail, update our references to it.
            """
            assert isinstance(old_name, Path)
            assert isinstance(new_name, Path)

            if old_name.samefile(self.which_video):
                self.which_video = new_name
                self.handle_new_thumbnail()
            if old_name.samefile(self.image_location):
                self.handle_new_thumbnail()

        def EVENT_file_tags_change(self, which_file: Path) -> None:
            """Nothing to do here: we don't deal with file tags in any way.
            """
            assert isinstance(which_file, Path)     # intentionally ignore this event

        def EVENT_file_change_on_disk(self, which_file: Path) -> None:
            """If the file we're displaying changed on disk, reload it.
            """
            assert isinstance(which_file, Path)     # probably, we need not do anything here.
            # If the user needs to, they can force re-creating the thumbnail with a button.

        def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
            """Nothing to do here: we don't deal with file processing parameters.
            """
            assert isinstance(which_file, Path)     # intentionally ignore this event

    box = TrimVideoSelectionWindow(parent=parent_window, video_loc=video_location,
                                   title=f"Set trim size for {video_location.name}")
    box.lift()
    box.focus_set()
    box.grab_set()
    box.wait_window()
    return ret


@gu.trap_and_report_errors
def schedule_frame_trim(parent_window: Union[Type[tk.Tk], Type[tk.Toplevel], Type[ttk.Frame], None],
                        database: 'MetadataStore',
                        which_files: Iterable[Union[str, Path]]) -> None:
    """Allow the user to specify a geometry to which all frames in the video will be
    trimmed.

    FIXME! Do we want to do a hard crop instead of a soft crop? https://video.stackexchange.com/a/4571
    """
    changed = False
    try:
        for f in which_files:
            new_dim = get_video_trim_size(parent_window, f)
            if new_dim:
                # FIXME! Check whether this is reliably placed before a 'scale' argument!
                database.set_vf_option(f, 'crop', new_dim.as_colon_delim(), before=('scale',), suppress_write=True)
                database.validate_crop_dimensions(f)
                changed = True
    finally:
        if changed:
            database.write_data()


class ProcessingHistoryWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """A window for displaying the processing history of a file in the database.
    """
    class ProcHistoryTreeView(gu.FlexibleTreeview):
        def __init__(self, *args, which_file: Path, **kwargs):
            assert isinstance(which_file, Path)
            assert which_file.is_file()

            gu.FlexibleTreeview.__init__(self, *args, **kwargs)
            self.which_file = which_file

            self['show'] = 'headings'
            columns = ('date', 'parameters')
            self['columns'] = columns
            self['displaycolumns'] = columns
            self.column('date', anchor='w', stretch=True)
            self.column('parameters', anchor='e', stretch=True)
            self.heading('date', text='date', command=lambda: self.sort_by_column('date'))
            self.heading('parameters', text='parameters', command=lambda: self.sort_by_column('parameters'))

        def rebuild_list(self) -> None:
            """Empty the list, then rebuild it from scratch.
            """
            db = self._root().displayed_database
            self.empty_list()
            if "process history" in db[self.which_file]:
                for date, params in db[self.which_file]["process history"].items():
                    self.insert('', 'end', iid=date, text=date, values=(date, shlex.join(params)))

    def __init__(self, *args, which_file: Path, **kwargs):
        assert isinstance(which_file, Path)
        assert which_file.is_file()

        tk.Toplevel.__init__(self, *args, **kwargs)
        self.which_file = which_file

        self.title(f"Processing history for {which_file}")

        buttons_pane = ttk.Frame(self)
        ttk.Button(buttons_pane, text="Done", command=self.destroy).pack(side=tk.RIGHT, expand=tk.NO)
        ttk.Button(buttons_pane, text="Refresh", command=self.rebuild_list).pack(side=tk.LEFT, expand=tk.NO)
        buttons_pane.grid(row=1, column=0, sticky=(tk.W + tk.E + tk.S))

        self.vscrollbar = ttk.Scrollbar(self)
        self.vscrollbar.grid(column=1, row=0, sticky=(tk.W + tk.S + tk.E + tk.N))
        self.history_pane = ProcessingHistoryWindow.ProcHistoryTreeView(self, which_file=which_file, height=8,
                                                                        yscrollcommand=self.vscrollbar.set)
        self.history_pane.grid(row=0, column=0, sticky=(tk.W + tk.E + tk.N + tk.S))
        self.vscrollbar.config(command=self.history_pane.yview)

        self.rebuild_list()

        self.rowconfigure(0, weight=1)
        self.columnconfigure(0, weight=1)

    def rebuild_list(self) -> None:
        """Convenience function that delegates to the embedded pane's method.
        """
        self.history_pane.rebuild_list()

    def close_window(self) -> None:
        """Handles the window-destroying moment.
        """
        self.destroy()

    def EVENT_filename_changed(self, old_name: Union[str, Path], new_name: Union[str, Path]) -> None:
        """If the file being renamed is the file whose processing history we're displaying,
        update window title and stored reference to the file.
        """
        if not isinstance(old_name, Path):
            old_name = Path(old_name)
        if not old_name.samefile(self.which_file):
            return

        if not isinstance(new_name, Path):
            new_name = Path(new_name)
        self.title(f"Processing history for {new_name}")
        self.which_file = new_name

    def EVENT_file_deleted(self, which_file: Path) -> None:
        """If the file whose deleition is being reported is the file whose processing
        history we're displaying, just close the window.
        """
        if not isinstance(which_file, Path):
            which_file = Path(which_file)
        if which_file.samefile(self.which_file):
            self.close_window()

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """If the file whose change is being reported is the file whose info we're
        displaying, rebuild the list, in case it reflects things that have changed
        """
        if not isinstance(which_file, Path):
            which_file = Path(which_file)
        if which_file.samefile(self.which_file):
            self.rebuild_list()

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """Intentionally ignore this event.
        """
        pass

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        """Intentionally ignore this event.
        """
        pass


class FileInfoWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """Window allowing for the display and editing of the info for a single file.
    """
    def __init__(self, which_file: Path,
                 which_database: mh.MetadataStore,
                 parent_window: 'MainWindow', *pargs, **kwargs):
        assert isinstance(which_file, Path)
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.which_file = which_file
        self.which_database = which_database
        self.parent_window = parent_window
        self.initial_tags = set(which_database.get_tags(which_file))
        if 'next_processing' in which_database[which_file]:
            self.initial_processing = which_database[which_file]['next_processing']
        else:
            self.initial_processing = [][:]
        self.protocol("WM_DELETE_WINDOW", self.handle_cancel)

        # now create widgets inside the window.
        content = ttk.Frame(self, padding=5)

        ttk.Label(content, text="Full path: ").grid(row=0, column=0, sticky=tk.E)
        self.full_path_label = gu.WrappedLabel(content)
        self.full_path_label.grid(row=0, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Originally cached: ").grid(row=1, column=0, sticky=tk.E)
        self.originally_cached_label = gu.WrappedLabel(content)
        self.originally_cached_label.grid(row=1, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Cached info updated: ").grid(row=2, column=0, sticky=tk.E)
        self.cached_info_updated_label = gu.WrappedLabel(content)
        self.cached_info_updated_label.grid(row=2, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Last file modification: ").grid(row=3, column=0, sticky=tk.E)
        self.last_modification_label = gu.WrappedLabel(content)
        self.last_modification_label.grid(row=3, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="File size: ").grid(row=4, column=0, sticky=tk.E)
        self.file_size_label = gu.WrappedLabel(content)
        self.file_size_label.grid(row=4, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Media playtime: ").grid(row=10, column=0, sticky=tk.E)
        self.media_playtime_label = gu.WrappedLabel(content)
        self.media_playtime_label.grid(row=10, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Media bitrate: ").grid(row=11, column=0, sticky=tk.E)
        self.media_bitrate_label = gu.WrappedLabel(content)
        self.media_bitrate_label.grid(row=11, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Total number of streams: ").grid(row=12, column=0, sticky=tk.E)
        self.total_num_streams = gu.WrappedLabel(content)
        self.total_num_streams.grid(row=12, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Number of audio streams: ").grid(row=13, column=0, sticky=tk.E)
        self.num_audio_streams = gu.WrappedLabel(content)
        self.num_audio_streams.grid(row=13, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Audio sample rate: ").grid(row=14, column=0, sticky=tk.E)
        self.audio_sample_rate_label = gu.WrappedLabel(content)
        self.audio_sample_rate_label.grid(row=14, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Audio channels: ").grid(row=15, column=0, sticky=tk.E)
        self.audio_channels_label = gu.WrappedLabel(content)
        self.audio_channels_label.grid(row=15, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Number of video streams: ").grid(row=16, column=0, sticky=tk.E)
        self.num_video_streams_label = gu.WrappedLabel(content)
        self.num_video_streams_label.grid(row=16, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Video dimensions: ").grid(row=17, column=0, sticky=tk.E)
        self.vid_dimensions_label = gu.WrappedLabel(content)
        self.vid_dimensions_label.grid(row=17, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Recompressed: ").grid(row=20, column=0, sticky=tk.E)
        self.recompressed_label = gu.WrappedLabel(content)
        self.recompressed_label.grid(row=20, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Incrementally tagged: ").grid(row=21, column=0, sticky=tk.E)
        self.incremental_tag_label = gu.WrappedLabel(content)
        self.incremental_tag_label.grid(row=21, column=1, sticky=tk.W, columnspan=3)

        ttk.Label(content, text="Tags: ").grid(row=30, column=0, sticky=(tk.E + tk.N))
        self.tags_pane = gu.FileTaggerPane(content, suppress_label=True)
        self.tags_pane.grid(row=30, column=1, columnspan=3, sticky=(tk.E + tk.W))

        self.which_database.ensure_next_processing(self.which_file)
        ttk.Label(content, text="Parameters:").grid(row=40, column=0, sticky=(tk.E + tk.N))
        self.params_pane = gu.ProcessingParameterPane(content, suppress_label=True)
        self.params_pane.grid(row=40, column=1, columnspan=3, sticky=(tk.E + tk.W))

        buttons_pane = ttk.Frame(content)
        ttk.Button(buttons_pane, text="OK", command=self.handle_ok).pack(side=tk.RIGHT, expand=tk.NO)
        ttk.Button(buttons_pane, text="Cancel", command=self.handle_cancel).pack(side=tk.RIGHT, expand=tk.NO)
        ttk.Button(buttons_pane, text="Processing History", command=self.proc_hist).pack(side=tk.LEFT, expand=tk.NO)
        ttk.Button(buttons_pane, text="FFProbe Info", command=self.ffprobe_info).pack(side=tk.LEFT, expand=tk.NO)
        buttons_pane.grid(row=100, column=0, columnspan=4, sticky=(tk.E + tk.W + tk.S))

        content.columnconfigure(0, weight=1)
        content.grid(row=0, column=0, sticky=(tk.W + tk.N + tk.E + tk.S))
        self.columnconfigure(1, weight=1)

        self.update_displayed_info()

    def update_displayed_info(self) -> None:
        file_info = self.which_database[self.which_file]

        self.full_path_label._setval(self.which_file.resolve().parent)
        self.originally_cached_label._setval(utils.time_str_from_timestamp(file_info['file_metadata']['cache_time']))
        self.cached_info_updated_label._setval(utils.time_str_from_timestamp(file_info['original_mdate']))
        self.last_modification_label._setval(utils.time_str_from_timestamp(file_info['file_metadata']['st_mtime']))
        self.file_size_label._setval(f"{file_info['file_metadata']['st_size']} bytes")
        self.media_playtime_label._setval(file_info['movie_metadata']['length'])

        self.media_bitrate_label._setval(file_info['movie_metadata']['bitrate'])
        self.total_num_streams._setval(len(file_info['movie_metadata']['all_streams']))
        self.num_audio_streams._setval(file_info['movie_metadata']['audio stream count'])
        self.audio_sample_rate_label._setval(file_info['movie_metadata']['audio sample rate'])
        self.audio_channels_label._setval(file_info['movie_metadata']['audio channels'])
        self.num_video_streams_label._setval(file_info['movie_metadata']['video stream count'])
        self.vid_dimensions_label._setval(f"{file_info['movie_metadata']['frame width']} x"
                                          f" {file_info['movie_metadata']['frame height']}")

        self.recompressed_label._setval(file_info['recompressed'] if 'recompressed' in file_info else False)
        inc_tagged_str = str(file_info['incrementally_tagged'] if 'incrementally_tagged' in file_info else False)
        if ('incremental_temp_skip' in file_info) and (file_info['incremental_temp_skip']):
            inc_tagged_str += " (skipped during this tagging pass)"
        self.incremental_tag_label._setval(inc_tagged_str)

        self.tags_pane.set_tags(self.which_database.get_tags(self.which_file))
        self.params_pane.set_params(self.which_database[self.which_file]['next_processing'])

        self.title(f"Details: {self.which_file.name}")

    def close_window(self) -> None:
        """Handles the window-destroying moment. Does not extract and update data. Does not
        ask the user if they want to cancel changes. Just destroys the window and
        updates the parent window's window list.
        """
        if self.which_file in self.parent_window.subwindows:
            del self.parent_window.subwindows[self.which_file]
        else:
            print(f"WARNING! {self.which_file} was never registered in window dictionary on main application window!")
        self.destroy()

    @gu.trap_and_report_errors
    def handle_ok(self) -> None:
        """Handle the OK button in the dialog box.
        """
        # make sure both are extractable before assigning the value of either to the relevant database entry.
        tags = self.tags_pane.extract_tags()
        params = self.params_pane.extract_params()

        self.which_database.set_tags(self.which_file, tags)
        self.which_database[self.which_file]['next_processing'] = params

        if tags != self.initial_tags:
            self.dispatch(message='EVENT_file_tags_change', which_file=self.which_file,
                          exception_windows=(self,))
        if params != self.initial_processing:
            self.dispatch(message='EVENT_file_processing_parameters_change', which_file=self.which_file,
                          exception_windows=(self,))
        self.close_window()

    def _needs_confirm_to_close(self) -> bool:
        """Check to see whether we need to bother the user asking if changes should be
        abandoned. If there have been no meaningful changes, return False; if there have
        been any meaningful changes, return True.
        """
        if self.initial_tags != set(self.tags_pane.extract_tags()):
            return True
        if self.initial_processing != self.params_pane.extract_params():
            return True
        return False

    def handle_cancel(self) -> None:
        """Handle the cancel button in the dialog box (or the close-window event).
        """
        if self._needs_confirm_to_close():
            if not not gu.confirm_prompt("Discard changes to file data?", "Discard changes?", self):
                return
        self.close_window()

    def proc_hist(self) -> None:
        """Display a 'processing history' window for a given file.
        """
        ProcessingHistoryWindow(which_file=self.which_file)

    def ffprobe_info(self) -> None:
        """Open a text-output window and use it to display the results of FFprobe for
        the indicated file.
        """
        def do_print_ffprobe() -> None:
            print(utils.run_process([pref('ffprobe_loc'), str(self.which_file)], print_output=False,
                                    stderr=subprocess.PIPE, stdout=subprocess.PIPE).stderr)

        file_loc = utils.relative_to_with_name(self._root().displayed_database.file_location, self.which_file)
        gu.redirect_prints_to_text_box(do_print_ffprobe, window_title=f"FFprobe output for {file_loc}")

    # And now, app event protocol handlers for the BasicWindowMessagingMixIn protocol.
    def EVENT_file_deleted(self, which_file: Path) -> None:
        """If the file whose window we're looking at disappears, close the info window.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)
        if which_file == self.which_file:
            self.close_window()

    def EVENT_filename_changed(self, old_name: Union[str, Path],
                               new_name: Path) -> None:
        """If the name for the file we're currently displaying has changed, then update
        the displayed info. This includes updating the window title and its index in the
        main window's subwindow list.
        """
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)

        old_name = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, old_name)
        new_name = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, new_name)

        if old_name == self.which_file:
            self.which_file = new_name
            self.update_displayed_info()
            self.parent_window.subwindows[new_name] = self.parent_window.subwindows[old_name]
            del self.parent_window.subwindows[old_name]

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """If the file's tags have changed, we need to update the tags displayed.

        This currently overwrites any changes made by the user in this window.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)
        if which_file == self.which_file:
            self.update_displayed_info()

    def EVENT_file_change_on_disk(self, which_file: Union[str, Path]) -> None:
        """If the file has changed on disk, we need to update the info we're displaying.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)
        if which_file == self.which_file:
            self.update_displayed_info()

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        """If the file has changed on disk, we need to update the info we're displaying.
        """
        self.update_displayed_info()


class MultiFileTaggingWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """Window that presents a UI for incrementally tagging files, one by one, handling
    user actions and putting changes into the underlying database.
    """
    def __init__(self, files_to_tag: Iterable[Union[str, Path]],
                 db: mh.MetadataStore,
                 num_already_tagged_files: int,
                 parent_window: 'MainWindow',
                 preferred_files: Iterable[Union[Path, str]] = (),
                 *pargs, **kwargs):
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.protocol("WM_DELETE_WINDOW", self.done_tagging)
        self.title(f"Tagging {db.file_location.name}")

        preferred_files = set(f if isinstance(f, str) else str(f) for f in preferred_files)
        f_to_skip = [f for f in files_to_tag if db[f].get('incremental_temp_skip')]
        self.files_to_skip = [f if (isinstance(f, str)) else str(f) for f in f_to_skip]
        self.files_to_skip.sort(key=lambda i: i in preferred_files, reverse=True)
        # testing for inclusion is a set is much faster than in a list, so build a set to test
        to_skip = set(self.files_to_skip)

        # All right, put together the list of files to tag. Put any "preferred files" in first, then others.
        self.files_to_tag = sorted([f if isinstance(f, str) else str(f) for f in files_to_tag if f not in to_skip],
                                   key=lambda i: i in preferred_files, reverse=True)

        # Now replace both lists with Path() equivalents
        self.files_to_skip = [Path(p) for p in self.files_to_skip]
        self.files_to_tag = [Path(p) for p in self.files_to_tag]

        self.database_to_modify = db
        self.current_file = None
        self.num_already_tagged_files = num_already_tagged_files
        self.parent_window = parent_window

        # now, create the dialog's widgets.
        self.make_window_menus()
        content = ttk.Frame(self, padding=5)

        top_pane = ttk.Frame(content)
        self.completion_label = ttk.Label(top_pane)
        self.completion_label.grid(row=0, column=0, sticky=tk.W)
        self.current_file_name_label = ttk.Label(top_pane)
        self.current_file_name_label.grid(row=1, column=0, sticky=tk.W)
        self.columnconfigure(0, weight=1)
        top_pane.pack(side=tk.TOP, expand=True, fill=tk.BOTH)

        self.tags_pane = gu.FileTaggerPane(content)
        self.tags_pane.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)
        bottom_pane = ttk.Frame(content)
        self.stop_button = ttk.Button(bottom_pane, text="Done tagging", command=self.done_tagging)
        self.stop_button.pack(side=tk.LEFT)
        self.skip_this_file_button = ttk.Button(bottom_pane, text="Skip this file", command=self.skip_this_file)
        self.skip_this_file_button.pack(side=tk.LEFT)
        self.next_file_button = ttk.Button(bottom_pane, text="Next file", command=self.display_next_file)
        self.next_file_button.pack(side=tk.LEFT)
        self.view_button = ttk.Button(bottom_pane, text="View file", command=self.view_current_file_button_handler)
        self.view_button.pack(side=tk.RIGHT)
        bottom_pane.pack(side=tk.BOTTOM, expand=True, fill=tk.BOTH)

        content.columnconfigure(0, weight=1)
        content.grid(row=0, column=0, sticky=(tk.W + tk.N + tk.E + tk.S))

        self.display_next_file()

    def make_window_menus(self) -> None:
        """Add utility menus to the top of the dialog, so the user can do various
        things with the file while in the middle of the tagging process.
        """
        base_menu = tk.Menu(self)
        self.config(menu=base_menu)

        file_menu = tk.Menu(base_menu, tearoff=pref('allow-tearoffs'))
        file_menu.add_command(label="Rename file ...", command=self.do_rename)
        file_menu.add_command(label="Delete file", command=self.do_delete)
        base_menu.add_cascade(label="File", menu=file_menu)

        schedule_menu = tk.Menu(base_menu, tearoff=pref('allow-tearoffs'))
        schedule_menu.add_command(label="Clear all processing plans for this file", command=self.clear_all_processing)
        schedule_menu.add_separator()
        schedule_menu.add_command(label="Schedule audio removal ...", command=self.schedule_audio_removal)
        schedule_menu.add_command(label="Schedule audio downsampling ...", command=self.schedule_audio_downsampling)
        schedule_menu.add_command(label="Schedule video resize ...", command=lambda: self.schedule_video_resize)
        schedule_menu.add_separator()

        menu_resize = tk.Menu(schedule_menu, tearoff=pref('allow-tearoffs'))
        schedule_menu.add_cascade(menu=menu_resize, label="Rotate and flip")

        menu_resize.add_command(label=" 90° clockwise", command=lambda: self.schedule_video_rotation(how_much=90))
        menu_resize.add_command(label="180° clockwise", command=lambda: self.schedule_video_rotation(how_much=180))
        menu_resize.add_command(label="270° clockwise", command=lambda: self.schedule_video_rotation(how_much=270))
        menu_resize.add_separator()
        menu_resize.add_command(label="Flip horizontally", command=lambda: self.schedule_video_flip(horizontal=True,
                                                                                                    vertical=False))
        menu_resize.add_command(label="Flip vertically", command=lambda: self.schedule_video_flip(horizontal=False,
                                                                                                  vertical=True))

        schedule_menu.add_separator()
        volume_menu = tk.Menu(schedule_menu, tearoff=pref('allow-tearoffs'))
        schedule_menu.add_cascade(menu=volume_menu, label="Adjust volume")
        volume_menu.add_command(label="Adjust volume ...", command=self.do_adjust_volume)
        volume_menu.add_command(label="Normalize volume", command=self.do_normalize_volume)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Add raw FFmpeg parameters ...", command=self.schedule_raw_ffmpeg_params)

        schedule_menu.add_command(label="Change aspect ratio ...", command=self.schedule_aspect_ratio_change)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Cut beginning of video ...", command=self.schedule_beginning_cut)
        schedule_menu.add_command(label="Cut end of video ...", command=self.schedule_end_cut)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Trim video frame size ...", command=self.schedule_frame_trim)

        schedule_menu.add_separator()
        schedule_menu.add_command(label="Re-containerize ...", command=self.schedule_recontainerize)
        base_menu.add_cascade(label="Schedule processing", menu=schedule_menu)

    @gu.trap_and_report_errors
    def clear_all_processing(self) -> None:
        """Clear out any processing plans for the currently displayed file.
        """
        self.database_to_modify.clear_next_processing(self.current_file)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              which_file=self.current_file, exception_windows=(self,))
        self._root().set_window_status(f"Cleared all processing plans for {self.current_file}.", True, True)

    @gu.trap_and_report_errors
    def schedule_audio_removal(self) -> None:
        """Friendly wrapper for database functionality.
        """
        self.database_to_modify.schedule_audio_removal(self.current_file, suppress_write=True)
        self._root().set_window_status(f"Scheduled audio removal for {self.current_file}.", True, True)

    @gu.trap_and_report_errors
    def schedule_audio_downsampling(self) -> None:
        """Wrapper for friendly interface to audio-downsampling code.
        """
        gd.schedule_audio_downsampling(self.database_to_modify, self.current_file, suppress_write=False)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              self.current_file, exception_windows=(self,))
        self._root().set_window_status(f"Scheduled audio downsampling for {self.current_file}.", True, True)

    def schedule_video_resize(self) -> None:
        """Wrapper for friendly interface to video-resizing code
        """
        gd.schedule_video_resize(self.database_to_modify, self.current_file)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              self.current_file, exception_windows=(self,))
        self._root().set_window_status(f"Scheduled video resizing for {self.current_file}.", True, True)

    @gu.trap_and_report_errors
    def schedule_video_rotation(self, how_much: int) -> None:
        """Wrapper for friendly interface to video-rotation code above.
        """
        self.database_to_modify.schedule_video_rotation(self.current_file, how_much=how_much, suppress_write=False)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              self.current_file, exception_windows=(self,))
        self._root().set_window_status(f"Scheduled {self.current_file} to be rotated.", True, True)

    @gu.trap_and_report_errors
    def schedule_video_flip(self, horizontal: bool = False,
                            vertical: bool = False) -> None:
        """Wrapper for friendly interface to video-flipping code above.
        """
        self.database_to_modify.schedule_video_flip(self.current_file, horizontal=horizontal, vertical=vertical,
                                                    suppress_write=False)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Scheduled {self.current_file} to be flipped.", True, True)

    @gu.trap_and_report_errors
    def do_adjust_volume(self) -> None:
        """Get parameters and schedule a volume change for the next processing run.
        """
        do_volume_change(media_loc=Path(self.current_file), which_db=self.database_to_modify, parent=self)
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              which_file=Path(self.current_file))

    @gu.trap_and_report_errors
    def do_normalize_volume(self) -> None:
        """Schedule an audio normalization for this file the next time a procesing run
        occurs.
        """
        opts = gd.get_loudness_normalization_params(1)
        if not opts:
            self._root().set_window_status_to_canceled(True, True)
            return

        which_db = self._root().displayed_database
        self.database_to_modify.schedule_audio_normalization(which_file=self.current_file, dual_mono=opts['Dual mono'],
                                                             target=opts['Loudness target'],
                                                             range=opts['Loudness range'], true_peak=opts['True peak'])
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                              which_file=self.current_file)
        which_db.write_data()
        self._root().set_window_status(f"Scheduled audio normalization for {self.current_file.name}", True, True)

    @gu.trap_and_report_errors
    def schedule_raw_ffmpeg_params(self) -> None:
        """Wrapper for friendly interface to "add raw FFmpeg parameters" dialog, above.
        """
        gd.generic_change_ffmpeg_parameters(self.database_to_modify, self.current_file)
        self.database_to_modify.write_data()
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Added processing parameters for {self.current_file} for next processing run.",
                                       True, True)

    @gu.trap_and_report_errors
    def schedule_aspect_ratio_change(self) -> None:
        """Convenience wrapper for top-level function near beginning of this module.
        """
        gd.schedule_aspect_ratio_change(self.database_to_modify, [self.current_file])
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Scheduled aspect ratio change for {self.current_file}.", True, True)

    @gu.trap_and_report_errors
    def schedule_beginning_cut(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        gd.schedule_beginning_cut(self.database_to_modify, [self.current_file])
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Scheduled video trim for {self.current_file}.", True, True)

    def schedule_end_cut(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        gd.schedule_end_cut(self.database_to_modify, [self.current_file])
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Scheduled video trim for {self.current_file}.", True, True)

    @gu.trap_and_report_errors
    def schedule_frame_trim(self) -> None:
        """A convenience wrapper for the friendly version in gui_utils.
        """
        schedule_frame_trim(self, self.database_to_modify, [self.current_file])
        gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change', self.current_file,
                                              exception_windows=(self,))
        self._root().set_window_status(f"Scheduled video frame trim for {self.current_file}.", True, True)


    @gu.trap_and_report_errors
    def schedule_recontainerize(self) -> None:
        """Schedule copying video/audio data into a new container. Sometimes helpful
        if there's something wrong with the container, e.g. if incorrect video
        timestamps are shown in media players.
        """
        def do_recontainerize(db: mh.MetadataStore,
                              which_file: Path,
                              even_if_size_increase: bool=False) -> None:
            """Just wraps the steps required to schedule re-containerization into a
            sub-function so it can be passed to redirect_prints_to_status_bar.
            """
            db.ensure_next_processing(which_file)
            db.validate_codec_specs(which_file)
            db.set_argument_parameter(which_file, '-acodec', 'copy', only_if_absent=True)
            db.set_argument_parameter(which_file, '-vcodec', 'copy', only_if_absent=True)

            # On this next setting: if the answer is "No," we don't want to turn it off for this file if another
            # processing step has already turned it on.
            allow_increase = even_if_size_increase or db[which_file]['next_processing_params']['allow_size_increase']
            db[which_file]['next_processing_params']['allow_size_increase'] = allow_increase

        data = gd.get_data_from_user({'Re-containerize even if size increases':
                                          {'kind': bool, 'initial': True, 'validator': None,
                                           'tooltip':
                                               'Normally, if VideoDataStore discovers that putting the data into a new '
                                               'container increases the overall file size, it instead keeps the old '
                                               'file in its old container. If this box is checked, it will instead '
                                               'keep the new file even if that file is larger on disk.'}})
        if data:
            gu.redirect_prints_to_status_bar(self._root().status_bar, True, func=do_recontainerize,
                                             db=self.database_to_modify, which_file = self.current_file,
                                             even_if_size_increase=data['Re-containerize even if size increases'],)
            gu.BasicWindowMessagingMixIn.dispatch('EVENT_file_processing_parameters_change',
                                                  self.current_file)
            self._root().set_window_status(f"Scheduled re-containerizing for {self.current_file}.", True, True)
        else:
            self._root().set_window_status_to_canceled(True)

    @gu.trap_and_report_errors
    def move_files_to_front(self, which_files: Iterable[Path]) -> None:
        """For each file in WHICH_FILES, move it to the front of the self.files_to_tag
        list, regardless of whether it is or is not (a) already incrementally tagged, or
        (b) in the already-incrementally-skipped list. If the file is already in self's
        files_to_tag or files_to_skip list, it is removed from those lists before being
        added to the front of the files_to_tag list.

        Any files that do not exist are silently skipped. Similarly, if any of
        WHICH_FILES are the file currently being tagged, that file is not added back
        into the files_to_tag list: that would be silly, since self.current_file is,
        from the user's standpoint "the first item in the list of files to tag." (Also,
        self.files_to_tag[0] is in fact the file currently being tagged, and other code
        expects that to be the case.)

        Note that this does NOT make the first (existing) file in WHICH_FILES into the
        file currently being tagged, largely because having to deal with whether the
        user wants partially tagged files to be interrupted is a pain.
        """
        assert isinstance(which_files, Iterable)
        for f in which_files:
            assert isinstance(f, Path), (f"ERROR! {self.__class__.__name__}.move_files_to_front was passed {f}, but "
                                         f"that is of type {f.__class__.__name__}, not a Path!")

        valid_files = [f for f in which_files if f.exists() and f.is_file() and not f.samefile(self.current_file)]
        valid_files_absolute = {f.resolve() for f in valid_files}

        if not valid_files:
            self._root().set_window_status("No existing files to add to front of queue!", immediate=True)
            return

        self.files_to_skip = [i for i in self.files_to_skip if not i.resolve() in valid_files_absolute]

        # If we currently have a list of files to tag, the first item (idx 0) is the file currently being tagged, and
        # we want to leave it in place, adding VALID_FILES after it, then any other files that were already in the
        # list of files to be tagged. If the list of files waiting to be tagged doesn't have anything other than
        # the current file, then just add VALID_FILES after that. (Building it from scratch helps if the list has
        # somehow become degenerate by eliminating self.current_file from its position at index zero.)
        if len(self.files_to_tag) > 2:
            self.files_to_tag = [self.files_to_tag[0]] + valid_files + \
                                [i for i in self.files_to_tag[1:] if not i.resolve() in valid_files_absolute]
        else:
            self.files_to_tag = [self.current_file] + valid_files

        self.update_status_labels()
        self._root().set_window_status(f"Added {len(valid_files)} file{'s' if (len(valid_files) > 1) else ''} to "
                                       f"front of queue! Not displacing file currently being tagged.", immediate=True)

    @gu.trap_and_report_errors
    def do_rename(self) -> None:
        """Rename the file we're currently tagging.
        """
        # CHECKED! This code path properly results in a call to change_item_iid
        old_name = self.current_file
        new_name = gu.rename_video(self.current_file, self.database_to_modify, parent_window=self)
        if new_name:
            try:
                if len(self.files_to_tag) > 0:
                    new_name = utils.relative_to_with_name(self._root().displayed_database.file_location, new_name)
                    if self.files_to_tag[0] == self.current_file:
                        self.files_to_tag[0] = new_name
                else:
                    self.files_to_tag = [new_name]
            except (TypeError,):
                pass
            self.current_file = new_name
            self.update_status_labels()
            self._root().set_window_status(f"Renamed {old_name} to {new_name}.", True, True)
        else:
            self.parent_window.set_window_status_to_canceled(True, True)

    @gu.trap_and_report_errors
    def do_delete(self, suppress_write: bool = False) -> None:
        """Delete the file we're currently tagging, then move on to the next file.
        """
        gu.delete_video(self.database_to_modify, self.current_file, parent_window=self, suppress_write=suppress_write)

        # Now move on to the next video that needs to be tagged: no point tagging a video that doesn't exist any more.
        self.skip_this_file()

    def update_status_labels(self) -> None:
        """Update the counts of tagged/untagged files in the dialog box, and also the
        name of the file currently being edited.
        """
        if self.current_file:
            self.current_file_name_label['text'] = f"Currently tagging: {self.current_file}"
        self.completion_label['text'] = (f"Tagged / Remaining / Skipped: {self.num_already_tagged_files} / "
                                         f"{len(self.files_to_tag)} / {len(self.files_to_skip)}")

    def _populate_tags(self, tags: List[str]) -> None:
        """Put the tags from TAGS into SELF's tags pane's editable tags field.
        """
        self.tags_pane.set_tags(tags)

    def _extract_tags(self) -> List[str]:
        """Just delegate to the tags pane's method
        """
        return self.tags_pane.extract_tags()

    def wrap_up(self) -> None:
        """Do any finalization that needs to happen, then destroy the window.
        """
        # self.parent_window.rebuild_list()           # Check: can we finally not need to call this?
        self.parent_window.subwindows['incremental tag'] = None
        self.destroy()

    def _approve_current_tags(self) -> None:
        """Apply the currently entered tags to the file currently being tagged.

        If there are no tags, instead pretend that we'd hit the "Skip this file" button.
        If the tags haven't changed relative to the tags currently in the database, i.e.
        haven't been edited while the dialog box was open, also silently behave as if
        the user had hit the "Skip this file" button.

        If there is no current reference to a file that's being tagged, or that file no
        longer exists, just silently do nothing.
        """
        if self.current_file and self.current_file.exists():    # deleted the current file? just move along.
            if (self._extract_tags()) and (self.database_to_modify.get_tags(self.current_file) != self._extract_tags()):
                # Only treat this file as tagged if we've actually tagged it.
                self.database_to_modify.set_tags(self.current_file, self._extract_tags())
                self._root().video_list.scroller.rebuild_single_list_item(self.current_file)
                if self.current_file in self.files_to_tag:
                    self.files_to_tag.remove(self.current_file)

                cur_t = datetime.datetime.now().isoformat(' ')
                self.database_to_modify[self.current_file]['incrementally_tagged'] = cur_t
                self.num_already_tagged_files += 1
                if 'incremental_temp_skip' in self.database_to_modify[self.current_file]:
                    del self.database_to_modify[self.current_file]['incremental_temp_skip']
                self.database_to_modify.write_data()
            else:                           # Otherwise, if tags field is empty, act as if we're skipping this file.
                self.skip_this_file()

    @gu.trap_and_report_errors
    def display_next_file(self) -> None:
        """Set up the dialog box for the next file to be edited. This involves populating
        self.current_file with the next file to be tagged, updating the stuff handled by
        update_status_labels(), and populating the tagging field with any existing tags
        for the new file. If there's already a current file, then we first need to apply
        the tags in the tagging field to that file and mark that file as 'incrementally
        tagged'.
        """
        try:
            self._approve_current_tags()
        except ValueError as errrr:
            gu.error_message_box("Cannot move on to next file: Unbalanced quotation marks!", errrr)
            return

        if self.files_to_skip and not self.files_to_tag:
            for f in self.files_to_skip:
                if 'incremental_temp_skip' in self.database_to_modify[f]:
                    del self.database_to_modify[f]['incremental_temp_skip']
            self.files_to_tag.extend(self.files_to_skip)
            self.files_to_skip = list()
            self._root().set_window_status(f"Moved {len(self.files_to_tag)} files back to the to-tag list.",
                                           True, True)

        if self.files_to_tag:
            while (len(self.files_to_tag) > 0) and (not self.files_to_tag[0].exists()):
                # Underlying db may have changed; make sure the file we're switching to still exists. Keep trying
                # until it does or until we run out of files to try.
                self.files_to_tag.pop(0)
            self.current_file = self.files_to_tag[0]
            self._populate_tags(self.database_to_modify.get_tags(self.current_file))
            self.update_status_labels()
            if self.parent_window.video_list.scroller.the_list.exists(self.current_file):
                self.parent_window.scheduler.queue_task(self.parent_window.video_list.scroller.the_list.selection_set,
                                                        self.current_file)
                self.parent_window.scheduler.queue_task(self.parent_window.video_list.scroller.the_list.see,
                                                        self.current_file)
            if pref('auto-play-when-tagging'):
                self.view_current_file_button_handler()
        else:
            self.wrap_up()

    def skip_this_file(self) -> None:
        """Do not modify the tags for this file. Ignore any data entry that has occurred.
        Put it at the end of the list of files to be tagged, mark it as skipped during
        incremental tagging so it doesn't appear in the list of files to tag next time
        (at least until we've skipped everything untagged), and move on to the next
        file, setting up the dialog box for that file.
        """
        if self.current_file and (self.current_file in self.database_to_modify):
            # Theoretically, the file could have disappeared (been deleted, etc.) If so, nothing to do but move on.
            if self._extract_tags() != self.database_to_modify.get_tags(self.current_file):
                if not gu.ask_yes_or_no("Really abandon existing tags?", title="Really?", parent=self):
                    return
            skipped_file = self.current_file
            if skipped_file in self.files_to_tag:
                self.files_to_tag.remove(skipped_file)
            self.files_to_skip.append(skipped_file)

            # already-incrementally-tagged files can wind up in the to-tag queue if the user explicitly puts them
            # there from the main window during tagging. If we skip them again, don't bother to mark them as
            # incrementally skipped; they won't be automatically put back in the queue during the next incremental
            # tagging session, anyway.
            if not (('incrementally_tagged' in self.database_to_modify[skipped_file]) and
                    (self.database_to_modify[skipped_file]['incrementally_tagged'])):
                self.database_to_modify[skipped_file]['incremental_temp_skip'] = datetime.datetime.now().isoformat(' ')

        self.current_file = None
        self.display_next_file()

    @gu.trap_and_report_errors
    def done_tagging(self) -> None:
        """Confirm that the user is done tagging, then call the final wrap-up routines.
        """
        if (self._extract_tags()) and (self._extract_tags() != self.database_to_modify.get_tags(self.current_file)):
            if not (gu.ask_yes_or_no(f"Save changes to tags for {Path(self.current_file).name} and stop tagging?",
                                     title="Done?", parent=self)):
                return
            self._approve_current_tags()
        elif not gu.ask_yes_or_no("Stop tagging?", title="Finished?", parent=self):
            return
        self.wrap_up()

    def view_current_file_button_handler(self) -> None:
        """Handle the 'view' button.
        """
        gu.view_media([self.current_file])
        if self.parent_window.video_list.scroller.the_list.exists(self.current_file):
            self.parent_window.video_list.scroller.the_list.selection_set(self.current_file)
            self.parent_window.video_list.scroller.the_list.see(self.current_file)

    # Now, the app event handlers that BasicWindowMessagingMixIn requires.
    def EVENT_file_deleted(self, which_file: Path) -> None:
        """Handle the 'file deleted' event by checking to see if it's on any of our lists
        and, if so, removing it. Also checks to see if it's the file we're currently
        tagging, and, if so, moves on to the next file waiting to be tagged.
        """
        assert isinstance(which_file, Path)

        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)

        if which_file in self.files_to_skip:
            self.files_to_skip.remove(which_file)
        if which_file in self.files_to_tag:
            self.files_to_tag.remove(which_file)
        if self.current_file == which_file:
            self.display_next_file()
        self.update_status_labels()

    def EVENT_filename_changed(self, old_name: Path,
                               new_name: Path) -> None:
        """Handle the 'file renaming' event bY checking to see if (a) the file renamed
        is in our to-tag to to-skip lists, and (b) checking to see if it's the file
        we're currently tagging.
        """
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)
        old_name = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, old_name)
        new_name = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, new_name)

        if old_name in self.files_to_skip:
            self.files_to_skip[self.files_to_skip.index(old_name)] = new_name
        if old_name in self.files_to_tag:
            self.files_to_tag[self.files_to_tag.index(old_name)] = new_name
        # FIXME! old_name has been modified to be relative to database, and now self.current_file cannot possibly
        # match the modified old_name!
        if self.current_file == old_name:
            self.current_file = new_name
        self.update_status_labels()

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)

        # FIXME! We should be ADDING NEW TAGS TO THE FIELD, not overwriting tags currently in the field!
        if self.current_file and which_file.samefile(self.current_file):
            self._populate_tags(self.database_to_modify.get_tags(which_file))

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """Handle the "file has changed on disk" notification to see whether the file in
        question is the file we're currently examining. If so, and if the prefs say we
        autoplay during tagging, then we restart the movie in question.

        # FIXME! In case prefs say we don't autoplay, but the user has started it
        manually, we should be tracking whether the "view file" button has been hit
        since we changed which file we're tagging.
        """
        assert isinstance(which_file, Path)
        which_file = utils.relative_to_with_name(self.parent_window.displayed_database.file_location, which_file)

        if (which_file == self.current_file) and (self.current_file.exists()):
            if pref('auto-play-when-tagging'):
                gu.view_media([self.current_file])

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        pass        # intentionally ignore this event.


class ProcessingQueueWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """A window that displays the current list of files waiting to be processed, along
    with their processing parameters.
    """
    def __init__(self, database_to_display: mh.MetadataStore,
                 parent_window: Union[tk.Tk, tk.Toplevel], *pargs, **kwargs):
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.parent_window = parent_window
        self.displayed_database = database_to_display
        self.title(f"Files to process in {database_to_display.file_location.name}")
        self.protocol('WM_DELETE_WINDOW', self.do_close)

        # now, create window widgets
        content = ttk.Frame(self, padding=5)
        button_frame = ttk.Frame(content)
        self.refresh_button = ttk.Button(button_frame, text="Refresh", command=self.rebuild_processing_list)
        self.refresh_button.pack(side=tk.RIGHT)
        self.process_button = ttk.Button(button_frame, text="Process files ...", command=self.do_batch_process)
        self.process_button.pack(side=tk.RIGHT)
        self.clear_queue_button = tk.Button(button_frame, text="Clear queue", command=self.do_clear_queue)
        self.clear_queue_button.pack(side=tk.RIGHT)
        self.edit_button = ttk.Button(button_frame, text="Edit", command=self.do_edit)
        self.edit_button.pack(side=tk.LEFT)
        self.view_button = ttk.Button(button_frame, text="View", command=self.do_view)
        self.view_button.pack(side=tk.LEFT)
        self.delete_button = ttk.Button(button_frame, text="Delete", command=self.delete_item)
        self.delete_button.pack(side=tk.LEFT)
        button_frame.pack(side=tk.BOTTOM, expand=tk.NO, fill=tk.X)
        self.vscrollbar = ttk.Scrollbar(content)
        self.vscrollbar.pack(side=tk.RIGHT, expand=tk.NO, fill=tk.Y)
        self.processing_list = gu.ProcessingTreeView(content, yscrollcommand=self.vscrollbar.set)
        self.processing_list.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)
        self.vscrollbar.config(command=self.processing_list.yview)
        content.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)
        self.rebuild_processing_list()

    def rebuild_processing_list(self) -> None:
        self.processing_list.rebuild_list()

    def delete_item(self) -> None:
        cur_sel = self.processing_list.selection()
        if not gu.confirm_prompt(f'Clear all processing plans for '
                                 f'{cur_sel[0] if (len(cur_sel) == 1) else "these {len(cur_sel)} files"}?',
                                 parent=self):
            self.parent_window.set_window_status_to_canceled(True, True)
            return
        else:
            for f in cur_sel:
                self.parent_window.displayed_database[f]['next_processing'] = list()
                if 'next_processing_params' in self.parent_window.displayed_database[f]:
                    del self.parent_window.displayed_database[f]['next_processing_params']

    def do_view(self) -> None:
        """Open the currently selected video(s) in the default media viewer.
        """
        gu.view_media(self.processing_list.selection())

    def do_edit(self) -> None:
        """Edit the processing parameters for the selected file(s).
        """
        cur_sel = self.processing_list.selection()
        self.parent_window.set_window_status(f'Changing processing parameters for {len(cur_sel)} files ...')

        for i, f in enumerate(cur_sel, start=1):
            data = gd.get_data_from_user(data_needed={
                'parameters': {
                    'kind': gu.ProcessingParameterPane,
                    'validator': None,
                    'initial': self.parent_window.displayed_database[f]['next_processing'],
                    'tooltip':
                        'The exact processing parameters to use for this file on the next batch-processing run. '
                        'Specifying incorrect or nonsensical parameters may cause video corruption or prevent the file '
                        'from being processed at all!',
                }}, window_title=f"Processing parameters for {f}:")
            if not data:
                if (i == len(cur_sel)) or gu.ask_yes_or_no(title="Cancel parameter change?",
                                                           message="Do you want to abandon adusting parameters for "
                                                                   "other files you haven't seen yet?"):
                    self.parent_window.set_window_status_to_canceled(True, True)
                    return
                else:
                    continue
            else:
                self.parent_window.displayed_database[f]['next_processing'] = data['parameters']

    def do_batch_process(self) -> None:
        num = self.parent_window.displayed_database.num_videos_to_process
        if gu.confirm_prompt(f"Begin processing {num} file" + ('s' if (num > 1) else '') + '?', parent=self):
            self.parent_window.do_process_videos()

    def do_clear_queue(self) -> None:
        """Entirely clear the processing queue.
        """
        self.parent_window.do_clear_processing_queue()

    def do_close(self) -> None:
        """Handle cleanup and destroy the window. Most notably, mark the 'tags in database'
        window as None for the parent window.
        """
        self.parent_window.subwindows['files to process'] = None
        self.destroy()

    # And now the handlers for app events required by the BasicWindowMessagingMixIn protocol
    def EVENT_file_deleted(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)
        self.rebuild_processing_list()

    def EVENT_filename_changed(self, old_name: Path,
                               new_name: Path) -> None:
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)
        self.rebuild_processing_list()

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        """Nothing to do here, we don't deal with tags.
        """
        assert isinstance(which_file, Path)         # intentionally ignore this event

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        """We're not really concerned with file contents.
        """
        assert isinstance(which_file, Path)         # intentionally ignore this event

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)
        self.rebuild_processing_list()


class TagsInDatabaseWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
    """A window that displays all the tags in the current database.
    """
    def __init__(self, database_to_display: mh.MetadataStore,
                 parent_window: Union[tk.Tk, tk.Toplevel], *pargs, **kwargs):
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.parent_window = parent_window
        self.title(f"Tags in {database_to_display.file_location.name}")
        self.displayed_database = database_to_display
        self.protocol("WM_DELETE_WINDOW", self.do_close)

        # now, make widgets for the window
        content = ttk.Frame(self, padding=5)
        button_frame = ttk.Frame(content)
        self.refresh_button = ttk.Button(button_frame, text="Refresh", command=self.rebuild_tags_list)
        self.refresh_button.pack(side=tk.RIGHT)
        self.rename_tag_button = ttk.Button(button_frame, text="Rename tag...", command=self.rename_tag)
        self.rename_tag_button.pack(side=tk.LEFT)
        self.delete_tag_button = ttk.Button(button_frame, text="Delete tag...", command=self.delete_tag)
        self.delete_tag_button.pack(side=tk.LEFT)
        button_frame.pack(side=tk.BOTTOM, expand=tk.NO, fill=tk.X)
        self.vscrollbar = ttk.Scrollbar(content)
        self.vscrollbar.pack(side=tk.RIGHT, expand=tk.NO, fill=tk.Y)
        self.tags_list = TagsTreeView(content, yscrollcommand=self.vscrollbar.set)
        self.tags_list.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)
        self.vscrollbar.config(command=self.tags_list.yview)
        content.pack(side=tk.TOP, expand=tk.YES, fill=tk.BOTH)
        self.rebuild_tags_list()

    def do_close(self) -> None:
        """Handle cleanup and destroy the window. Most notably, mark the 'tags in database'
        window as None for the parent window.
        """
        self.parent_window.subwindows['tags in database'] = None
        self.destroy()

    def rebuild_tags_list(self) -> None:
        """Rebuild the textual list of tags displayed in the window.
        """
        oldval = self.tags_list.yview()              # Save position, then restore it after rebuilding list.
        self.tags_list.rebuild_list()
        self.tags_list.yview_moveto(oldval[0])

    def rename_tag(self) -> None:
        """Prompt the user for an existing tag and a tag to replace it. The existing tag
        will be replaced with the new tag in all tags for every file throughout the
        database.
        """
        self.tags_list.do_rename_tag()

    def delete_tag(self) -> None:
        """Delete a specified tag from every file in the database that has it.
        """
        self.tags_list.do_delete_tag()

    # And now the handlers for app events required by the BasicWindowMessagingMixIn protocol
    def EVENT_file_deleted(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)         # intentionally ignore this event.

    def EVENT_filename_changed(self, old_name: Path,
                               new_name: Path) -> None:
        assert isinstance(old_name, Path)
        assert isinstance(new_name, Path)            # intentionally ignore this event.

    def EVENT_file_tags_change(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)
        self.rebuild_tags_list()

    def EVENT_file_change_on_disk(self, which_file: Path) -> None:
        assert isinstance(which_file, Path)         # intentionally ignore this event

    def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
        pass                                        # intentionally ignore this event.


class TagsTreeView(gu.FlexibleTreeview):
    """A TreeView scroller used to display the list of all tags in the database.
    """
    def __init__(self, *pargs, **kwargs) -> None:
        gu.FlexibleTreeview.__init__(self, *pargs, **kwargs)
        columns = ('tag', 'count')
        self['columns'] = columns
        self['displaycolumns'] = columns
        self.column('#0', width=(4 + self.column('#0', option='minwidth')))     # Limit col. zero to min. width
        self.column('tag', anchor='w', stretch=True)
        self.column('count', anchor='e', stretch=True)
        self.heading('tag', text='tag', command=lambda: self.sort_by_column('tag'))
        self.heading('count', text='count', command=lambda: self.sort_by_column('count'))

    def sort_by_column(self, col: str, reverse: bool = False) -> None:
        self.default_sort = (col, reverse)  # Make this the new default sort for the list.

        line = [(self.set(k, col), k) for k in self.get_children()]
        if not line:  # Dealing with an empty database? Don't bother trying to sort, which will throw errors anyway.
            return
        if col == "tag":
            line.sort(key=lambda t: utils.default_sort(t[0]), reverse=reverse)
        elif col == "count":
            line.sort(key=lambda t: int(t[0]), reverse=reverse)

        for index, (_, k) in enumerate(line):  # rearrange items in sorted positions
            self.move(k, '', index)

        self.heading(col, command=lambda *args: self.sort_by_column(col, not reverse))  # next time, reverse sort

    def rebuild_list(self) -> None:
        """Rebuild the list, based on the tags in the currently displayed database.
        """
        self.empty_list()
        last_prefix = None      # FIXME: eventually, we want to put totals for tags with prefixes in the prefix line.

        for t, count in self.master.master.displayed_database.all_tags_with_counts(sorted_order=True).items():
            if ':' not in t:        # the most common case: not a tag prefix, ergo no children; add to top level.
                if last_prefix is not None:
                    pass        # FIXME! Adjust total tag count for prefix we JUST FINISHED!
                self.insert('', 'end', iid=t, values=(t.strip(), str(count)))
                last_prefix = None
            else:
                t = t.strip()
                colon_loc = t.find(':')
                prefix, postfix = t[:colon_loc+1].strip(), t[colon_loc+1:].strip()
                if not postfix:
                    which_files = self.master.master.displayed_database.get_files_with_tag(t)
                    warnings.warn(f"Compound tag {t} is malformed: nothing after the colon! Found on "
                                  f"{len(which_files)} files: {[str(p) for p in which_files]}")
                    continue
                if not prefix:
                    which_files = self.master.master.displayed_database.get_files_with_tag(t)
                    warnings.warn(f"Compound tag {t} is malformed: nothing before the colon! Found on "
                                  f"{len(which_files)} files: {[str(p) for p in which_files]}")
                    continue
                if prefix not in self.get_children():    # prefix not yet in top-level list? create an entry for it!
                    self.insert('', 'end', iid=prefix, values=(prefix, "―"))
                self.insert(prefix, 'end', iid=t, values=(f"     {t}", str(count)))   # FIXME: config'ble spacing
                last_prefix = prefix

    def get_children(self, item=None):
        """Our direct superclass has a gatekeeper method that overrides its own
        superclass's method to check that ITEM is None before delegating to its own
        superclass. For the FlexibleTreeview direct superclass, this makes sense; but we
        want to be able to pass an ITEM parameter here, because, unlike
        FlexibleTreeView, we have elements that have child elements. So we skip the
        gatekeeper superclass function and delegate straight to Treeview's method
        """
        return ttk.Treeview.get_children(self, item)

    def do_rename_tag(self) -> None:
        """Rename the currently selected tag.
        """
        sel = self.selection()
        usable_sel = [i for i in sel if not self.get_children(i)]
        if not usable_sel:
            self._root().status_bar.set_text('Cannot rename prefix-only tags! (You can rename "url:example.com", but '
                                             'not "url:".) Nothing to do ...')
            return

        if len(usable_sel) < len(sel):
            self._root().status_bar.set_text("Ignoring prefix-only pseudo-items!")

        changed = [gd.rename_tag(database=self.master.master.displayed_database, main_window=self._root(),
                                 tags_window=self.master.master, suppress_tags_window_update=True,
                                 tag_to_rename=t) for t in usable_sel]
        if any(changed):
            self.master.master.rebuild_tags_list()

    def do_delete_tag(self) -> None:
        """Delete the currently selected tag from the database.
        """
        sel = self.selection()
        usable_sel = [i for i in sel if not self.get_children(i)]
        if not usable_sel:
            self._root().status_bar.set_text('Cannot rename prefix-only tags! (You can delete "url:example.com", but '
                                             'not "url:".) Nothing to do ...')
            return

        if len(usable_sel) < len(sel):
            self._root().status_bar.set_text("Ignoring prefix-only pseudo-items!")

        changed = [gd.delete_tag(database=self.master.master.master.displayed_database,
                                 tags_window=self.master.master, suppress_tags_window_update=True,
                                 main_window=self._root(), tag_to_delete=t) for t in usable_sel]
        if any(changed):
            self.master.master.rebuild_tags_list()

    def do_select_items_with_tag(self) -> None:
        """Find all items in the database that have a specified tag and select them in the
        main window's items list, unselecting any items that are already selected.
        """
        tags, new_sel_files = self.selection(), set()
        for t in tags:
            for entry in (f for f in self._root().displayed_database if f not in new_sel_files):
                if t in self._root().displayed_database.get_tags(Path(entry)):
                    new_sel_files.add(entry)
        if new_sel_files:
            self._root().video_list.scroller.the_list.selection_set(*new_sel_files)
            self._root().video_list.scroller.the_list.see(list(new_sel_files)[0])

    def make_popups(self) -> None:
        """Make the popup menu that's held in reserve for right-clicks.
        """
        self.column_body_popup = tk.Menu(self, tearoff=False)
        self.column_body_popup.add_command(label="Select files with this tag in main window",
                                           command=self.do_select_items_with_tag)
        self.column_body_popup.add_separator()
        self.column_body_popup.add_command(label="Rename tag ...", command=self.do_rename_tag)
        self.column_body_popup.add_command(label="Delete tag ...", command=self.do_delete_tag)
        self.column_body_popup.add_separator()
        self.column_body_popup.add_command(label="Refresh tag list", command=self.rebuild_list)
        gu.right_click_bind(self, self.popup)
        # No need to inherit superclass's method here -- it only handles column heading right-clicks,
        # and we don't have any column headings!

    def popup(self, event: tk.Event):
        """Pop up the popup menu.
        """
        region = self.identify("region", event.x, event.y)
        if region == "heading":
            gu.FlexibleTreeview.popup(self, event)     # headings are handled by the superclass!
        else:
            self.column_body_popup.post(event.x_root, event.y_root)
            self.column_body_popup.focus_set()

    def do_select_all(self) -> None:
        """Currently, we have no way to select every row in the TreeView, because
        the code that does so in the parent class assumes a single-level TreeView.
        #FIXME
        """
        gu.not_implemented_GUI()

    def do_select_none(self) -> None:
        """Currently, we have no way to deselect every row in the TreeView, because
        the code that does so in the parent class assumes a single-level TreeView.
        #FIXME
        """
        gu.not_implemented_GUI()

    def do_invert_selection(self) -> None:
        """Currently, we have no way to invert the selection in the TreeView, because
        the code that does so in the parent class assumes a single-level TreeView.
        #FIXME
        """
        gu.not_implemented_GUI()


def do_volume_change(media_loc: Path,
                     which_db: 'MetadataStore',
                     parent: Optional[Union[Type[tk.Tk], Type[tk.Toplevel], Type[ttk.Frame]]] = None) -> None:
    """Allow the user to adjust the volume of the file specified by the MEDIA_FILE parameter. #FIXME: better docstring
    """
    assert isinstance(media_loc, Path)
    ret = None

    class VolumeAdjustmentWindow(tk.Toplevel, gu.BasicWindowMessagingMixIn):
        """Display options allowing the user to adjust the volume of an audio file.
        """
        def __init__(self, *pargs, **kwargs):

            assert isinstance(media_loc, Path)
            self.media_loc = media_loc
            self.which_db = which_db
            tk.Toplevel.__init__(self, parent, *pargs, **kwargs)

            background = ttk.Frame(self)
            background.grid(row=1, column=1, sticky=(tk.W, tk.N, tk.E, tk.S))
            background.rowconfigure(0, weight=1)
            background.columnconfigure(0, weight=1)

            self.status_bar = gu.StatusBar(background, initial_messages=('Ready.',))
            self.status_bar.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.X)

            button_pane = ttk.Frame(background)
            ttk.Button(button_pane, text="OK", command=self.handle_ok).pack(side=tk.RIGHT, expand=tk.NO)
            ttk.Button(button_pane, text="Cancel", command=self.handle_cancel).pack(side=tk.RIGHT, expand=tk.NO)
            ttk.Button(button_pane, text="Analyze", command=self.do_analyze).pack(side=tk.LEFT, expand=tk.NO)
            button_pane.pack(side=tk.BOTTOM, expand=tk.YES, fill=tk.X)

            controls_pane = ttk.Frame(background)
            ttk.Label(controls_pane, text='Adjust volume by').grid(column=0, row=0, sticky=(tk.E,))

            self.adjust_value_field = gu.WrappedEntry(controls_pane, width=8)
            self.adjust_value_field._setval("0.0")
            self.adjust_value_field.grid(column=1, row=0, sticky=(tk.W, tk.E))
            controls_pane.columnconfigure(1, weight=2)

            t_text = '\n'.join(textwrap.wrap("""Enter the amount by which to adjust the audio volume in this """
                                             """field. A positive number increases the """
                                             """volume, and a negative number decreases it.\n\nTo determine the """
                                             """maximum amount by which the volume can safely be raised (without """
                                             """audio clipping), click the "Analyze" button.""",
                                             width=pref('tooltip-width'), replace_whitespace=False))
            self.adjust_value_field.tooltip = gu.Tooltip(self.adjust_value_field, t_text)

            self.adj_type = gu.WrappedCombobox(controls_pane, height=2, exportselection=False)
            self.adj_type['values'] = ('decibels', 'percent')
            self.adj_type.state(['readonly'])
            self.adj_type.bind('<<ComboboxSelected>>', lambda *args: self.adj_type.selection_clear())
            self.adj_type._setval('decibels')
            self.adj_type.grid(column=2, row=0, sticky=(tk.W,))
            controls_pane.columnconfigure(2, weight=1)

            t_text = '\n'.join(textwrap.wrap("""To adjust the volume by a percent of its existing volume, choose""" 
                                             """ "percent" in this menu; to adjust it by a specific number of """
                                             """decibels, choose "decibels" instead.""",
                                             width=pref('tooltip-width'), replace_whitespace=False))
            self.adjust_value_field.tooltip = gu.Tooltip(self.adj_type, t_text)
            controls_pane.pack(side=tk.LEFT, expand=tk.YES, fill=tk.X)

            info_pane = ttk.Frame(background)
            ttk.Label(info_pane, text="   Maximum volume: ").grid(row=0, column=0, sticky=(tk.E,))
            ttk.Label(info_pane, text="   Average volume: ").grid(row=1, column=0, sticky=(tk.E,))
            self.max_volume_label = gu.WrappedLabel(info_pane, text="???")
            self.max_volume_label.grid(row=0, column=1, sticky=(tk.W,))
            self.avg_volume_label = gu.WrappedLabel(info_pane, text="???")
            self.avg_volume_label.grid(row=1, column=1, sticky=(tk.W,))
            info_pane.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)
            t_text = "\n".join(textwrap.wrap('This pane displays information about the average and maximum volume '
                                             'of the media file, relative to the maximum possible volume the file '
                                             'can record. To avoid clipping audio in the louder parts of the file, '
                                             'avoid raising the volume by more than the amount by which the maximum '
                                             'voume is below the maximum possible volume. (For instance, if the '
                                             'maximum volume is listed as -7.1 dB, then do not raise the volume by '
                                             'more than 7.1 dB.'))
            info_pane.tooltip = gu.Tooltip(info_pane, t_text)

        def do_analyze(self) -> None:
            """Analyze the media file in question to determine the maximum safe volume boost.

            #FIXME: directly parses ffmpeg output and is therefore rather fragile.
            """
            self.status_bar.set_text(f'Analyzing {self.media_loc.name}...', immediate=True)

            res = utils.run_process([pref('ffmpeg_loc'), '-i', str(self.media_loc.resolve()),
                                     '-filter:a', 'volumedetect', '-f', 'null', '/dev/null'])
            try:
                avg_vol_line = [l for l in res.stdout.casefold().split('\n') if 'mean_volume' in l][-1]
            except (TypeError, IndexError):
                avg_vol_line = None

            try:
                max_vol_line = [l for l in res.stdout.casefold().split('\n') if 'max_volume' in l][-1]
            except (TypeError, IndexError):
                max_vol_line = None

            if avg_vol_line:
                avg_label, unit = "mean_volume:", "db"
                avg_vol = avg_vol_line[avg_vol_line.find(avg_label) + len(avg_label):avg_vol_line.find(unit)].strip()
                self.avg_volume_label._setval(avg_vol)
            if max_vol_line:
                max_label, unit = "max_volume:", "db"
                max_vol = max_vol_line[max_vol_line.find(max_label) + len(max_label):max_vol_line.find(unit)].strip()
                self.max_volume_label._setval(max_vol)

                # If the user hasn't yet done any data entry, helpfully fill in some options for her.
                try:
                    val = float(self.adjust_value_field._getval())
                    if val == 0:
                        self.adjust_value_field._setval(str(abs(float(max_vol))))
                        self.adj_type._setval("decibels")
                except (ValueError, TypeError,) as errrr:
                    self.status_bar.set_text(f"Unable to set recommended value as amount to adjust! "
                                             f"The system said: {errrr}", immediate=True)
                except (BaseException,) as errrr:
                    pass    # Do we ever get errors of any other type here?
            self.status_bar.set_default_text(immediate=False)

        def handle_ok(self) -> None:
            try:
                if self.adj_type._getval() == "percent":
                    val = float(self.adjust_value_field._getval())
                    self.which_db.set_af_option(self.media_loc, 'volume', str(val/100))
                    self._root().status_bar.set_text(f"Scheduled increase of {val/100:.2f}%% for "
                                                     f"file {self.media_loc.name}.", immediate=True)
                elif self.adj_type._getval() == "decibels":
                    val = float(self.adjust_value_field._getval())
                    self.which_db.set_af_option(self.media_loc, 'volume', f'{val}dB')
                    self._root().status_bar.set_text(f"Scheduled increase of {val}dB for file {self.media_loc.name}.",
                                                     immediate=True)
            except (ValueError, TypeError):
                gu.error_message_box(f"Cannot interpret the value {self.adjust_value_field._getval()} as an amount"
                                     f" to change volume!")
                return
            except (BaseException,) as errrr:
                gu.error_message_box("Unable to set amount for volume change!", errrr)
            self.destroy()

        def handle_cancel(self) -> None:
            self.destroy()

        # and handlers for the BasicWindowMessagingMixIn protocol
        def EVENT_file_deleted(self, which_file: Path) -> None:
            """If the file we're displaying was deleted, destroy the window.
            """
            assert isinstance(which_file, Path)
            if which_file.samefile(self.media_loc):
                self.handle_cancel()

        def EVENT_filename_changed(self, old_name: Path,
                                   new_name: Path) -> None:
            """If the name of the video we're displaying has been updated, or the
            current image thumbnail, update our references to it.
            """
            assert isinstance(old_name, Path)
            assert isinstance(new_name, Path)

            if old_name.samefile(self.media_loc):
                self.media_loc = new_name

        def EVENT_file_tags_change(self, which_file: Path) -> None:
            """Nothing to do here: we don't deal with file tags in any way.
            """
            assert isinstance(which_file, Path)     # intentionally ignore this event

        def EVENT_file_change_on_disk(self, which_file: Path) -> None:
            """If the file we're displaying changed on disk, reload it.
            """
            assert isinstance(which_file, Path)     # probably, we need not do anything here.

        def EVENT_file_processing_parameters_change(self, which_file: Path) -> None:
            """Nothing to do here: we don't deal with file processing parameters.
            """
            assert isinstance(which_file, Path)     # intentionally ignore this event

    box = VolumeAdjustmentWindow()
    box.title(f"Adjust audio volume for {media_loc.name}")
    box.lift()
    box.focus_set()
    box.grab_set()
    box.wait_window()


if __name__ == "__main__":
    print("Sorry, there is no self-test code in this module!")
