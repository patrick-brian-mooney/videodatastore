#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Provides utility GUI code that handles the Preferences window for the
VideoDataStore project. The Preferences window is an enhanced version of a
DataGetter window (as defined in get_data.py), with individual DataGetter panes
in a tabbed notebook.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022-24 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import collections
import copy
import pickle

import tkinter as tk
import tkinter.ttk as ttk

from pathlib import Path
from typing import Any, Dict


import vds.ffmpeg_utils as fu
import vds.gui_utils as gu
import vds.get_data as gd

import vds.utils as utils
from vds.utils import pref


class PrefsWindow(tk.Toplevel):
    """Display and interpret a Preferences window.

    Currently, this is ugly, but at least it's there.   # FIXME
    """
    # First: this dict is used as a template to fill in DataGetter requests for individual items that use the
    # dictionary format (as opposed to the simple string format).
    data_point_template = {
        'label': None,              # key in the data_requested dictionary (becomes a label in the DataGetter pane)
        'validator': None,          # validator func (None is left as None if unspecified)
        'kind': None,               # data type; if None, infer from current prefs
        'initial': None,            # initial value passed to DataGetter; if None, infer from current prefs
    }

    # Next: this class-level dict indicates which pref() data bit goes in each notebook page. It maps
    # pane name -> list of prefs keys on that pane. Items that are simple strings are filled out by inferring
    # other information from current prefs values.

    panes = {
        'file handling':
            [
                {'label': 'confirm_delete_db_entry', 'kind': bool,
                 'display': 'Confirm before deleting database entry',
                 'tooltip':
                     'If checked, the user will be asked to confirm before deleting an entry from the database.'
                 },
                {'label': 'delete_file_with_db_entry', 'kind': bool,
                 'display': 'Delete file with database entry deletion',
                 'tooltip':
                     'If checked, the file an entry represents will also be deleted when the entry is deleted. (If'
                     'not checked, the file will continue to exist on disk and may be added back into the database '
                     'later.)'
                 },
                {'label': 'confirm_file_deletion', 'kind': bool,
                 'display': 'Confirm file deletion',
                 'tooltip':
                     'If checked, the user will be asked to confirm before a file in the database is deleted from'
                     ' disk.'
                 },
                {'label': 'auto-play-when-tagging', 'kind': bool,
                 'display': 'Auto-play media during tagging',
                 'tooltip':
                     'If checked, each file being incrementally tagged will be played in the configured media player '
                     'when the incremental tagging window changes to show the data for that file. (If not checked, '
                     'the "View Media" button can still be used to play files manually.)'
                 },
                {'label': 'strippable_separators', 'kind': str,
                 'display': 'Characters to strip from right and left of filenames',
                 'tooltip':
                     'When cleaning filenames while importing from a holding directory (and during similar filename-'
                     'cleaning activities), each character in this field will be stripped repeatedly from the '
                     'beginning and end of the filename, just as configured "strip phrases" are stripped repeatedly'
                     'from the beginning and end of the filename.'
                 }
            ],
        'file locations':
            [
                {'label': 'ffmpeg_loc', 'validator': gd.executable_file_validator,
                 'kind': Path, 'masseuse': gd.none_or_str, 'display': 'location of FFmpeg',
                 'tooltip':
                     f'Full path to the ffmpeg{".exe" if utils.running_under_windows() else ""} program.'
                 },
                {'label': 'ffprobe_loc', 'validator': gd.executable_file_validator,
                 'kind': Path, 'masseuse': gd.none_or_str, 'display': 'location of FFprobe',
                 'tooltip':
                     f'Full path to the ffprobe{".exe" if utils.running_under_windows() else ""} program.'
                 },
                {'label': 'nice_loc', 'validator': gd.executable_file_validator,
                 'kind': Path, 'masseuse': gd.none_or_str, 'display': 'location of nice',
                 'tooltip':
                     'Full path to the "nice" program. As there is no equivalent to "nice" under Windows, should '
                     'be left blank under that operating system.'
                 },
                {'label': 'plugins_loc', 'validator': gd.directory_exists_validator,
                 'kind': Path, 'masseuse': gd.none_or_str, 'display': 'plugins directory',
                 'tooltip':
                     'Path (either full, or relative to the VideoDataStore installation folder) to the VideoDataStore'
                     ' "plugins" directory.'
                 },
                {'label': 'media-viewer', 'validator': gd.executable_file_validator,
                 'kind': Path, 'masseuse': gd.none_or_str, 'display': 'location of media viewer',
                 'tooltip':
                     'Full path to your preferred media-playing application.'
                 },
            ],
        "media specs":
            [
                {'label': 'known_video_types', 'kind': gu.EditableListPane, 'addfunc': gd.do_get_suffix,
                 'display': 'Known video types',
                 'tooltip':
                     'Extensions of files that FFmpeg can process as video files.'
                 },
                {'label': 'known_audio_types', 'kind': gu.EditableListPane, 'addfunc': gd.do_get_suffix,
                 'display': 'Known audio types',
                 'tooltip':
                     'Extensions of files that should be treated as audio files.'
                 },
                {'label': 'never_process_types', 'kind': gu.EditableListPane, 'addfunc': gd.do_get_suffix,
                 'display': 'Types to avoid processing',
                 'tooltip':
                     'Extensions of files that should never be processed.'
                 },
                {'label': 'deprecated_containers', 'kind': gu.EditableListPane, 'addfunc': gd.do_get_suffix,
                 'display': 'Deprecated container types',
                 'tooltip':
                     'Extensions for files that VideoDataStore should try to move data out of. For instance, this '
                     'happens when choosing "Re-containerize deprecated formats" from the Processing menu.'
                 },
                {'label': 'deprecated_audio_codecs', 'kind': gu.EditableListPane, 'addfunc': gd.do_get_suffix,
                 'display': 'Deprecated audio codecs',
                 'tooltip':
                     'This configuration parameter is currently unused.'
                 },
            ],
        'compression':
            [
                {'label': 'max_postcomp_size_ratio', 'kind': float, 'display': 'Max. size after recompression (ratio)',
                 'validator': gd.positive_number_validator_func,
                 'tooltip':
                     'The maximum acceptable ratio of the file size, after processing, to the original filesize.'
                     'For instance, a ratio of 0.85 means that, if the processed file is more than 85% of the size'
                     'of the original file before processing, a second, slower attempt is made to process the video '
                     'again, to be sure that it fits within that size. Setting this too lower will result in low-'
                     'quality video files after encoding.'
                 },
                {'label': 'max_kush_gauge_leeway', 'kind': float, 'display': 'High bitrate threshold',
                 'validator': gd.positive_number_validator_func,
                 'tooltip':
                     'The threshold ratio above the Kush-predicted "necessary bitrate" for good quality after which '
                     'a video file is considered to be "high bitrate" (for the purposes of the "Schedule high-bitrate '
                     'downsizing" task in the Processing menu). For instance, if the ratio is 1.1, then any file that '
                     'is more than 10% larger than the Kush-gauge prediction thinks is necessary is considered to be'
                     '"high-bitrate" for the purposes of the task, and will be re-encoded.'
                 },
                {'label': 'kush_gauge_motion_prediction', 'kind': float, 'display': 'Kush constant',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'Controls how much motion (change between frames) to anticipate in a video when calculating the'
                     'bitrate it will need to have acceptable visual quality. This quantity is multiplier: larger '
                     'values give better results with lots of on-frame movement, but result in larger files. 12 or 15 '
                     'is probably a good starting point for experimenting.',
                 },
                {'label': 'video_size_factor', 'kind': int, 'display': 'Factor for video size',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'Specifies a number that must be a factor of video height and width sizes. For instance, a '
                     'value of 2 means that video height and width must be multiples of two, i.e. even numbers. (This '
                     'is a common requirement for video codecs.) When scaling a video, VideoDataStore will make sure '
                     'that the resulting frame size honors this requirement.'
                 },
                {'label': 'max_video_width', 'kind': int, 'display': 'Maximum video width',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'The maximum width that a video is allowed to have when VideoDataStore is engaged in the '
                     '"Schedule large-video downsizing" task from the Processing menu. Videos wider than this width '
                     'will be scheduled to be scaled down proportionately so that their width is no more than this.'
                 },
                {'label': 'max_video_height', 'kind': int, 'display': 'Maximum video height',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'The maximum height that a video is allowed to have when VideoDataStore is engaged in the '
                     '"Schedule large-video downsizing" task from the Processing menu. Videos higher than this '
                     'will be scheduled to be scaled down proportionately so that their height is no more than this.'
                 },
                {'label': 'ffmpeg_preset', 'kind': str, 'display': 'FFmpeg speed preset',
                 'validator': gd.ffmpeg_preset_validator_func,
                 'tooltip':
                     f"""The default preset to use for FFmpeg encoding if a preset is not manually specified in a """ 
                     f"""file's processing options. This option controls how FFmpeg trades off processing quality """
                     f"""and file size for processing speed. Valid options are """
                     f"""{utils.oxford_comma_list_of(fu.get_ffmpeg_presets(), quote_char='"')}."""
                 },
                {'label': 'single_CRF_value', 'kind': int, 'display': 'Constant rate factor',
                 'validator': gd.bounded_integer_validator_factory(0, 51),
                 'tooltip': 'The Constant Rate Factor that VideoDataStore will use when trying to re-encode a file '
                            'with a single pass. For x264 encoding (VideoDataStore\'s default codec), the valid range '
                            'is 0 to 51, where a LOWER number means BETTER quality (but larger file size). A sane '
                            'range for most users is probably 18 to 28, where 18 is more or less visually '
                            'indistinguishable from the original source provided to FFmpeg.'
                 },
                {'label': 'proc_file_ext', 'kind': str, 'display': 'File type after compression',
                 'validator': gd.is_valid_extension,
                 'tooltip': 'The file extension for files that have been recompressed by VideoDatastore. ".mp4" '
                            '(without quotes) is a good choice. It is important that the extension you choose '
                            'represents a container format that can contain the audio and video codecs you choose '
                            'below — VideoDataStore currently makes no effort to verify this!'
                 },
                {'label': 'default_video_codec', 'kind': str, 'display': 'Default video codec',
                 'validator': lambda value: gd.is_valid_encoding_codec(value, 'video'),
                 'tooltip': 'The video encoding codec to use. Anything other than "libx264" is still highly '
                            'experimental. For a list of valid codec choices for FFmpeg you have installed, see the '
                            '"FFmpeg codec info" submenu in the Processing menu.'
                 },
                {'label': 'default_audio_codec', 'kind': str, 'display': 'Default audio codec',
                 'validator': lambda value: gd.is_valid_encoding_codec(value, 'audio'),
                 'tooltip': 'The audio encoding codec to use. Anything other than "aac" is still highly experimental.'
                            ' For a list of valid codec choices for FFmpeg you have installed, see the "FFmpeg codec '
                            'info" submenu in the Processing menu.'
                 },
            ],
        'batch processing':
            [
                {'label': 'num_ffmpeg_threads', 'kind': int, 'display': 'Number of FFmpeg threads',
                 'tooltip':
                     'Number of threads to ask FFmpeg to use while encoding. This should be a small number, less than '
                     'the total number of processors (or cores) on your system. A lower number will make your computer'
                     ' more usable for other tasks while FFmpeg is encoding; a higher number (up to a point) will '
                     'result in FFmpeg finishing sooner.'},
                {'label': 'nice_value', 'kind': int, 'display': 'Niceness parameter',
                 'validator': gd.bounded_integer_validator_factory(-20, 19),
                 'tooltip':
                     'Under Unix-like systems, sets the "nice" value of the FFmpeg process during batch processing. '
                     'This value adjusts the processor priority of FFmpeg; low values mean the encoding process takes '
                     'less time, but the host computer may be less responsive. High values mean that encoding may take '
                     'longer, but the computer may be more responsive to other tasks while encoding is happening. '
                     'Meaningful values range from -20 (FFmpeg runs as quickly as possible) to 19 (FFmpeg lets other '
                     'programs run as much as possible).\n\nUnder Windows, this parameter does nothing, because '
                     'Windows does not support nice-like priority adjustment.'
                 },
                {'label': 'max-consecutive-batch-errors', 'validator': gd.blank_or_positive_int_validator,
                 'kind': str, 'masseuse': gd.none_or_int, 'display': 'Max. consecutive batch errors',
                 'tooltip':
                     'If this parameter is not blank, it must be a positive integer. If batch processing encouters '
                     'errors on this many consecutive files, VideoDataStore will abort the batch-processing attempt. '
                     'If this is left blank, VideoDataStore will continue through the batch-processing queue, no '
                     'matter how many consecutive errors occur.'
                 },
            ],
        'UI':
            [
                {'label': 'right-click-mode', 'kind': int, 'display': 'Right-click mode',
                 'validator': gd.bounded_integer_validator_factory(0, 2),
                 'tooltip':
                     'What type of right-clicking behavior to use. Setting this to zero means that VideoDataStore '
                     'should auto-detect the type of system you are running on and behave appropriately; this is '
                     'usually the right thing to do. But if VDS is not properly handling right-clikcks (or control-'
                     'clicks), setting this to 1 forces the program to use the normal behavior for Windows and Unix '
                     'X11; setting this to 2 forces the program to use the normal behavior for macOS.'
                 },
                {'label': 'statusbar-delay', 'kind': float, 'display': 'Statusbar message delay (sec.)',
                 'validator': gd.positive_number_validator_func,
                 'tooltip':
                     'Controls how many seconds a message in the status bar will be displayed if there are additional '
                     'messages waiting. After a message has been displayed for this many seconds, the status bar '
                     'switches to displaying the next waiting message.'
                 },
                {'label': 'filter-delay', 'kind': int, 'display': 'Quick filter delay (msec.)',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'Controls how long (in milliseconds) to wait after a keystroke in the quick-filter bar above the '
                     'file list in the main window before filtering the file list based on the text in the quick-'
                     'filter bar. Values around 1000 to 2000 (1 to 2 seconds) are probabaly good choices for many '
                     'users.'
                 },
                {'label': 'tooltip-width', 'kind': int, 'display': 'Tooltip width (characters)',
                 'validator': gd.bounded_integer_validator_factory(30, 300),
                 'tooltip':
                     'Controls how wide tooltip popups are. All tooltips are wrapped to no more than this width.'
                 },
            ],
        'windows':
            [
                {'label': 'window-geometry', 'kind': str, 'display': 'Window geometry',
                 'validator': gd.window_geometry_validator,
                 'tooltip':
                     'This specifies the initial position and size of the main program window in the form '
                     'WIDTHxHEIGHT±X±Y, where X (horizontal) and Y (vertical) are either expressed as positive '
                     '(distance from the left or top of the desktop) or negative (distance from the right or bottom'
                     'of the desktop). All distances are expressed in pixels.',
                 },
                {'label': 'default-vis-width', 'kind': int, 'display': 'Default visualization width',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'The default width for visualization windows created by plugins, in pixels.'
                 },
                {'label': 'default-vis-height', 'kind': int, 'display': 'Default visualization height',
                 'validator': gd.positive_integer_validator_func,
                 'tooltip':
                     'The default height for visualization windows created by plugins, in pixels.'
                 },
                {'label': 'max-windows-opened-unwarned', 'kind': int,
                 'display': 'Max. windows to open without warning', 'validator': gd.nonnegative_integer_validator_func,
                 'tooltip':
                     'The maximum number of windows to open at a single time without warning the user how many '
                     'windows will be opened.'
                 },
            ],
        'advanced':
            [
                {'label': 'debugging', 'kind': bool, 'display': 'Enable debugging code',
                 'tooltip':
                     'Turn on debugging code, to make the Debugging menu appear and make some code output more text to '
                     'stdout while running.'
                 },
                {'label': 'allow-tearoffs', 'kind': bool, 'display': 'Use tearoff menus',
                 'tooltip':
                     'Turn on the ugly and archaic option to display menu tear-off lines.'
                 },
                {'label': 'UNSAFE_use_pickle_save', 'kind': bool, 'display': 'Use unsafe save format',
                 'tooltip':
                     'If this is checked, VideoDataStore will use the "pickle" format for save files, which saves and '
                     'loads much more quickly (this is especially noticeable with large databases). "Pickle" format '
                     'save files are safe, provided that you only open databases you have produced yourself. But '
                     '"pickle" format databases cannot be made completely secure, because malicious actors can use '
                     'them to craft attacks against your computer, and so you should never open such a database from '
                     'someone you do not trust completely.\n\nIf you save your databases in "pickle" format, you must '
                     'also enable the option to allow loading "pickle" format databases, or you will not be able to '
                     'open databases you have created yourself!'
                 },
                {'label': 'UNSAFE_allow_pickle_load', 'kind': bool, 'display': 'Allow loading from unsafe format',
                 'tooltip':
                     'If this is checked, VideoDataStore will be able to open databases saved in the "pickle" format '
                     'for save files, which saves and loads much more quickly (this is especially noticeable with '
                     'large databases). "Pickle" format save files are safe, provided that you only open databases you '
                     'have produced yourself. But "pickle" format databases cannot be made completely secure, because '
                     'malicious actors can use them to craft attacks against your computer, and so you should never '
                     'open such a database from someone you do not trust completely.\n\nThere is no point enabling '
                     'loading "pickle" format databases unless you either also enable saving in this format, or else '
                     'are receiving such databases from someone else whom you trust completely.'
                 },
                {'label': 'UNSAFE_pickle_version', 'kind': int, 'display': 'Pickle version for unsafe saves',
                 'validator': gd.bounded_integer_validator_factory(-1, pickle.HIGHEST_PROTOCOL),
                 'tooltip':
                     f'The version number of the "pickle" format to use when saving databases in the (unsafe) "pickle" '
                     f'format. This must be a number from 0 to {pickle.HIGHEST_PROTOCOL}, or else the number -1, to'
                     f'use the latest version possible with the version of Python being used to run VideoDataStore.'
                     f'\n\nLarger numbers generally mean that safe files are smaller and are created more quickly; but '
                     f'they require that later versions of Python be used to run VideoDataStore in order to open '
                     f'database files created using that version of the "pickle" format. Version 4 is generally a good '
                     f'choice for those who need database files to be readable with Python 3.6 or 3.7; most users who '
                     f'are only opening database files created on their own computer should leave this set to -1.',
                 },
            ],
    }

    # These prefs are in the Prefs mapping, but not editable through the GUI.
    undisplayed_prefs = [
        'statusbar-font', 'output-font', 'column-weights', 'hidden-columns', 'helper-apps',
        'title-font', 'label-font', 'small-label-font', 'tooltip-font',

        # This particular key is computed during startup and is not manually listed in the dict; needs to be listed
        # here to prevent the "all data points are accounted for" assertion below from choking.
        'all_processable_types'
    ]

    @classmethod
    def check_prefs_tab_validity(cls) -> None:
        """Check to be sure that every Prefs key in utils. defaults is either assigned
        to a tab, or in the undisplayed_prefs list.
        """
        known_data_points = set(cls.undisplayed_prefs)
        for tab in cls.panes:
            for item in cls.panes[tab]:
                if isinstance(item, dict):
                    known_data_points.add(item['label'])
                else:
                    assert isinstance(item, str)
                    known_data_points.add(item)
        assert known_data_points == set(utils.defaults.keys())

    def __init__(self, *pargs, **kwargs):
        self.check_prefs_tab_validity()
        self.panes = copy.deepcopy(self.panes)       # produce copy of class-level data before massaging it.
        tk.Toplevel.__init__(self, *pargs, **kwargs)
        self.protocol("WM_DELETE_WINDOW", self.handle_cancel)

        buttons_pane = ttk.Frame(self)
        ttk.Button(buttons_pane, text="OK", command=self.handle_ok).pack(side=tk.RIGHT)
        ttk.Button(buttons_pane, text="Cancel", command=self.handle_cancel).pack(side=tk.RIGHT)
        buttons_pane.pack(side=tk.BOTTOM, fill=tk.X, expand=tk.YES)

        self.notebook, self.tabs, self.name_mapping = ttk.Notebook(self), dict(), dict()
        for tab, datapoints in self.panes.items():
            tab_data = dict()
            for item in self.panes[tab]:
                if isinstance(item, str):
                    tab_data[item] = {'kind': type(pref(item)),
                                      'initial': pref(item),
                                      'validator': None}
                else:
                    assert isinstance(item, dict)
                    if 'display' not in item:
                        item['display'] = item['label']
                    self.name_mapping[item['display']] = item['label']
                    label = item['display']
                    tab_data[label] = dict(collections.ChainMap(item, self.data_point_template))
                    if not tab_data[label]['kind']:
                        tab_data[label]['kind'] = type(pref(item['label']))
                    if not tab_data[label]['initial']:
                        tab_data[label]['initial'] = pref(item['label'])
                    del item['label']

            self.tabs[tab] = gd.DataGetter(parent=self.notebook, data_needed=tab_data)
            self.notebook.add(self.tabs[tab], text=tab)

        self.notebook.pack(side=tk.BOTTOM, fill=tk.BOTH, expand=tk.YES)

    def validate_data(self) -> bool:
        """Validates that the data in the dialog is acceptable. If it is, returns True.
        If the entered data does not validate, displays a dialog box and returns False

        For data to be "acceptable" means that each DataGetter tab validates its own data as
        acceptable.
        """
        for tab in self.tabs.values():
            if not tab.validate_data():
                # dialog box will already have been displayed, but bring relevant tab to the front
                self.notebook.select(tab)
                return False

        return True

    def extract_data(self) -> Dict[str, Any]:
        """Extract the data encoded in the SELF window and return it. By the time this
        function is called, the entered data has already been validated as acceptable
        by self.validate_data().
        """
        ret = dict(collections.ChainMap(*[tab.extract_data() for tab in self.tabs.values()]))
        return {self.name_mapping[k]: v for k, v in ret.items()}

    def handle_ok(self) -> None:
        """Validate that the data in the entered preferences data is in an acceptable
        format; if not, complain and return. If so, extract the new data, store it, and
        save the preferences data to disk.
        """
        if not self.validate_data():
            return

        changed_data = {key: value for key, value in self.extract_data().items() if value != utils.prefs[key]}
        if changed_data:
            utils.prefs.update(changed_data)
            utils.prefs.save_preferences()

        self._root().subwindows['preferences'] = None
        self.destroy()

    def handle_cancel(self) -> None:
        self._root().subwindows['preferences'] = None
        self.destroy()


if __name__ == "__main__":
    print("Sorry, no self-test code in this module!")
