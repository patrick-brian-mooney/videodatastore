#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Handles plugins for VideoDataStore.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import abc
import copy
import importlib
import inspect
import subprocess
import sys
import uuid

from pathlib import Path
from typing import Iterable, List, Optional, Tuple, Type, Union

import tkinter as tk

import vds.gui_utils as gu
import vds.metadata_handling as mh

from vds.utils import pref


def ensure_installed(module_name: str) -> bool:
    """"Checks to see whether MODULE_NAME can be imported; if not, attempts to install
    it using pip, then tries to import it again. If it's able to import it at any
    time during this process, quits trying and returns True. If it's unable to
    load the module file even after all of this, returns False.

    Partially based on https://stackoverflow.com/a/50255019/5562328

    #FIXME! We are currently installing without asking the user for consent!
    """
    try:
        m = importlib.import_module(module_name)
        if m:
            return True
        else:
            raise ImportError
    except (ImportError,):
        pass

    try:
        subprocess.check_call([sys.executable, "-m", "pip", "install", module_name])
    except (subprocess.CalledProcessError,):
        return False

    if module_name in sys.modules:
        del sys.modules[module_name]
    try:
        m = importlib.import_module(module_name)
        if m:
            return True
        else:
            raise ImportError
    except (ImportError,):
        return False


class BasePlugin(object, metaclass=abc.ABCMeta):
    """A base class that plugins must derive from.
    """
    plugin_name = None          # the display name of the plug-in, visible in the plug-ins menu; must be a str
    needs_write_access = False  # True if the plug-in needs write access to the database.
    hide = False                # Set to True to prevent plug-in from appearing in menu; useful for ABC plugins.

    def __init__(self) -> None:
        """This is the standard __init__() method that all plugins have, at least
        implicitly. It will be called during the initialization of the top-level
        application, which is long, long before the user loads a database, let alone
        tries to use the plugin to process it in any way. It will never be passed any
        parameters.

        If a plugin needs to do any set-up at this time, it can, though it should not do
        anything that substantially delays the process of starting the application --
        this method, if present, will be run before the main window is even visible!
        Long processing here would make the application appear to be frozen.

        If long precessing at initialization time *really is* absolutely necessary,
        this method should spin off an additional thread to perform it and return
        quickly, then check to be sure that it's completed before doing any real
        processing later on. It's better to do long initialization from within the
        do_processing() method, though, which is called when the program has
        started, a database has been loaded, and the user is actually trying to use
        the plug-in.
        """
        pass

    @abc.abstractmethod
    def do_processing(self, database: mh.MetadataStore,
                      root_window: Optional[Type[tk.Tk]] = None
                      ) -> Iterable[Tuple[str, Type[tk.Toplevel]]]:
        """Do the actual processing needed. DATABASE is (by default) a (deep) *copy* of
        the database being operated on. If the plug-in needs to be able to alter the
        database, and therefore needs a reference to the database itself instead of to a
        copy of it, the plugin's class-level NEEDS_WRITE_ACCESS attribute must be True
        (or Truthy), and the user will be prompted to give write access to the database
        before it's passed in.

        ROOT_WINDOW is always None if the plug-in's NEED_WRITE_ACCESS attribute is
        False; if the plug-in needs access to the root window (which contains a
        reference to the actual database, and therefore leaks this access), then
        NEED_WRITE_ACCESS needs to be True, in which case a reference will be
        provided to the window.

        What the function returns must be a (possibly empty) list of 2-tuples
        representing the unclosed windows that the plugin leaves open when this method
        has finished running: each must be of the form (str, Window), where the first
        (string) element is an identifier that the plugin should assign and must try
        hard to make as unique as possible amongst all identifiers used by all windows
        in the application; the second element is a reference to a window that the
        plugin created and has not closed. Every window created but not closed must be
        included in this list.

        Windows created need not be tk.Toplevel windows; if the method is annotated,
        though, it should be annotated accurately to ensure that it is performant if
        the application and its plugins are compiled with Cython or otherwise analyzed.

        Plugins should try to create a unique identifier for the string parameter, but
        must not assume that the identifier will not be further modified: the plugin
        itself doesn't have enough information to ensure that this is true. The
        application may need to modify the returned identifier.

        The class object will be held in memory between runs and can be used to store
        data (e.g., default parameters) if necessary.
        """
        raise NotImplementedError


def do_about_plugins_box() -> None:
    gu.not_implemented_GUI()                # FIXME!


def get_plugin(f: Path) -> Union[Type[BasePlugin], None]:
    """Given F, the path to a file that might contain a plugin, load the module file
    and extract the plug-in class, and create and return an instance.

    If this is not possible for any reason, print info to stdout and return None.
    """
    assert isinstance(f, Path)

    try:
        # These next few lines thanks to https://stackoverflow.com/a/67692/5562328
        spec = importlib.util.spec_from_file_location(str(f), f)
        plugin_module = importlib.util.module_from_spec(spec)
        spec.loader.exec_module(plugin_module)

    except (ImportError,) as errrr:
        print(f"Cannot import {f} as a Python module! The system said: {errrr}.")
        return None
    except (IOError,) as errrr:
        print(f"Cannot load module {f} due to I/O error! The system said: {errrr}.")
        return None

    try:
        the_class = getattr(plugin_module, '__PLUGIN_CLASS__')
    except (AttributeError,):
        print(f"WARNING! Plug-in misconfiguration in {f}! No __PLUGIN_CLASS__ specified in plugin.")
        print("Attempting to guess at internal configuration. This may fail and SHOULD NOT BE RELIED ON!")
        potential_plugins = [p for _, p in inspect.getmembers(plugin_module) if issubclass(p, BasePlugin)]
        if not potential_plugins:
            print("    Unable to find any plug-ins!")
            return None
        elif len(potential_plugins) > 1:
            print("    Too many potential plug-ins in file! Declining to guess.")
            return None
        print("    One potential plug-in found. Using it. The plug-in file is still misconfigured!")
        return potential_plugins[0]()

    if issubclass(the_class, BasePlugin):
        return the_class()
    else:
        print(f"Plug-in misconfiguration in {f}! Specified __PLUGIN_CLASS__ is not a subclass of BasePlugin.")
        return None


def register_windows(main_window: 'MainWindow',
                     window_data: List[Tuple[str, Type[Union[tk.Tk, tk.Toplevel]]]]) -> None:
    """Take each window produced by a plugin and register it as a subwindow of MAIN_WINDOW.
    """
    for name, win in window_data:
        while name in main_window.subwindows:
            name = f"{name}-{uuid.uuid4()}"
        main_window.subwindows[name] = win


def call_plugin(main_window: 'MainWindow',
                plugin: Type[BasePlugin],
                plugin_name: str) -> None:
    """Given a reference to main_window, the main application window, and PLUGIN, the
    instance of a subclass of BasePlugin, call the plugin instance, passing it a
    *copy* of MAIN_WINDOW's displayed_database attribute.

    If PLUGIN's class has its needs_write_access attribute set to True, or something
    Truthy, then instead of getting a copy of the database:
        1. The user will be presented with a dialog asking them to approve giving
           the plug-in a reference to the database in memory; and
        2. If the user approves, then the plug-in is run, and is passed a reference
           to the database in memory, rather than to a copy of it.
        3. After the plugin is run, the file list in the main window is re-built.

    #FXIME: we should allow the user to say "Always" to "Allow write access?".
    """
    if plugin.needs_write_access:
        if not gu.ask_yes_or_no(f"The plug-in {plugin_name} wants write access to the database. Allow?",
                                title="Allow write access?"):
            return
        data = main_window.displayed_database
    else:
        main_window.set_window_status(f"Preparing data for plug-in {plugin_name} ...", immediate=True)
        data = copy.deepcopy(main_window.displayed_database)
    main_window.set_window_status(f"Running plug-in {plugin_name} ...", immediate=True)
    window_data = plugin.do_processing(data, main_window if plugin.needs_write_access else None)
    main_window.set_window_status(f"Plug-in {plugin_name} finished!", immediate=True)
    if plugin.needs_write_access:
        main_window.rebuild_list()
        main_window.do_save()

    if window_data:
        register_windows(main_window, window_data)


def init_plugins(main_window: 'MainWindow',
                 parent_menu: tk.Menu,
                 current_folder: Optional[Path] = None,
                 add_about_menu_item: bool = True) -> Union[tk.Menu, None]:
    """Initializes the plugin system. Builds a "Plugins" menu and returns it so that
    it can be attached to the main menu system for the MainWindow. It needs the Menu
    to which the Plugins menu is going to be attached because that menu needs to
    know what its parent widget is.

    Unless ADD_ABOUT_MENU_ITEM is False, adds an "about plug-ins" menu item to the
    menu. Calls itself recursively  to create new submenus for subfolders inside the
    designated plugins/ folder.

    Returns a Menu, if there are any plugins in the directory in question, or if the
    "About Plug-ins" menu item was created, or else None, if there are no plugins
    and the "About" item was not created.

    This function should only be called by program-initialization code, not by
    plugins themselves.
    """
    if not current_folder:
        return init_plugins(main_window, parent_menu, Path(pref('plugins_loc')))

    assert isinstance(current_folder, Path)
    ret = tk.Menu(parent_menu, tearoff=pref('allow-tearoffs'))
    added_separator, added_folders = False, False

    if add_about_menu_item:
        ret.add_command(label="About plug-ins ...", command=do_about_plugins_box)
        ret.add_separator()

    for fold in sorted(f for f in current_folder.glob('*') if f.is_dir()):
        m = init_plugins(main_window=main_window, parent_menu=ret, current_folder=fold, add_about_menu_item=False)
        if m:
            ret.add_cascade(label=fold.name, menu=m)
            added_folders = True

    filenames_by_plugin = {get_plugin(f): f for f in sorted(current_folder.glob('*py')) if f.is_file()}
    plugins_by_name = {p.plugin_name: p for p in filenames_by_plugin.keys()}
    for plug_name in sorted(plugins_by_name.keys()):
        p = plugins_by_name[plug_name]
        plug_file = filenames_by_plugin[p]
        if p and not p.hide:
            if (ret.index(tk.END) is not None) and added_folders and (not added_separator):
                ret.add_separator()
                added_separator = True
            ret.add_command(label=p.plugin_name, command=lambda plug=p, name=plug_file: call_plugin(main_window=main_window, plugin=plug, plugin_name=str(name)))

    i = ret.index(tk.END)
    if isinstance(i, int):
        return ret
    else:
        return None
