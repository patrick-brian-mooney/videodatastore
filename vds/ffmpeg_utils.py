"""A module that contains utility code to help abstract and provide a clean
interface to the command-line parameters of the ffmpeg application.

This module is part of VideoDataStore, a video database/processing utility.
VideoDataStore is copyright 2022 by Patrick Mooney. It is free software
released under the GNU GPL, either version 3 or (at your option) any later
version. See the file LICENSE.md for details.
"""


import collections
import functools

from pathlib import Path
from typing import Any, Dict, Iterable, List, Literal, Optional, Set, Tuple, Type, Union


import vds.utils as utils
from vds.utils import pref


def default_processing_goals(**kwargs) -> Dict[str, Union[bool]]:
    """Get a default "processing goals" dictionary. Fill in the dictionary based on
    keyword arguments that are passed in.
    """
    return dict(collections.ChainMap(kwargs, {'allow_size_increase': False}))


def get_ffmpeg_version() -> str:
    """Returns the version string that FFmpeg reports.
    """
    try:
        res = utils.run_process([pref('ffmpeg_loc'), "--help"], universal_newlines=True, print_output=False)
        if res.returncode:
            raise RuntimeError(f"Cannot get ffmpeg version!: got result code {res.returncode}. "
                               f"The system said {res.stdout}")

        lines = [li.strip().casefold() for li in res.stdout.split('\n') if "ffmpeg version" in li.casefold()]
        if not lines:
            raise RuntimeError("Cannot determine FFmpeg version from FFmpeg executable!")
        try:
            copyrt_loc = lines[0].index("copyright")
        except (ValueError,):
            return lines[0]     # Best we can do.

        ret = lines[0][:copyrt_loc].strip()
        try:
            version_end = ret.index('version') + len('version')
        except (ValueError,):
            return ret          # Best we can do.

        return ret[version_end:].strip()

    except (Exception,) as errrr:
        raise errrr


def get_argument_parameter(arguments: List[str],
                           which_argument: str) -> Union[str, None]:
    """Given ARGUMENTS, a list of command-line arguments, extract the parameter for
    WHICH_ARGUMENT. The "parameter" for an argument is what is specified as the next
    argument after WHICH_ARGUMENT. For instance, if the argument list includes
    -acodec aac, then "aac" is the parameter of the -acodec argument. That is, the
    argument to -acodec needs to specify additional information to do its job, and
    the string "aac" specifies that information.

    Note that not all arguments "take parameters," and this function does not make
    any attempt to deal with that situation: it just returns the next argument in
    the list. Similarly, there are other ways that an ARGUMENTS list can be badly
    formed, and this function does not attempt to deal with any of them, either: to
    pick just one example, if -acodec is the last-specified argument in the list and
    is not followed by an audio codec specifier. (This also means that the parameter
    list is not a valid invocation of ffmpeg, which this function also does not
    attempt to deal with.)

    If WHICH_ARGUMENT does not occur in ARGUMENTS, or is the last argument in the
    ARGUMENTS list, returns None.
    """
    try:
        assert which_argument in arguments
        return arguments[1 + arguments.index(which_argument)]
    except (IndexError, AssertionError, ValueError):
        return None


def set_argument_parameter(arguments: List[str],
                           which_argument: str,
                           argument_value: Optional[Any] = None) -> List[str]:
    """Given ARGUMENTS, a list of command-line arguments to be passed to ffmpeg,
    sets the "parameter" for the argument in the list. The "parameter" for a
    command-line argument is the argument following it. For instance, in
        ffmpeg -i fillum.wmv
    ... the parameter of the -i switch is "fillum.wmv".

    If WHICH_ARGUMENT already exists in ARGUMENTS, then the ARGUMENT_VALUE is
    replaced in the same position; otherwise, [WHICH_ARGUMENT, ARGUMENT_VALUE] is
    used to extend ARGUMENTS.

    In the special case that ARGUMENT_VALUE is None, the WHICH_ARGUMENT argument and
    its parameter are removed from ARGUMENTS. In all other cases, ARGUMENT_VALUE is
    type-coerced to a string, because that's what ffmpeg will expect.

    It is possible in theory for the same command-line argument to appear more than
    once in the argument list to ffmpeg -- for instance, if a complex filtergraph is
    applied to an input file to generate multiple output files. Because VDS does not
    make use of this functionality (at this point), this particular function makes
    no effort to detect or deal with this situation: it simply replaces the
    parameter to the first instance it finds of the argument, merrily ignoring any
    instances that come after it.

    ARGUMENTS is both modified in place and returned.
    """
    if which_argument in arguments:
        i = arguments.index(which_argument)
        if argument_value is None:
            arguments.pop(i)
            if not arguments[i].startswith('-'):
                arguments.pop(i)
        else:
            if i == (1 + len(arguments)):
                # degenerate case: list ends with a switch that's supposed to take a parameter, but it's missing
                arguments.append('')
            elif arguments[1 + i].startswith('-'):
                # degenerate case: switch should take parameter, but another switch comes next.
                arguments.insert(i + 1, '')
            arguments[i + 1] = str(argument_value)
    elif argument_value:
        arguments.extend([str(which_argument), str(argument_value)])
    return arguments


def set_crop(arguments: List[str],
             crop_dimensions: utils.Rect) -> List[str]:
    """Modify ARGUMENTS, an iterable of strings, representing the parameters to be
    passed to ffmpeg on the next processing run, so that the CROP option in the -vf
    filterchain represents the dimensions of CROP_DIMENSIONS.

    Returns a new list of arguments.
    """
    filters = FilterChain(arguments)
    filters.set_key_value_pair('crop', crop_dimensions.as_colon_delim())
    return filters.as_param_list()


def _extract_vf_option(arguments: List[str],
                       which_option: str) -> Union[str, None]:
    """Extracts a single filterchain option from the -vf parameters.

    If -vf is not in ARGUMENTS, or if WHICH_OPTION is not specified in its
    filterchain, returns None.
    """
    filters = FilterChain(arguments)
    if filters and which_option in filters:
        return filters[which_option]
    return None


def _extract_af_option(arguments: List[str],
                       which_option: str) -> Union[str, None]:
    """Extracts a single filterchain option from the -af parameters.

    if -af is not in ARGUMENTS, or if WHICH_OPTION is not specified in its
    filterchain, returns None.
    """
    filters = get_audio_filterchain(arguments)
    if filters and which_option in filters:
        return filters[which_option]
    return None


def specified_audio_codec(arguments: List[str]) -> Union[str, None]:
    """Gets the audio codec that scheduled to be used when the file is recompressed. If
    there is no audio codec specified in ARGUMENTS, returns None: this utility
    function does not attempt to guess what will be used if no plans have yet been
    made.
    """
    return get_argument_parameter(arguments, '-acodec')


def specified_video_codec(arguments: List[str]) -> Union[str, None]:
    """Gets the video codec that is scheduled to be used when the file is recompressed.
    If there is no video codec specified in ARGUMENTS returns None: this utility
    function does not attempt to guess what will be used if no plans have yet been
    made.
    """
    return get_argument_parameter(arguments, '-vcodec')


def specified_crop_size(arguments: List[str]) -> Union[utils.Rect, None]:
    """Returns the current cropping rectangle for the CROP parameter in ARGUMENTS, if
    cropping is scheduled, or None, otherwise
    """
    rect = _extract_vf_option(arguments, 'crop')
    if rect:
        try:
            top, left, bottom, right = [int(i.strip()) for i in rect.split(':')]
        except (ValueError,):
            print(f"Warning! Unable to understand the crop dimensions {rect}!")
            return None
        return utils.Rect.from_tlbr(top, left, bottom, right)
    return None


def validate_codec_specs(opts: List[str]) -> List[str]:
    """Validate that OPTS contains a list of command-line switches that respect the
    basic assumptions that the rest of the code makes about the -acodec and -vcodec
    switches. Mostly, validates that they only occur once; also validates that they
    use the -acodec and -vcodec switches instead of -c:a and -c:v or related. The
    '-filter:a' and '-filter:v' options are also replaced with '-af' and '-vf'. And
    makes sure that '-af' and '-vf' only appear once each by checking for multiples
    and, if they are found, combining them and moving the combined list to the very
    end of the OPTS list.

    There are plenty of other validations that could be, but currently aren't being,
    performed.

    Modifies the OPTS list in-place, then returns it.
    """
    for bad, replace in (('-c:a', '-acodec'), ('-codec:a', '-acodec'),
                         ('-c:v', '-vcodec'), ('-codec:v', '-vcodec'),
                         ('-filter:v', '-vf'), ('-filter:a', '-af'),
                         ):
        while bad in opts:
            where = opts.index(bad)
            opts[where] = replace
    for single in ('-acodec', '-vcodec'):
        while opts.count(single) > 1:
            where = opts.index(single)
            opts.pop(where)  # Pop the singleton element
            opts.pop(where)  # Pop the codec name, which is now in the same place.
    for singleton in ('-af', '-vf'):
        if opts.count(singleton) > 1:
            while singleton in opts:
                index = opts.index(singleton)
                opts.pop(index)                         # remove the SINGLETON switch
                filterchain_str = opts.pop(index)       # get its parameters
            opts.extend([singleton, filterchain_str])
    return opts


def is_only_copying_audio(arguments: List[str]) -> bool:
    """Check if audio is being merely copied, rather than recompressed.
    """
    return specified_audio_codec(arguments) == "copy"


def is_only_copying_video(arguments: List[str]) -> bool:
    """Check if video is being merely copied, rather than recompressed.
    """
    return specified_video_codec(arguments) == 'copy'


def is_only_copying(arguments: List[str]) -> bool:
    """Return True if -acodec is 'copy' and -vcodec is "copy" and both -acodec and
    -vcodec are specified. First, validates the codec specs to make sure each is
    specified at most once.
    """
    arguments = validate_codec_specs(arguments)
    return is_only_copying_audio(arguments) and is_only_copying_video(arguments)


class FilterChain(collections.abc.MutableMapping):
    """A FilterChain object represents an ffmpeg filterchain in an abstract way that is
    programmatically easy to work with. An ffmpeg "filterchain" is a kind of
    parameter to an ffmpeg option in which a series of options specifies a series of
    operations to be carried out on a specified file during processing.

    Because the details of filterchain handling can get hairy, this class provides
    a clean, high-level way to deal with these strings that helps hide the internal
    details of what's happening during the process. Create a FilterChain object by
    passing a string that specifies initial options, or by passing a list of string
    parameters; use the methods of the object to make changes; then use its as_str()
    method to export a string representation again.

    The underlying representation of a FilterChain is as a list of tuples stored in
    an instance. FilterChain options encoding a key=value pair use a 2-tuple:
    (key, value). Tuples encoding a flag-type option are stored as a 1-tuple:
    (flag,). This preserves the underlying order of operations, which is important
    in a FilterChain. FilterChains are also indexable as dictionaries, and it's
    possible to get and set items using dictionary-like syntax.

    FilterCHAINS are not the same thing to ffmpeg as FilterGRAPHS, which have a
    similar but more complex syntax and are used in a different way. As of this
    writing (23 Dec 2021), VideoDataStore has no representation of ffmpeg
    FilterGraphs.
    """
    def __init__(self, params: Optional[Union[str, List[str]]] = None):
        """The initialization routine takes an optional PARAMS argument, parsing it (if
        present) into the necessary underlying internal representation. For the sake of
        convenience, it accepts either a string that's a comma-separated list (which is
        the representation that's going to be passed to ffmpeg, eventually) or a list of
        strings (which is the representation that VideoDataStore uses internally) or a
        list of 1- and 2-tuples of strings (which is the representation that the
        FilterChain class uses internally).
        """
        self.filterchain = list()
        if params:
            assert isinstance(params, (str, list)), (f"Non-string, non-list, non-empty parameter {params} "
                                                     f"(type {params.__class__.name__} used to initialize a "
                                                     f"FilterChain!")
            if isinstance(params, list):
                for i, elem in enumerate(params):
                    assert isinstance(elem, str), (f"Non-string list element {elem} (item # {i}) was in PARAMS "
                                                   f"passed to FilterChain.__init__()!")
            if isinstance(params, str):
                params = [i.strip() for i in params.split(',')]
            for item in params:
                if '=' in item:
                    assert item.count('=') == 1, (f"Badly formed parameter string {item} used to initialize a "
                                                  f"FilterChain: more than one = sign in parameter!")
                    key, value = item.split('=')
                    self.filterchain.append((key.strip(), value.strip()))
                else:
                    self.filterchain.append((item.strip(),))

    @classmethod
    def _from_tuples(cls, tuples: Iterable[Tuple[str]]) -> 'FilterChain':
        """Alternate constructor: create a FilterChain from a properly formatted series of
        tuples already arranged in the way that the FilterChain class uses for its
        internal representation.
        """
        assert isinstance(tuples, Iterable), ("Parameters passed to FilterChain._from_tuples are not in the form of an"
                                              "iterable!")
        for i, t in enumerate(tuples):
            assert isinstance(t, tuple), (f"The iterable provided as a constructor to FilterChain._from_tuples contains"
                                          f" element {t} (type {t.__class__.__name__}) as item #{i}, which is not a "
                                          f"tuple!")
            assert 0 < len(t) < 3, (f"The iterable provided as a constructor to FilterChain._from_tuples contains "
                                    f"a malformed element, {t}, as item #{i}: it is a tuple of length {len(t)}, "
                                    f"but must be a 1- or a 2-tuple!")
            assert all([isinstance(elem, str) for elem in t]), (f"Element {t} (#{i}) passed to FilterChain._from_tuples"
                                                                f" contains non-string components!")

        ret = FilterChain()
        ret.filterchain = tuples
        return ret

    def clean_filterchain(self) -> None:
        """Does an in-place cleaning and validation of the FilterChain object to ensure it
        has the structure that FFmpeg and the rest of the VDS code expects.

        Modifies the filterchain in place; returns None.

        Currently, it:
          * Ensures 'crop' comes before 'scale'. (If 'crop' occurs multiple times, then
            only the first instance is moved before 'scale', on the theory that multiple
            crops might be sensible under some workflows.)
          * That's it, for now.
        """
        crop_pos = self.index('crop', all_occurrences=True, error_if_not_found=False)
        if crop_pos:
            scale_pos = self.index('scale', all_occurrences=True, error_if_not_found=False)
            if scale_pos:
                if scale_pos[0] < crop_pos[0]:
                    # we need to put the 'crop' filter before the 'scale' filter
                    scale_pos, crop_pos = scale_pos[0], crop_pos[0]
                    old = self.filterchain[:]
                    crop_tuple = old[crop_pos]
                    # now, produce a copy without the 'crop' tuple
                    new = old[:crop_pos] + old[1 + crop_pos:]
                    # now, add the 'crop' back in before the 'scale' command -- which will definitely not have moved
                    self.filterchain = new[:scale_pos] + [crop_tuple] + new[scale_pos:]

    def as_str(self) -> str:
        """Produce an as-string filterchain representation.
        """
        ret = ""
        for item in self.filterchain:
            assert 0 < len(item) < 3, "Underlying representation of a FilterChain contains malformed items!"
            if len(item) == 2:
                ret += f",{item[0]}={item[1]}"
            else:
                ret += f",{item[0]}"
        return ret.strip().strip(',').strip()

    def as_param_list(self) -> List[str]:
        """Produce an as-list-of-string-parameters representation of SELF. This is both
        the format needed by the 'next_processing' key in the database and the format
        consumed by the class's __init__().
        """
        return self.as_str().split(',')

    def __str__(self) -> str:
        return self.as_str()

    def __repr__(self) -> str:
        return f"FilterChain({self.as_str()})"

    def __getitem__(self, key: Union[str, int, slice],
                    only_first: bool = True,
                    forgive_missing: bool = False) -> Union[str, List[str], bool, List[bool], None, 'FilterChain']:
        """Goes through the items in the FilterChain's underlying representation,
        looking for KEY. If KEY is not in the underlying FilterChain, raises KeyError
        if FORGIVE_MISSING is False (the default). Indexing a missing key returns None
        instead of raising an error when FORGIVE_MISSING is True.

        If KEY *is* in the underlying representation:
          * if KEY indexes a value as part of a key-value pair:
            * if KEY only appears once, or if ONLY_FIRST is True (the default), returns
              the value from that pair.
            * if KEY appears multiple times, and ONLY_FIRST is False, returns a list of
              all values indexed by that key, in the order in which they appear (but
              does not return any information about their positions, except insofar as
              their ordering relative to each other is preserved).
          * otherwise, if KEY appears as a flag-type option:
            * if KEY only appears once, or if ONLY_FIRST is True, returns True.
            * if KEY appears multiple times, and ONLY_FIRST is False, returns a list of
              the value True, appearing as many times as the flag does in the
              FilterChain's underlying representation.

        If ONLY_FIRST is False, it is possible in theory for the list to contain both
        values extracted from key-value pairs and the boolean value True, signifying
        that the underlying filterchain both uses the flag as an equals-separated
        key-value pair and as a flag with no options. This function makes no effort
        to avoid this situation, even though the underlying filterchain is probably
        faulty from ffmpeg's point of view. The Right Thing To Do here is not to
        confuse flags with key-value pairs in the first place. The FilterChain object
        does not attempt to save you from making that mistake.

        A FilterChain can also be indexed *numerically*, as if it were a sequence
        instead of a dictionary. This means that KEY can also be an integer or a slice.
        This returns a new FilterChain with the relevant item(s). This is primarily a
        convenience for the FilterChain class itself, but other code is also welcome to
        use it. In this case, ONLY_FIRST and FORGIVE_MISSING are nonsensical and must be
        left at their default values.
        """
        if isinstance(key, str):
            try:
                if not self.filterchain:        # Underlying representation is Falsey? FilterChain does not contain KEY.
                    raise KeyError(f"FilterChain {self} does not contain key: {key.strip()}")
                assert isinstance(self.filterchain, list), (f"FilterChain {self} has corrupt non-list underlying "
                                                            f"representation {self.filterchain}!")

                ret = list()

                for item in self.filterchain:
                    assert len(item) < 3, (f"FilterChain {self} has malformed underlying representation containing too "
                                           f"many components in parameter {item}!")
                    if len(item) == 2:
                        rep_key, value = item
                        if rep_key.strip() != key.strip():
                            continue
                        if only_first:
                            return value.strip()
                        ret.append(value.strip())
                    else:
                        if key.strip() == item[0].strip():
                            if only_first:
                                return True
                            else:
                                ret.append(True)

                if only_first or (not ret):       # If we had found anything, we'd have already returned it.
                    raise KeyError(f"FilterChain {self} does not contain key {key.strip().upper()}")
                if len(ret) > 1:
                    return ret
                return ret[0]
            except KeyError:
                if forgive_missing:
                    return None
                raise
        else:
            assert isinstance(key, (int, slice))
            if not only_first:
                raise ValueError("ONLY_FIRST cannot be specified when indexing a FilterChain numerically!")
            if forgive_missing:
                raise ValueError("FORGIVE_MISSING cannot be specified when indexing a FilterChain numerically!")

            if isinstance(key, slice):      # the simple case: List[Tuple[str]][slice] gives List[Tuple[str]]
                return FilterChain._from_tuples(self.filterchain[key])
            else:           # key is an int; slightly different because List[Tuple[str]][int] gives Tuple[str]
                return FilterChain._from_tuples([self.filterchain[key]])

    def get_first(self, key: str) -> Union[str, bool, None]:
        """Get only the first result when indexing SELF for KEY. Returns a string value if
        KEY indexes a key-value pair, or True if KEY appears and is only a flag, or None
        if KEY does not appear; but never returns a list, and never returns False.
        """
        return self.__getitem__(key, only_first=True)

    def get_all(self, key: str) -> List[Union[str, bool]]:
        """Returns all instances of what KEY indexes in SELF. The return value is always a
        list. If KEY does not appear at all, returns an empty list. If it appears once,
        returns a list of one item.
        """
        ret = self.__getitem__(key, only_first=False)
        if not ret:
            return list()
        if not isinstance(ret, list):
            return [ret]
        return ret

    def __setitem__(self, key: str,
                    value: Union[str, Tuple] = (),
                    allow_repeated: bool = False,
                    before: Optional[Iterable[str]] = ()) -> None:
        """Sets KEY in SELF's underlying filtergraph to VALUE. If VALUE is an empty tuple
        (the default), it encodes a flag-type (present/not present, no arguments) entry
        in the filterchain. If it consists of a value, then it sets a key-value pair in
        the filterchain. KEY must be a string; if VALUE is not an empty tuple, it must
        also be a string.

        If ALLOW_REPEATED is False (the default), then the first instance of KEY, if
        already present in the filtergraph, is overwritten with the new VALUE, and any
        later options are removed from the filterchain. If KEY does not already appear
        in the filterchain, it (with any appropriate VALUE) is added to the end of the
        filterchain.

        If ALLOW_REPEATED is True (and BEFORE is empty), the KEY (and, if appropriate,
        its associated VALUE) are added to the end of the filterchain, without making
        any other modifications to the chain itself.

        If BEFORE is specified, it must be an iterable of strings constituting options
        -- the KEY part of the key-value pairs that are stored. If BEFORE is not empty,
        it specifies the list of options that the inserted option must precede. The
        option added to the filterchain will be placed before the first of these options
        occur in the filterchain, if any do.

        If BEFORE is specified and ALLOW_REPEATED is False, any preexisting values are
        deleted. (If ALLOW_REPEATED is False and the filterchain has an existing
        parameter with the same key and that existing parameter comes before any of the
        items in BEFORE, then the new key-value pair will replace the existing pair in
        place. Otherwise, the new key-value pair will be placed immediately before the
        first item from BEFORE that occurs, if any do. If no items from BEFORE exist and
        the KEY is not replacing an earlier identical KEY, then the key-value pair is
        placed at the end of the chain.)

        If BEFORE is specified and ALLOW_REPEATED is True, then the new key-value pair
        is placed immediately before the first-occurring item from BEFORE, if any occur.
        If none do, the new key-value pair is placed at the end. In neither case are any
        other occurrences of the key touched.

        __getitem__ allows passing a numeric or slice-based key as a convenience, but
        *this* function does not offer that possibility.
        """
        assert isinstance(key, str), "Non-string KEY passed to FilterChain.__setitem__() !"
        if not value:
            assert isinstance(value, tuple), "Non-tuple Falsey VALUE passed to FilterChain.__setitem__() !"
        else:
            assert isinstance(value, str), "Non-string VALUE passed to FilterChain.__setitem__() !"
        if isinstance(value, tuple):
            assert not value, "Non-empty tuple passed as VALUE to FilterChain.__setitem__()!"

        key, value = key.strip(), (value.strip() if value else value)

        # if we're handling a list of place-before constraints, figure how to satisfy those constraints first.
        if before:
            assert isinstance(before, collections.abc.Iterable)
            assert all([isinstance(x, str) for x in before])

            keys = tuple(i[0] for i in self.filterchain)
            earliest = 400000000000000
            for i in before:
                if i in keys:
                    earliest = min(earliest, keys.index(i))
            if key in keys:
                earliest = min(earliest, keys.index(key))
            pos = min(earliest, len(self.filterchain))
            if key in [i[0] for i in self.filterchain]:
                if allow_repeated:
                    fc = self.filterchain
                    self.filterchain = fc[:pos] + [(key, value)] if value else [(key,)] + fc[pos:]
                else:
                    fc = [i for i in self.filterchain if i[0] != key]
                    self.filterchain = fc[:pos] + [(key, value)] if value else [(key,)] + fc[pos:]
            elif pos >= (len(self.filterchain) - 1):
                self.filterchain.append((key, value) if value else (key,))
            else:
                fc = self.filterchain
                self.filterchain = fc[:pos] + [(key, value)] if value else [(key,)] + fc[pos:]
            return

        if allow_repeated:
            if value:
                self.filterchain.append((key.strip(), value.strip()))
            else:
                self.filterchain.append((key.strip(),))
            return

        found = False
        new_filterchain = list()
        for item in self.filterchain:
            if item[0].strip() == key.strip():
                if found:           # if we've found & handled KEY in this filterchain, skip any more instances of it
                    continue
                new_filterchain.append((item[0].strip(), value.strip()))
                found = True
            else:
                new_filterchain.append(tuple([i.strip() for i in item]))
        if not found:
            if value:
                new_filterchain.append((key.strip(), value.strip()))
            else:
                new_filterchain.append((key.strip(),))
        self.filterchain = new_filterchain

    def set_flag(self, key: str) -> None:
        """Sets a flag-type option in SELF, named by KEY. If the flag already is set, does
        nothing. If it is not set, add it to the end of the current FilterChain plan.
        """
        self.__setitem__(key, value=(), allow_repeated=False)
        self.clean_filterchain()

    def append_flag(self, key: str) -> None:
        """Sets a flag-type option in SELF, named by KEY. It does not matter if the flag is
        already set; it adds the flag to the end of the current FilterChain plan anyway.
        """
        self.__setitem__(key, value=(), allow_repeated=True)
        self.clean_filterchain()

    def set_key_value_pair(self, key: str,
                           value: str,
                           allow_repeated: bool = False,
                           before: Optional[Iterable[str]] = ()) -> None:
        """Set a key-value pair in the filterchain. Basically just a friendly interface
        to __setitem__().
        """
        self.__setitem__(key=key, value=value, allow_repeated=allow_repeated, before=before)
        self.clean_filterchain()

    def __delitem__(self, key: str,
                    only_first: bool = False) -> None:
        """Delete KEY from the FilterChain, along with any value it may be associated with.
        If ONLY_FIRST is True, deletes only the first instance of KEY; otherwise,
        deletes every instance.
        """
        assert isinstance(key, str), "Attempt to delete non-string key from a FilterChain!"
        if key.strip() not in [i[0].strip() for i in self.filterchain]:
            raise KeyError(key)

        new_filterchain = list()
        found = False

        try:
            for i in self.filterchain:
                if (not only_first) or (not found):
                    if key.strip() == i[0].strip():
                        found = True
                    else:
                        new_filterchain.append(tuple([item.strip() for item in i]))
                else:
                    new_filterchain.append(tuple([item.strip() for item in i]))
        finally:
            self.filterchain = new_filterchain
        self.clean_filterchain()

    def del_first(self, key: str) -> None:
        """Deletes only the first instance of KEY from the SELF FilterChain.
        """
        self.__delitem__(key, only_first=True)
        self.clean_filterchain()

    def del_all(self, key: str) -> None:
        """Deletes all instances of KEY from the SELF FilterChain.
        """
        self.__delitem__(key, only_first=False)
        self.clean_filterchain()

    def index(self, what: str,
              all_occurrences: bool = False,
              error_if_not_found: bool = True) -> Union[int, List[int], None]:
        """Returns the (zero-based) index where the key WHAT occurs in the SELF
        filterchain, if it does in fact occur and if ALL_OCCURRENCES is False.

        If ALL_OCCURRENCES is True, returns a list of all indices in the filterchain
        where WHAT occurs as a key, instead of the index of the first item only. If WHAT
        occurs only once, the list will be of one item; if it does not occur at all and
        ERROR_IF_NOT_FOUND is False, it will be a list of zero items.

        If ERROR_IF_NOT_FOUND is True, raises ValueError if WHAT does not occur in the
        filterchain. Otherwise, returns None (if ALL_OCCURRENCES is False) or an empty
        list (if ALL_OCCURRENCES is True).
        """
        keys = [i[0] for i in self.filterchain]
        if what not in keys:
            if error_if_not_found:
                raise ValueError
            elif all_occurrences:
                return list()
            else:
                return None

        if all_occurrences:
            return [i for i, k in enumerate(keys) if k == what]
        else:
            return keys.index(what)

    def __iter__(self):
        return iter(self.filterchain)

    def __len__(self):
        return len(self.filterchain)

    def __add__(self, other: 'FilterChain') -> 'FilterChain':
        assert isinstance(other, FilterChain), "FilterChains can only be added to other FilterChains!"
        return FilterChain._from_tuples(self.filterchain + other.filterchain)


def get_video_filterchain(arguments: List[str]) -> FilterChain:
    """Splits the filterchain specified as the parameter for -vf into a dictionary-
    like structure. If -vf is not specified in ARGUMENTS, returns an empty
    FilterChain.
    """
    return FilterChain(get_argument_parameter(arguments, '-vf'))


def get_audio_filterchain(arguments: List[str]) -> FilterChain:
    """Splits the filterchain specified as the parameter for -af into a dictionary-
    like structure. If -af is not specified in ARGUMENTS, returns an empty
    FilterChain.
    """
    return FilterChain(get_argument_parameter(arguments, '-af'))


def frame_size_is_changing(which_db: 'MetadataStore',
                           which_file: Path) -> bool:
    """Returns True if the next processing run is currently scheduled to change the
    size of the video frame; or False if not.

    Since we take some pains to make sure that cropping happens before scaling,
    just checking to see what size we scale to should be good enough here.
    Alternately, if we crop but don't scale, then we can assume that the video size
    is going to change.
    """
    movie_metadata = which_db[which_file]['movie_metadata']
    filters = get_video_filterchain(which_db[which_file]['next_processing'])
    if 'scale' in filters:
        h, v = (int(n.strip()) for n in filters['scale'].split(':'))
        if (h != movie_metadata['frame width']) or (v != movie_metadata['frame height']):
            return True
    if 'crop' in filters:
        return True
    return False


def get_video_dim_after_processing(which_db: 'MetadataStore',
                                   which_file: Path) -> Tuple[int, int]:
    """Returns a (width, height) tuple indicating the size that the video will be after
    processing occurs. Takes resizing into account.
    """
    filters = get_video_filterchain(which_db[which_file]['next_processing'])
    movie_metadata = which_db[which_file]['movie_metadata']
    if frame_size_is_changing(which_db, which_file):
        if 'scale' in filters:
            return tuple(int(n.strip()) for n in filters['scale'].split(':'))
        elif 'crop' in filters:
            # format is: {self.width}:{self.height}:{self.left}:{self.top}
            width, height, l, r = filters['crop'].split(':')
            return int(width.strip()), int(height.strip())

    return movie_metadata['frame width'], movie_metadata['frame height']


def video_duration_from_ffprobe(which_video: Union[Path, str]) -> Union[float, Type[BaseException]]:
    """Use ffprobe to get the duration of the movie in seconds.

    If ffprobe cannot determine the length of the movie, return the Exception object
    generated during the process, instead.
    """
    assert which_video
    cmd = [pref('ffprobe_loc'), "-v", "error", "-show_entries", "format=duration", "-of",
           "default=noprint_wrappers=1:nokey=1", str(which_video)]
    try:
        res = utils.run_process(cmd, universal_newlines=True)
        if res.returncode:
            raise RuntimeError(f"Cannot get duration of file {which_video}: got result code {res.returncode}. "
                               f"The system said {res.stdout}")
        else:
            return float(res.stdout)
    except (Exception,) as errrr:
        return errrr


@functools.lru_cache()
def _get_codec_list_text(direction: Literal['encode', 'decode']) -> List[str]:
    """Get the formatted list of codecs supported by FFmpeg, split it into lines, and
    return it. The call to FFmpeg takes time and the results are unlikely to change
    during a single run, so we cache the results.
    """
    data = utils.run_process(['ffmpeg', f'-{direction}rs'], print_output=False)
    lines = data.stdout.split('\n')
    sep = [lines for lines in lines if "------" in lines]
    begin = 1 + lines.index(sep[0])
    return [lines for lines in lines[begin:] if lines.strip()]


@functools.lru_cache
def get_codecs(kind: Literal['audio', 'video', 'subtitle', 'all'],
               direction: Literal['encode', 'decode', 'all']) -> List[Tuple[str]]:
    """Get a list of all codecs of the user-specified types.

    Parses FFmpeg output directly and is therefore rather fragile. #FIXME
    """
    selectors = {
        'audio': lambda flgs: flgs[0] == 'A',
        'video': lambda flgs: flgs[0] == 'V',
        'subtitle': lambda flgs: flgs[0] == 'S',
        'all': lambda flgs: True,
    }

    t_sel = selectors[kind]
    codecs = list()

    if direction == 'all':
        codec_text = _get_codec_list_text('encode') + _get_codec_list_text('decode')
    else:
        codec_text = _get_codec_list_text(direction)
    for codec in codec_text:
        try:
            if codec.strip():
                flags, identifier, text_name = codec[:8].strip(), codec[8:29].strip(), codec[29:].strip()
        except (IndexError,):
            continue
        else:
            if flags and identifier and text_name:
                if t_sel(flags):
                    codecs += [(flags, identifier, text_name)]

    return codecs


def get_codec_lists() -> Tuple[Set[str], Set[str], Set[str]]:
    """Determines what audio, video, and subtitle codecs the current FFmpeg supports.
    Returns three lists, each containing the internal codec names the relevant codec
    type. FFmpeg seems to have an additional codec category, with type D in the
    info output (which is not defined); this function does nothing about these, not
    even return the set, because VDS never needs it.

    Does not make any attempt to distinguish between codecs for encoding and
    codecs for decoding.
    """
    return ({codec[1] for codec in get_codecs('audio', 'all')},
            {codec[1] for codec in get_codecs('video', 'all')},
            {codec[1] for codec in get_codecs('subtitle', 'all')})


@functools.lru_cache(maxsize=None)
def get_ffmpeg_presets() -> List[str]:
    """Return a list of available encoding presets that the currently installed ffmpeg
    supports. Construct this list by running FFmpeg and parsing its output.

    We cache the results the first time this function is called because the results
    are highly unlikely to change on a particular run.

    # FIXME! This is rather fragile, as it parses FFmpeg output directly.
    """
    data = utils.run_process([pref('ffmpeg_loc'), '-hide_banner', '-f', 'lavfi', '-i', 'nullsrc',
                              '-c:v', 'libx264', '-preset', 'help', '-f', 'mp4', '-'], print_output=False)
    output = data.stdout.strip().casefold()
    lines = [line.strip() for line in output.split('\n') if line.strip()]
    relevant_lines = [lines for lines in lines if "possible presets:" in lines]
    if not relevant_lines:
        return list()
    line = relevant_lines[0]
    return line[line.index('possible presets:') + len('possible presets:'):].strip().split(' ')


@functools.lru_cache
def get_audio_codec_encoding_sample_rates(codec: str) -> Tuple[int]:
    """Get the list of sample rates that a particular audio codec supports using during
    encoding. CODEC is the formal name of the codec to be used.

    Parses FFmpeg output directly and therefore is rather fragile. #FIXME
    """
    if codec not in {i[1] for i in get_codecs('audio', 'encode')}:
        raise RuntimeError(f"ERROR! {codec.strip().upper()} is not a codec that can be used for audio encoding!")
    data = utils.run_process(['ffmpeg', '-h', f'encoder={codec}'], print_output=False).stdout.strip()
    output_lines = [line.strip().casefold() for line in data.split('\n') if line.strip()]
    relevant_line = [line for line in output_lines if line.startswith('supported sample rates:')][0]
    rates = [int(r) for r in relevant_line[len('supported sample rates:'):].strip().split(' ')]
    return tuple(sorted(rates))


if __name__ == "__main__":
    print("Running self-test code")
    b = FilterChain(['scale=1202:676', 'crop=504:391:29:15'])
    print(b)
    print(b[:])
    print(b[1])
    print(b[0])
    print(b[0:])

    b.clean_filterchain()
    print(b, '\n\n\n\n\n')

    c = FilterChain(['herp=derp',]) + b + FilterChain(['why=not'])
    c.clean_filterchain()

    a = FilterChain('hflip,scale=640:360')
    a.__setitem__('vflip', ())
    print(a)
    print(a['hflip'])
    print(a['scale'])
    a['scale'] = "480:480"
    print(a)
    del a['hflip']
    print(a)
    a['hflip'] = ()
    print(a)
    del a['scale']
    print(a)
    a['scale'] = '1080:720'
    print(a)
